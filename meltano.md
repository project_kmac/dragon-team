# Meltano

Hello Andy,

Thank you for our call today. I appreciate your invitation to send more information, as well as a follow up call on Tuesday, Feb 18 at 4 PM to learn if it makes sense to discuss further.

As we discussed, you are working on data science projects and use Git, so here are some resources on how GitLab may be relevant to your work.

GitLab for Data Science:
https://about.gitlab.com/solutions/data-science/

Using GitLab CI/CD to run data science projects: https://medium.com/@stijnvanorbeek/using-gitlab-ci-cd-to-run-data-science-projects-on-aws-a908522f5b8b

Furthermore, here is Meltano, our open source project for Data teams:

Video tutorial: https://www.youtube.com/watch?v=X-0wgwI-HYY

Podcast if you prefer audio: https://thenewstack.io/gitlabs-meltano-a-data-pipeline-that-uses-git-as-the-source-of-truth/

In any case, I appreciate your time today and hope that you have a great weekend.

With your success in mind,
Kevin