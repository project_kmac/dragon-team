# Messaging

## DevOps

Maybe they're starting to adopt DevOps practices to enable business agility. But sometimes, limited capabilities force their teams to build their CI/CD pipeline with off the shelf components so they don't have to reinvent the wheel.

Often, they've already built a mature DevOps pipeline. But occasionally, this means they're spending too much time on maintenance and integrations. So they might be looking into consolidating their toolchain.

Perhaps instead they're trying to shift security left. So they're looking for ways to automate testing in order to catch defects and remediate vulnerabilities earlier.


## Dev

Typically, the VPs of Application Development I work with tell me that they're undergoing a digital transformation journey.

They're either looking to create a DevOps/Agile pipleline that allows faster speed to market and innovation velocity. So they're trying to stand up the environment.

Or they've already got a sophisticated pipeline, which  often this means they have too many tools to integrate and maintain. So they're looking to consolidate their toolchain or rationalize their applications.

Perhaps instead they're trying to shift security left so that they can catch defects and remediate earlier. So they're looking for ways to automate testing/code review, and put application security tools into the hands of the developer.

Or maybe they are using the latest in cloud native technologies, microservices architecture, and containerization. But often times this means needing to automate orchestration so that their infrastructure remains highly scalable while minimizing costs.

Does any of this sound like it might be relevant to your world, or even worth a brief conversation?

****

## Ops

Typically, the VPs I work with in an infrastructure engineering role tell me what they are embarking on a digital transformation journey. 

They often have a very experienced, very good, senior IT leadership and have stood up a deployment pipeline that works really well. But occasionally, they run into some issues because of having severals tools and integrations, which means they're spending too much on time and resources managing the toolchain.

Or maybe they are using the latest in cloud native technologies, microservices architecture, and containerization. But often times this means needing to automate orchestration so that their infrastructure remains highly scalable while minimizing costs.

Does any of this sound like it might be relevant to your world, or even worth a brief conversation?

*****

## Security


GitLab is an application used by clients like Goldman Sachs and Verizon to protect customer data and secure their infrastructure.

Typically, the CISOs I work with tell me that they've got an excellent IT team and are generally happy with their security infrastructure.

Often they already have a sophisticated DevSecOps toolchain. But occasionally this means they have several tools to integrate and maintain, which then creates limited visibility into their pipeline and makes governance reporting and regulatory compliance more difficult.

Perhaps instead they're trying to shift security left so that they can catch defects and remediate earlier. So they're looking for ways to better leverage scarce security resources, automate testing/code review, and put application security tools into the hands of the developer.

Or alternatively, they're planning to ensure business continuity in the event of an unplanned outage, so they're moving to the cloud for high availability and disaster recovery.

You probably aren't experiencing any of these issues, are you?

****


Please accept my apologies for the intrusion. You mention on your LinkedIn profile that you initiated Autozone's journey to DevSecOps through the integration of automation (security tools) into existing build/deployment pipelines. As such, I thought this might be helpful to you.

GitLab is an "all-in-one" tool that handles the entire DevSecOps deployment. Additionally, as you may already know, there are several teams across multiple instances using GitLab (but that are not yet leveraging it's security capabilities). 

This may be helpful to you because unlike traditional application security tools, GitLab's security capabilities are built into the CI/CD workflow where the developers live. This allows them to identify vulnerabilities and remove them early. I believe your phrase is "introducing security without introducing friction."

At the same time, this also provides your security team a dashboard to view the items not resolved by the developers. This vulnerability-centric approach helps each role deal with items that are most important and also most relevant to their scope of work.

Even if you're already headed down this path, does any of this sound like it's relevant to your world or worth a brief 10-15 min discussion?

Warm Regards,
Kevin

****

## QA

GitLab is an application used by retailers like Macy's, Dillard's, and Walmart to enable faster software development and automate their QA testing.

Typically, the engineers I work with in a quality assurance role tell me that they've got an excellent IT team and are generally happy with their deployment pipeline.

Often, they already have some automated testing in their pipeline. But occasionally, they're a little frustrated with a little too much manual testing, which means that their build & release goes from minutes to hours.

Or maybe they've automated a lot, but sometimes they're wanting to shift security left and integrate more testing into their application development with SAST or DAST.

Additionally, they may be looking to improve Mean-Time-To-Resolution, Change Lead Time, Change Failure, and reduce the defect rate.

****

## CTO

I work with CTOs who often tell me that they've got an excellent IT team and have built a mature DevOps pipeline. 

But occasionally, this means their teams are frustrated with spending too much time on maintenance and integrations.

Or perhaps they're concerned about the lack of visibility throughout the pipeline - and the subsequent difficulty in reporting and change management compliance this causes.

Finally, typical practices create manual work that makes their cycle times longer. So they're looking to automate processes in order to speed up delivery times.

****

## Data Science, Machine Learning

Sean Carroll: Currently the go-to place for Data Science / Machine Learning source code is GitHub. ML at scale is DevOps-heavy and GitLab already supports BYO Kubernetes and native Jupyter Notebooks, so we are well-placed to value add. Are we considering promoting GitLab in this area?


## Application Release Orchestration / ARO


For ARO, GitLab has releases and associations to a milestone that accomplish plan vs delivered orchestration. This stage helps support functionality like incremental rollout, canary deployments and feature flags.

One possible. downside is that these capabilities are tightly coupled with Kubernetes, whereas you could work with traditional component based deployment orchestration without much scripting.

For the technical overview, here are four links you might find useful:

Feature Flags - https://docs.gitlab.com/ee/user/project/operations/feature_flags.html#overview

Merge trains- 
https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/

Canary:
https://docs.gitlab.com/ee/user/project/canary_deployments.html#use-cases

Environment Dashboard: 
https://docs.gitlab.com/ee/ci/environments/environments_dashboard.html

Finally, a comparison of Spinnaker and GitLab's features:
https://about.gitlab.com/devops-tools/spinnaker-vs-gitlab.html

## Microservices

Auto DevOps is an example of the things GitLab can do to enable full CD utilizing containers and Kubernetes. You don't necessarily need to be a "micro-service" to do so.

https://about.gitlab.com/direction/cicd/#progressive-delivery

https://about.gitlab.com/solutions/kubernetes/

https://about.gitlab.com/cloud-native/

https://about.gitlab.com/direction/microservices/