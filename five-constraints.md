# 5 Constraints

These are the 5 Constraints that organizations adopting DevOps predictably face:

1. How long does it take to provision an environment?
2. How frequently do you deploy code?
3. How long does it take to complete testing?
4. How tightly coupled is your architecture?
5. How many good ideas can you test?

It is likely that the 6th bottleneck is time spent on toolchain maintenance. 
"How much time are you spending on a toolchain?"

## The 1st Constraint: Environment Proivisioning

Environment creation: We cannot achieve deployments
on-demand if we always have to wait weeks or months for
production or test environments. The countermeasure is to
create environments that are on demand and completely
self-serviced, so that they are always available when we need
them.

## The 2nd Constraint: Code Deploy Frequency

Code deployment: We cannot achieve deployments on
demand if each of our production code deployments take
weeks or months to perform (i.e., each deployment requires
1,300 manual, error-prone steps, involving up to three
hundred engineers). The countermeasure is to automate our
deployments as much as possible, with the goal of being
completely automated so they can be done self-service by
any developer.

## The 3rd Constraint: Testing Automation

Test setup and run: We cannot achieve deployments on
demand if every code deployment requires two weeks to set
up our test environments and data sets, and another four
weeks to manually execute all our regression tests. The
countermeasure is to automate our tests so we can execute
deployments safely and to parallelize them so the test rate
can keep up with our code development rate

## The 4th Constraint: Complect-city

Overly tight architecture: We cannot achieve deployments
on demand if overly tight architecture means that every time
we want to make a code change we have to send our
engineers to scores of committee meetings in order to get
permission to make our changes. Our countermeasure is to
create more loosely-coupled architecture so that changes can
be made safely and with more autonomy, increasing
developer productivity.

‘Complect’ means to turn something simple into something complex.
“In tightly coupled and complected systems, it’s nearly impossible to
change anything, because you can’t just change one area of the code, you
must change one hundred, or even a thousand, areas of the code. And
even the smallest changes can cause wildly unpredictable effects in distant
parts of the system, maybe in something you’ve never even heard of.
“Sensei Hickey would say, ‘Think of four strands of yarn that hang
independently—that’s a simple system. Now take those same four strands
of yarn and braid them together. Now you’ve complected them.’ Both
configurations of yarn could fulfill the same engineering goal, but one
is dramatically easier to change than the other. In the simple system, you
can change one string independently without having to touch the others.
Which is very good.”

Erik laughs, “However, in the complected system, when you want to
make a change to one strand of yarn, you are forced to change the other
three strands too. In fact, for many things you may want to do, you simply
cannot, because everything is so knotted together.

“And when that happens,” he continues, “you’ve trapped yourself in
a system of work where you can no longer solve real business problems
easily anymore—instead, you’re forced to merely solve puzzles all day,
trying to figure out how to make your small change, obstructed by your
complected system every step of the way. You must schedule meetings
with other teams, try to convince them to change something for you,
escalate it to their managers, maybe all the way up the chain.

“Everything you do becomes increasingly distant from the real
business problem you’re trying to solve,” he says.

## The 5th Constraint: Hypotheses

After all these constraints have been broken, our constraint will
likely be Development or the product owners. Because our goal
is to enable small teams of developers to independently develop,
test, and deploy value to customers quickly and reliably, this is
where we want our constraint to be. High performers,
regardless of whether an engineer is in Development, QA, Ops,
or Infosec, state that their goal is to help maximize developer
productivity.
When the constraint is here, we are limited only by the
number of good business hypotheses we create and our ability
to develop the code necessary to test these hypotheses with
real customers.



## The 6th Constraint

Disconnected value streams are the bottleneck to productivity at scale



Upon examining the architecture of that solution, we realized that the developer problem was only the tip of the iceberg. The real problem at FinCo was that two fundamentally disconnected value streams had countless IT specialists spending a large amount of time each day manually entering information across tools and providing status updates and reports. And in each case, the manual updates were being made in one kind of project-management-and-tracking tool or another.

This layered onto the disconnect we had witnessed between the software architecture and the value streams, but at FinCo that disconnect was even worse. The operational infrastructure and lack of deployment automation and orchestration meant that not only the software architecture but the operational infrastructure were disconnected from the value stream. These observations led me to my second epiphany -- that disconnects between the project management model, the end-to-end delivery architecture, and the value stream are the bottleneck to software productivity at scale.