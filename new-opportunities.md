# New Opportunities

When a new opp is created.

### Opp Template for SFDC 

Opportunity Naming Convention:

Date Connected: 
Initial Meeting Date: 


Name of Prospect:
Title of Prospect:
Prospects’ LinkedIn:

Notes / Summary: List any information uncovered during research or prospecting that could help aid the SALs.

Suggested Discovery Questions: List out what you believe to be the most appropriate discovery questions for the next call.

Call prep notes. 

* VALUE DRIVER PATH?

* PAIN (Before/Negative Outcomes):

* DREAM (After/PBO)

* IMPORTANT: (Required Capabilities/Metrics)

* LOGISTICS:
- Tech Stack?
- Number of Users?
- Business Use Case?
- How are you going to migrate or adopt?


Campaign or Value Driver Path: 
aSDR - IOE
aSDR - Deliver
aSDR - Security

Net New or Existing: 
aSDR - Net New -- No opportunities at all ever
aSDR - Existing

Conversion Channel: 
aSDR - LinkedIn InMail
aSDR - LinkedIn Connection Request Message
aSDR - LinkedIn Connection Message
aSDR - Phone
aSDR - Outreach
aSDR - Direct Mail

### Calendar Invite 

Hello Tim, 

Thank you for our call today. I appreciate your invitation for a brief discussion on Fri, Sep 6 @ 4:00 PM EST. Kindly RSVP  to confirm that you've received this invite.

It's my understanding from my conversation with Jon Gibson that issues such as a) shifting away from HP Micro Focus, b) integrating QA with DevOps, and c) reducing time for manual QE testing may be of importance to you.

However, if you feel there are other more pressing topics we should cover during our 30 minute discussion, please advise and I am happy to accommodate.

Naturally you may have some questions for us, such as: how is GitLab relevant to your initiatives, how well we can integrate with your stack, etc..  

Occasionally, we'll need to have some context in order to answer those questions, like: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

Together, at the end of the brief call we can work out what the next steps will look like, and how to best proceed—even if it’s a no at this stage. Please feel free to add to the agenda or invite anyone else.

Also on the call will be your enterprise team supporting MetLife, Sean Billow (Strategic Account Leader) and Josh Rector (Solution Architect). 

Warm Regards,
Kevin

P.S. I recommend downloading the Zoom app prior to ensure that it goes smoothly. If you experience any issues, I can dial you in. 

****

### Setting up a Demo

Hello Bobby,

Thank you for our call today. It was an absolute pleasure meeting you, and we appreciate your invitation for a demo session on Thursday, December 19 @ 9:30 AM Central.

To ensure that your teams get a lot of value out of this call, we'll plan to make this interactive and tailored to your situations. You specifically mentioned using automated unit testing and continuous deployments as important, so we will be sure to address these during our demo.

As a refresher, GitLab is an "all-in-one" DevOps tool. This helps you reduce licensing costs and administrative overhead from the typical Atlassian/TFS stack. (Many of our clients have found GitLab to be less expensive than their current tool set: https://about.gitlab.com/roi/replace/)

Some of the features that our clients get the most use out of is the GitLab dashboard's metrics, automated security and code analysis with SAST/DAST, Container Registry, CI/CD, integration with Kubernetes, as well as performance metrics for deployed apps. (Here's a list of products we compete against: https://about.gitlab.com/comparison/

Here's a video demo going over the capabilities of what you can do with GitLab (Specifically worth a watch for you ):
https://www.youtube.com/watch?v=4Uo_QP9rSGM)

Naturally, throughout the demo, you’ll have some questions for us. Please feel free to interrupt to ensure that we're able to address any of your concerns. Occasionally, we'll to need to have some context in order to answer those questions, so we'll probably have more questions in return.

Together, at the end of this demo, product walkthrough, and discussion, we'll have a pretty good idea of what the next steps look like and how to best proceed. (Typically, that's an on-site visit or choosing a project to start a small proof of concept.) 

On the call from the GitLab side will be your enterprise team supporting MetLife, Sean Billow (Strategic Account Leader) and Josh Rector (Solution Architect). Please feel free to add to the agenda or invite anyone else.  

With your success in mind,
Kevin


Depending on the language used and the test suite, Auto Devops might be able to run your unit tests without any further configuration. The Direction Page for the Unit Testing category can probably shed some light on this for you: https://about.gitlab.com/direction/verify/unit_testing/

Specific to your use case, I think you might find our "Graph Code Coverage" feature to be useful. This analyzes your unit tests and displays how much of your code has coverage. You would also be able to track how much code coverage is increasing over time.

Additionally, GitLab now displays data about JUnit test results in the pipeline view. Previously, a user could see when a test failed but not get the data needed to resolve the failure and get a passing build easily. With this release, the results of passed, skipped, and failed tests and timing and a detailed view of individual tests including a trace of failed tests for faster identification of issues and contribution of fixes are now available. (This is included even in the open-source Core edition)

