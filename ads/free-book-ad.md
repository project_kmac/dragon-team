Get your FREE Copy of the Unicorn Project!

The highly anticipated sequel to "The Phoenix Project" is here & It can be yours for FREE‼️
We've reserved 100 copies of "The Unicorn Project" available on a first come, first served basis.

https://www.gitlab.com/freebook

****

In The Unicorn Project, we follow Maxine, a senior lead developer and architect, as she is exiled to the Phoenix Project, to the horror of her friends and colleagues, as punishment for contributing to a payroll outage. She tries to survive in what feels like a heartless and uncaring bureaucracy and to work within a system where no one can get anything done without endless committees, paperwork, and approvals.
One day, she is approached by a ragtag bunch of misfits who say they want to overthrow the existing order, to liberate developers, to bring joy back to technology work, and to enable the business to win in a time of digital disruption. To her surprise, she finds herself drawn ever further into this movement, eventually becoming one of the leaders of the Rebellion, which puts her in the crosshairs of some familiar and very dangerous enemies.
The Age of Software is here, and another mass extinction event looms—this is a story about rebel developers and business leaders working together, racing against time to innovate, survive, and thrive in a time of unprecedented uncertainty...and opportunity.
*****

Get your FREE Copy of The Unicorn Project!

The highly anticipated sequel to "The Phoenix Project" is here & It can be yours for FREE‼️
We've reserved 100 copies of "The Unicorn Project" available on a first come, first served basis.


Simply schedule a time to speak to your DevOps Acceleration team, and we'll send you a free copy of "The Unicorn Project"

Click The Link Below.

https://www.gitlab.com/freebook :point_left:

Don't scroll past this and miss out on how simple Accelerating your DevOps time-to-value can be... Meet with the Acceleration team and get your copy today


*****

Hello %FIRSTNAME%,

DevOps professionals... The highly anticipated sequel to The Phoenix Project is almost here & It can be yours for FREE!

We've reserved 100 copies of The Unicorn Project available on a first come, first served basis.

That's right. We'll send you a free copy of The Unicorn Project.

This book is written for technologists and business leaders who want to deliver better value faster.

Whether your enterprise is on the cutting edge with mature DevOps practices, or simply at the beginning of your DevOps journey, this book is a MUST READ.

“Every company going through a digital transformation needs to make this a must-read for all leaders.” —Courtney Kissler, VP Global Technology, NIKE, Inc.

In order to survive and thrive in the Age of Digital Disruption, companies must become dynamic, learning organizations. 

This book shows how modern organizations must be structured to achieve their goals.


You'll learn about:

✔️ The FIVE IDEALS that Enable Your Developers to be Highly Productive
✔️ How to Enable Your Developers and Engineers to be Highly Productive
✔️ Psychological Safety and How a "Just Culture" Fuels Learning Innovation 
✔️ THE FIRST IDEAL: Locality and Simplicity
✔️ THE SECOND IDEAL: Focus, Flow, and Joy
✔️ THE THIRD IDEAL: Improvement of Daily Work
✔️ THE FOURTH IDEAL: Psychological Safety
✔️ THE FIFTH IDEAL: Customer Focus



Simply click the link below to Claim Your Free Copy. 

*****

Hello %FIRSTNAME%,

Reaching out to you because you mention some DevOps tools on your LinkedIn profile, so I thought you might be interested in getting a free copy of The Unicorn Project.

If you've ever had to deal with IT taking forever to provision environments, or testing and security not being automated, or not being able to deploy code... this is especially for you.

Whether your enterprise is at the beginning of its DevOps journey or on the cutting edge of mature DevOps practices, this book is a MUST READ.

We've reserved 100 copies of The Unicorn Project available on a first come, first served basis. The only catch is that I want to chat with you about how you're doing DevOps for 10-15 mins.

Why? Well, I'm looking for members of the DevOps Rebellion.

As Gene says in the book... "We’re recruiting the best and brightest engineers in the organization, training and preparing in secret for the right time to overthrow the Empire, the ancient, powerful, and unjust order that definitely needs to be toppled."

To claim your free copy, simply click the link below, and I'll reach out to you to get the details on where to send it.




**** 

We've reserved 100 copies of The Unicorn Project available on a first come, first served basis.

That's right. We'll send you a free copy of The Unicorn Project.

This book is written for technologists and business leaders who want to deliver better value faster.

Whether your enterprise is on the cutting edge with mature DevOps practices, or simply at the beginning of your DevOps journey, this book is a MUST READ.

“Every company going through a digital transformation needs to make this a must-read for all leaders.” —Courtney Kissler, VP Global Technology, NIKE, Inc.

In order to survive and thrive in the Age of Digital Disruption, companies must become dynamic, learning organizations. 

This book shows how modern organizations must be structured to achieve their goals.

You'll learn about:

✔️ The FIVE IDEALS that Enable Your Developers to be Highly Productive
✔️ How to Enable Your Developers and Engineers to be Highly Productive
✔️ Psychological Safety and How a "Just Culture" Fuels Learning Innovation 
✔️ THE FIRST IDEAL: Locality and Simplicity
✔️ THE SECOND IDEAL: Focus, Flow, and Joy
✔️ THE THIRD IDEAL: Improvement of Daily Work
✔️ THE FOURTH IDEAL: Psychological Safety
✔️ THE FIFTH IDEAL: Customer Focus



Simply click the link below to Claim Your Free Copy. 




1. Premise - Should be the longest line
2. Body - talk in terms of value prop
3. CTA - Request ONE time to unpack the content mapped out in the premise. Provide an agenda

