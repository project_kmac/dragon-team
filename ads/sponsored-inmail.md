# LinkedIn Sponsored InMail

## MM

DELIVER Better Products Faster

[first name], [SDR name] here with the GitLab Acceleration Team.

We exist to help enterprise companies accelerate DevOps adoption. My job is to arm you with knowledge to help combat the “Three F’s” that are so prevalent in IT organizations today: Friction, Frustration, and Fragmented tools and processes.

We talk to IT leaders every day about how to tackle the Three F’s in an effort to help (you): 

1. Increase developer productivity and velocity
2. Disrupt the competition
3. Attract and retain top IT talent
4. Increase market share

We’d love to share ideas to help [company] do more with less. Would you be open to learning more?

Warm Regards,
[SDR name]

{{Sure, let’s chat”}}
{{No thanks, we are satisfied with our current strategy}}

## Deliver

Reaching out to you because you mention some DevOps tools on your LinkedIn profile, so I thought you might be interested in getting a free copy of The Unicorn Project.

If you've ever had to deal with IT taking forever to provision environments, or testing and security not being automated, or not being able to deploy code... this is especially for you.

We've reserved 100 copies of The Unicorn Project available on a first come, first served basis. The only catch is that I want to chat with you about how you're doing DevOps for 10-15 mins.

## Efficiency

Hello %FIRSTNAME%, 

My name is Matt Beadle and as a DevOps Evangelist with GitLab, I focus on connecting with technology focused individuals like yourself. 

Based on your DevOps focus and GitLab’s ability to automate software delivery it seemed relevant for us to connect. 

Below I have highlighted a few of the top initiatives that we help solve for our clients: 

Multi Cloud Approach - Easily Deploy anywhere, especially w/ k8s
Flexible Hosting Options - SaaS and Self-managed options with identical performance and flexibility 
High Performance and Easy Scalability - highly scalable integrated CI/CD and distributed runners which are scalable on demand 

Let’s set up a consultative session to talk in more detail the above initiatives and how we can align with your needs moving forward into 2020. 

Best, 
Matt

“Explore new techniques here” → CTA button 


With some of these initiatives potentially in your 2020 planning I would love to set up some time with you to discuss in more detail. Please click the link below to set up a consultative session with the GitLab team.

“Improve Efficiencies in your DevOps Toolchain” → CTA button

******