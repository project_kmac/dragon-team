# Persona

## LinkedIN Boolean Search

"devops" OR "CI/CD" OR "kubernetes" OR "microservices" OR "containerization" OR "Jenkins" OR "Gitlab" OR "open shift" OR "digital transformation" 

"kubernetes" OR "microservices" OR "containers" OR "devops" OR "CI/CD" or "GitLab" OR "Atlassian" OR "Azure" OR "Enterprise Architecture" OR "Docker"

"kubernetes" OR "microservices" OR "containers" OR "devops" OR "CI/CD" or "GitLab" OR "Atlassian" OR "Azure" OR "Enterprise Architecture" OR "Docker" OR "SAST" OR "DAST"

## DiscoverOrg Boolean Search

### Sample

((("Director" OR "President" OR "VP" OR "AVP" OR "Chief" OR "CIO" OR "CDO" OR "CTO") AND 
(DevOps OR Application OR Develop OR Architecture OR Infrastructure OR Security))


### By Title

("Director" OR "President" OR "AVP" OR "VP" OR "Chief" OR "CIO" OR "CDO" OR "CTO")


("Director" OR "Senior" OR "President" OR "AVP" OR "VP" AND

NOT ("Chief" OR "CIO" OR "CDO" OR "CTO")

### DevOps

DevOps

### AppDev
OR Application OR Develop OR Software

### InfraAch
OR Architecture OR Infrastructure NOT (Analyst OR Admin OR Network OR Corporate)


### QA

(Quality Assurance OR QA Testing OR Testing OR QA) NOT "Analyst"

### Security

Security NOT (Analyst OR Admin OR Network OR Corporate OR Physical)


### Delivery 

(Application OR Develop OR Software)
NOT ( Analyst  OR  Admin  OR  Network  OR  Corporate  OR  Epic  OR  SAP  OR  ERP  OR  Clinic  OR  Engineer  OR  Architect  OR  Oracle  OR  EBS )

### Efficiency

Engineer  OR  Architect OR Infrastructure OR Cloud 
NOT ( Analyst  OR  Admin  OR  Network  OR  Corporate  OR  Epic  OR  SAP  OR  ERP  OR CAD OR Clinic OR Clinical OR PACS OR NOC OR FGPA OR  Oracle  OR  EBS OR Citrix OR Software OR Application OR Product OR Telecommunications OR Cybersecurity OR Systems OR SQL OR Data OR Hardware OR Firmware OR Middleware OR Support OR Lead OR Process OR Model OR Research OR R&D)

### Named Accounts Exclusion

Deprecated. See "DiscoverOrg ListMatch Search below"

 "American Tire Distributors" OR Asurion OR AT&T OR AutoZone OR "Bank of America" OR "Bank OZK" OR "BBVA Compass" OR "BOK Financial" OR CNA  OR CommScope OR "Community Health Systems" OR Dillard's OR "Dollar General" OR "Duke Energy" OR EBSCO OR FedEx OR LabCorp OR Lenovo OR "Lincoln Financial Group" OR "Lowe's" OR Merck OR OR MetLife OR Omnicom  OR Paycom OR "Pilot Flying J" OR "PRA Health Sciences" OR Premier OR Qorvo OR "Regions Financial Corporation" OR "Reynolds American" OR SAS OR "Sealed Air" OR "Tyson Foods" OR Unum OR "Vanderbilt University Medical Center" OR Walmart

## DiscoverOrg ListMatch Search

Special thanks to Kaleb Hill for the simplifying via ListMatch, and Nichole LaRue for the SFDC Report idea.

1. Run SFDC report filtering by SAL for the desired territory -> https://gitlab.my.salesforce.com/00O4M000004dmzA
2. Export to CSV/Sheets and list of "Account Name"
3. Run a DiscoverOrg Advanced Search ListMatch -> https://go.discoverydb.com/eui/#/search
4. Filter further via title, role, or keyword
5. Upload to SDR Acceleration Team - Account Master List: https://docs.google.com/spreadsheets/d/1bfV5k0aB77h5x7ArES79t-pjko7Z-XUfOTEESs8vhmI/edit#gid=430755746

## ACCEL ListMatch Search

1. Copy "Account Name" column in Acceleration Target Accounts spreadsheet
2. Run a DiscoverOrg Advanced Search ListMatch -> https://go.discoverydb.com/eui/#/search
3. Filter further via title, role, or keyword
4. Upload to SFDC
5. Upload to Outreach w/ Tag such as -> "accel_east_oct19_security" 


****

### LISN Search 

In CRM
https://www.linkedin.com/sales/search/people?companyIncluded=Slalom%3A166000%2CGlobalLogic%3A164008%2CVeritas%2520Technologies%2520LLC%3A10003324%2CTIBCO%3A3420%2CStarbucks%3A2271%2CSlalom%2520Build%3A18880052%2CReply%3A4724%2CTwitch%3A2320329%2CYelp%3A31517%2CMicron%2520Technology%3A3690%2CValorem%2520Reply%3A447716%2CWeyerhaeuser%3A4494%2CVerity%2520Health%2520System%3A15229907%2CTriMet%3A18428%2CSamaritan%2520Health%2520Services%3A31805%2Csamhealth%3A5053423%2CFormFactor%2520Inc.%3A8834%2Cintuit&companyTimeScope=CURRENT&crmContacts=INCLUDE&doFetchHeroCard=false&geoIncluded=103644278&keywords=%22kubernetes%22%20OR%20%22microservices%22%20OR%20%22devops%22%20OR%20%22CI%2FCD%22%20or%20%22GitLab%22%20OR%20%22Atlassian%22%20OR%20%22Azure%22%20OR%20%22Enterprise%20Architecture%22%20OR%20%22Docker%22%20OR%20%22SAST%22%20OR%20%22DAST%22&logHistory=true&logId=4096375084&page=1&rsLogId=201541554&searchSessionId=mMCxIbgGRdiBwp9MNcBXxw%3D%3D

Not In CRM
https://www.linkedin.com/sales/search/people?companyIncluded=Slalom%3A166000%2CGlobalLogic%3A164008%2CVeritas%2520Technologies%2520LLC%3A10003324%2CTIBCO%3A3420%2CStarbucks%3A2271%2CSlalom%2520Build%3A18880052%2CReply%3A4724%2CTwitch%3A2320329%2CYelp%3A31517%2CMicron%2520Technology%3A3690%2CValorem%2520Reply%3A447716%2CWeyerhaeuser%3A4494%2CVerity%2520Health%2520System%3A15229907%2CTriMet%3A18428%2CSamaritan%2520Health%2520Services%3A31805%2Csamhealth%3A5053423%2CFormFactor%2520Inc.%3A8834%2Cintuit&companyTimeScope=CURRENT&crmContacts=ALL&doFetchHeroCard=false&geoIncluded=103644278&keywords=%22kubernetes%22%20OR%20%22microservices%22%20OR%20%22devops%22%20OR%20%22CI%2FCD%22%20or%20%22GitLab%22%20OR%20%22Atlassian%22%20OR%20%22Azure%22%20OR%20%22Enterprise%20Architecture%22%20OR%20%22Docker%22%20OR%20%22SAST%22%20OR%20%22DAST%22&logHistory=true&logId=4096375084&page=1&rsLogId=201541554&searchSessionId=mMCxIbgGRdiBwp9MNcBXxw%3D%3D

Seniority Level 
&seniorityIncluded=6%2C5%2C7%2C8