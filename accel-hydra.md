# Hydra - 9 Sequences 

#### CI/CD

Forgive me if I'm mistaken but at the end of the day when it comes to enterprise architecture, Chief Architects care about increasing efficiency. The ones I've spoken with often might tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?

#### Jenkins 

Came across your LinkedIn and saw that you're using Jenkins for continuous integration.

Quick question: how are you scaling CI without running into issues with dependencies or introducing vulnerabilities?

Goldman Sachs was able to scale their CI while avoiding the dependency and vulnerability issue by using 

Open to learning more?

In any case, I hope your week is going well.

#### SRE 

Forgive me if I'm mistaken but at the end of the day, SREs care about creating standards that empower the rest of the org. The ones I've spoken with often might tell me something along the lines of "SRE needs to have a bigger seat at the table in terms of overseeing tools. Because if one team is using something and it works better, we need to do that for other teams." Is this relevant to your world, or am I just rambling here?


P. S. Here's how we do SRE at GitLab, it's a short read that I thought you might enjoy: https://about.gitlab.com/blog/2019/12/16/sre-shadow/

Correct me if I'm wrong but as I understand it, when teams can choose their own tools that can be a challenge for the SREs.

Forgive me if I'm mistaken but at the end of the day when it comes to SRE, increasing efficiency is key. The {{title}}s I've spoken with often tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


#### Cloud

You mention on your LinkedIn using "Cloud platforms to increase delivery speed and agile development." 

Forgive me if I'm mistaken but when it comes to cloud infrastructure, using Kubernetes is a major areas of focus.

What I've heard from other Directors of Infrastructure is that "We're targeting deployment to GCP and Kubernetes for immediate future, with a hybrid/multi-cloud approach down the road." Is this something you've come across in your practice?


#### 

I'm going to focus more on Data Engineering because I see this as a field that's been left behind in the DevOps and SRE movements. There are plenty of great tools to make running web servers at scale (both in terms of feature velocity and users) relatively easy. Creating Data Pipelines and making the data useful for business reporting is still really hard and complex.

Self-healing infrastructure if you have not done this already. That is any failure that is considered "normal" (i.e. a host dying, network partition) should be self healing. But for me at least this is already part of how we build infrastructure.

Automation has been around for ages, I have been riding that wave for at least 7 years. But we're seeing a shift away from "learn kubernetes/puppet/awscli/ansible/terraform to deploy this thing" to "run this command to get an opinionated solution". This is a strong shift, that required years of maturation from Automation tools. Jess Frazelle's talk at re:Invent really drove this home for me, we must build tooling, whether it's with Github Actions or some other workflow tool, to create, deploy and tear down services as we need them. Convention over configuration.

Serverless where it makes sense, it's still in the experimental phase and we've already found a couple of use cases that are bad for it. Continuing to find the most valuable use cases for it will continue.

Everything as Code, so last year :P

ML, our company is using Machine Learning for a couple of things, specific use cases where it makes a lot of sense. But it's pretty canned examples and nothing revolutionary that needs anything like intense training pipelines. I'm an interested and certainly it will continue to make big waves but not for me yet. Big Data, see comments on Data Engineering. I'm still not sure focusing on how big your data set is is the right paradigm. Instead we should focus on how quickly we can adapt to changing markets and needs of the stakeholders to deliver them the information they need.

