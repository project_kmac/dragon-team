# Acceleration Team Campaign Requests
If you are requesting acceleration team run a campaign to an account, territory, or event, please fill out the details below.  If you don't know all the details, fill in as much as you can.

**Goal:** Standup targeted outbound campaign to create qualified meetings for new SAL.

**Qualified Meeting Target:** 50

**Execution Dates:** October 1 - October 31

## :goal: Goals for Campaign:
* [ ]  Qualified Meetings (on path of GitLab value driver(s))
* [ ]  SAO Conversion (50%)
* [ ]  Engagement (150 engaged leads)
* [ ]  SDR Acceleration (^ engagement and conversion rates)

Campaign Brief: https://docs.google.com/document/d/1HtHDo2tXI06dbbrhZserBZjNBkWwjT_RX7TXaBKlziE/edit

**Territory:** EAST

**Target Accounts:** 
[Existing Accounts in Salesforce](https://gitlab.my.salesforce.com/00O4M000004dnIq) || [Accounts in Google Sheets (filterable by account information)](https://docs.google.com/spreadsheets/d/1e9QjJfZw4XR0sMNr22fQB7sDPmM9YBuB/edit#gid=1819943354) |
________________

## :notepad_spiral: Event Details
*  Official Event Name: All Things Open
*  Location: Raleigh, NC
*  Website or Landing Page: https://allthingsopen.org/
*  Event Issue: [All Things Open - Oct 13-15th - Booth space 90](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/416)
*  MPM: Zac Badgley

________________

Priority Tasks:
* [ ]  Outbound Content - General (non-event focused)
* [ ]  Outbound Content - Event (tailored for All Things Open)
* [ ]  Triple Touch Outbound Plan (Email, Social, Phone)
* [ ]  Build Account Target List
* [ ]  Remove SDR Exclusion List
* [ ]  Identify Target Personas (VP + Director of DevOps, Cloud, Security, Infra)
* [ ]  Divide Execution Responsibilities (MM)
* [ ]  Schedule Kickoff Call (Acceleration SDRs, Regional SDR, SAL)

Other Tasks:
* [ ]  Direct Mail Campaign
* [ ]  Digital & Social Ads

________________

## Outreach and Follow Up
#### Personal outreach and setting up meetings with customers at event ~"SDR"
**Invite focus accounts to meet at events**
* [ ] Create focus account list
* [ ] Send personalized Outreach sequence
* [ ] Send personalized invitation
* [ ] Send personalized InMail / LinkedIn




## 📝✍️ Task List 📝✍️

- [ ] Persona Data

- [ ] Messaging & Creative

- [ ] Channels 

- [ ] Hypothesis & Goals

- [ ] Retrospective & Results 

## 👽👻 Persona 🕵️🤖
### This information is required for every type of promotion.  Fill in as much as you can.

* Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. cities, states, countries, etc.)`

* Accounts: `Enter desired industries to target (we will match as closely as possible)`

* Roles/Titles: `Enter targeted job titles`

Do NOT upload a CSV containing PII to this issue, because CSV is not Secure.


##  ✏🖍️ Messaging & Creative ✏🖍️
### We need at least 2 weeks to prepare paid social campaigns

* Messaging: `Include buyer specific pain points, desires, and call to action.`

* Creative Assets: `Enter URL to GitLab creative asset to use -- this may require a separate ask for Design team, please make request in Corporate project and factor into timeline `
     * Platform creative requirements:
          - Facebook: `1200x628 pixels`
          - LinkedIn: `1200x628 pixels`
          - Twitter: `800x418 pixels`


## Campaign 
### We need at least 2 weeks to prepare paid social campaigns

* Landing Page: `Enter URL to drive traffic to and add link to issue if content is not live yet`

* Purpose of campaign: `Enter in the reasoning for this campaign`

* Campaign Optimization: `Enter goal for this campaign`

- [ ] Reach: 

- [ ] Traffic (Link Clicks or Landing Page Views): 

- [ ] Engagement: 

- [ ] Messages: 

- [ ] Video Views: 

- [ ] Conversions: 

- [ ] Other (please describe): 


* Campaign Start and End Dates: `Enter start and end date -- if this content expires, add the expiration date`

* Budget for Campaign: `Enter in budget for campaign, you may not know this, but if you have spend, add it here`



## 📨📮 Channels 📨📮

Channels to run campaign

- [ ] Calls: 

- [ ] Emails: 

- [ ] LinkedIN: 

- [ ] Social: 

- [ ] Other (please describe): 

## 🤔💭 Hypothesis & Goals 🤔💭 


## 🔍📊 Retrospective and Results 🔍📊

Number of Prospects -
Conversations - 
Meetings Booked - 
SAOs -

Event Registrations -


/assign @gitlabkevin

/label 