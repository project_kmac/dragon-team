# Template Operations Tasks

## :clipboard: Coverage Overview  

**Coverage Dates:** May 1 - June 30, 2020

## **Teams & Target Territories** :dart: 

**:large_blue_circle: Delta Team:** bgreenwell, mlebeau, @mbeadle, @samcguire, @mstangl 
*  Central & Gulf Carolinas (Chris Graham, SMcguire)
*  Lake Michigan (Tim Kuper, MStangl)

**:red_circle: Bravo Team:** @k-mac k-mac, ShawnWinters, dfishis1, @brandonbrooks  @aglidden   
*  Texas (Matt Petrovick) 
*  Massachusetts (Tony Scafidi, New SAL TBH)


**Link to Planning Issue:** #255 

____________________

## :red_circle: : Bravo Ops Tasks
**Ops Lead:** Kevin McKinley

https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#target-account-identification


****
@aglidden tasks:
* [ ] Identify known accounts from Salesforce.com using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG/e?retURL=%2F00O4M000004dmXG)
* [ ] Identify net new/unknown accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
* [ ] Combine Salesforce Known & DataFox NetNew target account lists into [this spreadsheet](https://docs.google.com/spreadsheets/d/1RUKlII7LNfSo50YmILusgTuhJ0wsG-lFqJCLvTRODc4/edit#gid=1757652222)
* [ ] Dedupe & Review
* [ ] Share Target Account List with ABM team for Demandbase appending and targeting
* [ ] List Match in DiscoverOrg (by company domain then by name)
* [ ] Execute saved LinkedIn Boolean searches and append prioritization data to target lists
* [ ] Run CE Usage Reports & Append data
* [ ]  Divvy up accounts for each acceleration SDR
* [ ] Review final Target Account list with Regional SDR & SAL teams
* [ ] Pull target prospects (by titles & keywords) from DiscoverOrg
* [ ]  `Mon, Apr 27, 2020`: Prospect Uploads Due: Submit Issue for Marketing Ops DiscoverOrg Prospect List upload
****
@brandonbrooks tasks:
* [x] Identify known accounts from Salesforce.com using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG/e?retURL=%2F00O4M000004dmXG)
* [ ] Identify net new/unknown accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
* [ ] Combine Salesforce Known & DataFox NetNew target account lists into [this spreadsheet](https://docs.google.com/spreadsheets/d/1RUKlII7LNfSo50YmILusgTuhJ0wsG-lFqJCLvTRODc4/edit#gid=1757652222)
* [ ] Dedupe & Review
* [ ] Share Target Account List with ABM team for Demandbase appending and targeting
* [ ] List Match in DiscoverOrg (by company domain then by name)
* [ ] Execute saved LinkedIn Boolean searches and append prioritization data to target lists
* [ ] Run CE Usage Reports & Append data
* [ ]  Divvy up accounts for each acceleration SDR
* [ ] Review final Target Account list with Regional SDR & SAL teams
* [ ] Pull target prospects (by titles & keywords) from DiscoverOrg
* [ ]  `Mon, Apr 27, 2020`: Prospect Uploads Due: Submit Issue for Marketing Ops DiscoverOrg Prospect List upload
****
## :large_blue_circle: : Delta Ops Tasks
**Ops Lead:** Matthew Beadle
****
@samcguire tasks:
* [x] Identify known accounts from Salesforce.com using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG/e?retURL=%2F00O4M000004dmXG)
* [ ] Identify net new/unknown accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
* [ ] Combine Salesforce Known & DataFox NetNew target account lists into [this spreadsheet](https://docs.google.com/spreadsheets/d/1RUKlII7LNfSo50YmILusgTuhJ0wsG-lFqJCLvTRODc4/edit#gid=1757652222)
* [ ] Dedupe & Review
* [ ] Share Target Account List with ABM team for Demandbase appending and targeting
* [ ] List Match in DiscoverOrg (by company domain then by name)
* [ ] Execute saved LinkedIn Boolean searches and append prioritization data to target lists
* [ ] Run CE Usage Reports & Append data
* [ ]  Divvy up accounts for each acceleration SDR
* [ ] Review final Target Account list with Regional SDR & SAL teams
* [ ] Pull target prospects (by titles & keywords) from DiscoverOrg
* [ ]  `Mon, Apr 27, 2020`: Prospect Uploads Due: Submit Issue for Marketing Ops DiscoverOrg Prospect List upload
****
@mstangl tasks:
* [x] Identify known accounts from Salesforce.com using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG/e?retURL=%2F00O4M000004dmXG)
* [ ] Identify net new/unknown accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
* [ ] Combine Salesforce Known & DataFox NetNew target account lists into [this spreadsheet](https://docs.google.com/spreadsheets/d/1RUKlII7LNfSo50YmILusgTuhJ0wsG-lFqJCLvTRODc4/edit#gid=1757652222)
* [ ] Dedupe & Review
* [ ] Share Target Account List with ABM team for Demandbase appending and targeting
* [ ] List Match in DiscoverOrg (by company domain then by name)
* [ ] Execute saved LinkedIn Boolean searches and append prioritization data to target lists
* [ ] Run CE Usage Reports & Append data
* [ ]  Divvy up accounts for each acceleration SDR
* [ ] Review final Target Account list with Regional SDR & SAL teams
* [ ] Pull target prospects (by titles & keywords) from DiscoverOrg
* [ ]  `Mon, Apr 27, 2020`: Prospect Uploads Due: Submit Issue for Marketing Ops DiscoverOrg Prospect List upload
****

<!-- Please leave the label below on this issue -->









<!-- Please leave the label below on this issue -->