## Security in Context, Not Isolation


"What we're trying to do is not improve security in a slice of silo, which is improve vulnerability scanning, that's not our goal. Our goal is to improve our quality overall. How vulnerable the code is. We're looking at those in context. Not in isolation."

That's a direct quote from an application security architect I spoke with today, and it is REFRESHING to hear a holistic approach to security.

It's not about improving the local optima.

SAST, DAST, Container Scanning, SAML SSO... these are all just pieces of the puzzle.

The best DevSecOps professionals I've met with are looking to improve security across the board by integrating security into the entire deployment pipeline.

That means securing applications, securing identity and access management, securing the deployment pipeline itself...

That means reducing MTTR, reducing MTTD, and reducing the attack surface vectors...

The traditional approach to information security is incongruent with DevOps.

Shifting security left is necessary, but not sufficient. 

#enterprisedevsecops #devopsacceleration

*****



 I don't think I understand need to charge enterprise customers for enterprise solutions for enterprise architecture things like security. But okay. I'll bull and like all right. Fine. I'm sure if that's or I can also see why check would throw this out as this that were true. So I would also say that he probably wouldn't implement that as kind of alternative with it just wasn't an a significant decision making cost factor either. So there must be a reason why we want to either. Yeah look at trying to do something else or see what else is out there because that should have been a no brainer turn something on doing like the cause back to

 Twos competing Estill sees because we have 2 different large development centers. It's historical bid based on our company history here. So we have a waterfall process. It's pretty well defined has all and I steps in it are they executing well on like everybody else? Yeah to some degree. Yes. And some degree now um



And there's disjointed software development across multiple language and things like that is going onto, but essentially process look like a high level of waterfall development based gating through the large security face case tend to be compliance and security review for compliance needs security architecture reviews. Those are me right now. And all we're looking at solution being an engineering and delivery and through those cycles they will touch on scan static scanning, also, do some dynamic scanning and some last testing for web applications. We don't have any like what Ryan said. We don't have any, we're just moving into container. So we're in trying to figure out a container security link, Oregon, or plane. Are you out by now



Yeah, exactly, okay. So right now all of those components are up for review. We're looking at them all right now because right now most that's done through open source. Except for our last scanning, which is tied to our PCI compliance. So we have a pretty



Vendor there right now that eventually might replace, but we're not looking to replace anything. That's not a weakness. So are, we missing are on everything but the last scanning, so we're looking to improve those. So that's kind of where we're at from. Estill see perspective. Okay.

We're trying to do is not improve it in a slice of silo, which is improve vulnerability skiing, not our goal. Okay. Our goal is to improve our quality right overall when which how vulnerable be, how vulnerable the coat is to some XYZ path as a component of that. But realistically we're improving by moving from water file to waterfall to an agile as TLC moving out of


Historic or prehistoric a version control systems we are into you know like common version control system across the enterprise things like that are all happening at same time. So. Okay. We're looking at those in context. Not in isolation.

