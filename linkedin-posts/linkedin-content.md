# LinkedIn Content

## 2019-01-27

**A**

Alan Shimel posted an article titled "The Battle for End-to-End DevOps Champion is On"

He describes the current landscape and the players in this fight for supremacy. 

The question is no longer "Best of Breed" vs "Best of Suite"... it's now "Which end-to-end solution will you choose?"

**B**

The Unicorn Project is one of the most important books you can read - here's why:

If you've ever been frustrated with spending too much time in meetings where nothing happens, or waiting for things they need to get work done, or having to resort to heroics and firefighting (working late nights and weekends)... this book was written for you.

The philosophy and practices within are the key to removing friction so you can be more fulfilled at work.

More thoughts in the video:

**C**

How an SDR Should Approach a Greenfield Territory

This is something I wish I would have been taught when I was just starting as an SDR. Without this, I wasted a LOT of time flailing around and retreading dead accounts, not getting any traction. 

I've felt that unfortunately, SDRs are often left to their own devices when it comes to figuring out what accounts to prioritize in their territory - or that accounts are prioritized somewhat arbitrarily by name recognition.

Here's how I'm approaching territory planning and account prioritization today:

1. Gather list of all target accounts within territory
2. Boolean search on LinkedIn SalesNavigator 
3. Usage data
4. ????
5. Profit

This approach allows you to focus and prioritize so that each touch becomes more effective, and is even more powerful when combined with personalized outreach.

## 2019-01-28


**A** 

On accountability, taking ownership, and getting the job done.

Sometimes you need a kick in the butt. When a teammate calls you out and holds you accountable, that's a gift.

It means they care enough about you to have that conflict out in the open, rather than allowing problems to fester.

Holding someone's feet to the fire on unproductive or counterproductive is helps prevent dysfunctions within a team.


**B**

I was trying to post a longer video, but LinkedIn only allows videos to be up to 10 mins.

So instead of getting the full length, unfiltered, cold call session, I can only give you the highlight reel.

The reason I spend so much time personalizing my outreach is because I know that it's exponentially more impactful.

My promise to you as a prospect, is that if I'm calling you, or emailling you, or trying to send you telepathic messages, it's because I've done my research and have a reasonable level of assurance that what I'm about to say could be important or impactful to you.

I'm not coming at you empty handed, asking for a meeting.

If I'm right and what I'm saying is relevant to you, it might just be the thing that solves a problem for you.

*(note that this also means I could be wrong)*

But either way, I'm hoping to present contextual information at the right time.

Nobody wants to be spammed and harrassed, and this is the only way I've found to be respectful of that.

**C**

I've always wanted my own montage.

So here's two hours of me writing and sending cold emails compressed into two minutes

(Thank LinkedIn's 10-min video time limit for sparing you for watching this at regular Earth speed. Music is "Algorithms" by Chad Crouch)

## 2020-01-29

**Travis Intro - iPhone**

What's the easiest way to understand what GitLab does? The metaphor I've found most helpful is that it's like the iPhone.

Before the iPhone came out in 2007, before smartphones became prolific, we needed a variety of tools to manage our personal and professional life.

Your phone, your calendar (Filofax or Franklin Covey), your camera, your iPod/CD player/walkman, your computer for emails and web browsing...

Today, all of those tools have been consolidated into one.

In today's DevOps landscape, tools have proliferated in much the same way to assist us in managing software and product development.

Organizations typically have a variety of these tools. But as the tools multiply, so does the complexity of the environment. And this leads to time-consuming maintenance.

GitLab does for software development, what the iPhone did for your personal management.

And so when you see it that way, the question changes from "Should I use an iPhone?" to "How soon will you pick one up?"

It's not "Will you use an end-to-end DevOps pipeline?"; it's "When?"

Because I guarantee you that the early adopters and fast movers are the companies reaping the benefits.


** Travis - What are the most common use cases for GitLab?

There are three patterns I've seen for when companies start to adopt and be successful in using GitLab.

1. They have an established DevOps pipeline, and are starting to feel the pains of managing and maintaining that toolchain.

2. They're at the beginning of their DevOps journey. They may especially have limited manpower or resources, and do not want to maintain it on their own.

3. They are looking to shift security left by integrating security testing throughout the pipeline.

Not an end-all-be-all list, simply an observation. 


** Travis - How can Partners/Resellers/Consultants/SystemsIntegrators get into this Marketplace **

How can Partners/Resellers/Consultants/SystemsIntegrators get into this Marketplace?

Travis asked, "What type of companies do you see using GitLab, and where are the opportunities for partners/resellers to get into the marketplace?"

I'm not the expert on this. For that I'd ask Tina Sturgis.

Tina, I gave an answer here, but what are your thoughts and how would you answer this question?



** Travis - GitLab by Analogy **

Super thought provoking question from Travis about helping partners/resellers understand how GitLab compares to other tools in the space.

It took me back to when I first started here, with only a rudimentary understanding of DevOps.

And it can be a tough question to answer. The way I got comfortable with it is by analogy.

GitLab is like BitBucket and GitHub, if you're looking for SCM. It's like Jenkins, or Bamboo, or Travis, if you're looking for CI. And so on for pretty much every category in the software development lifecycle. 

If you're using a tool, GitLab probably does that. Or if it doesn't, it's on the roadmap and will be here soon enough.

... But if you're looking for the whole enchilada, there's only one GitLab.


**The Five Constraints**

In "The DevOps Handbook", Gene Kim et al. identified five constraints predictably encountered by organizations on their DevOps adoption journey.

It is important to know these constraints so that we can identify where an organization lies in terms of maturity, and thus be able to address the bottlenecks preventing productivity and exponential growth.

Knowing the constraint allows us to know where our highest impact activity lies.

## 2020-01-30

** Account Prioritization **

Ever wondered what it takes to succeed in a greenfield territory?

It starts off with systematically prioritizing every single account.

Even if you have to open 1,600 tabs to do it.

Just a day in the life of the Acceleration team.



** LinkedIn Connection Request Breakdown **

Had two people try to book a meeting with me today, and wanted to share how it made me feel and what I think they could have done better.

The problem is that it's obvious to your prospects when you're sending the same exact message to 50 different people.

If that's the tactic you're still using, it's time to level up.

It's okay to have a framework, but each message should be personalized.

I'm actually rethinking that term, because it still sounds like something you could automate.

Let me be very clear. This is binary. Either you're speaking to a person one on one, or you're not.

There's no middle ground.




## 2020-02-06

**A**

"I would rather our teams deliver software, than build CI/CD pipelines."

If you've ever spent time working on integrating point products, you may have had this thought.

Enterprises often spend a lot of time, money, and resources on making tools work together, so this is a common refrain that I hear.

But not everyone is willing to make the switch to a consolidated tool.

So my two questions to you are...

a) How bad does the pain of integrating tools have to be?

b) What would the alternative solution have to have in order for you to consider switching?

## 2020-02-07

**A**

Instead of trying to stitch together tools, or spending your time maintaining integrations...

Use an end-to-end DevOps solution so your teams can focus on building code.

It's not about DevOps for DevOps' sake. It's about using DevOps to achieve your business goals


#devops #devopsrebellion



## 2020-02-12

"Instead of using what shared services provides, would it be more efficient for us to build our own platform?"

Had a powerful conversation with a Director of Engineering today.

Like many enterprises, they have a stack with Jenkins, Docker, etc.

However, those tools are owned and maintained by a different team (often a shared services team, in this case owned by SRE).

Because of this they weren't able to scale the way they needed.

The question then becomes, "Does it make sense for us to look at building out what we need in our own instance?"

Dependencies between teams introduces friction and adds lead-time and WIP to the system.

Ownership of your platform is important if you want to scale-as-needed or have self-service.

Here's my question to you, should you be facing this scenario...

At what point would the benefits of building and maintaining your own platform outweigh the costs?

#devops #devopsrebellion


## 2020-02-21

I've never met an enteprise architect I didn't like. 

Something about the way they think, I suppose.

Here's what stood out to me about the conversation I had with one today...

"A lot of teams bring their own tools. But as an enterprise architect, I want teams to be able to say 'I'm building my app and deploying to AKS a container image that has been vetted.'"

There's an interesting core conflict between architecture and the teams they support.

Architecture would like to be able to dictate standards in order to manage the tool sprawl.

While some teams, feeling constrained by the provided platform, look to strike out on their own and use the tools that suit their situation. Whether those tools are approved or not.

