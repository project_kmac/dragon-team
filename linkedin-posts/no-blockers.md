# NO BLOCKERS Offer

Had a really insightful conversation yesterday with an innovation group. 

They're an incubator within a large enterprise for some of the more ambitious ideas. 

Case in point, they're using multiple Kubernetes clusters for each "subscriber" to deploy to 100s of access pods. That's a lot of Kubernetes.

Anyway, the mandate is that this team needs to be Autonomous and empowered to, and I quote, "Do More Awesome"

This means No Blockers. Removing everything that stands in their way, and unburdening every developer on the team so that they can be 100% focused on developing software.

*sighs* If only every team could be as awesome as theirs, right?

They were able to identify a key problem, something that had been bugging me for a while. A question I hadn't been able to find an answer for.

"Why would a team like this, with pretty advanced DevOps practices, choose GitLab over whatever else it is that's already in their stack?"

Fair question that I've been asked by clients on Azure DevOps that I didn't dare regurgitate the standard marketing blah-blah to.

You've already built much of the darn thing. I get that there's no reason to switch.

Here's the deal though. They're using *under breath* JIRA.

Look, I won't bag on JIRA. It's too easy, and you could probably do it better than me. Suffice it to say, their instance was slow and huge.

BUT. And this is a huge revelation.

JIRA was owned by another team.

And it just so happens that this autonomous team was beholden to JIRA to be available.

If Jira has downtime, cannae get work done.

Not only that, they can't go fix the problem themselves.

"If we have downtime, it should be due to someting we did, not something someone else caused."

Smart.

On top of that, there are all of these out-of-band, manual processes involved with getting into JIRA to track user stories, move items around on a kanban board, etc.

Can you imagine how painful that is for an innovation team? A smart, bleeding edge, automate the heck out of everything group?

That's kind of like having to police yourself.

Rzesz wrote about this piece of it end of Dec 2018.

"the epicenter of this movement [DevOps] is doing the right thing for people. How some of the movement's focus on burnout, empowerment, and progression are all the right things for people." - https://www.linkedin.com/pulse/i-find-your-lack-faith-disturbing-john-rzeszotarski/

DevOps done well means that the right thing to do is the easiest thing to do.

We make it the path of least resistance.

Imagine a DevOps pipeline like this. 

Whether you're already entrenched in DevOps tools or just starting up your pipeline, here's my offer to you.

If your team is looking to be more AUTONOMOUS, to have ZERO BLOCKERS, and be unburdened and empowered so that every developer can be 100% focused on developing software, we have the perfect solution.

I'd like to test this with a handful of clients, maybe 4-5. If you're not already somewhat autonomous, or your toolhain is owned by shared services - probably not the right thing for you.

But if you're looking to reduce dependencies, (as an example, on JIRA or Jenkins) and have a tool or two to replace in your stack so that you can function more independently, then we should talk.

We can figure out if this makes sense for you, and what implementation would look like.

Simply fill out this form - http://survey.enterprisedevsecops.com/ - tell me about your situation, and I'll reach out to you to schedule a call.


P.S. 