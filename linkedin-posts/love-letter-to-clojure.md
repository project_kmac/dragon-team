# Love Letter to Clojure

In his talk, "Love Letter To Clojure: And A Datomic Experience Report" Gene Kim explains the Second Ideal of Focus, Flow, and Joy. He gives a contrast to the ideal, sharing that "Not Ideal is all your time is spent trying to solve problems you don't even want to solve, like figuring out how to write a Kubernetes YAML file."

This reminds me of a comment by a Sr Director of Infrastructure we work with.

His team had invested in building an Azure DevOps pipeline, and yet, despite having mature devops tooling, were interested in moving it all to GitLab.

When pressed as to why, he said, "I want my developers building pipelines, NOT integrating SAST tools with Azure." (The question was, why use GitLab instead of integrating app sec into the current pipeline.

That Gene's and this Sr. Director's comments echo each other, to me, portends a shift towards self-service platform becoming the norm.

The question is, as Josh Rector so eloquently puts it, "Are your developers spending more time 'keeping the lights' on than doing 'value added' work?"

I'll leave you with a final quote, this time straight from #TheUnicornProject...

> “A hundred years ago, most large factories had a CPO—a chief power officer—who ran the electricity generation processes. It was one of the most important roles in manufacturing, because no electricity, no production. It was a Core process,” he says. “But that role has disappeared entirely. Electricity has become infrastructure that you buy from a utility company. It is interchangeable, and you choose suppliers primarily on price. There is rarely a competitive advantage to generating your own power. It is now merely Context, no longer Core. You don’t want to be the organization that has a large staff providing internal power generation.

In the same way, I believe that your deployment pipeline will move from a Core process to a Context process. There's little reason or benefit to building, maintaining, and integrating a bunch of tools... just so you can deploy code to production. 

The pipeline isn't the end goal, it's the code that's produced. That's what creates value for your customers. The pipeline is simply the utility.

****

In his talk at Clojure/conj 19, Gene Kim explains the Second Ideal of Focus, Flow, and Joy. He gives a contrast to the ideal, sharing that "Not Ideal is all your time is spent trying to solve problems you don't even want to solve, like figuring out how to write a Kubernetes YAML file."

This reminds me of a conversation I had with a Sr Director of Infrastructure. His team had invested in building an Azure DevOps pipeline, and yet, despite having mature devops tooling, were interested in moving it all to GitLab.

When pressed as to why, he said, "I want my developers building pipelines, NOT integrating SAST tools with Azure." 

That Gene's and this Sr. Director's comments echo each other, to me, portends a shift towards self-service platform becoming the norm.

The question is, as Josh Rector so eloquently puts it, "Are your developers spending more time 'keeping the lights' on than doing 'value added' work?"

I'll leave you with a final quote, this time straight from #TheUnicornProject.

> “Electricity has become infrastructure that you buy from a utility company. It is interchangeable, and you choose suppliers primarily on price. There is rarely a competitive advantage to generating your own power. It is now merely Context, no longer Core. You don’t want to be the organization that has a large staff providing internal power generation.

In the same way, I believe that your deployment pipeline will move from a Core process to a Context process. There's little reason or benefit to building, maintaining, and integrating a bunch of tools... just so you can deploy code to production.

The pipeline isn't the end goal, it's the code that's produced. That's what creates value for your customers. The pipeline is simply the utility. 