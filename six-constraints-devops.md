# The Six Constraints of DevOps

## ReadMe

Key Outcomes: Create a contextual marketing funnel that reaches prospects who are "less aware" of GitLab, by addressing common, predictable bottlenecks that Enterprise DevOps adopters run into. The KPI for this is engagement with the content. (We will then leverage their engagement in postbound outreach and retargeting.)

"In the technology value stream, it always amazes me how the pattern of which the constraints are broken are pretty similar. " GK, BTPP

## 1. Environment Creation

Q: "How long does it take to get an environment provisioned?" 

Had a conversation with an SRE. Like many enterprises, they had your typical toolchain with tools like JIRA, Jenkins, etc. Asked him how long it took to get an environment/server. His response was that it took 7 days.

Symptoms:
* Have to wait for weeks/months to get a prod/test/dev environment.
* Long lead times.
* Lots of WIP.

Treatment: 
* Create environments on demand and self-service.
* We do this by putting everything into VCS. (Ideally this becomes your SSOT) 
* Infrastructure as code. Containerization: make it easier to rebuild vs repair 

*****

Does it take weeks or months for your organization to stand up a production, test, or dev environment?

Is this preventing your teams from getting work done?

If your organization is adopting DevOps, chances are that you've run into the issue of environment creation.

This is The 1st Constraint of DevOps: a bottleneck that encountered by teams in their early stages of implementation.

You might be facing this constraint if you ever find yourself waiting weeks or months for an environment or server to be provisioned. Other symptoms include long lead times and lots of WIP in the system.

In order to move past this bottleneck, the first step is to version control everything. 

(Ideally, this could be something like GitLab where your SCM and CI are in the same tool, and acting as your "Single Source of Truth"/SSOT)

This would allow you to have Infrastructure-As-Code, ensuring that your environments are consistent across the different stages. Having consistent Dev, Test, Staging, and Production environments is key.

You'll also want to containerize your environments. This makes it easy to rebuild (instead of attempting to troubleshoot and repair) in the event that something goes wrong.

Teams should be able to create environments on-demand.


#devops #gitlab #sixconstraints

*****

This is The First Constraint of DevOps, and a bottleneck that rears its ugly in the early stages of every DevOps implementation.

As a concrete example, during a conversation with an SRE, I asked him how long it took to get an environment up and running. His response was that it took seven days.

For any DevOps team, that's certainly seven days too long.

You might be facing this constraint if you ever find yourself waiting weeks or months for an environment or server to be provisioned. Other indicators include long lead times and having lots of WIP in the system.

The end result? Diminished throughput, spending time firefighting, and stale/neglected work.

In order to move past this bottleneck, the first step is to version control everything. 

(Ideally, this could be something like GitLab where your SCM and CI are in the same tool, and acting as your "Single Source of Truth"/SSOT)

This would allow you to create Infrastructure-As-Code, which ensures that your environments are consistent across the different stages. Having consistent Dev, Test, Staging, and Production environments is key to avoiding the "It works on my laptop" problem.

You'll also want to containerize your environments. This makes it easy to rebuild (instead of wasting critical time attempting to troubleshoot and repair) in the event that something goes wrong.

Finally, it's important that your teams are able to create environments on-demand and in an elastic, self-service manner. They cannot achieve automated deployments and reap the benefits of DevOps if they must submit a ticket and wait every single time they need a new environment.

My question to you is, "How long does it take to provision an environment?"

Tag someone you know in IT who should see this.

****

There are six predictable bottlenecks every organization encounters when adopting DevOps.

The First Constraint is Environment Creation.

Teams cannot achieve automated deployments and reap the benefits of DevOps if they must submit a ticket and wait every single time they need a new environment.

#devops #sixconstraints #gitlab


## 2. Deployment Frequency

Q: "How often are you deploying to master?"

If it's not trunk based, small batch, frequent deployment, then it's not continuous integration. It's not continuous integration if there's no daily commits to master.

Symptoms: Long lived feature branches creates informational silos that make it painful to resolve merge conflicts. This creates a negative feedback loop of "Resolving merge conflicts suck so I'm going to work on my own branch." 

Treatment: We cannot deploy on demand if we have error prone, manual steps requiring high levels of coordination.
* Reduce the number of handoffs. Reduce the number of manual steps.

KPIs include deployment frequency, and change failure rate.

*****

Here's my question to you: "How often are you deploying to master?"

If you've ever been in an organization that releases code only twice a year, you've probably seen how painful it can be to finally launch all that code.

Deploying code infrequently like so creates silos that make merge requests dangerous and releases difficult to coordinate.

This is The 2nd Constraint of DevOps, the issue of Deployment Frequency.

You'll know that you're facing this bottleneck if your code repo is filled with long lived feature branches, if resolving merge conflicts is the worst part of the job, or if your teams are only spending a fraction of their time actually building new features (because of rework).

Counter-intuitively, the solution to painful merge requests is to do it more often. This limits the scope of the changes and makes it easier to detect and resolve errors.

I'll say that again. Long lived feature branches are the reason why it's painful to resolve merge conflicts.

The solution is to use trunk-based, small batch, frequent deployments.

Remember this: "It's not continuous integration if there's no daily commits to master."

Tag a colleague who should see this in the comments.

#devops #gitlab #sixconstraints


*****

The Second Constraint that enterprises encounter in their DevOps journey is Deployment Frequency.

Organizations forfeit the advantages of DevOps when they deploy infrequently, or have releases that require error prone manual steps and high levels of coordination.

Tag a colleague who should see this in the comments.

#devops #gitlab #sixconstraints



## 3. Automated Testing 

Q:"How long does it take to test your deployments?"

Symptoms: Have a CI Pipeline and are now able to do more and be faster. But that's revealed the new bottleneck, like automated testing. There may even be some automated testing, but there's also some manual testing. And having that manual testing means that pipelines aren't finishing in hours, they're finishing in days.

Treatment: Automate and parallelize testing through your CI pipeline. We cannot deploy frequently if each deployment takes hours/days of testing, which means we aren't getting fast feedback. 

"The only way to make trunk-based developments safe is to have this effective automated test suite that we can rely upon so that we can get to our theoretical ideal where every time a developer checks code into trunk it can be safely deployed into production" - GK, BTPP

Proof Point: Paessler. The QA engineer’s tasks – about an hour a day in total – have been slashed to 30 seconds, a 120x speed increase. 

https://about.gitlab.com/customers/paessler/

**** 

Had a conversation with a Principal Software Engineer leading teams in the implementation of a DevOps and cloud strategy. 

He mentioned that they have a CI Pipeline and are now able to do more and be faster. 

But that's revealed the new bottleneck.

Because they still have manual testing processes in their pipeline, it means their builds are taking hours, if not days, to complete.

As with any team adopting DevOps, once you've enabled environments on-demand and are practicing continuous integration, the bottleneck becomes how long your deployments are taking.

Therefore, Automated Testing is The 3rd Constraint of DevOps.

We cannot deploy frequently if each deployment takes hours or days of testing. 

The solution is to automate and parallelize testing through your CI pipeline.

To quote Gene Kim in Beyond The Phoenix Project, "The only way to make trunk-based developments safe is to have this effective automated test suite that we can rely upon so that we can get to our theoretical ideal where every time a developer checks code into trunk it can be safely deployed into production."

A important concept in DevOps is that you want to be in an Always Deployable State.

Automated testing allows you to validate this, and provides you with fast feedback if a change has made you undeployable.


****

The Third Constraint that enterprises encounter in their DevOps journey is Automated Testing.

As Gary Gruver is quoted as saying in The DevOps Handbook, "Without automated testing, continuous integration is the fastest way to get a big pile of junk that never compiles or runs correctly."

We cannot deploy on-demand if each deployment requires manual testing, or time-consuming testing steps that hold up the process.

Tag a colleague in Testing who should see this in the comments.



## 4. Tightly Coupled Architecture

Q: "How many meetings does it take to push a change through?"


Symptom: Teams are spending more time in meetings than writing code. Because the overly tight architecture requires enormous amounts of coordination in order to not break. We cannot deploy on demand if the tight architecture means we need to get permissions or are afraid to make changes in case something breaks. CAB, Dependencies, Coordination.
* "Having to get permissions from 50 different people every time you want to make a small change."
* "My developers spend only 15% of their time coding. The rest is spent in meetings."


‘Complect’ means to turn something simple into something complex. “In tightly coupled and complected systems, it’s nearly impossible to change anything, because you can’t just change one area of the code, you must change one hundred, or even a thousand, areas of the code. And even the smallest changes can cause wildly unpredictable effects in distant parts of the system, maybe in something you’ve never even heard of. “Sensei Hickey would say, ‘Think of four strands of yarn that hang independently—that’s a simple system. Now take those same four strands of yarn and braid them together. Now you’ve complected them.’ Both configurations of yarn could fulfill the same engineering goal, but one is dramatically easier to change than the other. In the simple system, you can change one string independently without having to touch the others. Which is very good.” - GK, TUP


Treatment: Move to a microservices or loosely coupled architecture that allows us to be autonomous and deploy changes safely.

The TEP-LARB example in Unicorn Project is a perfect illustration of this.

*****

Are you having to get permissions from 50 different people every time you want to make a small change?

Are your developers spending most of their time in meetings instead of actually writing code?

Here's my question to you: "Is it hard to change your code?"

If you find yourself saying "Yes" to any of the questions above, you're now facing the 4th Constraint of DevOps - Tightly Coupled Architecture.

If you've ever had to sit before a TEP-LARB or CAB, then you know what I'm talking about.

Overly tight architecture means that any small change can cause something to break, so we often need to get permissions or work out dependencies in order to not break the system.

The way to move past this bottleneck is to enable low-risk releases.

We do this by decoupling deployments from releases, moving to a microservices architecture, and deploying the strangler application pattern where necessary to safely transition from legacy software.

In doing so, we move to a loosely-coupled architecture that enables developer productivity and code stability.

*****

The Fourth Constraint of DevOps is Tightly Coupled Architecture.

In The Unicorn Project, Gene Kim, speaking as Erik, quotes Rich Hickey to explain this "In tightly coupled and complected systems, it’s nearly impossible to change anything, because you can’t just change one area of the code, you must change one hundred, or even a thousand, areas of the code. And even the smallest changes can cause wildly unpredictable effects in distant parts of the system, maybe in something you’ve never even heard of."


Tag a colleague in Architecture who should see this in the comments.



## 5. Good Ideas

Q: "How many ideas are you testing?"

* How often are we testing new ideas? 
* How are we finding and funding new ideas to be able to test?
* How many ideas are we testing at any given time?

Symptoms: 
* You have environments on demand, automated testing, and are doing continuous deployments. 
* Not being innovative. Not coming up with new products.

Ronny Kohavi Study - Only one out of three ideas create value. One out of three is neutral, and one out of three is detractive.

Treatment: 
* Optimize for speed instead of cost. Smaller, autonomous teams.
* Fund Products and Services, not Projects.
* Test more ideas. Make more small bets.

Silver Bullet: 
* Teams are limited in their ability to innovate and create value if they are in siloes or are in a disconnected value stream. Remove those siloes, and enable innovation for-all-by-all, through a single application.

Key Idea: You can't have good ideas in a bureaucratic environment. That's really what this comes down to. Give your teams autonomy and resources and this will open up creativity to generate returns.


*****

Once you've built out a robust DevOps pipeline, with environments on-demand, continuous integration, automated testing, and a low-risk architecture, you may find yourself at the next bottleneck.

The 5th Constraint of DevOps is "How many good ideas can you test?"

It's entirely possible that you may feel that your organization is not being innovative enough or is not coming up with new products.

After all, despite $3.9B having been spent on DevOps software, almost 90% of the organizations that have adopted DevOps are disappointed with the results.

One explanation can be found in a study by Ronny Kohavi, who tells us that only 1 out of 3 ideas create value, 1 out of 3 is neutral, and 1 out of 3 is negative.

The way to forward is to test more ideas

We do this by optimizing for speed instead of cost, by having smaller autonomous teams, and by funding products and services (instead of projects).

The questions to keep in mind are: 

* How often are we testing new ideas? 
* How many ideas are we testing at any given time?
* How are we finding and funding new ideas to be able to test?

One last thing.

Teams are limited in their ability to innovate and create value if they are in siloes. Having a DevOps platform in a single application solves this.

#devops #gitlab #sixconstraints

*****

The Fifth Constraint of DevOps is Good Ideas.

A study by Ronny Kohavi, who tells us that 1 out of 3 ideas create value, 1 out of 3 is neutral, and 1 out of 3 is negative.

With this in mind, the way forward is to test more ideas by optimizing for speed, having smaller autonomous teams, and funding products and services.

Still, teams are limited in their ability to innovate and create value if they are in siloes. Having a DevOps platform in a single application solves this.

Tag a colleague who should see this in the comments.

#devops #gitlab #sixconstraints


## 6. Disconnected Value Stream


Q: How complicated is your CI/CD pipeline?

"Disconnected Software Value Streams are the bottlenecks to software productivity at scale."

At a recent company dinner, a Head of Risk at one of the big banks mentioned she had a team of 25 managing their toolchain. A GitLab customer from a FinServ enterprise responded that they had 1 person spending 25% of their time maintaining their GitLab for over 1500 users. The Big Bank had your typical disconnected, stitched together toolchain.

Symptoms:
* Context switching, multi-tasking.
* Manually entering information across tools
* Hard to support and maintain toolchain
* Brittle integrations often break
* Updating tools causes integrations to break, which means tools are updated less frequency
* Can't get accurate metrics because you're having to stitch together data from different tools
* "Are your teams spending a lot of time on maintenance instead of doing value added work?"

Treatment:
* Connect the value stream, make the details visible and accessible in one place. Have a dedicated team to only managing the toolchain.
* Magic Bullet - Use an already connected toolchain like GitLab. Consolidate your toolchain and rationalize your applications.

*****

Last question for you.

How complicated is your CI/CD pipeline?

You see, having a complicated pipeline turns out to be the Sixth DevOps Constraint.

From Mik Kersten's "Project to Product- "Disconnected Software Value Streams are the bottlenecks to software productivity at scale."

To give you an example, at a company dinner a year ago, a Head of Risk at one Big Bank mentioned she had a team of 25 managing their toolchain. 

A GitLab customer from a FinServ enterprise responded that they had 1 person spending 25% of his time maintaining their GitLab instance for over 1500 users. 

The Big Bank had your typical stitched together toolchain, and this complicated pipeline was the bottleneck to productivity
at scale.

The Symptoms of having a Disconnected Value Stream include:
* Context switching, multi-tasking.
* Entering information manually across tools
* Hard to support and maintain toolchain
* Brittle integrations often break
* No accurate metrics because data is in different tools

The solution is to connect the value stream, make the details visible and accessible in one place. 

You can do this by having a dedicated team managing your tools and integrations.

...Or you could use a DevOps platform delivered as a single application like GitLab.

#devops #gitlab #sixconstraints

****

The Sixth Constraint of DevOps is Disconnected Value Streams.

A complicated CI/CD pipeline is the bottleneck to software productivity at scale.

You can solve this by connect the value stream, making the details visible and accessible in one place. 

But in a complex enterprise environment with countless tools, this may require having a dedicated team managing the toolchain.

The Silver Bullet? 

Use a complete, end-to-end solution like GitLab.

#devops #gitlab #sixconstraints
