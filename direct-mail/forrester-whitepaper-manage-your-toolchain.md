# Forrester Roadshow Whitepaper: Hidden Costs of DevOps Toolchain 

## General

Dear {{first_name}}

I hope you are well. Attached please find a Forrester Research whitepaper on "Managing Your Toolchain." I believe you may find this relevant to your DevOps initiatives.

Organizations worldwide have spent over $3.9B on DevOps software. Why is it that despite this enormous investment, 87% of the organizations which have implemented DevOps practices report that they are disappointed in the results?

As a quick preview, here are some of the highlights:

* Over 61% of organizations use at least 2 disparate software delivery toolchains. 
* 56% reported that their toolchain is comprised of 6 or more tools
* Many estimate managing tool chains consumes 10% of theri development team
* 50% of DevOps time is wasted on logistics & repeatable tasks
* 4 out of 10 professionals noted that they struggle with insufficient expertise, skills, and resources to **maintain and integrate** the tools

In short, toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised. Teams suffer from the resource load of integrating, and administering multiple tools. Secondly, they pay the tax of context switching between the tools. Finally, the organization's speed is reduced because of the time required and cost to train users on a complex toolchain.

Are these issues of toolchain complexity and standardization relevant to your world, or worth a brief conversation? If so, I would like to suggest that we arrange a phone call. Otherwise, who in your organization might benefit from such a discussion?



Warm Regards,
Kevin McKinley

	


## Brief 1 Pager

Dear {{first_name}}

I hope you are well. Attached please find a Forrester Research whitepaper on "Managing Your Toolchain." I believe you may find this relevant to your DevOps initiatives.

Organizations worldwide have spent over $3.9B on DevOps software. Why is it that despite this enormous investment, 87% of the organizations which have implemented DevOps practices report that they are disappointed in the results?

As a quick preview, here are some of the highlights:

* Over 61% of organizations use at least 2 disparate software delivery toolchains. 
* 56% reported that their toolchain is comprised of 6 or more tools
* Many estimate managing tool chains consumes 10% of their development team
* 50% of DevOps time is wasted on logistics & repeatable tasks
* 4 out of 10 professionals noted that they struggle with insufficient expertise, skills, and resources to **maintain and integrate** the tools

In short, toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised. Teams suffer from the costs of acquiring, integrating, and administering multiple tools. They pay the tax of context switching between the tools. Finally, the organization's speed is reduced because of the time and cost to train users on a complex toolchain.

Are these issues of toolchain complexity and standardization relevant to your world, or worth a brief conversation? If not, who in your organization might benefit from having such a conversation about the topics in this whitepaper?


Warm Regards,
Kevin McKinley

	

## Referral

Dear {{first_name}}

I hope this missive finds you well. Attached please find a Forrester Research whitepaper on "Managing Your Toolchain." I believe you may find this relevant to your DevOps initiatives.

Organizations worldwide have spent over $3.9B on DevOps software. Why is it that despite this enormous investment, 87% of the organizations which have implemented DevOps practices report that they are disappointed in the results?

As you’ll see that I've highlighted in the whitepaper, over 61% of organizations use at least 2 disparate software delivery toolchains. But that complexity begets more complexity. The more toolchains, the more tools per toolchain. Of those organizations, 56% reported that their toolchain is comprised of 6 or more tools. The more complex a toolchain, the more labor-intensive it becomes to maintain.

Couple this with the findings that a) over 50% of DevOps time is wasted on logistics & repeatable tasks, and b) the standard resource load of tool chain management is roughly 10%. A good question to ask is, "How much of our teams' time is eaten up by maintenance and firefighting?"

In short, toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised. Teams suffer from the resource load of integrating and administering multiple tools. Secondly, they pay the tax of context switching between the tools. Finally, the organization's speed is reduced because of the time required and cost to train users on a complex toolchain.

Are these issues of toolchain complexity and standardization relevant to your world, or worth a brief conversation? If so, I would like to suggest that we arrange a phone call. Otherwise, who in your organization might benefit from such a discussion?


Warm Regards,
Kevin McKinley

****

Writing in hopes of finding the appropriate person who handles DevOps. I also wrote to {{!Comment person x, person y, and person z}} in that pursuit. 

GitLab enables companies to release software up to 26x faster while reducing costs by as much as 30%. We do this via a comprehensive, single application for the entire Software Development Lifecycle, which increases visibility, reduces integration complexity, and eliminates security and quality bottlenecks. Companies like {{company}} can incremental steps to reduce their overhead by integrating with their existing toolchain to better automate workflow and speed cycle time. GitLab is used by over 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel.

If you are the appropriate person to speak with, what does your calendar look like for a brief discussion? If not, with whom should I speak?




## Joe Gatt - Metife

Joe Gatt - Senior Principal, Head of DevSecOps
MetLife
101 Metlife Way
Cary, NC 27513

Dear Joe,

I hope you are well. Attached please find the Forrester Research whitepaper on "Managing Your Toolchain." It’s my understanding from my conversation with Jon Gibson that you may find this relevant to your DevSecOps initiatives.

Organizations worldwide have spent over $3.9B on DevOps software. Why is it that despite this enormous investment, 87% of the organizations which have implemented DevOps practices report that they are disappointed in the results?

As you’ll see that I've highlighted in the whitepaper, over 61% of organizations use at least 2 disparate software delivery toolchains. But that complexity begets more complexity. The more toolchains, the more tools per toolchain. Of those organizations, 56% reported that their toolchain is comprised of 6 or more tools. The more complex a toolchain, the more labor-intensive it becomes to maintain.

Couple this with the findings that a) over 50% of DevOps time is wasted on logistics & repeatable tasks, and b) the standard resource load of tool chain management is roughly 10%. A good question to ask is, "Are my developers doing value-added work, or simply keeping the lights on?"

Toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised. Teams suffer from the costs of acquiring, integrating, and administering multiple tools. They pay the tax of context switching between the tools. Finally, the organization's speed is reduced because of the time and cost to train users on a complex toolchain.

Are these issues of toolchain complexity and standardization relevant to your world, or worth a brief conversation? If not, whom in your organization might benefit from having such a conversation about the topics in this whitepaper?

Warm Regards,
Kevin McKinley

****
