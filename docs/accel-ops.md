accel-ops.md

# Acceleration OPS Procedure

Steps:
1. Run Salesforce Target Account Report and download as CSV

2. Run DataFox Report for Net New Accounts and download as CSV

3. Filter through DiscoverOrg ListMatch within SFDC Plugin

4. Upload to SFDC under SAL/with appropriate Account Ownership

5. Run ListMatch for ALL Accounts (SFDC Target Account Report + DataFox NetNew), export all employees to CSV for Digital


On Master Spreadsheet


Regional SDRs , could you kindly list the accounts you are working and do NOT want us to touch by marking them "Yes"? Please coordinate with your respective SALs to avoid rework and duplication. Ideally, you would exclude both 1) the accounts you are actively prospecting into, and 2) the accounts where the are ongoing conversations. 

Finally, there was originally over 500 accounts on this list (252 for Drumtra, 323 for Camillo). We removed existing customers and accounts that seemed to be incorrect. However, if you feel that there is an account that is missing which should be included in this list, kindly let me know. Additionally, if there are accounts which show up but should be excluded, kindly note that as well. We try hard to keep this as accurate as possible, but between the multiple tools and list uploads, something may get lost.


SALs , could you both kindly take a look at this account list and mark "Yes" as protected any accounts that you do NOT want the Accel team to prospect into for the month of Dec? Both your SDRs have been kind enough to get this started, so just wanted your eyes on it before we finalize.
