# Objection Handling

Thanks Jim, I appreciate your quick response.

Not a problem, I'll be sure not to send you any more solicitations. Hopefully I didn't overstep my bounds here.

I've been getting that response a lot lately, and I get the feeling that maybe its the way I'm explaining it.

Do you mind sharing with me what made you think my message wasn't relevant or of interest to you?


****

## Objection Handling - https://www.youtube.com/watch?v=HXBy67dHsf4

> The only objection you can't counter is No Response.

95/5 Rule - 95% of Objections are not real, 5% of the time they are real

* Not the Right time
* Not the Right Person
* Not Relevant
* No Budget
* Happy with Current Solution
* Send me an email

#### 6 Bedrock Principles

1. Avoid Child Ego State - Never justify, explain, or defend
2. Pattern Interrupt
3. Elephant in the Room
4. Fall on Your Sword
5. Mentee Chair
6. Self-Deprecation

### Call Objections 

#### Shallow Objection Counter
* Not Interested
* Running into a meeting
* In a meeting and I took this call because I thought you were someone else
* Send me an email

|Pattern Interrupt  	|Elephant in the Room 	
|Empathy  	|Up-Front Contract 	

**Example**
> *Can I level with you? I've been hearing "{{objection}}" from a lot of people recently. And I'm getting the feeling that's your polite way of saying, "Go away, you annoying sales rep." Did I get that right? 
> {{pause-for-response}} 
> I completely understood, I often do the same thing. How about this. Would you be willing to give me 30 seconds to explain what GitLab is and why I thought you might find it beneficial, and at the end of that 30 seconds you can tell me whether you want to continue the conversation. Fair?*

#### I Already have a Solution 

1. I already have a solution and I'm Happy with it. 
	1. "Glad that you already have everything covered with them. I guess it's probably not the right time. Before I leave, one more question, If there was one thing they could do better, what would it be?"
2. I already have a solution. (No Information)
	2. "How has your experience with them been?"
3. I already have a solution, and I'm good friends with (political tie)
	3. "I completely understand. No matter what anyone says, relationships are important. But I have to ask, isn't enabling your team to hit their numbers more important than politics?"
4. Pain point
	4. Pain funnel

#### Not the Right Person

Right person
> *Can I level with you? I've been hearing "I'm not the right person" a lot recently. And I get the feeling that it's just the way I'm explaining this to you. If even half of my research is correct, I think you are the right person. How about this. You give me 30 seconds to explain what GitLab is and why I thought you might find it beneficial, and at the end of that 30 seconds you can tell me whether you want to continue the conversation. Fair?*

Don't know who is, and they're not
> *No worries. I'll do a bit of digging and see who might be more relevant.*

Nothing Else
> *Any advice on who might be the best person for that?*

Give you a name
> Great. Thank you for the name. Any advice?

**Close**
Thank you for the direction. I'll let you know if I'm able to set up some time. Wish me luck!

### Countering via Email

Objection:
* Not interested
* Not the right time
* No thank you

#### Not Interested

**Call 3 Times:**
> *Hi Shawn, I'm the annoying person who keeps sending you emails, and I just got your response where you said you're not interested.
> *Can I level with you? I've been hearing "I'm not interested" a lot recently. And I'm getting the feeling that's your polite way of saying, "Go away, you annoying sales rep" because of the way I'm explaining it. Did I get that right? 
> {{pause-for-response}} 
> I completely understood, I'm sure you get a lot of emails, and I want to be respectful of your time. How about this. You give me 30 seconds to explain what GitLab is and why I thought you might find it beneficial, and at the end of that 30 seconds you can tell me whether you want to continue the conversation. Fair?*

**If No Answer (on 3 calls):**

1. Follow Up Action
2. Fall on Your Sword
3. Value Add Without Agenda
4. Prospect Fandom
5. Hard Walk Out Principle

> No worries, I won't send you any more messages. I wanted to make sure I didn't overstep my bounds here. Given your {{personalization}}, I thought you might like {{value-add}}. I'm so excited about {{prospect_fandom}}. Let me know if I can be of any support to you or your team down the line.

#### I Already Have a Solution

1. I already have a solution and I'm Happy with it. 
	1. "Glad that you already have it covered. Sounds like now's not the right time. Before I leave, one more question, If there was one thing they could do better, what would it be?"
2. I already have a solution. (No Information)
	2. "How has your experience with them been so far?"
3. I already have a solution, and I'm good friends with (political tie)
	3. "I completely understand. No matter what anyone says, relationships are important. But I have to ask, isn't enabling your team to hit their numbers more important than politics?"
4. Pain point
	4. "I'm sorry to hear that. Might be awkward over email, would you be willing to discuss over a brief call?"

#### Not The Right Person

1. Bond yourself to Person A
2. A Reference + 
3. Accountable Self Deprecation + Custom Pitch
4. Accountable Self-Deprecation + Mentee Chat (Go back to Referring Prospect)
5. Fall On Your Sword + Value + Hard Walk Out
6. Fall On Your Sword with Reference + Hard Walk Out

#### Unsubscribe

1. Respond immediately
2. "No problem, I'll be sure not to send you any more messages."
3. Pattern Interrupt
4. Accountable Self-Deprecation
5. "Do you mind telling me what made you think this was a boilerplate template?"