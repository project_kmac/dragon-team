// Phantombuster configuration {

"phantombuster command: nodejs"
"phantombuster package: 4"
"phantombuster flags: save-folder" // Save all files at the end of the script

const Buster = require("phantombuster")
const buster = new Buster()

const Nick = require("nickjs")
const nick = new Nick()

// }

const scrape = (arg, done) => {
  var data = $("body > div.container-fluid.mt-3 > hr").map(function () {
    return ({
      hr: $(this).find("body > div.container-fluid.mt-3 > hr").text().trim(),
    })
  })
  done(null, $.makeArray(data))
}


const scrapePage = async (tab, url) => {
  await tab.open("https://version.gitlab.com/current_host_stats?utf8=%E2%9C%93&search_type=Host+URL&search=wirecard&sort=url&direction=desc&button=")
  await tab.waitUntilVisible(".body > div.container-fluid.mt-3 > table")
  await tab.inject("../injectables/jquery-3.0.0.min.js")
  await page.screenshot({path: 'screenshot.png'})
  await page.setCookie({ name: "_version-gitlab-com_session", value: "a1Y4ZXduYWJiaXFNVTgwT0svcXA0ZzA4T0RNUXFDWUx5aitMb0lYMHRySHpadmV4YzErVzNheGdvUkZDeWgwYkNTSVBYYW9kODkxMW9wNHZqeS9jUVZDZndXb2pyTjhpUVpYaW44TVVabGgvL3h1d3luY0llUjVwdVFScVpZR2xJbm9ReUtVaGhNMmR0azVJa2VaazBpemw3YTFubGdxQW9qV0VCOTFNZk1heDRKN2RZVVpUOURSWFRjaFBqdUNTcld5OGVxdGRKQnRwdnkwNHlhZ1cxNUh3WkJzdkpLbXllTFkwdm9rMWpTUWhOZGxjSHQ4ME5ZOWQ4Uy92QTJLcWRxaVNIMmFKWVo1NVVoUjJBTHdYZ0FPRHlzSXF4eDlIUXlxcGNFaENyemUxdFE1Yk50aU5oeVIzdy9iazFXcWgtLTdNL2UremtrS2hqSWt0R09mNXhQbUE9PQ", domain: "version.gitlab.com" })
  const result = await tab.evaluate(scrape)
  return data
}




/* Notes Section
_version-gitlab-com_session=a1Y4ZXduYWJiaXFNVTgwT0svcXA0ZzA4T0RNUXFDWUx5aitMb0lYMHRySHpadmV4YzErVzNheGdvUkZDeWgwYkNTSVBYYW9kODkxMW9wNHZqeS9jUVZDZndXb2pyTjhpUVpYaW44TVVabGgvL3h1d3luY0llUjVwdVFScVpZR2xJbm9ReUtVaGhNMmR0azVJa2VaazBpemw3YTFubGdxQW9qV0VCOTFNZk1heDRKN2RZVVpUOURSWFRjaFBqdUNTcld5OGVxdGRKQnRwdnkwNHlhZ1cxNUh3WkJzdkpLbXllTFkwdm9rMWpTUWhOZGxjSHQ4ME5ZOWQ4Uy92QTJLcWRxaVNIMmFKWVo1NVVoUjJBTHdYZ0FPRHlzSXF4eDlIUXlxcGNFaENyemUxdFE1Yk50aU5oeVIzdy9iazFXcWgtLTdNL2UremtrS2hqSWt0R09mNXhQbUE9PQ%3D%3D--623fdf07fbdfe122ecd84bb3f337c37c462df94e

body > div.container-fluid.mt-3 > table


//Total Hosts Found
document.querySelector("body > div.container-fluid.mt-3 > hr")


*/