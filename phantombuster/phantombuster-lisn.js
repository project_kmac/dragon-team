'phantombuster command: nodejs';
'phantombuster package: 5';
"phantombuster dependencies: lib-StoreUtilities.js, lib-LinkedIn-pptr.js"

const { parse, URL } = require("url")

const Buster = require("phantombuster")
const buster = new Buster()
const puppeteer = require("puppeteer")
const StoreUtilities = require("./lib-StoreUtilities")
const utils = new StoreUtilities(buster)
const LinkedIn = require("./lib-LinkedIn-pptr")
const linkedIn = new LinkedIn(buster, utils)

let disconnected = false


// Scrape
/*const lisn_boolean_num = (arg, callback) => {
	const data = []
	const total = document.querySelector('.artdeco-spotlight-tab__primary-text').innerText
	callback(null, total
	)
}
*/


// Get 


const scrapeLists = (arg, cb) => {
	const results = document.querySelectorAll("artdeco-spotlight-tab__primary-text")
	const scrapedData = []
	let profilesScraped = 0
	for (const result of results) {
		if (result.querySelector("#search-spotlight-tab-ALL > span.artdeco-spotlight-tab__primary-text")) {
			const lisn_boolean_num = result.querySelector("#search-spotlight-tab-ALL > span.artdeco-spotlight-tab__primary-text").textContent()
			const newData = { lisn_boolean_num }
			const urlObject = new URL(lisn_boolean_num)
			const vmid = urlObject.pathname.slice(14, urlObject.pathname.indexOf(","))
			if (vmid) {
				newData.vmid = vmid
			}
			newData.timestamp = (new Date()).toISOString()
			profilesScraped++
			scrapedData.push(newData)
		}
		if (profilesScraped >= arg.numberOnThisPage) { break }
	}
	cb(null, scrapedData)
	console.log(scrapedData)
}

// Execute
;(async () => {
    const { page } = await utils.launchPuppeteer(puppeteer)
    const args = buster.arguments
	let { sessionCookie, searches, columnName, csvName,  } = utils.validateArguments()
//    const sessionCookie = args.sessionCookie
    await linkedIn.login(page, sessionCookie)


	if (!csvName) { csvName = "result" }
	let result = await utils.getDb(csvName + ".csv")
	let currentResult = []

    await page.goto("https://www.linkedin.com/sales/search/people?companyIncluded=Kimberly-Clark%3A3439&companyTimeScope=CURRENT&doFetchHeroCard=false&geoIncluded=us%3A0&keywords=%22kubernetes%22%20OR%20%22microservices%22%20OR%20%22devops%22%20OR%20%22CI%2FCD%22%20or%20%22GitLab%22%20OR%20%22Atlassian%22%20OR%20%22Azure%22%20OR%20%22Enterprise%20Architecture%22%20OR%20%22Docker%22%20OR%20%22SAST%22%20OR%20%22DAST%22&logHistory=true&logId=8486255533&page=1&rsLogId=123596457&searchSessionId=OMy6HrWgQbmFmgN8bG5xww%3D%3D&")
	await page.waitForSelector(".search-spotlights__tablist")

/*
	const result = await page.evaluate(() => {
 	  let lisn_boolean_num = document.querySelector('.artdeco-spotlight-tab__primary-text').textContent.trim()
    return {
	    lisn_boolean_num
	  }
*/

    await buster.saveBase64(await page.screenshot({ type: "jpeg", fullPage: true, encoding: "base64" }), "login.jpg")
  	console.log(result)

	await utils.saveResults(currentResult, result, csvName)

    process.exit()
})()


.catch((err) => {
    console.log(err)
    process.exit(1)
})