// Phantombuster configuration {

"phantombuster command: nodejs"
"phantombuster package: 4"
"phantombuster flags: save-folder" // Save all files at the end of the script

const Buster = require("phantombuster")
const buster = new Buster()

const Nick = require("nickjs")
const nick = new Nick()
// }

const scrape = (arg, done) => {
	var data = $("div.person > div.panel-body").map(function () {
		return ({
			name: $(this).find(".name").text().trim(),
			birth_year: $(this).find("birth_year").text().trim(),
			death_year: $(this).find("death_year").text().trim(),
			gender: $(this).find("gender").text().trim(),
			marital_status: $(this).find("marital_status").text().trim(),
			spouse: $(this).find("spouse").text().trim(),
			pclass: $(this).find("pclass").text().trim(),
			ticket_num: $(this).find("ticket_num").text().trim(),
			ticket_fare: $(this).find("ticket_fare").text().trim(),
			residence: $(this).find("residence").text().trim(),
			job: $(this).find("job").text().trim(),
			companions_count: $(this).find("companions_count").text().trim(),
			cabin: $(this).find("cabin").text().trim(),
			first_embarked_place: $(this).find("first_embarked_place").text().trim(),
			destination: $(this).find("destination").text().trim(),
			died_in_titanic: $(this).find("died_in_titanic").text().trim(),
			body_recovered: $(this).find("body_recovered").text().trim(),
			rescue_boat_num: $(this).find("rescue_boat_num").text().trim(),
		})
	})
	done(null, $.makeArray(data))
}

; (async () => {
	// create a new tab in your browser
	const tab = await nick.newTab()
	await tab.open("http://scraping-challenges.phantombuster.com/onepage")
	await tab.waitUntilVisible(".panel-body")
	await tab.inject("../injectables/jquery-3.0.0.min.js")
	const result = await tab.evaluate(scrape)
	await tab.screenshot("screenshot.jpg")
	await buster.setResultObject(result)
	nick.exit()
})()

//all scraping code above
.then (() => {
	console.log("Job done!")
	nick.exit()
})
.catch ((err) => {
	console.log('Something went wrong : ${err}')
	// Exit the program with errors
	nick.exit(1)
})


/*
// this is the code I came up with that didn't work. yeah, there was no way I was going to figure all the above out on my own
nick.newTab().then(async (tab) =>{
	  await tab.open("http://scraping-challenges.phantombuster.com/onepage")
	  await tab.waitUntilVisible("div.person")
	  await tab.inject("../injectables/jquery-3.0.0.min.js")

const allInOnePage = await tab.evaluate((arg, callback)) => {
	const data = []
	$("div.panel-body").each((index, element) => {
		data.push({
			name:$(element).find("name.property-value").text()
		})
	})
}
callback(null, data)


await buster.setResultObject(allInOnePage)
await tab.screenshot("allInOnePage.jpg")

//all scraping code above
})

.then (() => {
	console.log("Job done!")
	nick.exit()
})
.catch ((err) => {
	console.log('Something went wrong : ${err}')
	nick.exit(1)
})

*/