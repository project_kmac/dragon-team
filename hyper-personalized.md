# Hyper Personalized

We exist to help enterprise companies accelerate DevOps adoption. My job is to arm you with knowledge to help combat the “Three F’s” that are so prevalent in IT organizations today: Friction, Frustration, and Fragmented tools and processes.

We talk to IT leaders every day about how to tackle the Three F’s in an effort to help you: 

1. Increase developer productivity and velocity
2. Disrupt the competition
3. Attract and retain top IT talent
4. Increase market share

*****

Hello David,

The reason I'm reaching out is because I had a nice conversation with Terry Parrish, who thought it might be beneficial we speak. I promised him I'd reach out to you. More importantly, 


****

Hello Sathish,

The reason I'm reaching out to you today is because I saw your post on LinkedIn where you shared the Forbes article about work-life integration. Couldn't agree more when you said, "achieving "Integration" will surely make professionals more effective in their careers."

Correct me if I'm wrong, but when it comes to Enterprise Architecture, having to achieving integration is key to making your deployment pipeline more effective. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool, already integrated for you. Wouldn't this allow your teams to be more effective and focus less on managing the infrastructure and maintaining integrations?



****
Hello Baldemar,

The reason I'm reaching out to you today is because I saw your post on LinkedIn about your CHTP and wanted to congratulate you on closing the circle. But more importantly, you specifically mentioned that you work closely with the "business units providing IT leadership to help them achieving their goals." This is why I thought it beneficial that we speak.

Correct me if I'm wrong, but when it comes to closing the circle and achieving business goals, creating a fast feedback loop is key. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow your teams to achieve business goals faster? This is the main value GitLab offers to you by enabling your teams to work out of a single application for software development.

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?


With your success in mind,
{{sender.first_name}}



Hello Baldemar,

The reason I'm reaching out to you today is because I saw your post on LinkedIn about your CHTP and wanted to congratulate you on achieving that goal. But more importantly, you specifically mentioned that you're "responsible for the technical strategy, direction, and management of IT operations." This is why I thought it beneficial that we speak.

When it comes to IT Operations, the main value GitLab offers to you is by enabling your teams to work out of a single application. Imagine having an out-of the-box git repo, CI/CD, project management, and more, all in one tool. Wouldn't this allow your teams to focus less on managing the infrastructure and maintaining integrations?

Looking to schedule some time with you to discuss and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
{{sender.first_name}}

****

Hello Phil,

The reason I'm reaching out to you is that I was reading your LinkedIn post about guest lecturing at University of Toronto about Business Process Architecture. I couldn't agree more with your comment that, "I firmly believe that our legacy is about those that we teach and that sharing our experience, knowledge and ideas with those eager to learn is how progress happens."

When it comes to effecting business change by sharing knowledge, this is one of the key principles of DevOps and why I thought it beneficial we speak. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow your teams to share knowlege and make progress happen faster? This is the main value GitLab offers to you by enabling your teams to work out of a single application for software development. 

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin


****

Hello Sandesh,

The reason I'm writing to you is that I was reading in your LinkedIn profile where you highlighted DevSecOps and CI/CD. One line that really stood out to me was that you're leading the "transformation journey of converting a monolithic application into a cloud-native microservices based architecture", which is why I thought it'd be important we speak.

When it comes to DevSecOps and moving to a microservices architecture, the main value GitLab offers to you is by enabling your teams to work out of a single application. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow them to focus less managing the infrastructure, instead spending more value added time on converting the monolithic application into cloud native?

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****
Hello Amitabh,

The reason I'm writing to you is that you mentioned on your LinkedIn profile that you're leading the data engineering efforts for Mars. You specifically mentioned that you're moving "both internal and external data sources into the Mars Data Lake", which is why I thought it'd be important we speak.

When it comes to creating a data lake, the main value GitLab offers to you is by enabling the data infrastructure to be scalable and continously integrated (CI). Your teams can run automated testing and have an effective CI pipeline for their data warehouse. If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****

Hello Ryan,

The reason I'm reaching out is because I had a nice conversation with Chuck Phipps, who thought it might be beneficial we speak. I promised him I'd reach out to you to. But more importantly, I saw on your LinkedIn profile that Vulnerability Scanning and Web Application Security are among your interests.

When it comes to app sec and vulnerability scanning, the main value GitLab offers to you is enabling developer teams to be able to identify vulnerabilities and remediate them immediately. Unlike traditional application security tools which are typically periodic scans, GitLab scans for vulnerabilities during every code commit and displays the results in the CI/CD workflow where the developers live.

Hoping to schedule some time with you to discuss your security initiatives and share why I thought you might find GitLab useful. Would you be open next Thursday at 2 PM?

With your success in mind,
Kevin

P.S. If now's not a good time, I thought you mights still find some value in this slide deck I put together for you: https://ptdrv.linkedin.com/4rv0uvj

*****
Hello Jason,

The reason I'm writing to you is that you mentioned on your LinkedIn profile that you're leading the data science team that owns the marketing analytics strategy. You specifically mentioned that you're "Platforming all data onto the Google Cloud Platform", which is why I thought it'd be important we speak.

When it comes to creating a holistic data architecture approach, the main value GitLab offers to you is by enabling the data warehouse infrastructure to be scalable and continously integrated (CI). If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****
Donald Nolan @ ALG

Hello Donald,

The reason I'm writing to you is that you shared an article on your LinkedIn about using data analytics to improve project outcomes. One line that really resonated with me was that, "Data and analytics will become infused in an organization's architecture from end to end, creating a holistic approach." But more importantly, because of your role in Business Intelligence Development, I thought it'd be important we speak.

Correct me if I'm wrong, but my understanding is that ALG is currently moving all data onto the Google Cloud Platform. When it comes to creating a holistic data architecture approach, the main value GitLab offers to you is by enabling the data warehouse infrastructure to be scalable and continously integrated (CI). If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin


****
Wendy Hoeckwater @ Apple Leisure Group

Hello Wendy,

Thank you for your response. The reason for my outreach is that your quote about "aLGV360 distilling hours of work into seconds" is exactly what we do for marketing analytics.

The main value GitLab offers to you is by enabling the architecture that gives Marketing the ability to create data driven promotions. If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Based on my research, the person that would benefit the most from having this conversation is Jason West, your Director of Performance Marketing who leads data science team that owns the marketing analytics strategy and is currently platforming all of your data into Google Cloud Platform.

I'm hoping you could connect me with Jason to schedule some time to discuss your Data initiatives and share why you might find GitLab useful.


With your success in mind,
Kevin


*****

Jim @ Pella


Hello Jim,

The reason I'm reaching out to you is because I saw your interview on theCube where you talk about your approach to strategic partnerships, risk management, and your test dev environment. One comment that really stood out to me was,"Our challenges continuing to go forward is 'How do we consolidate and pull?'"

What if you could choose a strategic partner for your software development pipeline that allows you to consolidate across tools? Wouldn't this enable scalability, flexibility and long-term economics in your pipeline similar to what you've achieved with Dragonhawk? This is what GitLab can do for you as a platform that includes git repo, CI/CD, application security, and more.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Matt

*****

Matthew Dunlop, Ph.D. , CISO @ UnderAmour

Hello {{first_name}},

Writing to you because I listened to your appearance on the CyberWire Daily, and really resonated with your comments on cloud security, workplace hiring, and cyber hygeine. I couldn't agree more when you said, "At the end of the day, you really have to make sure you're doing the simple stuff right. If you're not doing the simple stuff, I don't care what tools you have in place."

What if you could make it really easy to do the simple stuff right? Especially when it comes to connected devices and application security. That's part of what GitLab does by scanning every line of code, checking it for vulnerabilities, and immediately returning a report to the developers of what needs remediation attention.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought the your initatives could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin


Reaching out because I was watching your Cybersecurity Ventures and was pleasantly surprised to hear you talk about SecDevOps. One statement that really stood out to me was you saying that, "It's about bringing security people onto your scrums and agile teams from the beginning."

What if, likewise, you enabled the developer teams to be able to identify vulnerabilities and remediate them at the beginning. One way to do this is by builing application security (like SAST, DAST, etc.) into the CI/CD workflow where the developers live.

Unlike traditional application security tools which typically scan at the end of the lifecycle, GitLab does this at the beginning, scanning for vulnerabilities during every code commit.

Hoping to schedule some time with you to discuss your SecDevOps initiatives and share why I thought you might find GitLab useful. Would you be open this upcoming Thursday at 2 PM?

With your success in mind,
{{sender.first_name}}


****

Ann Funai, SVP Engineering @ UnderArmour

Hello {{first_name}},

Writing to you because I saw the Partnering For Purpose panel, and I appreciate the insights you shared. One quote that really resonated was where you said,  "I went out and looked for interesting assignments. Those things that at the end of the day, I could come home and look myself in the mirror and that there's some purpose to it." 

Correct me if I'm wrong but the way I understand it, a core ideal of the DevOps philosophy is to enable Focus, Flow, and Joy so that work is interesting and purposeful. As part of GitLab, I work with enterprises to accelerate their DevOps adoption so that they can increase developer productivity and velocity, disrupt the competition, and attract and retain top IT talent. 

Hoping to schedule some time with you to discuss your initiatives and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
{{sender.first_name}}



***

Hello Brad,

Reaching out to you because I read your article on "Problems with 'Headcount'". Two of your sentiments that really resonated are "Using headcount as a proxy to control costs often creates political problems" and "Managers make lower-quality decisions with headcount vs. dollars."

In "Project to Product", Dr. Mik Kersten echoes this by warning us that, "The problem is that we are relying on proxy metrics for decision making rather than finding the metrics that directly correspond to busines outcomes."

What if there was a way to enable your engineering teams to use the metrics that directly correlate to the business outcomes? GitLab's Productivity Analytics provides graphs and reports to help engineering leaders understand team, project, and group productivity so they can uncover patterns and best practices to improve overall productivity. 

Would you be willing to give me a few minutes next Wednesday at 2 PM, to share with you exactly why I thought the MapMyRun related initatives could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin




Hoping to schedule some time with you to discuss your initiatives and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
Kevin

**First Ideal of Locality and Simplicity**
We need to design things so that we have locality in our systems and the organizations that build them. And we need simplicity in everything we do. The last place we want complexity is internally, whether it’s in our code, in our organization, or in our processes. The external world is complex enough, so it would be intolerable if we allow it in things we can actually control! We must make it easy to do our work.”

**The Second Ideal is Focus, Flow, and Joy.** It’s all about how our daily
work feels. Is our work marked by boredom and waiting for other people
to get things done on our behalf? Do we blindly work on small pieces of
the whole, only seeing the outcomes of our work during a deployment
when everything blows up, leading to firefighting, punishment, and burn-
out? Or do we work in small batches, ideally single-piece flow, getting fast
and continual feedback on our work? These are the conditions that allow
for focus and flow, challenge, learning, discovery, mastering our domain,
and even joy.”

The **Third Ideal is Improvement of Daily Work.** Reflect upon what the
Toyota Andon cord teaches us about how we must elevate improvement of
daily work over daily work itself.

The **Fourth Ideal is Psychological Safety,**
where we make it safe to talk about problems, because solving problems
requires prevention, which requires honesty, and honesty requires the
absence of fear. In manufacturing, psychological safety is just as important
as physical safety. 

And finally, the **Fifth Ideal is Customer Focus,** where we
ruthlessly question whether something actually matters to our customers,
as in, are they willing to pay us for it or is it only of value to our functional
silo?"