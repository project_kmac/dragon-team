# Value Streams 

## readme

Key Outcomes:
Discover top prospects for each “value stream”
Kick off value streams / top of funnel campaign messaging


**Key question to answer: How do we structure and organize our outbound content to make it simple to deliver the appropriate content to the right audience?**


Matt’s Idea: Value Streams
What is a value stream? An outbound flow or sequence of content delivered to prospects highlighting a specific value, use case, differentiator, or area of focus. Some examples include: the GitLab value drivers, GitLab use cases, competitive takeouts, industry-specific values, persona-based values, cultural differentiators, etc.


What types of value streams are there?
VITO
* C-Level
* Exec Buyers 
* VPs
* Biz Unit Leads
* Global Head
* Managing Director

Influencer
* Directors
* Architects
* DevOps Leads
* Engineering Managers

User + Groundswell
* Developers
* Engineers
* Admins


How are value streams and content & sequences organized in Outreach?
Value stream type - Topic / Area of Focus 
VITO - Deliver better products faster
VITO - IOE
VITO - Sec


How do we measure value streams while giving SDRs the ability to incorporate their individuality into messaging?
How many prospects / company / value stream?
What are our top value streams?
Simplest way to map personas / titles / keywords to value streams


****

Outreach Style Spreadsheet:
https://docs.google.com/spreadsheets/d/1nFVt3dv2_vd5Lk79eph7nx2SXSQ9DtegMqFLw9P_owI/edit#gid=0

**Persona Buckets**

* VITO 
* Influencer 
* User 
* Groundswell

**Value Streams**

VITO
* Single App
* Tool Consolidation
* Value Drivers
* Biz Outcomes/ROI
* Industry Leader Stories
* Attract + Retain Top Talent
* Revenue
* ROI / Biz Outcomes
* Tool consolidation
* Attracting & retaining top talent
* Value Drivers
* Performance improvements / Efficiency
* Total cost of ownership 
* Visionary -- Long term 
* Industry Lookalike / Customer Stories / Case Studies
* Investments 
* Best of breed technology
* Partnerships / Networks
* Stakeholder loyalty (Referrals)


Influencer
* Flexibility
* Toolchain Complexity
* Value Drivers 
* Use Cases
* Automation
* Collaboration
* Competitive Takeouts
* Champion Building
* Efficiency Improvement
* Innovative Technologies (AI/ML, Data Science)
* Value Drivers
* Total cost of ownership
* Partnerships / Network
* Industry Lookalike / Customer Stories / Case Studies
* Use Cases
* Competitive Takeouts
* Collaboration 
* Reduction of reactive tasks (Automation)
* Innovative Technologies (AI, ML)
* Reducing risk
* Appetite to move up the ladder (champions)
* Cloud Strategy
* Mobile/Web Development Teams

User + Groundswell
* Automation
* Time to focus on more strategic work
* Use Cases 
* Simplified Workflows

*****

## VITO

CTA for VITO is referral down.

### Single Application

Hello {{first_name}},

I hope this missive finds you and your family well during this difficult time.

The reason for my email today is because I was reading through your LinkedIn profile where you mention your role in software. 

In spending my time with software development teams, I've learned that often, the way their tool infrastructure is set up creates a bottleneck. If you've ever had to wait for an environment to be provisioned, for example, you've experienced this firsthand.

One way we've helped teams address this is to simplify their software delivery and remove this bottleneck. High performing teams are finding that using a single application speeds up their productivity.

If you'll meet with me next Wed at 1 PM, I'll share the approach we've used successfully with other enterprises. But if you feel that it's not a fit, I promise not to jam up your inbox like an annoying salesperson.

In any case, stay safe out there.

With your success in mind,
{{sender.first_name}}

### Tool Consolidation

Hello {{first_name}},

I hope this missive finds you and your family well during this difficult time.

The reason for my email today is because I was reading through your LinkedIn profile where you mention your leadership role in software.

Like many enterprises, you've probably seen the cambrian-like explosion of tools supporting software delivery over the last few years. If so, you might find that they've hit diminishing returns with adding more to their stack, and are therefore looking for ways to consolidate your tools.

As the ancient Nordic saying goes, why use two DevOps tools when you could just use Mjolnir?

If you'll meet with me next Wed at 1 PM, I'll share the approach that other organizations have used to successfully consolidate their complex toolchains, without diminishing any of their capabilities. If at the end you feel that it's not a fit, I promise not to jam up your inbox like an annoying salesperson.

In any case, stay safe out there.

With your success in mind,
{{sender.first_name}}

### Value Drivers - Delivery

Correct me if I'm wrong, but at the end of the day VITOs care about delivering software faster.

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software." George Grant


### Value Drivers - Efficiency

Correct me if I'm wrong, but at the end of the day VITOs care about increasing their operational efficiency.


### Value Drivers - Security

Correct me if I'm wrong, but at the end of the day VITOs care about reducing their security and compliance risk.

### Business Outcomes / ROI

Hello {{first_name}},

I was compelled to reach out because I saw that you {{!insert_personalization}}

Forgive me if I'm mistaken, at the end of the day, VITOs care about achieving business outcomes like digital transformation and increased productivity. What if there was a platform that enabled you to achieve those outcomes and gave back measurable ROI?

George Grant, a VP and Technology Fellow at Goldman Sachs had this to say...

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software.”

If you'll meet with me next Wed at 1 PM, we can discuss your initiatives and I can share how GitLab can help you achieve your strategic business outcomes.

With your success in mind,
{{sender.first_name}}


### Cost Control

Hello {{first_name}},

I hope this missive finds you and your family well during this difficult time.

The reason for my email today is because I was reading through your LinkedIn profile where you mention your leadership role in software.

Like many enterprises, your organization may be evaluating spending in light of the current economic environment. For example, many software teams have picked up costly and inefficient tooling, and so it may be wise to rationalize applications in order to control costs.

If you'll meet with me next Wed at 1 PM, I'll share an open-source approach that other organizations have used to succesfully reduce tooling spend without diminishing their capabilities. If at the end you feel that it's not a fit, I promise not to jam up your inbox like an annoying salesperson.

In any case, stay safe out there.

With your success in mind,
{{sender.first_name}}


### Industry Leader Stories 



### Attract + Retain Top Talent

subject: How are you attracting top talent?

Hello {{first_name}},

First off, I'm not a recruiter. But I was listening to a VP share some war stories, and he told me one I thought might resonate with you.

They were looking for a Sr Director of IT, had been recruiting this one candidate for four months, and finally the guy accepted an offer. All good, right?

The day he was slated to start, he gets in, starts his assessment, and within four hours had left the building. Why? Apparently the tooling they had was that bad.

Now, I wasn't privvy to the details and I heard this second-hand. What I do know for sure is that it's hard to attract top talent, and even harder to retain them. 

These days having the right DevOps tooling can make that difference. For enterprise, that means it has to be fast (while still being reliable), has modern features (while still being cost effective), and secure (without clunky bureaucratic process). That's what GitLab can offer you.

If you'll take the time to meet with me next Wed at 1 PM, I'll share with you why enterprises users are begging leadership to switch to GitLab... and why leadership is to the moon with the results.

*****

## Influencer

CTA is Direct Meeting + Referral 

### Flexibility

Hello {{first_name}},

I felt compelled to reach out after {{!insert-personalization}}

Correct me if I'm wrong, but often teams can feel restricted by the centralized tooling provided by their enterprise. The problem is that they don't have the buget, time, or resources to be able to manage their own platform.

What if there was an easy way to adopt DevOps and have a flexible, automated, end-to-end DevOps platform, without any of the resource load?

If you'll meet with me next Wed at 1 PM, I'll share an approach other teams have successfully used to adopt their own DevOps platform. But if it's not the right approach, I promise not to jam up your inbox like an annoying salesperson.


With your success in mind,
{{sender.first_name}}


### Toolchain Complexity

Hello {{first_name}},

I spend a lot of my time speaking with DevOps teams. Often they come talk to us when they're looking to move away from AzureDevOps/TFS or BitBucket/Atlassian because they want it all in once place. They're tired of toolchain complexity.

You know what a Sr Director of Infrastructure and Operations once said to me? He said, "I want my developers building pipelines, NOT integrating SAST tools with Azure."

So imagine if you could pull a repo, push to master, build pipelines, scan the code, and do this autonomously all the way to production... using only one tool.

That's what GitLab can do for you. It worked for that Sr Director. He's now the CTO.

If you'll take the time to meet with me next Wed at 1 PM, we can talk about your initiatives and how we might be able to help. And hey, if it's not a fit, no harm done - I'm not some pushy salesperson. 

Either way, give some thought to how you're handling toolchain complexity.

With your success in mind,
{{sender.first_name}}

--

As they created a new infrastructure, they kept four key components in mind: architecture, automation, extensibility, and being proactive and prepared for the future. They wanted to rebuild their data centers in less than 12 hours, instead of 30 days. They had a goal of 100 percent CI/CD. They wanted to remove manual deployments, especially around the server and network deployments. The team also focused on avoiding vendor lock-in by seeking open source tools to help them accomplish these goals.

"Some of the other features that we really loved, and we didn’t find with any other CI/CD tool, are the project management features," Mehdi says. "GitLab replaced a bunch of disparate systems for us like Jira, BitBucket, and Jenkins. GitLab provided us with a one-stop solution."

### Value Drivers 

### Use Cases

### Automation

Hello {{first_name}},

Was reaching out to you because you mentioned working on automation in your LinkedIn profile.

An enterprise architect told me that he was looking for a "Full Orchestration of DevOps Pipeline". He described this as getting code from repo, to automated build, with automated testing, and automated deployment. 

Am I just rambling here, or is this something that you're also attempting to create? If so, I can share an approach other architects have used to automate DevOps.

If you'll meet with me next Wed at 1 PM, we can talk about the ways to successfully automate your deployment pipeline. Maybe GitLab can help you get what you're looking for. 

Either way, hope you're hanging in there.

With your success in mind,
{{sender.first_name}}

### Collaboration

Hello {{first_name}},

Reaching out to you because {{!insert-personalization}}

Like many enterprises, yours probably has a multitude of tools in your software development life cycle. One challenge created by this, is that the amount of tools actually makes it harder to collaborate and creates silos in your org.

What if there was a way to easily foster collaboration across teams and reduce the silos in your organization?

With your success in mind,
{{sender.first_name}}

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software." George Grant


Rzesz on Silos: https://youtu.be/tKtrIRQ-VBo?t=181


### Competitive Takeouts

### Champion Building

Hello {{first_name}},

Are you looking to move up to a senior leadership role? For many {{title}}, the answer is yes.

One of my personal favourite success stories here at GitLab is about what happened to a Sr Director of Infrastructure and Operations at a Fortune 500 company.

They had invested heavily over the years in using Azure DevOps and JIRA. But they were finding that the high coordination required and dependencies between tools was a bottleneck.

He said, "I want my developers building pipelines, NOT integrating SAST tools with Azure."

In order to achieve their strategic goals, they needed to automate more of their deployment. For them, this meant being able push to master, build pipelines, scan the code, and run tests... automatically at the push of a button. 

So they switched to GitLab. In doing so, they were able to decommission much of their other tools (still working on getting rid of JIRA, unfortunately), get the automated deployments they wanted, and even implement things like canary deployments on Kubernetes.

Less than a year later, that Sr Director is now CTO of another subsidiary of that organization. True story.

If you'll take the time to meet with me next Wed at 1 PM, we can talk about your initiatives and how we might be able to help. Maybe GitLab can get you to where you want to go. And hey, if it's not a fit for your situation, at the very least we can talk about some best practices you might find valuable.

With your success in mind,
{{sender.first_name}}

### Efficiency Improvement

### Shared Services

subject: {{first_name}}, are you blocked by shared services?

Hello {{first_name}},

Is your team's progress blocked by a dependency on shared services?

I was recently speaking with a Director of Engineering. As is the case in many enterprises, they had your typical standard issue DevOps pipeline with tools like Jenkins, Docker, etc.

The problem in their scenario was that their tools were owned and maintained by the shared services team, which meant that they weren't able to scale on demand.

The question they came to us with is, "Instead of using what shared services provides, would it be more efficient for us to build our own platform? Does it make sense for us to look at building out what we need in our own instance?"

Dependencies like this introduces friction and adds lead-time and WIP. Ownership of your platform is important if you want to scale-as-needed or have self-service.

If you'll meet with me next Wed at 1 PM, we can talk through the pros and cons of setting up your own pipeline. With GitLab being a complete DevOps platform, it might just do the trick.

Hope all is well with you.

With your success in mind,
{{sender.first_name}}

### Starting DevOps




*****

## User + Groundswell

Oriented around Discovery, Intent, and Gathering Data. "How are you doing DevOps? What tools are you using today?" Confirmation of a problem we can solve so that next steps have a clear direction of the Influencers that need to get involved.

### Automation


Hello {{first_name}},

Reaching out to you because I was reading through your LinkedIn profile where you mentioned working on automation.

Correct me if I'm wrong but when it comes to automation, a big challenge comes from the need to integrate your many tools. What if there was an easy way to get a completely automated DevOps pipeline?

Here's a quick video demo of how to accomplish this on GitLab: https://www.youtube.com/watch?v=nMAgP4WIcno&feature=youtu.be

If you'd like to learn more, I'm hosting a webinar on how to automate DevOps pipelines the easy way. Simply reply back with a quick "Yes" or "No, I'm not interested" and I'll send you the details.

With your success in mind,
{{sender.first_name}}


### Time to focus on more strategic work
### Use Cases 
### Simplified Workflows

### Lack of Support

Hello {{first_name}},

Was reading across some DevOps forums online when I came across this gem:

> I've been very frustrated with the way my company and team(s) sees DevOps/SRE. Doing anything the "right way" is shot down for reasons ranging from red tape to "because we said so". I've pushed very, very hard to change things to the best of my ability for as long as I've worked here, and in some ways things have improved, but only marginally so. I just do not have any buy in from the teams I work with or anyone in management/leadership positions as much as they love to talk about us "doing devops" because we have a decent CI/CD flow and a few other things. I've basically thrown my hands up and transitioned back to a standard SWE position.

Is this something you've come across in your organization? Sometimes, shared services can become the bottleneck stunting the dependent teams' abilities to scale and grow as needed.

If you'll meet with me next Wed at 1 PM, we can talk about the ways to address this issue. We've worked with change agents who wanted to kickstart a DevOps revolution get the buy-in and the $$$ they needed by proving concrete ROI. Maybe GitLab can help you get what you're looking for. 

With your success in mind,
{{sender.first_name}}
