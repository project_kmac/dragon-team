# Forrester Roadshow - Avoiding the Hidden Costs of DevOps Toolchains

w/ Special Guest, Principal Analyst Chris Condo

* Issue - https://gitlab.com/gitlab-com/marketing/field-marketing/issues/446
* Landing Page - https://page.gitlab.com/roadshow-dc.html
* SDR Resources - https://gitlab.com/gitlab-com/marketing/sdr/issues/132
* Webcast - https://www.youtube.com/watch?v=ynhmDjGA88M
* Whitepaper - https://about.gitlab.com/resources/downloads/201906-gitlab-forrester-toolchain.pdf



## Email 1

subject: meet at Convene on Tysons - Oct 17th?

{{first_name}}, please accept my apology for the intrusion. I thought you might enjoy our complimentary workshop and fireside panel on "Avoiding the Hidden Costs of DevOps Toolchains"

We're hosting a roadshow bringing leading IT executives together to discuss the latest research completed by Forrester. This research showcases devastating effects of complexity on DevOps toolchains... As a quick preview, here are some of the highlights:

* Over 61% of organizations use at least 2 disparate software delivery toolchains. 
* 56% reported that their toolchain is comprised of 6 or more tools
* Many estimate managing tool chains consumes 10% of their development team
* 4 out of 10 professionals noted that they struggle with insufficient expertise, skills, and resources to **maintain and integrate** the tools

In short, toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised.

Forrester Principal Analyst Christopher Condo will be presenting these findings and more, followed by an interactive "Fireside Chat" discussion on how to manage the visibility, security and productivity challenges DevOps leaders face today.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You wouldn't be interested in joining, would you?


Warm Regards,
{{sender.first_name}}

## Email 2

{{first_name}}, 

Here is one of the insights that might be relevant to you:

Forrester's research discovered that over 61% of organizations use at least 2 disparate software delivery toolchains. 

But that complexity begets more complexity. The more toolchains, the more tools per toolchain. 

56% reported that their toolchain is comprised of 6 or more tools. 

And the more complex a toolchain, the more labor-intensive it becomes to maintain.

Doe

## Email 3

Toolchain complexity creates a crisis wherein the benefits of adopting DevOps are not realised. Teams suffer from the costs of acquiring, integrating, and administering multiple tools. They pay the tax of context switching between the tools. And the organization's speed is reduced because of the time and cost to train users on a complex toolchain.

## Email 4

Hi {{first_name}},

With this workshop just three weeks away, wanted to share more of the details with you.

"Avoiding the Hidden Cost of DevOps Toolchains" is a workshop chat covering the latest DevOps research as well as an interactive discussion on the difficulties and importance of managing a complex toolchain to move forward on your DevOps journey.

We can talk about some of the DevOps challenges you're facing in your organization, or you can share what you're experiencing as a practitioner within enterprise.

Chris Condo from Forrester, John Jeremiah, and other local IT leader are slated to speak.

Here are the details:
Date: Thursday, October 17, 2019
Time: 3:00 pm - 8:00 pm
Location: Convene
Address: 1800 Tysons Blvd, McLean, Virginia, 22102

You probably wouldn't be available to join us, would you?

Warm Regards,
{{sender.first_name}}

## Email 5 

Any thoughts?

## Email 6

Hi {{first_name}}

With "Avoiding the Hidden Costs of DevOps Toolchains" happening at Convene tomorrow, figured this was a longshot on late notice. I wanted to resurface it to you in case this was of interest. It is by RSVP only so please do let me know if we should reserve your spot.

Cheers,
{{sender.first_name}}

## Responded Email

Thank you for your interest in "Avoiding the Hidden Cost of DevOps Toolchains." We've got an excellent guest list with 60+ folks registered thus far (including others from {{company}}), and are excited to have you join us. As promised, here are the details:

"Avoiding the Hidden Cost of DevOps Toolchains" is a workshop chat covering the latest DevOps research as well as an interactive discussion on the difficulties and importance of managing a complex toolchain to move forward on your DevOps journey.

Here are the details:
Date: Thursday, October 17, 2019
Time: 3:00 pm - 8:00 pm
Location: Convene
Address: 1800 Tysons Blvd, McLean, Virginia, 22102

We will have local speakers, GitLab experts, free food, and drinks available to attendees. 

There's no obligation as this is a complimentary event. Please feel free to bring a guest. However, in that case, I would ask that your guest registers as well as this event is by invitation only. You can view the details on this registration page: https://page.gitlab.com/roadshow-dc.html

Warm Regards,
Kevin

### Snippets

*****

Spend more time writing code, less maintaining toolchains
Decrease toolchain downtime
Improve toolchain security
Increase developer capacity and efficiency
Increase agility to respond to the market


Everyone is trying to ship software faster and deliver new features. However, the mixture of custom tools and integrations prevents them from reaching their goals. Recent research quantified this challenge and highlights suggested strategies to minimize the toolchain overhead.



discussion on toolchain complexity and how to manage the visibility, security and productivity challenges.



We’ll be discussing key topics such as: 

* Innovative ways to drive culture change within your organization
* Streamline application delivery, and foster continuous improvement on a day to day basis 
* Discuss best practices to succeed in today’s digital landscape by using automation to unlock velocity 
* Methods to avoid spending too much time complicating your application security 
* The importance of monitoring key metrics to ensure success

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be available to join us, would you?

Warm Regards,
Kevin


*****


Date: Thursday October 17

Time: 3 pm - 7:30 PM

Location: Convene at 1800 Tysons Blvd - Tysons Lounge Room

Main roadshow landing page: https://about.gitlab.com/events/roadshow-2019/


DC Landing Page: https://page.gitlab.com/roadshow-dc.html


RSVP List: https://gitlab.my.salesforce.com/00O4M000004dlQE

Join GitLab and guest speaker Forrester Principal Analyst Christopher Condo for an interactive discussion on toolchain complexity and how to manage the visibility, security and productivity challenges.

Date: Thursday, October 17, 2019
Time: 3:00 pm - 8:00 pm
Location: Convene
Address: 1800 Tysons Blvd, McLean, Virginia, 22102


*****

Thank you for your interest in Roadshow: Avoiding the Hidden Cost of DevOps Toolchains. We've got an excellent guest list with 60+ folks registered thus far (including others from Freddie Mac), and are excited to have you join us. As promised, here are the details:

GitLab Connect DC is a workshop covering real-life DevOps transformation stories as well as an interactive discussion on the difficulties and importance of getting started and moving forward on your DevOps journey. 

Date: Tuesday, August 13, 2019
Time: 2:00 pm - 7:00 pm
Location: TopGolf National Harbor - Chairman's Room
Address: 6400 Clipper Way, Oxon Hill, MD 20745

We will have local speakers, GitLab experts, free food, golf, and drinks available to attendees. 

There's no obligation as this is a complimentary event. Please feel free to bring a guest. However, in that case, I would ask that your guest registers as well as this event is by invitation only. You can view the details on this registration page:   https://page.gitlab.com/gitlab-connect-dc.html  
