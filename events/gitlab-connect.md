# GitLab Connect


## LinkedIn Message

	To someone who has replied that they are interested in joining.

{{first_name}}, thanks for your interest in joining us at GitLab Connect DC on August 13th at National Harbor. I've sent over the details in an email, and would like the chance to speak with you prior to the event. Would you be open to connecting on a brief call?

Warm Regards,
{{sender.first_name}}

## Vidyard

Hello Washington Metro! Kevin McKinley here at GitLab, and we're m going to be in your city next Tuesday Aug 13! 

We've rented out the Chairman's Suite Room at TopGolf National Harbor for this complimentary invite-only workshop.

We’ll have food, drinks, and golf games. Local IT leaders like Scott Jaffa from ValidaTek, Justin Senseney from NIST, John Bellone from SS&C, and Harold Smith III from Monkton are slated to speak and share their Digital Transformation stories.

We've got an excellent guest list with over 60 people registered thus far, and would be excited to have you join us. 

It'll be an extremely informative day as they cover topics such as integrated CI/CD pipelines, how to leverage containers and Kubernetes, shifting application security left, and more.

It is by RSVP only, so please let me know if you're interested in attending by filling out the form below.
 
I hope you can join as our guest. If not, let me know if there's an interest and I'll keep you on the list for future events.

https://embed.vidyard.com/share/7AcgnkhnNizrG2tmEziZsk?