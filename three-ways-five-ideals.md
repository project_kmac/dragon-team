# The Five Ideals and The Three Ways

## Five Ideals

1. Focus, Flow, and Joy
2. Locality & Simplicity
3. Improvement of Daily Work
4. Psychological Safety
5. Customer Focus

## Three Ways

1. Flow
2. Feedback
3. Continuous Learning