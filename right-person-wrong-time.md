Right Person, Wrong Time

https://gitlab.my.salesforce.com/00T4M00002VANnD?srPos=24&srKp=00T

>>>Michael

You reached the right person, however I’m booked solid untill the end of the year. Any way you can schedule a call for mid January.

Thank You
Edwin Martinez

****

Ed,

Appreciate the quick response.

While I can understand that you're booked solid until the end of the year, I'm not even sure if that conversation in January is necessary.

Many times IT leaders are just curious and really have no immediate interest in talking further, while others may have specific needs in mind for their organization.

Do you have even a brief 5-10 min window? If at the end of that short call, you don’t feel like our platform could solve your app sec issues, I won’t bother you anymore. But if you feel like it could potentially help, we'll go ahead and schedule that call for next year.

Warm Regards,


P.S. As I recognize this may not be the best time for you, if we were able to find the 10 minutes, I’d be happy to send you a copy of the latest book GitLab just published with O’Reilly - “10 Steps Every CISO Should Take to Secure Next-Gen Software.”


****

Flavio, it's a pleasure to meet you here as well. Thank you for taking the time to respond and for being transparent.

I can totally understand if the timing isn't right. Even if you're comfortable with the current toolstack and there's no impetus to change, it might be of value to have an informal discussion about some trends we're seeing across the industry, as well as challenges other folks in your position are coming up against. (Even if it's just to learn or for a down the road interest.)

If I were to tell you that I have zero expectations for selling you, would you be open to having a call?
