# Making Changes to the GitLab Handbook (for SDRs)

[Video Walkthrough of how to make changes to the GitLab Handbook for the SDR org](https://drive.google.com/file/d/1BjvMNgQgimYZOnaX0NpgaFvbYl47Jcwa/view?usp=sharing)

One of our Values is being [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so that the whole organization has access to that process. (aka The DevOps ideal of turning "Localized Discoveries" into "Global Knowledge".)

Steps:

1. Have a change you want to make to the handbook? Great!
2. Navigate to the source code of the page in the handbook (e.g. [Link to edit the SDR page in the Handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/handbook/marketing/revenue-marketing/sdr/index.html.md) )
3. Click either "Edit" or "Web IDE" to make your changes.
4. Have a brief but descriptive "Commit message" (e.g. "Add new section on 'How to Make Changes to the Handbook'") and commit your changes
5. Fill out the Merge Request details


## MR Details

Why is this change being made?

One of our Values is being [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). We're not currently practicing this in the SDR org. Part of that reason is because there aren't clear instructions on how SDRs can make changes to this page. Another aspect is because we're not encouraging this behaviour, and treating the handbook as documentation, rather than as a living document that can and should be changed as easily as a google doc. This MR seeks to change address those two points.