# Self-Serve List Upload

1. Advanced Search of Employees DiscoverOrg
2. From Advanced Search in DiscoverOrg, Upload to SFDC as LEADS
3. From Advanced Search in DiscoverOrg, Upload to Outreach (Use Tags for ease of sorting. You can also upload directly to a sequence from here.)

[Kaleb's Video Walking Through the Process](https://drive.google.com/file/d/107hhZQX6IhDVk1uWcAqCB8qbh__fWG8j/view)

IMPORTANT: No Mass Sequencing. We don't want to see a hundred people from the same company all placed into one sequence, getting the same message. Segment your prospects by title, keywords, role, etc. and place them each in respective sequences with relevant and tailored messaging for that person. 

If you're segmenting across your territory, we want it to be between 40-125 prospects per sequence. If you're segmenting within a single company, you'll want to have no more than 5-10 prospects per sequence.

Any less, and your segmentation is probably too fine grained to have the effective volume. Any more, and your segmentation is probably too broad to have an effective offer.
