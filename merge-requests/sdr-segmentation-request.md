# SDR Segmentation Request Issue Template

### This issue template is to be used by anyone internally requesting the creation of SDR segmentation lists. 


## :clipboard: SDR Segmentation Request

**Requestor:** `Use @gitlab handle please`

**Segmentation Title:** `i.e., "Business Resiliency Messaging" or "Telecom Vertical"`

## :inbox_tray: Description

**Required Information:** 
*  Region : `i.e.,ENT-LARGE-Central-Gulf-Carolinas`
*  SAL: `The name of the SAL for the territory`
*  SDR: `The name of the SAL for the territory`
*  Justification: `Describe the reason for needing further segmentation. The more information the better. (e.g. additional leads needed, in-region event, strategic opportunity)`
*  Related Links: `Include links to any relevant Issues or other sources of information, i.e., examples that will be helpful for creating the segmentation list.`

### :alarm_clock: Timeline

**Date Needed:**
*  Requested Date: `ISO Format (example: 2020-06-21)` 


/label ~"SDR" ~"SDR Segmentation"
/assign @k-mac @mmalcolm





****

:bulb:idea alert:bulb:What do you think about creating an sdr segmentation request issue template?
I'm thinking we could start with a version tailored for SDR leaders?
In the template, capture info like:
region
sdr name
SAL
justification (additional leads needed? in-region event? strategic opportunity? etc..feel free to add others)
outbound campaign start date (requested)
...feel free to iterate, bounce ideas off me via slack, or we can discuss tomorrow am













