## Acceleration Workflow & Process

### Issue Board & Labels

The Acceleration team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~Acceleration label anywhere within the GitLab repo. 

The Acceleration team uses this [global issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened).

Global labels used by the team:

- `accel abm amer - prospecting`: Issues related to EMEA acceleration abm sdr prospecting and campaigns
- `accel abm amer::plan`: Issues related to planning for the AMER acceleration abm sdr team
- `accel abm emea - prospecting`: Issues related to EMEA acceleration abm sdr prospecting and campaigns
- `accel abm emea::plan`: Issues related to planning for the EMEA acceleration abm sdr team
- `accel abm::queue`: Issues or requests related to the accel abm sdr team that are currently pending

- `SDR Content`: For all issues related to outbound messaging or content creation
- `SDR Content::FYI`: SDR Content | FYI - Workflow for all issues related to SDR content that outbound should be aware of (FYI)
- `SDR Content::Queue`: SDR Content | Queue - Workflow for all issues related to SDR outbound content requests in the queue to be started
- `SDR Content:: In Progress`: SDR Content | In Progress - Workflow for all issues related to SDR outbound content currently being worked on
- `SDR Content::Completed`: SDR Content | Completed - Workflow for all issues related to SDR outbound content that have been completed
- `SDR - High`: High Priority | SDR Content Request
- `SDR - Medium`: Medium Priority | SDR Content Request
- `SDR - Low`: Low Priority | SDR Content Request

- `SDR Segmentation`: For all issues related to prospect targeting, bucketing, and segmentation
- `SDR Segmentation::FYI`: Workflow for all issues related to SDR segmentation that outbound should be aware of (FYI)
- `SDR Segmentation::Queue`: Issues related to SDR list building and strategic segmentation that are currently in queue
- `SDR Segmentation::In Progress`: Issues related to SDR list building and strategic segmentation that are currently in progress
- `SDR Segmentation::Completed`: Issues related to SDR list building and strategic segmentation that have been completed


### Diff

Removed these outdated labels:

- `Acceleration`: adds the issue to the Acceleration board. Used for all acceleration initiatives.
- `Acceleration - Planning`: issues that are related to planning of a territory warm up campaign
- `Acceleration - Prospecting`: issues related to active acceleration campaigns or initiatives
- `Acceleration - EMEA`: issues related to the EMEA Large Acceleration team
- `Acceleration - Programs:` work in progress projects and process improvements

### MR Details

Why is this change being made?

Removing outdated labels and replacing them with the new labels currently in use.

Removed Labels: 
- `Acceleration`: adds the issue to the Acceleration board. Used for all acceleration initiatives.
- `Acceleration - Planning`: issues that are related to planning of a territory warm up campaign
- `Acceleration - Prospecting`: issues related to active acceleration campaigns or initiatives
- `Acceleration - EMEA`: issues related to the EMEA Large Acceleration team
- `Acceleration - Programs:` work in progress projects and process improvements

### Commit Message

Remove outdated Accel board labels and replace them with the new labels currently in use.