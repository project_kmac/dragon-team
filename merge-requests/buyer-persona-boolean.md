# Buyer Persona Boolean Searches 

We need to start aligning the outbound sdr campaigns and framework to the "buyer personas". The first step is to determine our audience. That work has been done with the development of the buyer personas. Now we need to start to define and document HOW SDRs can uncover and reach those prospects within their accounts using both LinkedIn Sales Nav and DiscoverOrg.


https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas

https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/

#### Narrow to Broad 

**Start with the smaller segments first. You can offer them more relevant, highly targeted, and personalized messaging.**

One way to approach segmentation is to start outreach with the most highly engaged groups, before including those with broader keywords.

For example, reach out to those with the keyword "GitLab" first, before "DevOps", then finally reaching out to those with "Enterprise Architecture." There are FAR more Enterprise Architects than DevOps folks, and even fewer GitLab users, so you want to address the smaller subsegments first. This allows your messaging to be more relevant and impactful. 

(If you do it the other way, send a message to all with the "Enterprise Architecture" keywords, you may end up reaching some folks in the smaller group as well with messaging that's less relevant to them.)

It's worth noting that Seniority Level for both DO and LISN isn't always accurate, so you may want to test with and without to see which returns better results for your specific searches. 

As a counter example, it is possible to add too many keywords, and get such a narrow search in return, that you'll have remove some keywords to get enough prospects to work with. That's the part you'll have to test in your specific territory.



****
## Buyer Personas 

Buyer Personas as Defined in the Handbook: https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/

#### Alex - the Application Development Manager

**DiscoverOrg**

* Employee Criteria
* Department: Information Technology & TEDD
* Seniority Level: Mid-Level Management; Lead, Manager, Principal, Program Manager, Project Manager, Senior Manager, Supervisor, Team Lead


**LinkedIn SalesNavigator**

* Function: Information Technology, Engineering
* Title: "Manager"
* Keywords: "application development" or "software development"

 
#### Dakota - the Application Development Director

**DiscoverOrg**

* Employee Criteria
* Department: Information Technology & TEDD
* Seniority Level: Mid-Level Management; Director, Executive Director, Senior Director

**LinkedIn SalesNavigator**

* Function: Information Technology, Engineering
* Title: Director
* Keywords: "application development" or "software development"


#### Erin - the Application Development Executive (VP, etc.)

**DiscoverOrg**
* Employee Criteria
* Department: Information Technology & TEDD
* Seniority Level: Senior Level Management; (Select All Visible, Remove Chief Information Security Officer)

**LinkedIn SalesNavigator**

* Seniority Level: VP
* Function: Information Technology, Engineering, Program and Project Management
* Title: VP, Vice President
* Keywords: application development or software development


####  Skyler - the Chief Information Security Officer

**DiscoverOrg**

* Employee Criteria
* Department: Information Technology & TEDD
* Seniority Level: Senior Level Management; Chief Information Security Officer

**LinkedIn SalesNavigator**

* Title: CISO, Chief Information Security Officer

*****

## Boolean Searches

The key to success with Boolean is to avoid vague and ambiguous keywords. For example terms like “Innovation” or “Strategy” can pertain to just about any role and persona.

#### OR (all upper case)

If you want to broaden your search to find profiles that include one or more terms, you can separate those terms with OR. OR is most often used to search for alternate spellings, or for terms that mean the same thing. For example:
“Sales Operations” OR “Sales Ops”
“Vice President” OR VP OR “V.P.” OR SVP OR EVP

#### NOT (all upper case)

If you want to exclude a particular term from your search, type that term with NOT before it. Your search results will exclude any profile containing that term. For example:
NOT director
Director NOT executive NOT VP NOT “Vice President”
Note: 
The least used and least understood modifier is NOT. NOT helps you pare down your search results by excluding prospects you know are not qualified, or would not necessarily be interested in your product or service. 

#### Parentheses

If you want to do a complex search you can combine terms and modifiers. For example:
Marketing AND (B2B OR B2C)
Advertising NOT (print OR “B2C”)

#### AND (all upper case)

If you want to search for profiles that include two terms, you can separate those terms with AND. However, you don’t have to use AND; if you enter two terms the search engine will assume there is an AND between them. The following formats are read the same:
Sales AND Director
“Sales manager” AND “business to business”
Sales prospecting

#### Quotes

If you want to search for an exact phrase or terms that include punctuation, enclose the phrase in quotation marks. You can use these in addition to other modifiers. For example:
“sales manager”
“account representative”
“human resources manager”

### Filtering

Keywords seem to best be used for particular skills. If you're looking for a specific segment based on role, it may work better to use the Title filter. (Additionally be aware that, LISN's "Seniority Level" often seems to include some of the wrong people and exclude some of the right people.)

****

## LinkedIn Boolean Search Examples
Below is a suggested list of LinkedIn Skills and keywords for your LISN boolean searches.

#### DevOps

"DevOps" OR  "DevSecOps"

#### Tools

OR "Git" OR "GitLab" OR "BitBucket" OR OR "Azure DevOps" OR "VSTS" OR "TFS" OR "GitHub" OR "Jira" OR "Atlassian" OR "Jenkins" OR "Bamboo" OR "Travis CI" OR "Circle CI" OR "Confluence" OR "Mercurial" OR "SVN" OR "SCM" OR "Version Control"


#### Cloud

“Google Cloud" OR "GCP" OR "Amazon Web Services" OR "AWS" OR "Azure" OR "Microsoft Azure" OR "Amazon EC2" OR "AWS RDS"

OR "Cloud Enablement" OR "Cloud Applications" OR "Cloud Development" OR "Cloud Storage" OR "Cloud Computing" OR "Cloud Architecture" OR "Cloud Administration"

OR "Pivotal Cloud Foundry" OR "Cloud Foundry"

OR "Kubernetes" OR "Google Kubernetes Engine" OR "GKE" OR "Amazon EKS" OR "EKS"

OR "Docker" OR "Chef" OR "Puppet" OR "Ansible" OR "Salt" OR "Terraform" OR "JetBrains" OR "MicroFocus" OR "Octopus" OR "Splunk" OR "New Relic" OR "Dynatrace" OR "DataDog"


#### CI/CD

OR "CI / CD" OR "CI/CD" OR "Continuous Integration" OR "Continuous Deployment" OR "Continuous Delivery" OR "Deployment" OR "Continuous Build" OR "Release Engineering" OR "Build and Release" OR "Release" OR "Platform Delivery"


#### Development

OR "Application Development" or "Software Development" OR "Software Engineer" 

#### Architecture

OR "Enterprise Architecture" OR "Architecture" OR "Application Architecture" OR "Platform" OR "TOGAF"

#### Engineering

OR "Engineering" OR "Testing" OR "Integration" OR

#### Information Security

OR "Information Security" OR "SAST" OR "DAST" OR "IAST" OR "RASP" OR "Sonarqube" OR "OWASP" OR "Zero Trust"


#### Agile

OR "Agile" OR "Waterfall" OR "Scrum" OR "Scrum Master" OR "Legacy"


#### Initiatives

OR "Digital Innovation" OR "Digital Transformation" OR "Digital Strategy" OR "Emerging Technologies" OR "Emerging Technology" OR "Emerging Tech" OR "Digital" OR "Consumer Technology" OR "Digital Experience"

#### Data

OR "Data Warehousing" OR "Data Center" OR "Data Lake"


#### Misc

OR "Open Source" OR Automation" OR "Tooling" OR "SDLC" OR "Middleware"

****

## DiscoverOrg Boolean Search Examples

#### Sample

((("Director" OR "President" OR "VP" OR "AVP" OR "Chief" OR "CIO" OR "CDO" OR "CTO") AND 
(DevOps OR Application OR Develop OR Architecture OR Infrastructure OR Security))


#### By Title

("Director" OR "President" OR "AVP" OR "VP" OR "Chief" OR "CIO" OR "CDO" OR "CTO")


("Director" OR "Senior" OR "President" OR "AVP" OR "VP" AND

NOT ("Chief" OR "CIO" OR "CDO" OR "CTO")

#### DevOps

DevOps OR  "DevSecOps"  OR "Site Reliability Engineer" OR "SRE"

#### AppDev
OR Application OR Develop OR Software

#### InfraAch
OR Architecture OR Infrastructure NOT (Analyst OR Admin OR Network OR Corporate)


#### QA

(Quality Assurance OR QA Testing OR Testing OR QA) NOT "Analyst"

#### Security

Security NOT (Analyst OR Admin OR Network OR Corporate OR Physical)


#### Delivery 

(Application OR Develop OR Software)
NOT ( Analyst  OR  Admin  OR  Network  OR  Corporate  OR  Epic  OR  SAP  OR  ERP  OR  Clinic  OR  Engineer  OR  Architect  OR  Oracle  OR  EBS )

#### Efficiency

Engineer  OR  Architect OR Infrastructure OR Cloud  OR Operations
NOT ( Analyst  OR  Admin  OR  Network  OR  Corporate  OR  Epic  OR  SAP  OR  ERP  OR CAD OR Clinic OR Clinical OR PACS OR NOC OR FGPA OR  Oracle  OR  EBS OR Citrix OR Software OR Application OR Product OR Telecommunications OR Cybersecurity OR Systems OR SQL OR Data OR Hardware OR Firmware OR Middleware OR Support OR Lead OR Process OR Model OR Research OR R&D)

#### Negative Keywords

Field OR Windows OR Project OR Systems OR Firmware OR Hardware OR Middleware OR Mechanical OR Electrical OR Storage OR R&D OR Research OR Lab OR Clinic OR SAP OR ERP OR Facilities OR Voice OR Pega OR Oracle OR Citrix OR Embedded OR NOC OR EPIC RF OR Radio OR Wireless OR Websphere OR VOIP OR Communications OR Telecom OR Content OR Cisco OR Escalation OR Identity OR IAM OR Coding OR  

****

#### Resources


DevOps" OR "Google Cloud" OR "AWS" OR "GitLab" OR "GCP" OR "Confluence" OR "GitHub" OR "Jira" OR "Open Source" OR "Git" OR "Atlassian" OR "Cloud" OR "CI / CD" OR "CI/CD" OR "Continuous Integration" OR "Continuous Deployment" OR "Continuous Delivery" OR "Automation" OR "Scrum Master" OR "Legacy “" OR "Emerging Technologies" OR "Emerging Technology" OR "Emerging Tech" OR "Digital" OR "Consumer Technology" OR "Digital Experience" OR "Information Security" OR "Kubernetes" OR "SAST" OR "DAST" OR "Jenkins" OR "SVN" OR "Application Development" OR "Agile" OR "Waterfall" OR "Scrum" OR "SQL" OR "Bitbucket" OR "Python" OR "SCM" OR "Tooling" OR "Collaboration" OR "Engineering" OR "Testing" OR "Integration" OR "Middleware" OR "Release" OR "Platform Delivery" OR "SDLC" OR "Digital Innovation" OR "Digital Transformation" OR "Digital Strategy" OR "Architecture

Ent Amer Outbound process framework: https://docs.google.com/document/d/1GkocgndrILDvKa56NODp-h2qmn5sL2YIzmnoESuhOJU/edit#heading=h.rwhn38sf8qlk

SDR Account Prep
https://docs.google.com/document/d/1Bxc4g2AxEMerPThjCl_7cScPTGiItvvi8sLfXV_VJBg/edit#

LinkedIn Requests Examples:
https://docs.google.com/document/d/1Z6a7KMmiumwvt5OjNu-aTNeo0QxN6b-MFJkFzK8ZWps/edit#

Glidden's:
https://docs.google.com/document/d/1TVkhGKfsJZllqfu6rz8_x1FYbXszbG8Taos1VRUcGWs/edit?ts=5eebaa0f

Recruiter:
https://docs.google.com/spreadsheets/d/1wRGwx_GT14udxnoMOF-gP2XQaRhJA_8bEVwWwIdBio4/edit#gid=666961789


BCK:
https://docs.google.com/document/d/1MDrDZ8uxHSI3V5hMYfYcWnjv7kXUt3weXFDD_oySOw8/edit

MacFarlane and BCK Video:
https://gitlab.zoom.us/rec/play/vJcrcu-vrjI3HtOcsQSDC_B6W43oLa2s0yke8qYPxUiwViFSZ1ukZuAQNuProPukUahNcVom7iiKJ5jQ?autoplay=true&startTime=1582829352000