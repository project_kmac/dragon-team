# ABM Campaign Tier 3 DiscoverOrg list pull

### [Account List](https://gitlab.my.salesforce.com/00O4M000004aAdS)

## Target Segment

* **VIP**: *CTO, CIO, CSO, CISO, VP of IT, Managing director of IT, Chief Architect, Chief Systems architect, Global Head of DevOps, Head of DevOps, Head of Engineering, Head of Development*
* **Influencer**: *Directors or Managers of software development, software dev, software engineering, engineering, application development, app dev, devops, security, site reliability, site reliability engineer, cloud engineer, cloud architect, cloud services, cloud native app dev, cloud native application development, enterprise architect, principal architect, software architect, data architect, technical architect, application architect, architect, consultant*
* **User**: *Developers, engineers, Q/A, test, business users, interns/students*

`Please note: these should be imported as LEADS, not contacts`


Steps: 
1. Run SFDC Report and Export your Tier 3 [Account List](https://gitlab.my.salesforce.com/00O4M000004aAdS)
2. DiscoverOrg List Match your Tier 3 Accounts and view in Advanced Search
1. Execute Boolean Search for each of the Target Segment in DiscoverOrg
2. Export each search into a CSV that is broken up by Target Segment

[Video Walkthrough](https://drive.google.com/file/d/1Ggn6z1NSOtQ_DCH7mRjuB744rGphqb1x/view?usp=sharing)

## MR Details



Defining the DiscoverOrg Prospect Discovery process in the handbook and adding to the ABM framework. This process will be performed by whomever owns the Tier 3 accounts. This process is something that most SDRs should be intimately familiar with, but which thus far has not been codified in the handbook. The MR above also includes the suggested persona buckets and search terms.

https://gitlab.com/gitlab-com/marketing/sdr/-/issues/313#note_355380341