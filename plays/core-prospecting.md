# Core Prospecting

## 1 - Email

subject: your package is on the way

Hi {{first_name}}, 

Wanted to give you a heads up that I sent you a copy of the latest Forrester Research whitepaper titled "Manage Your Toolchain". I thought this would be helpful for you because {{!reason}}

Keep an eye out for this in the mail over the next few days - I've also thrown in a few extra goodies :)

Also happy to share what we're doing with DevOps in FinServ. Let me know if you'd like to discuss.

Warm Regards,
{{sender.first_name}}


## Facilitate An Introduction

subject: re:your package is on the way

We are actively reaching out to {{!name/title}} in your IT organization to discuss how DevOps strategies can help you remove bottlenecks in your software delivery. It's about delivering quality software faster.

We're helping IT organizations deliver on their DevOps initiatives and be more nimble and automated while integrating security in your pipeline.

Would you be open to facilitating an introduction to your head of DevOps?

Warm Regards,
{{sender.first_name}}

## Haven't Heard Back

I haven't heard back from you and wanted to see if you had any interest in learning more about Enterprise DevSecOps. Happy to coordinate calendars if it makes sense.

I know that usually the {{title}} is in charge of this sort of thing, but if that's not the case at {{company}} and you think someone else could benefit from that conversation, I would greatly appreciate if you would be so kind as to facilitate an introduction.

Warm Regards,
{{sender.first_name}}
