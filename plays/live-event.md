# Live Event

## Invite

subject: dinner at {{venue}}?

Hi {{first_name}},

Please accept my apology for the intrusion. I thought you might enjoy our complimentary event because {{!reason}}

We're hosting an executive dinner at {{venue}} on {{date}}. It's going to be a medium-sized gathering (60-ish senior DevOps leaders) so we can network and discuss DevOps best practices.

Do you think you'd be able to make it? Should we save a spot for you?

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You wouldn't be available to join us, would you?

Warm Regards,
{{sender.first_name}}

****

## Event Details

Hi {{first_name}},


Following up from my previous email. I'm hoping you can make it to our executive dinner at {{venue}} starting at {{time and date}}.

DevOps leaders from {{!companies}} will be in attendance - we would be honored to have you join us.

Would you be available that evening?

Warm Regards,
{{sender.first_name}}
