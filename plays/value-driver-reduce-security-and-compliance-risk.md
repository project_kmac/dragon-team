# value-driver-reduce-security-and-compliance-risk

## Persona

This is intended to be the volume email coverage outreach. As such, here's the list I'm targeting: https://docs.google.com/spreadsheets/d/1EcXdAFtrAJOjlI3i29pDJ9M97ySKK9gNA1Ms-xE9Z30/edit#gid=195726803

## Resources

* Cindy Blake's CISO EMEA Summit Presentation: https://docs.google.com/presentation/d/1xA6-q_JAuBOjaAefSO6rTHmo41LgmGzLe_SD41Z0uXw/edit#slide=id.g5df25acc9a_0_955
* Forrester Research Whitepaper on Zero Trust: https://drive.google.com/file/d/1Aj7yuFlVa11BkYgmckaT6Xq5hRnDSnHa/view

## CoM

PAIN: 
Unsatisfying trade-offs
Security exposure 
Missed deadlines  
No traceability (compliance)

DREAM:
Confidence in security posture
Alignment between security and dev   
Consistent compliance to policy   
Predictably on time and on budget   
Reduced security exposure 
Cleaner and easier audits

IMPORTANT:
Fix vulnerabilities at point of code change   
One consistent view for collaboration 
Reporting and visibility  
Enforcing common controls   
Auto-remediation  

FIX:
Single app for the DevOps Lifecycle
Built-in security and compliance   
End-to-end visibility and insights   
Collaborative/transparent cust exp   
Flexible hosting options    

## 30 Second Commercial

Typically, the security professionals I speak with tell me that they're looking to shift security left. 

Meaning, security shouldn't be an afterthought. It should be integrated into their deployment pipeline (both in terms of application security and securing the pipeline itself)

Additionally, if traceability was built in to the toolchain, it would make compliance with audit and governance easier.

## Referral Play

Abstract: 
Create PURPOSEFUL outreach that is built with 1) the individual prospect and 2) a desired outcome in mind

This play is designed to get us a referral into the appropriate person into an account. One of the key bottlenecks within account development is finding the right person who can help us get a foot in the door. Tools such as DiscoverOrg and LinkedIn SalesNav assist with this by providing org charts and profile matching, respectively.

However, these tools are still inadequate.(How many times, for example, does a person matching your key terms in LinkedIn already have initiatives underway that they are all-in and committed to? Or for that matter, how often is it that the correct person DOESN'T have an updated LinkedIn profile which includes those keyterms?)

Nothing compare to a friendly insider who is willing to point us in the right direction. This play is designed to get us to that person. The prospect we are trying to reach is the one involved in the DevSECOps aspect, primarily within securing the applications or securing the pipeline. While there are certain pain points within Network Security we can address, we will leave that out of the scope of this outreach.

Questions:
Is this written to the CISO? Someone in Application Security? Information Security? Is it directed to the target, or are we looking to send it to someone lower that could refer us up?


****
#### Subject Lines

* Total Cost of Ownership 💸

* Subject: {{company}}'s Digital Transformation ⚡️
* Subject: Lower Cycle Time // GitLab CI ♻️
* Subject: Simplify Your Toolchain ✅
* Subject: 🚀GitLab + K8s☁️
* Subject: DevSecOps // GitLab 🔍
* Subject: This Isn’t Goodbye…💔



#### email 1: Leading Value

v1 - direct 
subject line: advice? / appropriate person / is this you?

body: 

{{first_name}},

You mention on LinkedIn that you're in {{!role}} at {{company}}, and so I thought this conversation might be relevant to you.

I work with {{title}}s who typically tell me that they're working on initiatives to shift security left. 

Meaning that security shouldn't be an afterthought. It should be integrated into the deployment pipeline (both in terms of application security and securing the pipeline itself)

Additionally, for them, if traceability was built in to the toolchain, it would make compliance with IT audit and governance regulation easier.

Is this relevant to your world, or worth a brief conversation?

Warm Regards,
{{sender.first_name}}

v2
You mentioned on LinkedIn that you’re {{!reason}}. As such, I thought this might be relevant to you.


Typically, the {{title}}s I work with tell me that they're looking to reduce security and compliance risk. 

Often they already have a sophisticated DevSecOps toolchain. But occasionally this means they have multiple toolchains to integrate and maintain, which then creates limited visibility into their pipeline and makes governance reporting and regulatory compliance more difficult.

Perhaps instead they're trying to shift security left so that they can catch defects and remediate earlier. So they're looking for ways to better leverage scarce security resources, automate testing/code review, and put application security tools into the hands of the developer.

Or alternatively, they're planning to ensure business continuity in the event of an unplanned outage, so they're moving to the cloud for high availability and disaster recovery.

Does any of this sound like it’s relevant to your world, or worth even a brief conversation?



#### email 2: Quick Follow Up/Bump

v1
Any thoughts? Does it make sense for us to discuss this further?

v2
Making sure my last note reached your inbox. I know things can get lost and I wanted to get back on your radar.

When can we get together for a few minutes on your calendar to talk?

v3
Bumping to make sure you got my last email, as I know they can fall through the cracks sometimes. 

How does your calendar look for a quick call?...Or if you aren't the right person, would you be so kind as to point me in the right direction?

v4
Checking in for an update. Can you give me a thumbs up  or thumbs down on whether you are interested or not?

v5
{{first_name}}, can we help you? I am not sure yet. 

A quick 10 minute phone call should help us figure that out. I will call you Thursday at 11 AM. 

If that time is inconvenient, please suggest an alternative.

v6
I tried calling but was unable to reach you by phone. Does it makes sense to talk further? 

v7
I hope this missive finds you well. You probably wouldn't be interested in learning more about this, would you?

v8
When can we have a quick 20 minute call to share how you might be able to see similar results? Be happy to include other members of the marketing team in the meeting, as well.

v9
Wanted to resurface this in case it was of interest. I can only assume you're incredibly busy, but would appreciate a quick "Yes, let's find a time" or "No, I'm not interested" 

#### email 3: Expanding the Value

subject: necessary, but not sufficient

{{first_name}},

Had a conversation with a security architect the other day that I thought you might find interesting.

He shared how in their organization they've built a robust security testing suite using tools like Fortify, Sonarqube, and Checkmarx.

But those tools can be expensive due to the pricing model of pay-per-line of code scanned and the integrations required. Over the course of our discussion, it became clear this meant that security is relegated to the end of their deployment pipeline. 

Here's why I thought you might find this relevant: traditional full app security scans as outlined above is incongruent with modern software development methodologies like DevOps' daily deploys. 

Shifting security left is necessary, but not sufficient. I'm working with security professionals who want to lead a new era where security is baked into the SDLC.

Does any of this sound like it's worth a brief conversation?

****

Shifting security left is necessary, but not sufficient. I'm working with security professionals who want to lead a new era where security is baked into the SDLC.

Had a conversation with a security architect the other day that I thought might be important to share with you.

He shared how in their organization they've built a robust security testing suite using tools like Fortify, Sonarqube, and Checkmarx.

But those tools can be expensive due to the pricing model of pay-per-line of code scanned and the integrations required. Over the course of our discussion, it became clear this meant that security is relegated to the end of their deployment pipeline. 

Here's why I thought you might find this relevant: traditional full app security scans as outlined above is incongruent with modern software development methodologies like DevOps' daily deploys. 



#### email 4: Referral Request

subject line: advice? / appropriate person / is this you?

body: 

{{first_name}},

You mention on LinkedIn that you're in {{!role}} at {{company}}, and because of this I was hoping you could help point me in the right direction.

I work with {{title}}s who typically tell me that they're working on initiatives to shift security left. 

Meaning that security shouldn't be an afterthought. It should be integrated into the deployment pipeline (both in terms of application security and securing the pipeline itself)

Additionally, for them, if traceability was built in to the toolchain, it would make compliance with IT audit and governance regulation easier.

I know that usually the {{title}} is in charge of this sort of thing, (which in my research, would be {{!name_of_persona}}). But if that's not the case at {{company}} and you think someone else could benefit from that conversation, I would greatly appreciate if you would be so kind as to facilitate an introduction.

Who should I be reaching out to over there?

Warm Regards,
{{sender.first_name}}

#### email 5: Proof Points

I hope you are well. Attached please find a Forrester Research whitepaper on "Managing your Toolchain." I believe you may find this relevant to your Security and Compliance initiatives.

As a quick preview, here are some of the highlights:

* Over 61% of organizations use at least 2 disparate software delivery toolchains
* 4 out of 10 professionals noted that they struggle with insufficient expertise, skills, and resources to maintain and integrate the tools
* Maintaining security and visibility across tools is a key challenge
* An out-of-the-box toolchain allows identity and authentication to be managed uniformly, thereby reducing potential vulnerabilities and attack vectors.

In short, toolchain complexity creates a crisis wherein the benefits of adopting security tools are diminished. Secondly, identity and access management becomes exponentially more difficult with each integration across the toolchain. Finally, this complexity creates significant challenges within governance and regulatory compliance. 

Our response is that automation can reduce the scope of security compliance. I'm looking for security professionals who want to lead a new era where security is baked into the SDLC.

Are these issues of application security, network trust, or compliance controls worth a brief conversation? If so, I would like to suggest that we arrange a phone call. Otherwise, who in your organization might benefit from such a discussion?


****

Totally forgot to add these links. Sorry about that, here it is. Let me know about setting up a call.

Have a great day.

P.S. Here's how we solve regulatory compliance issues: https://about.gitlab.com/solutions/financial-services-regulatory-compliance/#common-controls

P.P.S. This presentation by Cindy Blake covers the next generation of Security: https://docs.google.com/presentation/u/1/d/1xA6-q_JAuBOjaAefSO6rTHmo41LgmGzLe_SD41Z0uXw/edit#slide=id.g615e31196b_0_567


#### email 6: Quick Question

How does toolchain complexity impact your team’s ability to collaborate and communicate in a timely and efficient manner?
 
Errors and downtime, caused by complex integrations and manual processes kill productivity and force developers to focus on maintaining the toolchain instead of building great software.

****

Some of the {{title}}s I work with tell me that their teams are too busy firefighting and trying to keep the lights on, that there's little time left for actual development work. How much time are your dev teams spending on tool maintenance?

**** 


#### email 7: Expand Value Drivers

subject: curious

{{first_name}}, maybe I got my research wrong and reducing security and compliance risk isn't all that relevant to you.

If that is the case... removing bottlenecks and delivering software faster are typically the other top priorities for the {{title}}s we speak with.

Often, they're in the process of standing up a DevOps/Agile pipleline that allows faster speed to market and innovation velocity.

Alternatively, they've got a mature pipeline... but this could mean that they have many tools to integrate and maintain. So instead, they're looking to consolidate and standardize their toolchain.

I'm curious, do those responsibilities fall to you, or someone else?


#### email 8: Now or Later?

