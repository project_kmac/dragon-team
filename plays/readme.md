# Plays - ReadMe

We need a handful of Plays or "Microcampaigns" that we are repeatable with predictable results. These are account-based, highly-personalized, and can take a territory from underserved to well-established in 30 days.

Think about who we have to target, the goal of the campaign, and ways to get us closer to the account. Instead of simply targeting the DRI (Directly Responsible Individual), who else should we be reaching out to? What are other doorways we can leverage?

Types of Plays include:

* Executive Sponsor Play
* Committee Alignment Play
* Door Opener Play(One-to-Few)
* Executive Dinner Play
* Referral Play
* Meeting Incentive Play
* Wake the Deal/ Shake the Tree Play
* Core Prospecting Play(Directly to the Relevant Prospect)
* By Buying Center (Dev, Ops, Sec, QA, PMO, BI/DATA)
