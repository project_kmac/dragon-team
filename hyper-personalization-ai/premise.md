# 1 - Premises & Excerpt

The reason for my outreach

The reason I'm writing to you today

The purpose for my outreach is

The purpose of 

You mention on your LinkedIn profile that

****

I saw that you commented on Jon Smart's LinkedIn post about his upcoming book. One line that stood out to me was "I hope you weave in learnings from your Barclays days."
I saw on LinkedIn that you liked a post about the Cloud-Native Kubernetes meetup coming up on Jan 16th.
But more importantly, I was reading through your LinkedIn profile and noticed that you list Azure, Kubernetes, and Docker under tools that you use.
But more importantly, I was reading through your LinkedIn profile and noted that your specialties includes .NET development. Correct me if I'm wrong, but this likely means you're using TFS or Azure DevOps.
In a video interview titled "Worst Security Vendor Email Pitches" from BlackHat 2019, your example was when vendors send an email and the follow ups progressively become more and more desperate.
You had mentioned that you use BitBucket and were concerned about costs going up to use Okta for OAuth. Additionally, during my conversation with Ryan and Duane, we found that there were alignments both for the application and cloud security initiatives.
You had mentioned that you were working on AWS and Terraform (switching roles, managing dev accounts, and managing Terraform modules), as well as interested in migrating from Jenkins CI.
You mention on LinkedIn that one of your specialties is technology rationalization and creating "enterprise strategies that cascade throughout an organization."
You mention on LinkedIn that part of your responsibilities included " significantly increas[ing] interdepartmental efficiencies."
You mention on LinkedIn that you're responsible for creating an Operational Data Lake and the migration of the data platform to AWS.
You mention on your profile that you're using JIRA and working on .NET, which might mean that you're using some form of Azure DevOps or TFS.
You mentioned during our exchange that your work in Canada is part of the global IT initiative so I respect that this means there's little influence over the tools. I'd simply like to share some insights around what we've seen from other companies, in the event you're looking to drive DevOps adoption and IT efficiency with your team in the future.
You mentioned in your case study for Sophos Cloud Optix that "Our compliance team is now able to run reports for compliance audits in seconds, which was previously manual and exceedingly time consuming."
Thank you for connecting with me My reason for reaching out to you is because I enjoyed reading through your article on "3 things to consider when operating Kafka Clusters in Production." I couldn't agree more with your sentiment that "you will need a highly technical team to keep it running. It is definitely not a platform you will implement and forget about it."
Thank you for connecting with me, I hope the new year is finding you well. The reason I'm writing to you is because I was reading through your LinkedIn profile and saw that you've used GitLab. One line that stood out to me was where you wrote about how you drove "continuous delivery of value to the Customer and business, moving towards a DevOps environment."
Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIn profile about your experience designing highly available and resilient systems with Terraform. One line that stood out to me was how you've designed and implemented "HA and enterprise compliance focused infrastructure."
Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIn profile about your experience with Cloud DevOps. But more importantly, you specifically mentioned that you're "Always looking a way to bring something special and brand new. Always seeking an idea how to improve the company I work for."
Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIN profile about your role in developing the continuous software release management services. One line that really resonated with me was how you've been able to enable "smaller more continuous delivery of software updates to the customer."
Thank you for connecting with me. My reason for reaching out to you is because I was reading through your profile about your role in "Growing our DevOps practice" there at Slalom. But more importantly, you've mentioned specifically that you've automated "legacy infrastructure with Chef, Ansible."
Thank you for connecting with me. My reason for reaching out to you is because I was reading through your profile about your role in "managing application architecture, development & execution of end to end Quote to Cash business process."
Thank you for connecting with me. The purpose for my outreach is because I was reading through your LinkedIn profile and your implementation of systems automation using tools like Puppet/Ansible/Terraform caught my eye. One line you wrote that really stood out was how you've "created many new build and deployment patterns that save us time and headache."
Thank you for connecting with me. The purpose of my email is because I was really digging your Volcan Acatenango photos -- #18 was my favourite. But more importantly, you mention on your LinkedIn that you're responsible for driving cost optimizations within Micron's IT infrastructure.
Thank you for connecting with me. The reason for my email is because I was reading on your LinkedIn profile about your proficiency with DevOps and CI/CD. But more importantly, you list that you're using tools like Jenkins, Chef, and Maven for release and deployment management.
Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and couldn't help but notice your experience with leveraging microservices architecture. Two lines that stood out to me were how you "led development of an end to end solution" and streamlined efficiency through a microservice build system.
Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and was struck by your championing of DevOps at VSP. One line that really stood out to me was how you "Initiated DevSecOps movement within the organization to bring better collaboration and instill a 'shift left' mentality" between the teams.
Thank you for connecting with me. The reason for my email is because I was reading through your profile and couldn't help but take notice of your DevOps experience. One line that stood out to me was, "continuous integration/deployment, shortening the feedback loop, metrics-driven decision making."
Thank you for connecting with me. The reason for my email is because I was reading through your profile and couldn't help but take notice of your DevOps experience. You specifically listed Kubernetes, TFS, and Microsoft Azure under your tools and technologies.
Thank you for connecting with me. The reason for my email is because I was reading through your profile and really appreciate your mission "to fuse cloud, automation, and security capabilities into increasingly business-facing ones (e.g. data, apps, and organizations) as quickly as it can be accepted".
Thank you for connecting with me. The reason for my email today is because I was reading through your LinkedIn profile. More importantly, you specifically mentioned your expertise in technologies like "Docker Containers, Virtualization, Kubernetes."
Thank you for connecting with me. The reason for my message is because I couldn't stop reading on about your use of server automation with continuous integration. But more importantly, you specifically mention using tools like Jenkins, Ansible, and Kubernetes.
Thank you for connecting with me. The reason for my message is because I couldn't stop reading on your LinkedIn profile about your implementation of DevOps and DevSecOps methodologies. One line that really stood out to me was that you've been responsible for the "quality center of excellence and success of the software test automation initiative."
Thank you for connecting with me. The reason for my message is because I enjoyed watching your presentation "Stream SQL with Flink." What really stood out to me was when you said that "Everytime we introduced a more powerful tool, the data produced into the same infrastructure is growing." I couldn't agree more.
Thank you for connecting with me. The reason for my outreach is because I was reading through your LinkedIn profile and noted your expertise on K8s and GCP. One line you wrote that stood out to me was, "Implementation of DevOps best practices and consultancy on organizational and technological transformations."
Thank you for connecting with me. The reason for my outreach is because I was reading through your LinkedIn profile relate to your interest in automating everything. One line that really stood out to me was how you led the, "DevOps efforts around automating build and integration pipelines to ensure the repeatability and stability of builds, incorporating security hardening, product testing and metrics gathering."
Thank you for connecting with me. The reason for my outreach is because I was reading through your profile and was struck by this line you wrote; "Designing and building CICD infrastructure for automatic code deployments to AWS."
Thank you for connecting with me. The reason for my reaching out to you is because I was reading through your LinkedIn profile about your experience with DevOps. But more importantly, you specifically mentioned using tools like Azure DevOps, Salt, and F5.
Thank you for connecting with me. The reason I'm reaching out to you is because I enjoyed reading your LinkedIn article "Culture Shift, Transformation, and Change." One line That stood out to me is your exhortation that "Investments in [...] #Automation, #Simplification", [...] #AppModernization, #DigitalTransformation, etc., hold promise. Pursuing these promises require us to act. And we need to act now."
Thank you for connecting with me. The reason I'm reaching out to you is because I enjoying watching your video "Citrix-as-a-Service in 3 minutes." One line that resonated with me was where you said, "What if there was a way to simplify the management of your Citrix to eliminate the need for upgrade and updates, to have a highly available management component, that is deployed in hours instead of weeks."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about how you drive adoption of CI/CD. But more importantly, you listed using tools like JIRA, Jenkins, Ansible, and Kubernetes.
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about how you've lately been playing with Big Data. One line that really stood out to me was how you're focused on "Bringing DevOps and CI/CD concepts to Data Pipelines."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about your work in DevOps. One line that stood out to me was that you're "Responsible for managing all back-end integrations, transactional site operations, and middleware application development"
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience as a DevOps consultant. One line that stood out to me was how you've provided "CI/CD for multiple environments through Git, Azure DevOps, and ARM Templates and into the Azure Cloud environment."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience with creating a fully automated CI/CD environment. One line that stood out to me was where you wrote, that you created a "Jenkins parallel pipeline tests to reduce test time and improve test coverage."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your history with CloudOps. But more importantly, you specifically mentioned using tools like Azure, Jenkins, and Docker.
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your work with Agile methodology and microservices.
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile and couldn't help but take note of your integral role in architecting SRE. One line that stood out to me was that you're "I’m defining requirement and integrating monitoring and alerting tooling."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile and noticed that you've used GitLab, alongside other tools like Jira, Confluence, and BitBucket.
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was how you're "Leading the build infrastructure charter using GIT, bit-bucket and artifactory."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was that you emphasize "the importance of adopting an Agile mindset across the entire organization, not just IT."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was your role in "develop and align the product roadmap across all ITIL processes."
Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your profile and appreciate what you wrote about finding freedom and therapy in coding.
Thank you for connecting with me. The reason I'm writing to you is because I was reading through your LinkedIn profile and couldn't help but notice your experience with building a DevOps team. One line that stood out to me was your "strong knowledge of the infrastructure and services needed to support the demands of large enterprise companies."
Thanks for connecting with me.
Thanks for connecting with me. The reason I wanted to connect with you was because I read on your LinkedIn profile about how you're working in a DevOps/SRE capacity. One comment that really stood out was how you said that "I think this RPA is key to DevOps success factors...  I would use this for continuous testing, developing, etc."
Thanks for connecting with me. The reason I'm writing you today is because I enjoyed reading your article, "7 Laws that Guide Tech Leaders." I couldn't agree more with your sentiments around Conway's law and how to mitigate it's effects.
Thanks for connecting with me.The reason I'm writing to you today is because I was reading through your LinkedIn profile about how you've successfully built out teams covering DevOps.
The purpose for my email is because I saw a video from BlackHat 19 titled "What still scares you about Cybersecurity?" It really resonated when you said, "It really just comes down to security having a hard time keeping up."
The purpose for my outreach is because I was reading on your LinkedIn profile about your experience in DevOps.
The purpose of my email is because I noticed on your LinkedIn profile that you mention using GitLab. But more importantly, you specifically mentioned that your work is in appyling "statistical and programming knowledge to various types of laboratory and clinical data" to produce reports and analysis data.
The purpose of my email is because I was enjoying watching your presentation from AWS re:invent 2015. The part "Our vision is actually continuous deployment, where deployments into production are all automatic, and deployment approvals are built in."
The purpose of my email is because I was reading on LinkedIn how you lead DevOps and Infrastructure services.
The purpose of my email is because I was reading your LinkedIn profile. You mention that your current area of focus is on "security controls commensurate with the risk profiles and risk tolerances of business."
The purpose of my email is because I was watching your interview with Apigee where you discuss how APIs help Shutterfly bring the right products to market quickly.
The purpose of my outreach today is because I thoroughly enjoyed your article "The Toughest Software Bugs." The line that really resonated with me was, "the toughest bugs are the oldest bugs [...] the faster you find it, the easier it is to deal with." I couldn't agree more.
The purpose of my outreach today is because I was enjoying your interview from the CISO Security Vendor Relationship Podcast. One line you shared that really resonated was, "We all have a mountain of work to do in security, there's always debt, there's always new things to clean up."
The purpose of my outreach today is because I was reading on your LinkedIn profile about you're using Kubernetes to enable increased speed, quality, and hygiene while reducing toil.
The purpose of my outreach today is because I was reading your LinkedIn profile how you lead the data management strategies to enable BI analysis and reporting.
The reason for my email is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was how you created a team of QA testers and put "testing process in place and reducing number of code release-related production issues by 35%."
The reason for my email is because I couldn't stop reading through your LinkedIn profile. The line that really stood out to me was, "I love solving puzzles, and solution architecture gives me the opportunity to solve business and technology puzzles which is why I love what I do." I appreciate and relate to that.
The reason for my email is because I couldn't stop reading your article, "Who will beat Amazon? Perhaps a native AI company?" One line that stood out to me was your comment on data warehousing, "even in today’s native internet company’s data is siloed in verticals and it is difficult to access data easily. So, for an AI company, it is critical to creating a unified data architecture right from the get-go. No more data stewards fighting over access and control over each other’s data."
The reason for my email is because I couldn't stop reading your Linked profile. One line that stood out to me was you "demonstrated success realizing business value through strategic planning and IT Architecture."
The reason for my email is because I couldn't stop reading your LinkedIn profile about how you've modernized Starbucks Loyalty. But more importantly, you specifically mentioned defining the operating models for IT "including DevOps,(Chef, Puppet and Jenkins)."
The reason for my email is because I enjoyed watching your panel with Vish and Annette. Your comment to "Make friends with your security organization" and to work with them because security is everyone's problem really resonated with me.
The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.
The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.
The reason for my email is because I read your post, "20:20 Vision in 2020" and saw that one of your goal of learning frameworks like Serverless. (I was also the kid always building with Legos)
The reason for my email is because I was reading a press release with TechRepublic where you said that "we're constantly evolving and optimizing our business" including your delivery of Zulily's big data platform.
The reason for my email is because I was reading an article on BankInfoSecurity where you were quoted as saying that shadow IT is a major concern because, "Anyone with a corporate card come in and download a cloud service."
The reason for my email is because I was reading on LinkedIn how you attended your 4th AWS re:invent and expanded your team's knowledge of serverless.
The reason for my email is because I was reading on your LinkedIn profile about your DevOps experience. One line that stood out was that you insulate your team from "administrative minutiae and specification roadblocks to maximize their productivity." I couldn't agree more, and that's what my favourite managers have always done for me.
The reason for my email is because I was reading on your LinkedIn profile about your focus on continuous integration. But more importantly, you specifically mentioned tools like Hudson/Jenkins and Groovy.
The reason for my email is because I was reading on your LinkedIn profile how you're an "Evangelist for building solutions leveraging DevOps and Dev-SecOps principles and practices."
The reason for my email is because I was reading on your LinkedIn profile how you're an evangelist for next-gen data engineering. You specifically mentioned using Docker and CI/CD as part of your architecture.
The reason for my email is because I was reading on your LinkedIn profile how you're building the Cloud Foundation Services. One line that really stood out was that you're looking to make it "an automated, repeatable, and secure shared platform [...] that allows them to innovate faster and deploy more frequently."
The reason for my email is because I was reading on your LinkedIn profile that your specialties include DevOps, CI/CD, and Containerization.
The reason for my email is because I was reading through your LinkedIn profile about your experience, and it was really inspiring to see that you've "Led IT Cloud Strategy to enable 34% cost savings, faster time to market, and hyper scale capability on demand."
The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you mention you're an "Autonomous contributor of thorough work and end-to-end solutions." But more importantly you mentioned using tools like Jenkins, Kubernetes, and Chef.
The reason for my email is because I was reading through your profile about your DevOps experience, and you specifically mentioned tools like Jenkins and JIRA.
The reason for my email is because I was reading your LinkedIn profile. What stood out to me is that your specialties are in DevOps engineering and CI/CD automation.
The reason for my email today is because I get the feeling that maybe I got my research wrong and DevOps and digital transformation aren't all that relevant to you.
The reason for my email today is because I read an article recently about being relevant and always bringing value to your clients, and I was conflicted about that last email I sent.  Maybe I got my research wrong and DevOps is old-hat and not all that compelling to you.
The reason for my email today is because I was reading on your LinkedIn Profile how you're implementing serverless micro-service application architecture. One thing that stood out to me was that you're "Passionate about scale and reliability, we are on a journey to achieve the 12 factor app."
The reason for my email today is because I was reading on your LinkedIn profile. One line that really stood out was that you're passionate about "designing innovative solutions to high impact problems."
The reason for my email today is because I was reading your LinkedIn profile and noticed that you list GitLab under your skills. But more importantly, I also saw that Fred Hutch recently spun up two instances of GitLab.
The reason for my email today is because you had previously mentioned that 2020 might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.
The reason for my email today is because you had previously mentioned that mid-January might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.
The reason for my email today is because you had previously mentioned that the beginning of this year might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.
The reason for my message is because I came across your post on growing up professionally at Gateway in '92, and how you've seen amazing cultural changes at UOPX the last two years. I couldn't agree more with you that culture is key.
The reason for my message is because I couldn't stop reading through your LinkedIn profile. Couldn't agree more when Patton said, "Do everything you ask of those you command." But more importantly, you mentioned leading teams in "24×7 operations for web delivered application development."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. I couldn't agree more when you wrote that "Some of the most successful people think outside the box and utilize creativity." But more importantly, you mention that your role is in "delivering global solutions in the areas of robotic process automation, digital transformation and application integration."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was that you're "directly involved in the development and implementing of multiple technologies and hosting platforms."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was where you said "Organizations looking to improve workforce productivity need to start by delivering a seamless user experience."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was your "Focus on providing solutions that increase efficiency and performance while maintaining an economic upside to the user."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you "Created a new Devops Organization focused on Products and Services for FinTech."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you've "Evangelized new technology trends, and identify opportunities to improve product productivity and performance."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was where you said that you are "highly driven at guiding business strategy with established and emerging technologies to achieve maximum productivity impacts with minimum resource expenditures."
The reason for my message is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you've generated the " continuous integration processes to support build and release pipeline via Microsoft Azure DevOps to support agile software development efforts."
The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me is that you're supporting the "software development and design, deployment of cloud services."
The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was how you "Increased team efficiencies, predictability and productivity by 45% through development practices and process improvements."
The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was how you migrated "multiple legacy backend services from Intuit Data Center to Kubernetes" without impacting the apps in production.
The reason for my message is because I couldn't stop reading your LinkedIn profile. What stood out to me was that you "worked at a financial institution building apps and microservices for a merger."
The reason for my message is because I couldn't stop reading your LinkedIn profile. What stood out to me was your use of DevSecOps and OWASP.
The reason for my message is because I couldn't stop reading your LinkedIn profile. Your line that "Future is Digital" resonates with me. But more importantly, you mention being familiar with DevOps and tools like JIRA, Confluence, and Jenkins.
The reason for my message is because I enjoyed reading the Slalom blog post of their Q&A with you. The line that I really resonated with was where you said that "Business is a team sport, and you succeed and win together."
The reason for my message is because I enjoyed reading your article "Negative is Normal." One line that stood out to me was " If you can find a diamond in the mud… then you will end up with many diamonds, my friend."
The reason for my message is because I enjoyed reading your articles on LinkedIn. One line from "Courageous Collaboration" that spoke to me was "The most important thing we can do to create this safe space is to consistently encourage not shut-down."
The reason for my message is because I was reading on your LinkedIn profile about your use of Azure DevOps. What stood out to me is that you were "brought on to build a new distributed, serverless API." What if there was a way to allow you to run your own serverless functions-as-a-service an any infrastructure, through a single UI, without the traditional vendor lock-in of cloud function services?
The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that you're working on a pretty cool project in IRSA. But more importantly, you mention using tools like Jenkins and Groovy.
The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that your "teams are responsible for instrumentation of POC’s to drive innovation forward."
The reason for my message is because I was reading through your LinkedIn profile and noticed that you used tools like Github and Maven.
The reason for my message is because I was reading through your LinkedIn profile resonated with your line that you're a "Linux lover." But more importantly, you specifically mentioned using tools like Docker and Kubernetes.
The reason for my message is because I was reading through your LinkedIn profile. One line that really stood out to me was that you've "Promoted microservices architecture, dev-ops culture, shared services, software as a service, shared infrastructure and applications to reduce costs and improve information flows."
The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "accomplish goals through effective communication, collaboration, and partnership."
The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you help businesses "embrace the cloud while automating tasks and increasing business productivity."
The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're using a "DevOps culture to enable businesses."
The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you've led "global organizations undergoing rapid growth and change in response to technology shifts."
The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your "responsibilities included automating all aspects of the SDLC and environment management."
The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was that you're "Responsible for the design of enterprise cloud architecture."
The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was your use of tools like Splunk and SSO.
The reason for my message is because I was reading trough your LinkedIn profile and noticed that you use GItLab for source control. But more importantly, you mention that you have an understanding of "all the technical capabilities of SaaS, PaaS, on-premise and IaaS from Integration and Extension perspective."
The reason for my outreach is because I was reading an article about FutureStores where you were quoted as saying, "We want to enable the interactions and not just show technology for the sake of it. The goal is to make the technology invisible and seamless to the customer experience.”
The reason for my outreach is because I was reading on your LinkedIn profile that you're currently leading your department through a DevOps transformation. One line that stood out to me was, "if you don't test it now, you'll have to debug it later." I couldn't agree more about.
The reason for my outreach is because I was reading on your LinkedIn profile that you're currently working on an enterprise-wide Agile transition and adoption.
The reason for my outreach is because I was reading through your LinkedIn profile about your responsibilities within infrastructure and operations. One line that caught my eye was, "driving service quality up and cost optimizations through adoption of innovative technology"
The reason I'm for my email is because I was looking at your LinkedIn profile and noticed that you're working on AI and ML using tools like Kubernetes. I'm currently working on an machine learning project as well and resonated with your headline; "Passionate about utilizing AI to develop next-generation technologies."
The reason I'm reaching out is because I was looking through your LinkedIn profile and noticed you use tools like Kubernetes. One line that stood out to me was you "experience leading initiatives in the SRE / DevOps and service development."
The reason I'm reaching out is because I was reading through your LinkedIn profile and noticed your focus on DevOps and cloud. What stood out to me was that you listed tools like Kubernetes.
The reason I'm reaching out to you is because I saw your post on LinkedIn about hiring an infrastructure automation engineer. But more importantly, a line that caught my eye from your profile is that you're "Executing shared DevOps vision."
The reason I'm reaching out to you is because I was reading on your Linked in profile where you specifically mentioned tools like JIRA, SVN, and Groovy.
The reason I'm reaching out to you is because I was reading on your LinkedIn profile how you're "Overseeing multiple projects run in continuous delivery mode." But more importantly, you specifically mentioned containerization and using Kubernetes.
The reason I'm writing to you is because I couldn't stop reading your article "Shifting Security to the Left" on DevSecOps.org. One line that really stood out was where you described how security documents are cumbersome and "ultimately burdens a software developer interested in playing their part." (Yikes! friendly fire)
The reason I'm writing to you is because I enjoyed your article on SoftwareTestingHelp about the Best Bug Tracking and Defect/Issue Tracking tools of 2020. One line that really stood out to me was where you said, "I am not a big fan of tools that are single-purpose [...] you want it to serve you in multiple ways."
The reason I'm writing to you is because I was reading on your LinkedIn profile that your specialty is in "Scaling and building large physical or cloud deployments."
The reason I'm writing to you is because I was reading on your LinkedIn profile where you mention your experience in DevOps. One line that stood out was how you're helping to "secure a collaborative data science platform and applications."
The reason I'm writing to you is because I was reading through your LinkedIn profile about how you've built scalable server-less solutions and a "mature CI/CD pipeline with CircleCI for our APIs with full integration testing process."
The reason I'm writing to you is because I was reading through your LinkedIn profile about your experience with DevOps, and one line you wrote caught my eye; "optimizing organizations for increased happiness at the workplace, and in turn, increased productivity." I couldn't agree more how important that is, and correct me if I'm wrong, it sounds like you're describing the ideals from Gene Kim's "The Unicorn Project."
The reason I'm writing to you is because I was reading through your LinkedIn profile and couldn't help but notice your experience with DevOps. One line that really stood out to me was your, "Leadership of a design, integration, orchestration, automation, and operations organization of DevOps, Systems Engineering, and Service Operations teams."
The reason I'm writing to you is because I was reading through your LinkedIn profile, and one line you wrote stood out; "Implementing solutions to verify application security during CI/CD to keep the features flowing." What if there was a way to have application security built into the CI/CD pipeline natively and have it work out of the box, using a tool you already have in house? This way you'd have implemented security standards where developers get immediate feedback for remediation.
The reason I'm writing to you is because I was reading your LinkedIn profile and saw that you led the DevOps initiative and transition "towards a true CI/CD environment."
The reason I'm writing to you today because I was reading your interview with CIO.com. One line that resonated with me was, “When you grow up in a world where you have to build everything in order to be successful, that gets baked into your DNA."
The reason I'm writing to you today is because I enjoyed your interview on "Beyond the Blue Badge." I couldn't agree more when you said, "Technology and technological advances power science." You also mentioned that the biggest challenges of your role are around digital transformation, modernization, moving to the cloud, and optimizing.
The reason I'm writing to you today is because I enjoyed your keynote from DevOps Days Boston on "Settlers of DevOps." It really resonated with me when you made the distinction that it needs to be a "pull nature" to be successful.
The reason I'm writing to you today is because I enjoyed your series of articles on DevOps. One line that really resonated with me is from part 4, where you said, "Infrastructure is hardest to automate but most critical in my mind."
The reason I'm writing to you today is because I was reading on your LinkedIn profile how you're leading the automation of DevOps infrastructure build-out from the ground up.
The reason I'm writing to you today is because I was reading on your LinkedIn profile that you list Jenkins under tools & technologies. Other security architects I speak with tell me that self-service Jenkins can be difficult to secure and the plugins often introduce vulnerabilities.
The reason I'm writing to you today is because I was reading the case study of your use of Prevoty RASP, where you discussed how it "has been effective at covering gaps and providing risk reduction that allows us to focus on engaging engineering teams early in the lifecycle."
The reason I'm writing to you today is because I was reading through your GitHub page and noticed that one of your current projects includes using GitLab for build-time container scanning. But more importantly, you also said that you're working to "Optimize DevOps resources by migrating business applications to a Kubernetes cluster".
The reason I'm writing to you today is because was reading through your LinkedIn profile about how you configured a fully automated CI/CD pipeline ("utilizing CircleCI to build Docker images from version control and push them to Docker Hub and Jenkins to deploy containerized applications into prod.")
The reason I'm writing you today is because I was reading through your LinkedIn profile and noticed your interest in tools like GitLab, Jenkins, and Ansible.

The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was that you're using tools like Jenkins, Docker, Kubernetes, and Chef.

The reason for my letter is because I couldn't stop reading through your LinkedIn profile. The line that resonated most with me was where you wrote, "I love enabling people to be better with technology and I relish the feedback. As I was once told: 'If people are not complaining about your solution is because they are not using it.'

The reason for my letter is because I thoroughly enjoyed reading your article "The 10 Best Books I Read in 2019." But more importantly, as I was reading through your LinkedIn profile, one line stood out, that you "partner with my peers on both Product and Business at a strategic level to translate business imperatives into roadmaps."

The reason for my message is because I was reading through your LinkedIn profile where you talked about reducing the end-to-end timein deploy & test. But more specifically, you mention using tools like JIRA, Jenkins, and Groovy.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that "both Fast and Good IT are what our business demands - and I have the responsibility to help IT transform to be a dynamic and value creating organization."

The reason for my email is because I was reading through your LinkedIn profile. What resonated with me was where you wrote, "We are taking a DevOps enablement approach that allows teams to select and configure tools in harmony with the uniqueness and collaborative synergies of their processes."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you're "Responsible for Operational excellence and enabling foundational capabilities tied to ALM and DevOps in Enterprise Applications ."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "Developed and deployed an effective DevOps capability in a startup R&D environment."

The reason for my email is because I was reading through your LinkedIn profile, and it stood out to me that you specifically mention DevOps as part of your technologies in Data.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "Developed and deployed an effective DevOps capability in a startup R&D environment."

The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience with creating a fully automated CI/CD environment. One line that stood out to me was where you wrote, that you created a "Jenkins parallel pipeline tests to reduce test time and improve test coverage."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you manage tools like Atlassian JIRA, Confluence, Crucible, and more.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you're "Leading the transformation of monolith Billing and Commerce systems to Microservices based architecture."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically mentioned using tools like Jenkins, Puppet, and Chef.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically mentioned your "core competency in Programming, DevOps and automation frameworks." 

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote on your about.me that "Ideas lead to concepts, concepts lead to innovation, innovation leads to change, change leads to transformation which in turn leads to new ideas -- Its a 'vicious' circle !!"

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you've developed "Jenkins files to automate the entire CICD process for corresponding services."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you've previously built the "entire CI/CD pipeline and automated database deployments across environments."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically that you've designed and built out "mechanisms for continuous delivery supporting all Intuit development teams."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you've architected the "CI/CD pipeline to automate the application deployments using AWS, Docker, and Kubernetes."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you lead the teams responsible for" enterprise applications, internal application development, and endpoint management." But more importantly, I noticed that you're using Atlassian JIRA and Confluence, alongside Okta.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention under skills that you're using tools like JIRA, Kubernetes, and Maven.

The reason for my email is because I was reading through your LinkedIn profile. Very cool that you received your Kubernetes certificate from The Linux Foundation.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you're chartered with improving the experience for developers and automating common tasks for infrastructure and tool creation.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you've "Led, architected, designed and developed end-to-end Engineering solutions for all phases of SDLC."

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention High Availability and Disaster Recovery under your industry knowledge section.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you're using tools like Kubernetes and Docker.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you're using GitHub and JIRA.

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that as part of ensuring 100% uptime, you're using "tools like AWS, Spinnaker, Jenkins, Terraform."

The reason for my email is because I couldn't stop reading through your LinkedIn articles. Really enjoyed the 27 life lessons, #21 definitely resonated with me. But more importantly, you mention that you're "Currently leading Twitch’s Data Platform Team. We build Infrastructure, services and tools to transform data into insights for all of Twitch."

The reason for my email is because I saw that you shared the HBR article "Don't Bring Me Problems, Bring Me Solutions" which I enjoyed as well. But more importantly, you mention that your teams have "successfully increased capacity and efficiency of our infrastructure while continuing to drive down our on-call burden."

The reason for my email is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "Responsible for writing integration and reusable components."

The reason for my email is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you're currently "working on tooling and infrastructure to enable faster and more meaningful feedback loops, increased stability, and automation for development engineers."

The reason for my message is because I was reading on your LinkedIn profile that you "introduced Agile, CI and DevOps practices and tools to improve application delivery."

The reason for my message is because I was reading on your LinkedIn profile that you're using tools like Kubernetes and VMWare.

The reason for my message is because I was reading on your LinkedIn profile that you're a "Leader in containerization POC, creating the automation patterns for Docker adoption."

The reason for my message is because I was reading on your LinkedIn profile that you're "Responsible for leading the rearchitecture of the security operation."

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that you're using the Atlassian tool suite.

The reason for my email is because I was reading through your LinkedIn profile, and I thoroughly enjoyed your article on the Design Thinking. One line that stood out to me was "Create a feedback loop with the customer to get early feedback before iterating further. Fail fast and early is always better while building user-centric products."

The reason for my email is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you're currently establishing "the role of reliability engineering by merging the product development, security and operations (DevSecOps) talent into one team, increasing efficiencies companywide."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is your expertise in "standardizing DevOps initiatives, practices and schedules on an enterprise level."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is your "Experience leading teams using Agile/DevOps methodologies in product and feature development."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out where you wrote that you "Drive faster digital transformation and technology adoption"

The reason for my message is because I was reading your LinkedIn profile. One line that you mention DevSecOps as an area of interest.

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is that you "love technologies that simplify life for worldwide consumers."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is how you "Provided leadership for deployment of a serverless solution in Microsoft Azure fully supported by CI and CD and end to end automation."

The reason for my message is because I was reading on your LinkedIn profile that you're using tools like Kubernetes. But more importantly, one line that stood aut is that you're "building scalable micro-services architecture based applications."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your "Data driven approach to deliver e2e solutions following vanilla SDLC and DevOps (agile) lifecycles."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Initiated automation training for manual QA engineers using Java, WebDriver, TestNG, Git, Maven, and Spring."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your focus on "Enterprise Architecture, Integration, Security, Cloud, Containers/Clustering, DevOps and Big Data." But more importantly, one line that stood out is your "Keen ability to “tame”/”de-fud” technology and lead technical teams to produce consistently architected/scalable/maintainable software systems."

The reason for my email is because I was reading your LinkedIn profile. One line that stood out to me was that your teams are "responsible for running all of Yelp's technical infrastructure, as well as software development for core shared systems and developer tooling."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your focus on "covering Enterprise Architecture, Integration, DevOPS."

The reason for my message is because I was reading your LinkedIn profile about your use of DevOps." One line that stood out to me was your "Primary focus on fast-paced problem solving."


The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "transformed teams to enable continuous agile delivery and DevOps culture to focus on digital Products and Services for People Technology."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "Accelerate product delivery in full software lifecycle including CI/CD and DevOps" in order to deliver  business capabilites at scale.

The reason for my message is because I was reading your LinkedIn profile and found your DevOps teams' mantra to be inspiring.

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "Manage a team of devops teams in our Roasting plants, these teams deliver operational infrastructure that helps drive the management and delivery of roasting operations."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you're "Building a CIP strategy for IT that brings together DevOps, Lean and Agile for IT modernization."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that, "As a leader of our organization's DevOps and Lean transformation journey, I learn and coach the CALMS model (Culture, Automation, Lean, Measurement, Sharing) using transformational leadership."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Established services framework to streamline service creation, testing, CI/CD, monitoring and alerting"

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Defined product roadmap that decentralized release management, increasing the rate of releases per year by 25x"


The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you've "Built from the ground-up, a team of DevOps engineers utilizing a global follow-the-sun support model to convert IDPS into a fully managed service."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're currently leading the "Tech Infrastructure / Applications Operations (DevOps)."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you  "Lead the transformation of the engineering organization to devops model."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you  "provide leadership and direction on the transformation / modernization of our application development methodologies (Agile, DevOps)."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you are  "Defining & driving KPIs for Incident management like MTTD, MTTE and MTTR."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that your "team's core focus is to make Shutterfly sites fast and reliable for millions of users."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're managing teams across DevOps and security.

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Created hybrid best-in-class build systems using Python, Bash, Maven, Makefile, Docker and Jenkins pipeline scripts to create stable Java build infrastructure."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Instill DevOPS and Quality mindset into the team by driving people and
tooling transformation, getting more automation in place and improving release velocity."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're "Passionate about establishing an environment that balances developer productivity and effectiveness with the discipline necessary for reliable software production."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're "Leading teams with vision and strategy for increasing automation, templating, archetypes increasing DevOps maturity and software delivery from months to hours."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're "Driving migration to Kubernetes for Intuit Developer Platforms."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using tools like Kubernetes, Docker, and Maven.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you "Developed new training process, utilizing Docker, Kubernetes and VMWare based servers to speed up environment set up issues, resulting in successful new hire issue handling in less than three weeks." What a great achievement.


The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using "CI/CD, Kubernetes, Golang, Docker, Jenkins, TeamCity, Microservices, Typescript, React"

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using "Microservices for the platform with technology stack based on Azure Kubernetes(AKS), Docker, Kafka"

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Transformed the org to shift left by merging Software Configuration Management and Operations teams to DevOps and quality with development engineers, employing continuous performance testing."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Led a group of cross-functional distributed architects & technical leads (10) to come up with detailed architecture and high-level design including CI/CD Pipelines & Testing Stacks."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using tools like Kubernetes on Azure.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you "Created strategy for kubernetes and containers for tier1 applications to deliver persistent storage, replication and HA."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you're "transitioning into a technical systems engineering role responsible for monitoring cloud (AWS and Azure) operations, financials and governance."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "own Product Mgmt strategy & PM execution for our Cloud Data application stack, architecture & ecosystem.""

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "Lead the NetBackup and Cloud Solutions product portfolio for private/public cloud, virtualization, disaster recovery, BaaS/SaaS solutions etc."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you created the "QA UI Design Analyst to apply quality checks into the early design process reducing the number of late cycle UI/UX issues." What a great creative solution.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "Drove transition to SaaS and Cloud/container capabilities and business models."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that as "Product owner for Starbucks IOT initiatives," you're using DevOps and agile practices.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you're "Transforming the technology to cloud native - Kubernetes, Docker."


The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "transforming IT as a service and modernizing technology."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Partner with engineering teams to innovate, build, and implement enterprise-wide integration strategies."

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that as part of your technologies, your teams are using tools like JIRA, TFS, and Jenkins.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that in addition to using DevOps, you "Collaborate with other IT Solution Delivery teams to share knowledge, ensure alignment and demonstrate unified IT solutions and services.

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're "Partnering with cross-functional teams to deliver our cloud strategy of moving more than 100 PB of user images out of Data Center to AWS."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Led cloud implementation of highly scalable, available and performant micro services utilizing Azure Event Hubs"

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you listed "continuous integration, docker, and test automation" under your skills, a

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're leveraging "Azure, and CI/CD tooling to develop highly performant systems to process thousands of requests across multiple systems."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you led the effort to allow workloads to AWS, Azure, and GCP, and that you also "Led efforts to "Dockerize" all products in my group."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "delivering data engineering automation, dev ops and developing self-service deep analytics capabilities."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "Led IT Cloud Strategy to enable 34% cost savings, faster time to market, and hyper scale capability on demand"

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you're "Managing and migrating Infrastructure From instances to Micro services using Lambda, EKS, IKS(intuit kubernetes services."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you empower "developers to efficiently build delightful experiences for our community, crafted upon a foundation of security and reliability."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Established a process tailored agile SDLC framework across a global team that helped reduce the project delivery times by half."

What if there was a way to reduce project delivery times even more, and allow your teams to ship software faster by using a DevOps platform?

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Moved the company from a review-checkpoint model to a comprehensive security program including threat modeling, code analysis, and automation, with education and tools to allow engineering teams to assess and improve their own security."

The reason for my message is because I was reading your LinkedIn profile and couldn't help but notice your use of SAST/SCA/DAST. But more importantly, one line that stood out to me is where you mention that you "Initiated DevSecOps movement within the organization to bring better collaboration and instill a “shift left” mentality between Development, Security and Infrastructure teams."

The reason for my message is because I was reading your LinkedIn profile, where you mention that you "Lead IT security operations." What if there was a way to build application security capabilities into the CI/CD workflow where the developers live? (Thereby allowing devs to identify vulnerabilities and remove them early.)

The reason for my message is because I was reading your LinkedIn profile, and your objective resonated with me. This line specifically struck me as important, your question of "How can we empower the rest of the organization to be security champions, rather than requiring our team to handle everything?"

Here's an idea. What if there was a way to build security capabilities are built into the CI/CD workflow where the developers live. This allows devs to identify vulnerabilities and remove them early. At the same time, this also provides your security team a dashboard to view the items not addressed by the developers.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're building and enhancing "Big data, Analytical and Search solutions from the ground up."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you created a "scale-out shared-nothing architecture."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Ansible and Puppet for automation.

The reason for my message is because I was reading your LinkedIn profile about your building of tools and automations within Salesforce. But more importantly, you specifically mention "implementing a total development cycle including Git, Continuous Integrations, Code Review and Code Quality Guidelines."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out is that you established "Platform as a Service (PaaS) to drastically speed development and enable key best practices with code."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "leading a technology transformation to leverage the cloud, big data, machine learning and artificial intelligent knowledge based systems."

The reason for my message is because I was reading through your LinkedIn profile and as a Sr Manager of Infrastructure and DevOps, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you delivered technology standards that fueled "platform architecture and faster development productivity."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you mention "Engineering and Optimization; Site Reliability Engineering."

Correct me if I'm wrong, but at the end of the day when it comes engineering optimization and site reliability engineering, having a DevOps platform that's easy to use is key. What if there was a way have an  end-to-end  platform that allows your teams to increase developer productivity, while increasing operational efficiency? 

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you've driven cross functional programs and initiatives  "to reduce risk, improve security/compliance awareness."

The reason for my message is because I was reading through your LinkedIn profile and noticed that you listed Enterprise Architecture under skills.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you are "Managing all aspects of Site Reliability Engineering (SRE), DevOps, and Database Engineering functions."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you were "Leading a team that focuses on Developer Productivity, Tech-Hygiene, Cloud Hosting, Security, GDPR, and Operational Excellence.."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your experience includes "Business systems implementation, integration and program/project management experience."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "restructured the entire operational framework using agile and DevOps methods to start consistently meeting SLAs and KPIs and, by designing and spearheading the migration to cloud, we saved millions from the IT budget while having almost no impact on business because of close collaboration across all business functions." What an achievement.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that "Engineering Effectiveness is responsible for making development fast, efficient and easy for the 850+ engineers."

The reason for my message is because I was reading through your LinkedIn profile, and as because of your role in enterprise architecture and integration, I thought this might be relevant to you.

The reason for my message is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you "Facilitated the migration of the team to modern source code control technologies and best practices with Azure DevOps."

The reason for my message is because I was reading your LinkedIn profile, and because you mentioned that you were "Part of the Engineering Effectiveness organization whose focus was to find ways to make engineer work better, faster and more efficiently." As such, I thought this might be relevant to you. 

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that your "Proven accomplishments include driving enterprise level technology transformation initiatives as a C-level leader and orchestrating organizational effectiveness and change initiatives."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you've developed "process improvements to achieve cost effectiveness and time saving."

The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was that your team built a "best of breed platform: microservices, distributed architecture, running on public cloud, built and delivered using DevSecOps practices."

The reason for my message is because I was reading your LinkedIn profile, and because you mentioned that you're "Well practiced in software development and QA methodologies (Agile, Scrum, Waterfall, automated testing, continuous integration" I thought this might be relevant to you. 


The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that "Engineering Effectiveness is responsible for making development fast, efficient and simple for all engineers at Yelp."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that your "goals as a leader are to provide autonomy and empowerment to my team; and to develop self-organizing delivery teams."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Champion design, coding and operational best practices, with in-depth design, security and PR reviews."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you're a "Thought leader in the field of information security architecture and process."

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Ansible and Puppet. But more importantly, you talk about having a "passion for turning around unstable infrastructures."

The reason for my message is because I was reading your LinkedIn profile, and as a software engineering manager, I thought this might be relevant to you. 

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Jenkins, Circle CI, and Docker.

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using infrastructure tools like "Puppet, Ansible, Terraform, Dockers."


The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "Excel at leading end-to-end SDLC and enhancing existing tools for measurable impact"

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you are "currently responsible for managing application architecture, development & execution of end to end Quote to Cash business process."

The reason for my message is because I was reading through your LinkedIn profile and wanted to congratulate you on your new role as Head of Data and Platforms. One line that stood out to me was that you had "successfully scaled up a Data lake solution leveraging AWS S3. This provided a comprehensive data analytics platform for structured and telemetry data and built a predictive insights capability within the organization."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was this: "My ability to standardize and consolidate enterprise, business, and development systems—and to rationalize and streamline the procurement process—has consistently driven increased operational efficiency while reducing cost."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you've had "Success in continuously improving delivery team performance and outcomes by leading investment in Agile, Lean and DevOps practices."

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you. 

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in application security, I thought this might be relevant to you. But more importantly, because of your having been a part of the due diligence team for the Veracode acquisition, I thought your personal experience might be important.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security, I thought this might be relevant to you. 

The reason for my message is because I was reading through your LinkedIn profile. A couple of thing stood out to me, namely your work in "Kubernetes infrastructure migration: Dockerization, and containerization" and "CICD optimization"

The reason for my message is because I was reading through your LinkedIn profile. Because of your role in the EBS architecture, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you achieved a "Velocity increase of 54% over less than 12 months."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you were"Selected to lead program to build new state-of-the-art Data Center facility to support Inuit's high-availability and disaster recovery strategy."

The reason for my message is because I was reading through your LinkedIn profile and noticed that you mention DevOps under your skills section.


The reason for my message is because I was reading through your LinkedIn profile. Noticed that you're responsible for "core ERP systems" and EBS, but more importantly you listed DevOps under your core competencies.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you modernized the "server infrastructure to stabilize the environment which has reduced our technical support calls by 25% and re-architected legacy applications to improve bandwidth by 30%."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Responsible for Architecture Programs and Initiatives, Governance, Standards and Design for Shutterfly."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Experienced with designing and implementing highly available, scalable, and robust enterprise applications."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Well known for working across the organization to drive vision of the product and common business goals."

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security, I thought this might be relevant to you. 

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me is that you "implemented best practices across the Development, QA and DevOPs teams which significantly improved our ability to deliver new functionality."

The reason for my message is because I was reading through your LinkedIn profile. One line that I appreciate is that you're "Exploring MicroServices architecture for deployments with Zero down time."

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me was that you previously "Achieved a robust CI/CD pipeline to incrementally delivery features."

The reason for my message is because I was reading through your LinkedIn profile and because of your role as a software development manager, I thought this might be relevant to you. But more importantly you specifically mention using tools like Kubernetes.

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me was that you transformed "traditional Enterprise IT through DevOps teams embedded" within business units.

The reason for my message is because I was reading through your LinkedIn profile and appreciate your use of DevOps. But more importantly, you mention that you "Created and executed the DevOps function and team for the Enterprise Collaboration DevOps Solution Delivery team".

The reason for my message is because I was reading through your LinkedIn profile and appreciate your use of DevOps within QA. But more importantly, what stood out to me is that you're using tools like Jenkins, GitHub, and TFS.

The reason for my message is because I was reading through your LinkedIn profile and really appreciated your line about being a "Master Innovator". But more importantly, what stood out to me is that you're using tools like Kubernetes or Ansible, and even using Serverless/FaaS.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that under your leadership, "Starbucks Technology is focused on delivering innovative, cloud-based and digital solutions."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're known for "identifying strategic opportunities, driving innovation, accelerating revenues and reducing costs."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you were "Responsible for building and managing internal private cloud to provide IaaS and PaaS solution."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you compel and lead "with an outcome based approach to drive sustained results"

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you're "leading and transforming the Information Technology function for Veritas, including its infrastructure, systems, processes, and security."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "connect people, drive execution, and solve problems."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "leading agile teams with focus on delivering business value in line with enterprise objectives."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you we're previously "Leading high performing DevOps production development and support team."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "Simplifying the business of life @ intuit!"


The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "lead a team of amazing product leaders at TIBCO whose mission is to build products & technologies that interconnect everything, everywhere."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're a "Product Engineer and Technical Leader for a wide variety of business applications."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're working on the "developer platform team focusing on design and development of our primary web presence."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you manage 3 scrum teams and are accountable for the SMB Lending Platform, Payment Sign-up platform, and the Payment Center.

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "focused on replacing painful customer experiences with delightful product features"

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "believe developing teams and managing data are foundational to accelerating business results through technology."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "responsible for attracting the very best technical product talent, challenging them to innovate for customers."

The reason for my message is because I was reading through your LinkedIn profile, and because of your role as VP of CyberSecurity, I thought this might be relevant to you. 

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your teams "are responsible for designing, building, and operating the hosting platform in our internal data centers."

The reason for my message is because I enjoyed watching your keynote "Getting to 100% in the cloud." I couldn't agree more with your statement that "Complexity is the enemy" and that this is key to balancing speed, operability, and security.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you.

The reason for my message is because was reading through your LinkedIn profile. One line that stood out to me is that you are "Leading a team of world-class engineers in building the next generation Data Platform for Analytics."

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're the "Security leader for central platform engineering organization." 

The reason for my message is because was reading through your LinkedIn profile, and because of your role, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you. One line that stood out to me is that you're helping "secure a collaborative data science platform and applications."

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in cloud architecture, I  this might be relevant to you. One line that stood out to me is that you're helping "Scaling and building large physical or cloud deployments."

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in product and engineering, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in infrastructure services and DevOps, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in DevSecOps, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in DevOps and SRE, I  this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in infrastructure services, I  this might be relevant to you. One line that stood out to me is that you're leading the "Development and Transformation of Zumiez Infrastructure Cloud Strategies."

The reason for my email is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "covering end-to-end development of client, server & database tiers."

The reason for my email is because I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.



The reason for my message is because I was reading through your LinkedIn profile, and because of your role in software engineering, I thought this might be relevant to you. One line that stood out to me is that you "are implementing a serverless micro-service application architecture using Kubernetes and Kafka for our event-driven system deployed to AWS."

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in  engineering, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in software engineering, I thought this might be relevant to you. One line that stood out to me is your focus on "large-scale distributed systems, web applications, and devops"

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in engineering, I thought this might be relevant to you. One line that stood out to me is that you're implementing systems automation using tools like "Puppet/Ansible/CloudFormation/Terraform."

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in DevOps, I thought this might be relevant to you.

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in global infrastructure and operations, I thought this might be relevant to you.

The reason I'm writing to you today is because I was reading through your LinkedIn profile. One line that resonated with me was where you said that being prescriptive "is the wrong way to be agile, focus on the outcomes you want. Big transformation can lead to a failure to launch."

The reason for my message is because was reading through your LinkedIn profile. One line that stood out to me is that you are "Leading a team of world-class engineers in building the next generation Data Platform for Analytics."

The reason I'm writing to you today is because I was listening to your interview on "Beyond the Blue Badge." One line that resonated with me was where you said that "we all come together to solve this giant complex problem, and in order to do that we can't do that we can't be working in silos."

The reason for my outreach is because I was reading on your profile how you're leading secure software delivery strategies and systems. It's especially refreshing to read that you're building the processes into the applications and infrastructure (rather than having security be separate). 

The reason I'm reaching out is because I read on your profile that you were able to lower the TCO by simplifying and modernizing VSP's system landscape.

The reason I'm writing to you is because I couldn't stop reading your article "Shifting Security to the Left" on DevSecOps.org. One line that really stood out was where you described how security documents are cumbersome and "ultimately burdens a software developer interested in playing their part." (Yikes! friendly fire)

The reason I'm writing to you is because I was reading your LinkedIn profile and saw that you led the DevOps initiative and transition "towards a true CI/CD environment."

The purpose of my outreach today is because I was reading your LinkedIn profile how you lead the data management stratgies to enable BI analysis and reporting.

You mention on your profile that you're using JIRA and working on .NET, which might mean that you're using some form of Azure DevOps or TFS.

The reason I'm writing to you is because I enjoyed your article on SoftwareTestingHelp about the Best Bug Tracking and Defect/Issue Tracking tools of 2020. One line that really stood out to me was where you said, "I am not a big fan of tools that are single-purpose [...] you want it to serve you in multiple ways."

The reason I'm writing to you today is because I was reading on your LinkedIn profile how you're leading the automation of DevOps infrastcuture build-out from the ground up.

The purpose of my email is because I was enjoying watching your presentation from AWS re:invent 2015. The part "Our vision is actually continuous deployment, where deployments into production are all automatic, and deployment approvals are built in."

I saw on LinkedIn that you liked a post about the Cloud-Native Kubernetes meetup coming up on Jan 16th.

The purpose of my outreach today is because I was reading on your LinkedIn profile about you're using Kubernetes to enable increased speed, quality, and hygiene while reducing toil.

The reason I'm writing to you today is because I enjoyed your series of articles on DevOps. One line that really resonated with me is from part 4, where you said, "Infrastructure is hardest to automate but most critical in my mind."

You mention on LinkedIn that you're responsible for creating an Operational Data Lake and the migration of the data platform to AWS.

The purpose of my outreach today is because I was enjoying your interview from the CISO Security Vendor Relationship Podcast. One line you shared that really resonated was, "We all have a mountain of work to do in security, there's always debt, there's always new things to clean up."

The reason I'm writing to you today is because I was reading the case study of your use of Prevoty RASP, where you discussed how it "has been effective at covering gaps and providing risk reduction that allows us to focus on engaging engineering teams early in the lifecycle."

You mentioned in your case study for Sophos Cloud Optix that "Our compliance team is now able to run reports for compliance audits in seconds, which was previously manual and exceedingly time consuming."

The reason I'm writing to you today because I was reading your interview with CIO.com. One line that resonated with me was, “When you grow up in a world where you have to build everything in order to be successful, that gets baked into your DNA."

The purpose of my email is because I was watching your interview with Apigee where you discuss how APIs help Shutterfly bring the right products to market quickly.

You mention on LinkedIn that part of your responsibilities included " significantly increas[ing] interdepartmental efficiencies." 

The reason for my email is because I was reading on your LinkedIn profile how you're an evangelist for next-gen data engineering. You specifically mentioned using Docker and CI/CD as part of your architecture.

The purpose for my email is because I saw a video from BlackHat 19 titled "What still scares you about Cybersecurity?" It really resonated when you said, "It really just comes down to security having a hard time keeping up."

In a video interview titled "Worst Security Vendor Email Pitches" from BlackHat 2019, your example was when vendors send an email and the follow ups progressively become more and more desperate.

The reason I'm writing to you today is because I was reading on your LinkedIn profile that you list Jenkins under tools & technologies. Other security architects I speak with tell me that self-service Jenkins can be difficult to secure and the plugins often introduce vulnerabilities.

The reason for my email is because I was reading on LinkedIn how you attended your 4th AWS re:invent and expanded your team's knowledge of serverless.

The purpose of my outreach today is because I thoroughly enjoyed your article "The Toughest Software Bugs." The line that really resonated with me was, "the toughest bugs are the oldest bugs [...] the faster you find it, the easier it is to deal with." I couldn't agree more.

In your article "Software Security is Not An Option", you place the onus on the software professional to "make sure that software security is baked in from the beginning."


The reason I'm writing to you is because I was reading on your LinkedIn profile that your specialty is in "Scaling and building large physical or cloud deployments."

The reason I'm writing to you today is because I enjoyed your keynote from DevOps Days Boston on "Settlers of DevOps." It really resonated with me when you made the distinction that it needs to be a "pull nature" to be successful.

The reason for my email is because I read your post, "20:20 Vision in 2020" and saw that one of your goal of learning frameworks like Serverless. (I was also the kid always building with Legos)


The reason I'm writing to you today is because I enjoyed your interview on "Beyond the Blue Badge." I couldn't agree more when you said, "Technology and technological advances power science." You also mentioned that the biggest challenges of your role are around digital transformation, modernization, moving to the cloud, and optimizing.

The purpose of my email is because I was reading your LinkedIn profile. You mention that your current area of focus is on "security controls commensurate with the risk profiles and risk tolerances of business." 

The reason for my email is because I read your post, "20:20 Vision in 2020" and saw that one of your goal of learning frameworks like Serverless. (I was also the kid always building with Legos)

The reason for my outreach is because I was reading on your LinkedIn profile that you're currently working on an enterprise-wide Agile transition and adoption.

The reason I'm reaching out is because I was reading through your LinkedIn profile and noticed your focus on DevOps and cloud. What stood out to me was that you listed tools like Kubernetes.

The reason for my email is because I was reading an article on BankInfoSecurity where you were quoted as saying that shadow IT is a major concern because, "Anyone with a corporate card come in and download a cloud service."

The reason for my email is because I was reading through your profile about your DevOps experience, and you specifically mentioned tools like Jenkins and JIRA.

The purpose for my outreach is because I was reading on your LinkedIn profile about your experience in DevOps.

The reason for my email is because I was reading on your LinkedIn profile about your DevOps experience. One line that stood out was that you insulate your team from "administrative minutiae and specification roadblocks to maximize their productivity." I couldn't agree more, and that's what my favourite managers have always done for me.

The reason for my email is because I was reading a press release with TechRepublic where you said that "we're constantly evolving and optimizing our business" including your delivery of Zulily's big data platform.

The reason I'm for my email is because I was looking at your LinkedIn profile and noticed that you're working on AI and ML using tools like Kubernetes. I'm currently working on an machine learning project as well and resonated with your headline; "Passionate about utilizing AI to develop next-generation technologies."


The reason I'm reaching out is because I was looking through your LinkedIn profile and noticed you use tools like Kubernetes. One line that stood out to me was you "experience leading initiatives in the SRE / DevOps and service development."

The reason I'm writing to you today is because I was reading through your GitHub page and noticed that one of your current projects includes using GitLab for build-time container scanning. But more importantly, you also said that you're working to "Optimize DevOps resources by migrating business applications to a Kubernetes cluster".

The purpose of my email is because I was reading on LinkedIn how you lead DevOps and Infrastructure services. 

The reason for my email today is because I was reading on your LinkedIn profile. One line that really stood out was that you're passionate about "designing innovative solutions to high impact problems."

The reason for my email today is because I was reading on your LinkedIn Profile how you're implementing serverless micro-service application architecture. One thing that stood out to me was that you're "Passionate about scale and reliability, we are on a journey to achieve the 12 factor app."

The reason for my email is because I was reading your LinkedIn profile. What stood out to me is that your specialties are in DevOps engineering and CI/CD automation.


The reason for my outreach is because I was reading through your LinkedIn profile about your responsibilities within infrastructure and operations. One line that caught my eye was, "driving service quality up and cost optimizations through adoption of innovative technology"

The reason for my email is because I was reading through your LinkedIn profile about your experience, and it was really inspiring to see that you've "Led IT Cloud Strategy to enable 34% cost savings, faster time to market, and hyper scale capability on demand."

The reason for my email is because I enjoyed watching your panel with Vish and Annette. Your comment to "Make friends with your security organization" and to work with them because security is everyone's problem really resonated with me.

The reason for my outreach is because I was reading an article about FutureStores where you were quoted as saying, "We want to enable the interactions and not just show technology for the sake of it. The goal is to make the technology invisible and seamless to the customer experience.” 

The reason I'm reaching out to you is because I saw your post on LinkedIn about hiring an infrastructure automation engineer. But more importantly, a line that caught my eye from your profile is that you're "Executing shared DevOps vision."

Thanks for connecting with me. The reason I'm writing to you today is because I was reading through your LinkedIn profile about how you've successfully built out teams covering DevOps.

Thanks for connecting with me. The reason I'm writing you today is because I enjoyed reading your article, "7 Laws that Guide Tech Leaders." I couldn't agree more with your sentiments around Conway's law and how to mitigate it's effects.

The reason I'm writing you today is because I was reading through your LinkedIn profile and noticed your interest in tools like GitLab, Jenkins, and Ansible.

The reason I'm writing to you today is because was reading through your LinkedIn profile about how you configured a fully automated CI/CD pipeline ("utilizing CircleCI to build Docker images from version control and push them to Docker Hub and Jenkins to deploy containerized applications into prod.")

The reason I'm writing to you is because I was reading through your LinkedIn profile, and one line you wrote stood out; "Implementing solutions to verify application security during CI/CD to keep the features flowing." 

The reason I'm writing to you is because I was reading through your LinkedIn profile about how you've built scalable server-less solutions and a "mature CI/CD pipeline with CircleCI for our APIs with full integration testing process."

The reason I'm writing to you is because I was reading through your LinkedIn profile about your experience with DevOps, and one line you wrote caught my eye; "optimizing organizations for increased happiness at the workplace, and in turn, increased productivity." I couldn't agree more how important that is, and correct me if I'm wrong, it sounds like you're describing the ideals from Gene Kim's "The Unicorn Project."

The reason I'm writing to you is because I was reading through your LinkedIn profile and couldn't help but notice your experience with DevOps. One line that really stood out to me was your, "Leadership of a design, integration, orchestration, automation, and operations organization of DevOps, Systems Engineering, and Service Operations teams."

Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and couldn't help but notice your experience with leveraging microservices architecture. Two lines that stood out to me were how you "led development of an end to end solution" and streamlined efficiency through a microservice build system.

Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and was struck by your championing of DevOps at VSP. One line that really stood out to me was how you "Initiated DevSecOps movement within the organization to bring better collaboration and instill a 'shift left' mentality" between the teams.

Thank you for connecting with me. The reason for my outreach is because I was reading through your LinkedIn profile and noted your expertise on K8s and GCP. One line you wrote that stood out to me was, "Implementation of DevOps best practices and consultancy on organizational and technological transformations."

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience with creating a fully automated CI/CD environment. One line that stood out to me was where you wrote, that you created a "Jenkins parallel pipeline tests to reduce test time and improve test coverage." 

The reason for my outreach is because I was reading on your LinkedIn profile that you're currently leading your department through a DevOps transformation. One line that stood out to me was, "if you don't test it now, you'll have to debug it later." I couldn't agree more about.

Thank you for connecting with me. The purpose for my outreach is because I was reading through your LinkedIn profile and your implementation of systems automation using tools like Puppet/Ansible/Terraform caught my eye. One line you wrote that really stood out was how you've "created many new build and deployment patterns that save us time and headache."

Thank you for connecting with me, I hope the new year is finding you well. The reason I'm writing to you is because I was reading through your LinkedIn profile and saw that you've used GitLab. One line that stood out to me was where you wrote about how you drove "continuous delivery of value to the Customer and business, moving towards a DevOps environment."

