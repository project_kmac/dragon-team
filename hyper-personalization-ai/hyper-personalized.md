 # Hyper Personalized

*****
## BH

Example 1:

**PremiseExcerpt**
The reason for my outreach is that I enjoyed your article. One excerpt that really stood out to me was where you said, "..."

The reason for my email is because I couldn't get enough of.

What really captured my attention was...

**Value Prop**
What if you could do x, and as a result...

I have an idea on how you could ensure that your reps are having great conversations.

**Call to Action**
If you give me a shot to unpack how {{titles}} decrease onboarding and ramp time

If you give me a chance to meet with you and unpack that idea, I promise I won't jam your inbox if you don't think we're a fit.

**GracefulDismount**
Either way, I hope you're having a strong start to Q1.

In any case, I hope you're 

**FallOnYourSword**

The reason for my email today is because I read an article recently about being relevant and always bringing value to your clients, and I was conflicted about that last email I sent.  Maybe I got my research wrong and DevOps is old-hat and not all that compelling to you.



*****

## Examples

USE THIS FOR SERVERLESS ValueProp
https://drive.google.com/file/d/1IBDbhMuFAnXlz0mZPBHLwAPlMIfVYDST/vie


any thoughts?

 

P.S. I put together an overview of GitLab in this slide deck in case you were interested in more information, but didn't want to have a conversation: https://ptdrv.linkedin.com/mhnq7b9  
____

Hello Vivek,

The reason for my email today is because I was reading through your Medium article "Empathy Engineering." One line that resonated with me is where you wrote that "Empathy, when applied in engineering, will promote great team insights and creating a culture of warmth." Being in a team that's very much has that, I've really been thinking about how important this is.

We spend so much of our waking lives at work and having this empathy is so crucial to our happiness. Because without it, work can become a hostile and scary place. (I'm thinking now about Safety Culture and those principles as they apply to DevOps) 

In any case, great article, thank you for writing that up.

Was reading through your LinkedIn profile and I saw that you like Harvard Business Review articles on Marketing. I thought you might enjoy this one titled "The Loyalty Economy." 

This opening line really stood out to me: "Under relentless earnings pressure, they often feel cornered, obliged to produce quick profits by compromising product quality, trimming services, imposing onerous fees, and otherwise shortchanging their customers. This short-termism erodes loyalty, reducing the value customers create for the firm."

Hello Gabriel,

I keep seeing from your LinkedIn that you're hiring enterprise architects. I hope that the hiring is going well.

Forgive me if I'm mistaken but at the end of the day when it comes to enterprise architecture, Chief Architects care about increasing efficiency. The ones I've spoken with often might tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how others have solved this problem. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


Hello Steve,

The reason for my email today is because of something I read on your LinkedIn profile: "Continuous Integration and Continuous Delivery/Deployment across all services utilizing Configuration Management tools such as Chef"


Forgive me if I'm mistaken but at the end of the day when it comes to CI/CD, Directors of SRE care about increasing efficiency. The directors I've spoken with often might tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how other SRE Directors have solved this. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


Hello Sridhar,

The reason for my email is because was reading on your LinkedIn profile where you mentioned that you "Created technical vision for cloud platforms using service oriented architecture." 

Forgive me if I'm mistaken but at the end of the day when it comes to managing cloud platforms, Directors care about creating a scalable cloud native architecture.

If you'll give me a chance next Friday at 3 PM, I'll share how other Directors have achieved this. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


Hello Tyler,

The reason for my email today is because I was reading on your LinkedIn profile about your leadership in cloud engineering and operations.

I've spoken with a few Directors of Engineering and often, they might tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how other Directors of Engineering have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


Hello Adam,

Two things clearly stood out to me from reading your LinkedIn profile: a) You are a wordsmith, and b) I can feel the passion you have for your work. I appreciate that.

Warm Regards,
Kevin



Hello Moninder, 

The reason for my email today is because I was reading on your LinkedIn profile about your role in leading the cloud practice at Global Logic.

Forgive me if I'm mistaken but as I understand it, a Global Head of Cloud cares about optimizing cloud costs. Alternatively, one might say something along the lines of, "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?

If you'll give me a chance next Friday at 3 PM, I'll share how others have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.
 
 In any case, I hope you have a wonderful weekend.




*****

I've spoken with a few CIOs and often, they tell me something along the lines of "I'm looking to align our technology with business outcomes--and this means reducing cycle times, focusing on operational efficiciency, and tying it back to how it will benefit our customers." Is this relevant to your world, or am I just rambling here?

If you'll give me a chance next Friday at 3 PM, I'll share how other CIOs have improved operational efficiency while reducing cycle times. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.

I've spoken with a few CISOs and often, they tell me something along the lines of "I want a single pane of glass, because I'm tired of defects being pushed into production that should have been fixed earlier." Is this relevant to your world, or am I just rambling here?

If you'll give me a chance next Friday at 3 PM, I'll share how other CISOs have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.

In any case, I hope that your week is going well, and good luck on your upcoming panel at OptivCon!

I've spoken with a few Directors of Engineering and often, they tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how other Directors of Engineering have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


In any case, I hope that your week is going well.

I've spoken with a few Directors of Engineering and often, they tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how other Directors of Engineering have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up emails.


I spend a lot of my time with Sr Directors in an Infrastructure role and often, they tell me something along the lines of "I want my teams writing software, not figuring out how to build CI/CD pipelines." Is this relevant to your world, or am I just rambling here?


If you'll give me a chance next Friday at 3 PM, I'll share how other Sr Directors of Infrastructure have solved this issue. But if you feel that it's not a fit, I promise not to jam up your inbox with annoying follow-up  emails.


Forgive me if I'm mistaken but, as I understand it, Sr Managers care about enabling continuous build, integration, and deployments. What if there was a way to give your developers a platform that makes CI/CD easy?

If you'll give me a chance next Friday at 3 PM, I'll share how Sr Managers have used GitLab for Continuous Integration, Continuous Build, and Continuous Deployment systems. But if you feel that it's not a fit, I promise not to be annoying.

Forgive me if I'm mistaken but, as I understand it, Chief Enterprise Architects care about enabling modernization and digital transformation. What if there was a way to drive adoption of modernization and transformation initiatives by creating an ecosystem your engineers actually want to use?

If you'll give me a chance next Friday at 3 PM, I'll share how Chief Architects have found their end users are actively helping drive towards their strategic goals. But if you feel that it's not a fit, I promise not to be annoying.


On your LinkedIn, you talk about being "responsible for building the innovative solutions needed to launch thousands of products."

Forgive me if I'm mistaken but when it comes to building that many products, having a development platform that is stable and scalable is important. What if there was a way to enable your teams to have a more efficient platform, and thus launch and build those innovative solutions? 

If you'll give me a chance next Friday at 3 PM, I'll share how VPs have made their teams more efficient. But if you feel that it's not a fit, I promise not to be annoying.

In any case, I hope that your week is going well.


You talk in your LinkedIn profile about maximizing magic by "creating an environment that draws people, projects and organization together"

Forgive me if I'm mistaken but, as I understand it, Software Engineering Managers also care about enabling their teams to deliver software faster. What if there was a way to enable your teams to be more efficient and deliver products faster, thereby creating that magic in the environment?

If you'll give me a chance next Friday at 3 PM, I'll share how engineering managers have accomplished this. But if you feel that it's not a fit, I promise not to be annoying and follow up.


****

Hello Ed,

The reason for my message is because was reading through your LinkedIn profile. One line that stood out to me is that you are "Leading a team of world-class engineers in building the next generation Data Platform for Analytics."

Correct me if I'm wrong, but when it comes to next generation Data Platforms, using DevOps and CI/CD principles in your data platform is key. What if there was an easy way to apply DevOps and CI/CD practices like infrastructure as code to ensure speed and reliability of your Data infrastructure?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how other Data Platform teams are able to bring DevOps and CI/CD to their data pipeline by using GitLab, I promise I won't be an annoying salesperson if you don't think we're a fit.

Either way, I hope that your week is off to a great start.



*****

Hello Kha,

The reason I'm writing to you today is because I was reading through your LinkedIn profile. One line that resonated with me was where you said that being prescriptive "is the wrong way to be agile, focus on the outcomes you want. Big transformation can lead to a failure to launch."

Correct me if I'm wrong but at the end of the day but when it comes to focusing on outcomes, AVPs care about enabling transformation in their org. What if there was a way to have a DevOps platform that's your teams actually want to use, and therefore enables adoption and transformation naturally?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.



Hello Kathy,

The reason I'm writing to you today is because I was listening to your interview on "Beyond the Blue Badge." One line that resonated with me was where you said that "we all come together to solve this giant complex problem, and in order to do that we can't do that we can't be working in silos."

Correct me if I'm wrong but at the end of the day, CIOs care about removing silos in order to enable their teams to deliver software faster, while increasing operational efficiency. What if there was a way to have a non-siloed end-to-end DevOps platform that's easy to use and helps your teams collaborate, deliver their software faster, and reduce the operational overhead?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, thank you for the enjoyable insights from your appearance on the podcast.


Hello David,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in engineering, I thought this might be relevant to you. One line that stood out to me is that you're implementing systems automation using tools like "Puppet/Ansible/CloudFormation/Terraform."

Correct me if I'm wrong but at the end of the day, Systems Engineers care about enabling their teams to deliver software faster, while increasing operational efficiency. What if there was a way to have an automated end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster, while reducing the number of integrations needed to maintain?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Marc,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Directors of Engineering care about enabling their teams to deliver software faster, while increasing operational efficiency. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster, while reducing the number of integrations they need to maintain?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


*

Hello Suresh,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in DevOps, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Sr IT Managers care about enabling their teams to deliver software faster, while increasing operational efficiency. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster, while reducing the number of integrations they need to maintain?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.

Hello Karl,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in global infrastructure and operations, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Sr Directors of Operations care about enabling their teams to deliver software faster, while increasing operational efficiency. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster, while reducing the number of integrations they need to maintain?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Sharadha,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in  engineering, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Engineering Directors care about enabling their teams to deliver software faster. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Marty,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in software engineering, I thought this might be relevant to you. One line that stood out to me is that you "are implementing a serverless micro-service application architecture using Kubernetes and Kafka for our event-driven system deployed to AWS."

Correct me if I'm wrong but at the end of the day, when it comes to a serverless microservice architecture, having a platform that's easy to use is key . What if there was a way to have a end-to-end DevOps platform that makes it easy to deploy serverless, microservices, and use Kubernetes?


If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.



Hello Michael,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in software engineering, I thought this might be relevant to you. One line that stood out to me is your focus on "large-scale distributed systems, web applications, and devops"

Correct me if I'm wrong but at the end of the day, Software Engineering Directors care about enabling their teams to deliver software faster. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


*

Hello Bindu,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in product and engineering, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, VPs of Product and Engineering care about enabling their teams to deliver software faster. What if there was a way to have a end-to-end DevOps platform that's easy to use and helps your teams deliver their software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Marc,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in infrastructure services and DevOps, I thought this might be relevant to you.

Correct me if I'm wrong but when it comes to DevOps, having a platform that's easy to use is key. What if there was a way to have a end-to-end DevOps platform that's easy to use, easy to maintain, and has built in security scans?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Simon,

The reason for my message is because I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Directors of Engineering care about enabling their teams to deliver software faster. What if there was a way to have a end-to-end DevOps platform so that your teams can develop software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Caroline,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in DevSecOps, I thought this might be relevant to you.

Correct me if I'm wrong but when it comes to DevSecOps, having a platform that's easy to use is key. What if there was a way to have a end-to-end DevOps platform that's easy to use, easy to maintain, and has built in security (SAST, DAST, Container Scanning)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.



Hello Nick,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in DevOps and SRE, I  this might be relevant to you.

Correct me if I'm wrong but when it comes to DevSecOps, having a platform that's easy to use is key. What if there was a way to have a end-to-end DevOps platform that's easy to use, easy to maintain, and has built in security scans?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.



*

Hello Gaven,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in infrastructure services, I  this might be relevant to you. One line that stood out to me is that you're leading the "Development and Transformation of Zumiez Infrastructure Cloud Strategies."

Correct me if I'm wrong but when it comes to cloud infrastructure, IT Directors care about having a platform that's easy to use. What if there was a way to have a DevOps platform that's natively built for scalable, multi-cloud deployments, that works with technologies like Docker and Kubernetes?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Himanshu,

The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.

But more importantly, I was reading through your LinkedIn profile, and because of your leadership role in engineering, I thought this might be relevant to you.

Correct me if I'm wrong but at the end of the day, Directors of Engineering care about enabling their teams to deliver software faster. What if there was a way to have a end-to-end DevOps platform so that your teams can develop software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Stuart,

The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.

But more importantly, I was reading through your LinkedIn profile. One line that stood out to me is that you're "covering end-to-end development of client, server & database tiers."

Correct me if I'm wrong but when it comes to end-to-end development, having a platform that's easy to use. What if there was a way to have a end-to-end DevOps platform so that your teams can develop software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Tom,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in cloud architecture, I  this might be relevant to you. One line that stood out to me is that you're helping "Scaling and building large physical or cloud deployments."

Correct me if I'm wrong but when it comes to scaling deployments, Cloud Architects care creating a platform that's easy to use. What if there was a way to have a DevOps platform that's natively built for scalable, multi-cloud deployments, that works with technologies like Docker and Kubernetes?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Mike,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you. One line that stood out to me is that you're helping "secure a collaborative data science platform and applications."

Correct me if I'm wrong but when it comes to application security, Information Security Architects care about shifting security left. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.


****

Hello Tim,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you.

Correct me if I'm wrong but when it comes to application security, Chief Security Architects care about shifting security left. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.


Hello Giriraj,

The reason for my message is because was reading through your LinkedIn profile. One line that stood out to me is that you are "Leading a team of world-class engineers in building the next generation Data Platform for Analytics."

Correct me if I'm wrong, but when it comes to next generation Data Platforms, using DevOps and CI/CD principles in your data platform is key. What if there was an easy way to apply DevOps and CI/CD practices like infrastructure as code to ensure speed and reliability of your Data infrastructure?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how other Data Platform teams are able to bring DevOps and CI/CD to their data pipeline by using GitLab, I promise I won't be an annoying salesperson if you don't think we're a fit.

Either way, I hope that your week is off to a great start.

Hello Patrick,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're the "Security leader for central platform engineering organization." 

Correct me if I'm wrong but when it comes to the central platform engineering, Directors of Platform Security care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.



Hello Atticus,

The reason for my message is because was reading through your LinkedIn profile, and because of your role, I thought this might be relevant to you.

Correct me if I'm wrong, but at the end of the day, CIOs care about enabling their organizations to deliver better software faster while reducing risk. 

What if there was a way have an  end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? Like Greg Hughes said in that CES Keynote you shared, "Our enterprises, our missions require that the systems work at all times."

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.

Hello Andy,

The reason for my message is because I enjoyed watching your keynote "Getting to 100% in the cloud." I couldn't agree more with your statement that "Complexity is the enemy" and that this is key to balancing speed, operability, and security.

Correct me if I'm wrong but when it comes to balanding speed, operability, and security, having a DevSecOps platform that's easy to use is key . What if there was an end-to-end DevSecOps platform that minimized complexity, while allowing your teams to deliver secure software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I appreciate your keynote. You've been part of this great cloud migration and it's super helpful to hear your stories from in the trenches.

*

Hello Erik,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role as VP of CyberSecurity, I thought this might be relevant to you. 

Correct me if I'm wrong but at the end of the day, VPs of Cyber Security care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.


Hello David,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your teams "are responsible for designing, building, and operating the hosting platform in our internal data centers."

Correct me if I'm wrong, but at the end of the day when it comes to product infrastructure and operations, Group Managers care about increasing operational efficiency. 

What if there was a way have an end-to-end DevOps platform that allows your teams to reduce the complexity within the environment, minimize WIP, and simplify your infrastructure?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why other Infrastructure and Operations Managers find it useful. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.

Hello Eric,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "focused on replacing painful customer experiences with delightful product features"

Correct me if I'm wrong, but at the end of the day when it comes to replacing painful customer experiences with delightful product features, Sr Engineering Managers care about enabling their teams to deliver these software improvements faster. 

What if there was a way have an end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.


Hello Ravi,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you manage 3 scrum teams and are accountable for the SMB Lending Platform, Payment Sign-up platform, and the Payment Center.

Correct me if I'm wrong, but at the end of the day when it comes to building products like these, Sr Engineering Managers care about enabling their teams to deliver better software faster. 

What if there was a way have an end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.



Hello Michael,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "believe developing teams and managing data are foundational to accelerating business results through technology."

Correct me if I'm wrong, but when it comes to managing data to accelerate business results, using DevOps and CI/CP principles in your data platform is key. What if there was an easy way to apply DevOps and CI/CD practices like infrastructure as code to ensure speed and reliability of your Data Lake?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how other Business Intelligence teams are able to bring DevOps and CI/CD to their data pipeline by using GitLab, I promise I won't be an annoying salesperson if you don't think we're a fit.


In any case, I hope that your week is off to a great start.


*

Hello Vijay,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "responsible for attracting the very best technical product talent, challenging them to innovate for customers."

Correct me if I'm wrong, but when it comes to attracting talent and challenging them to innovate, having a development platform that enables theses behaviours is key.

What if there was a way have a cutting end-to-end DevOps platform that attracts the best talent, while empowering your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.


Hello Rajeev,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "lead a team of amazing product leaders at TIBCO whose mission is to build products & technologies that interconnect everything, everywhere."

Correct me if I'm wrong, but at the end of the day wjen it comes to building products and technologies, VPs of Product Management care about enabling their teams to deliver better software faster. 

What if there was a way have an end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start.




Hello Gautam,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're a "Product Engineer and Technical Leader for a wide variety of business applications."

Correct me if I'm wrong, but at the end of the day, as a leader for business applications, Engineering Managers care about enabling their teams to deliver better software faster. 

What if there was a way have an end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your week is off to a great start. Thanks for sharing that article on retiring the word "manager." Very interesting angle to look at some of the issues for sure.


Hello George,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're working on the "developer platform team focusing on design and development of our primary web presence."

Correct me if I'm wrong, but at the end of the day when it comes to developer platforms, Engineering Directors care about enabling their teams to deliver better software faster. 

What if there was a way have an  -to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I appreciate your work on FolkMind. The idea of a folksonomy and the ability to work at any level of abstraction are very interesting to me.


Hello Archana,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you compel and lead "with an outcome based approach to drive sustained results"

Correct me if I'm wrong, but at the end of the day when it comes to sustained results, Group Development Managers care about enabling their teams to deliver better software faster. 

What if there was a way have an  end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your week is off to a great start.


-----

Hello John,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you're "leading and transforming the Information Technology function for Veritas, including its infrastructure, systems, processes, and security."

Correct me if I'm wrong, but at the end of the day, CIOs care about enabling their organizations to deliver better software faster while reducing risk. 

What if there was a way have an  end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? Like Greg Hughes said in that CES Keynote you shared, "Our enterprises, our missions require that the systems work at all times."

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February has been off to a great start.


Hello David,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "connect people, drive execution, and solve problems."

Correct me if I'm wrong, but when it comes to connecting people, driving execution, and solving problems, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to connect, drive execution, and solve problems?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, huge fan of the work you do.



Hello Faraz,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "Simplifying the business of life @ intuit!"

Correct me if I'm wrong, but you've probably experienced first-hand how time-consuming it can be to maintain SDLC tools and their integrations. 

When  it comes to simplifying the business of software development, having a development platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform out of the box so your teams can focus on delivering business value faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February has been off to a great start.


Hello Trisha,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you we're previously "Leading high performing DevOps production development and support team."

Correct me if I'm wrong, but when it comes to DevOps and high performance, having a development platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform out of the box so your teams can focus on delivering business value faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I appreciate that you called out ageism in your Disrupting Bias Training, because it's the bias we address the least and gets the least airplay.

Hello Anthony,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "leading agile teams with focus on delivering business value in line with enterprise objectives."

Correct me if I'm wrong, but when it comes to Agile teams and delivering business value, having a development platform that's easy to use is key. What if there was a way have an end-to-end Agile SDLC platform so your teams can focus on delivering business value faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I really appreciated you sharing that great HBR article on time management.(I learned the word 'polychronicity' from it)


-----
Hello Tariq,

The reason for my message is because I was reading through your LinkedIn profile. A couple of thing stood out to me, namely your work in "Kubernetes infrastructure migration: Dockerization, and containerization" and "CICD optimization"

Correct me if I'm wrong, but at the end of the day but when it comes to using tools like these, having a development platform that's easy to maintain is key. What if there was a way have an end-to-end DevOps platform that works out of the box with Kubernetes without the maintenance or integration management, so your teams can focus on delivering code?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Theresa,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security, I thought this might be relevant to you. 

Correct me if I'm wrong but at the end of the day, CTOs of Cyber Security care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February

Hello Rajagopal,

The reason for my message is because I was reading through your LinkedIn profile. Because of your role in the EBS architecture, I thought this might be relevant to you.

Correct me if I'm wrong, but at the end of the day, Chief Architects want their teams delivering software instead of integrating tools or making platforms work. What if there was a way to have an end-to-end that works out of the box without the maintenance or integration management, so your teams can focus on delivering software?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Annette,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you achieved a "Velocity increase of 54% over less than 12 months."

Correct me if I'm wrong, but at the end of the day but when it comes to increasing velocity and creating high performing teams, having a development platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that works out of the box without the maintenance or integration management, so your teams can focus on delivering code?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 

Hello Raju,


The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you were"Selected to lead program to build new state-of-the-art Data Center facility to support Inuit's high-availability and disaster recovery strategy."

Correct me if I'm wrong, but when it comes to high availability and disaster recovery, having a platform that's easy to use is key. What if there was a way have an end-to-end platform that works out of the box without the maintenance or integration management, so your teams can focus on delivering highly available and scalable applications?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 

-

Hello Shisheer,

The reason for my message is because I was reading through your LinkedIn profile and noticed that you mention DevOps under your skills section.

Correct me if I'm wrong, but when it comes to leveraging DevOps for AI, having a platform that's easy to use is key. What if there was a way to leverage an end-to-end DevOps platform that works out of the box and is built for use with Big Data, ML, and AI?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Randy,

The reason for my message is because I was reading through your LinkedIn profile. Noticed that you're responsible for "core ERP systems" and EBS, but more importantly you listed DevOps under your core competencies.

Correct me if I'm wrong, but when it comes to leveraging DevOps for ERP and EBS, having a platform that's easy to use is key. What if there was a way to leverage an end-to-end DevOps platform that works out of the box without the maintenance or integration management, so you can focus on implementing more best practices?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Paul,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you modernized the "server infrastructure to stabilize the environment which has reduced our technical support calls by 25% and re-architected legacy applications to improve bandwidth by 30%."

Correct me if I'm wrong, but when it comes to rearchitecting legacy applications, having a platform that's easy to use is key. What if there was a way to leverage an end-to-end DevOps platform that works out of the box without the maintenance or integration management, so you can focus on implementing more best practices?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 



Hello Syed,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Responsible for Architecture Programs and Initiatives, Governance, Standards and Design for Shutterfly."

Correct me if I'm wrong, but when it comes to architecture standards and design, Chief Architects want their teams delivering software instead of integrating tools or making platforms work. What if there was a way to have an end-to-end that works out of the box without the maintenance or integration management, so your teams can focus on delivering software?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 

Hello Rahul,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Experienced with designing and implementing highly available, scalable, and robust enterprise applications."

Correct me if I'm wrong, but when it comes to highly available and scalable applications, Sr Managers of Platform Engineering want their teams delivering software instead of integrating tools or making platforms work. What if there was a way have an end-to-end SDLC that works out of the box without the maintenance or integration management, so your teams can focus on delivering highly available and scalable applications?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 

-

Hello Murthy,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me is that you're "Well known for working across the organization to drive vision of the product and common business goals."

Correct me if I'm wrong, but at the end of the day but when it comes to business goals, Sr Directors of Engineering want their teams delivering software instead of integrating tools or making platforms work. What if there was a way have an end-to-end SDLC that works out of the box without the maintenance or integration management, so your teams can focus on delivering code?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Brian,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me is that you "implemented best practices across the Development, QA and DevOPs teams which significantly improved our ability to deliver new functionality."

Correct me if I'm wrong, but at the end of the day, Sr Director of Infrastructure Operations want their teams delivering software instead of integrating tools or making DevOps platforms work. What if there was a way have an end-to-end DevOps platform that works out of the box without the maintenance or integration management, so your teams can focus on delivering code?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Lucas,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security, I thought this might be relevant to you. 

Correct me if I'm wrong but at the end of the day Directors of Information Security Operations care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Deepak,

The reason for my message is because I was reading through your LinkedIn profile. One line that I appreciate is that you're "Exploring MicroServices architecture for deployments with Zero down time."

Correct me if I'm wrong, but at the end of the day when it comes to leveraging microservices, having a platform that's easy to use is key. What if there was a way have an  end-to-end DevOps platform that's built for leveraging microservices architecture and makes this easier? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your team could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 



Hello Bo,

The reason for my message is because I was reading through your LinkedIn profile and because of your role as a software development manager, I thought this might be relevant to you. But more importantly you specifically mention using tools like Kubernetes.

Correct me if I'm wrong, but at the end of the day when it comes to leveraging Kubernetes, having a platform that's easy to use is key. What if there was a way have an  end-to-end DevOps platform that's built for leveraging Kubernetes and Big Data technologies? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 

-
Hello Isadora,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me was that you previously "Achieved a robust CI/CD pipeline to incrementally delivery features."

Correct me if I'm wrong, but at the end of the day when it comes to leveraging DevOps and using CI/CD, having a platform that's easy to use is key. What if there was a way have an  end-to-end DevOps platform without the maintenance or having to manage integrations? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start. 


Hello Jeff,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice your use of DevOps. One line that stood out to me was that you transformed "traditional Enterprise IT through DevOps teams embedded" within business units.

Correct me if I'm wrong, but at the end of the day when it comes to leveraging DevOps, CIOs care about enabling their organizations to deliver better software faster. What if there was a way have an  end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. (As you may be aware, Disney uses GitLab for DevOps). And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, as a venti Cold Brew man myself, I hope that your February is off to a great start. 


Hello Chad,

The reason for my message is because I was reading through your LinkedIn profile and appreciate your use of DevOps. But more importantly, you mention that you "Created and executed the DevOps function and team for the Enterprise Collaboration DevOps Solution Delivery team".

Correct me if I'm wrong, but when it comes to leveraging DevOPs, you've probably seen how much time it takes to integrate and make the tools work together. What if there was a way to have an end-to-end DevOps platform without the maintenance or integration?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought you might find it beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Marty,

The reason for my message is because I was reading through your LinkedIn profile and appreciate your use of DevOps within QA. But more importantly, what stood out to me is that you're using tools like Jenkins, GitHub, and TFS.

Correct me if I'm wrong, but when it comes to using those tools, you've probably seen how much time it takes to integrate and make the tools work together. What if there was a way to have an end-to-end DevOps platform in QA without the maintenance or integration?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought you might find it beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Matthew,

The reason for my message is because I was reading through your LinkedIn profile and really appreciated your line about being a "Master Innovator". But more importantly, what stood out to me is that you're using tools like Kubernetes or Ansible, and even using Serverless/FaaS.

As the SME, you've probably seen firsthand that for the above innovations, having a DevOps platform that's easy to use is key. What if there was a way have an end-to-end platform that's natively built to allow your teams to use Kubernetes, Serverless, etc.? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your SRE Org could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, very cool of you to do the Climb For Clean Air last year. Hope that all is well in your world!



----
Hello Kris,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're known for "identifying strategic opportunities, driving innovation, accelerating revenues and reducing costs."

Correct me if I'm wrong, but at the end of the day when it comes to driving innovation, accelerating revenues, and reducing costs, having a DevOps pipeline that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to deliver better software faster, while accelerating revenue, as well as reducing toolchain costs? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Gerri,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that under your leadership, "Starbucks Technology is focused on delivering innovative, cloud-based and digital solutions."

Correct me if I'm wrong, but at the end of the day when it comes to innovative, cloud based solutions, CTOs care about enabling their organizations to deliver better software faster. What if there was a way have an  end-to-end DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency, as well as reducing security and compliance risk? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

-

Hello Imran,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you were "Responsible for building and managing internal private cloud to provide IaaS and PaaS solution."

Correct me if I'm wrong, but IaaS and PaaS, having a platform that's easy to use is key. What if there was a way to use an end-to-end DevOps platform that makes Paas, IaaS, and even FaaS more easily manageable to increase performance and reduce operational cost?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your team could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Lori,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you've had "Success in continuously improving delivery team performance and outcomes by leading investment in Agile, Lean and DevOps practices."

Correct me if I'm wrong, but at the end of the day when it comes to DevOps practices and improving delivery team performance, having a platform that's easy to use is key. What if there was a way to use an end to end DevOps platform to streamline your development, increase performance, and reduce operational cost?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Rand,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in security architecture, I thought this might be relevant to you. 

Correct me if I'm wrong but at the end of the day Security Architects care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does for  security architecture and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Anders,

The reason for my message is because I was reading through your LinkedIn profile, and because of your role in application security, I thought this might be relevant to you. But more importantly, because of your having been a part of the due diligence team for the Veracode acquisition, I thought your personal experience might be important.

Correct me if I'm wrong but at the end of the day Senior Managers of Application Security care about reducing their security and compliance risk. What if there was a way to integrate application security into your DevSecOps platform such that your teams can have their code scanned at every code commit (instead of only periodically)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does for application security and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that you're enjoying the move to Twitch. Big fan of your platform and the community you've built.

Hello Scott,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "Excel at leading end-to-end SDLC and enhancing existing tools for measurable impact"

Correct me if I'm wrong, but at the end of the day when it comes to SDLC, IT Directors care about delivering software faster. What if there was a way to use an end to end SDLC platform to streamline your development, increase performance, and reduce operational cost?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Sree,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you are "currently responsible for managing application architecture, development & execution of end to end Quote to Cash business process."

Correct me if I'm wrong, but at the end of the day when it comes to application architecture and development, IT Directors care about delivering software faster. What if there was a way to use an end to end DevOps platform to streamline your application architecture and deliver software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Sameer,

The reason for my message is because I was reading through your LinkedIn profile and wanted to congratulate you on your new role as Head of Data and Platforms. One line that stood out to me was that you had "successfully scaled up a Data lake solution leveraging AWS S3. This provided a comprehensive data analytics platform for structured and telemetry data and built a predictive insights capability within the organization."

Correct me if I'm wrong, but when it comes Data, having a platform that's easy to use is key. What if there was a way have an end-to-end  platform that allows your teams to easily leverage Data, AI, and BI?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Justin,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was this: "My ability to standardize and consolidate enterprise, business, and development systems—and to rationalize and streamline the procurement process—has consistently driven increased operational efficiency while reducing cost."

Correct me if I'm wrong but when it comes to making standardization, rationalization, and consolidation, having a platform that's easy to use is key. What if there was a way to have an automated, end-to-end DevOps platform enables your teams to standardize and consolidate while achieving those goals of increased operation efficiency and reduced cost?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Ken

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that "Engineering Effectiveness is responsible for making development fast, efficient and easy for the 850+ engineers."

Correct me if I'm wrong but when it comes to making development fast, efficient, and easy, having th right platform is key. What if there was a way to have an automated, end-to-end DevOps platform that enables your teams to deliver better software faster and more securely?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Payman,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "restructured the entire operational framework using agile and DevOps methods to start consistently meeting SLAs and KPIs and, by designing and spearheading the migration to cloud, we saved millions from the IT budget while having almost no impact on business because of close collaboration across all business functions." What an achievement.

Correct me if I'm wrong but at the end of the day CISOs care about enabling their organizations to deliver better software faster while reducing security and compliance risk. What if there was a way to integrate security into your DevSecOps platform such that your teams can deliver better software faster and more securely?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



*
Hello Scott,

The reason for my message is because I was reading through your LinkedIn profile, and as because of your role in enterprise architecture and integration, I thought this might be relevant to you.

Correct me if I'm wrong but with your experience, you've probably seen how important it is to simplify architecture and consolidate integrations wherever possible. What if there was a way to increase operational efficiency while reducing the time-consuming maintenance of integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why Directors of Enterprise Architecture find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Debbie,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you "Facilitated the migration of the team to modern source code control technologies and best practices with Azure DevOps."

Correct me if I'm wrong but with your experience, you've probably seen how important it is to have a DevOps platform that's easy to use and maintain. What if there was a way to get automated end-to-end DevOps without having to maintain all the tools and integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why other Azure DevOps users find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

I hope that your February is off to a great start.

Hello Ian,

The reason for my message is because I was reading your LinkedIn profile about your building of tools and automations within Salesforce. But more importantly, you specifically mention "implementing a total development cycle including Git, Continuous Integrations, Code Review and Code Quality Guidelines."

Correct me if I'm wrong, but when it comes to a total development cycle, you've probably seen how important it is to have platform that's easy to use and maintain. What if there was a way have an end-to-end development platform that allows your teams to have these capabilities without the integrations or maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Tim,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Ansible and Puppet for automation.

Correct me if I'm wrong, but when it comes to automation tools, you've probably seen how time-consuming it can be to maintain a platform's integrations. What if there was a way have an automated end-to-end DevOps platform that allows your teams to have these capabilities without the integrations or maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Cameron,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you created a "scale-out shared-nothing architecture."

Correct me if I'm wrong, but at the end of the day when it comes to scaled, highly available architecture, CIOs care about enabling their organizations to deliver better software faster. What if there was a way have an  end-to-end  DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

-----

Hello Rob,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out is that you established "Platform as a Service (PaaS) to drastically speed development and enable key best practices with code."

Correct me if I'm wrong, but at the end of the day when it comes to enabling PaaS and speeding up development, having a platform that's easy to use is key. What if there was a way have an end-to-end platform that allows your teams to increase developer productivity, enable best practices, and leverage PaaS in their workflow? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Ayan-Anwar,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're building and enhancing "Big data, Analytical and Search solutions from the ground up."


Correct me if I'm wrong, but when it comes big data, having a platform that's easy to use is key. What if there was a way have an end-to-end  DevOps platform that allows your teams to easily leverage big data?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Rob,

The reason for my message is because I was reading through your LinkedIn profile and as a Sr Manager of Infrastructure and DevOps, I thought this might be relevant to you.

Correct me if I'm wrong, but at the end of the day when it comes to infrastructure and operations, having a DevOps platform that's easy to use is key. What if there was a way have an  end-to-end DevOps platform that allows your teams to increase developer productivity, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Jack,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "leading a technology transformation to leverage the cloud, big data, machine learning and artificial intelligent knowledge based systems."

Correct me if I'm wrong, but when it comes leveraging the cloud and big data, having a  platform that's easy to use is key. What if there was a way have an end-to-end  DevOps latform that allows your teams to easily leverage big data and the cloud?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Sembian,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you mention "Engineering and Optimization; Site Reliability Engineering."

Correct me if I'm wrong, but at the end of the day when it comes engineering optimization and site reliability engineering, having a DevOps platform that's easy to use is key. What if there was a way have an  end-to-end  platform that allows your teams to increase developer productivity, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Ivan,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you delivered technology standards that fueled "platform architecture and faster development productivity."

Correct me if I'm wrong, but at the end of the day when it comes to developer productivity, having a DevOps platform that's easy to use is key. What if there was a way have an  end-to-end  platform that allows your teams to deliver better software faster, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.




Hello Mark,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you've driven cross functional programs and initiatives  "to reduce risk, improve security/compliance awareness."

Correct me if I'm wrong, but at the end of the day when it comes to reducing security and compliance risk, having those integrated into the platform is key. What if there was an easy way to architect security capabilities into the SDLC workflow where the developers live? (thereby allowing vulnerabilities to be spotted earlier, while having the reporting that reduces compliance risk

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does for security and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*


Hello Jason,

The reason for my message is because I was reading through your LinkedIn profile and noticed that you listed Enterprise Architecture under skills.

Correct me if I'm wrong, but at the end of the day when it comes to enterprise architecture, Distinguished Engineers care about enabling their organizations to deliver better software faster. What if there was a way have an end-to-end  DevOps platform that allows your teams to deliver better software faster, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your Lunar New Year is off to a great start.



Hello Brian,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you were "Leading a team that focuses on Developer Productivity, Tech-Hygiene, Cloud Hosting, Security, GDPR, and Operational Excellence.."

Correct me if I'm wrong, but when it comes to helping developer productivity and operational excellence, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to increase developer productivity, while also increasing operational excellence?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Deepak,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you are "Managing all aspects of Site Reliability Engineering (SRE), DevOps, and Database Engineering functions."

Correct me if I'm wrong, but when it comes to helping SRE and DevOps, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to deliver software faster, without the maintenance or time-consuming integrations?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Sharon,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your experience includes "Business systems implementation, integration and program/project management experience."

Correct me if I'm wrong, but at the end of the day when it comes to implementation and integration, CIOs care about enabling their organizations to deliver better software faster. What if there was a way have an  end-to-end  platform that allows your teams to deliver better software faster, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your Lunar New Year is off to a great start.



Hello Jyothy,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you've developed "process improvements to achieve cost effectiveness and time saving."

Correct me if I'm wrong, but at the end of the day when it comes to cost effectiveness and time saving, having the right platform is key. What if there was a way have an  end-to-end software development platform that allows your teams to be cost effective while saving time? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


*

Hello Ravindra,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that your "Proven accomplishments include driving enterprise level technology transformation initiatives as a C-level leader and orchestrating organizational effectiveness and change initiatives."

Correct me if I'm wrong, but at the end of the day when it comes to technology transformation, CIOs care about enabling their organizations to deliver better software faster. What if there was a way have an  end-to-end  platform that allows your teams to deliver better software faster, while increasing operational efficiency? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your organization could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Maris,

The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was that your team built a "best of breed platform: microservices, distributed architecture, running on public cloud, built and delivered using DevSecOps practices."

Correct me if I'm wrong, but when it comes to DevSecOps, having an integrated platform that's easy to use is key. What if there was a way have an automated end-to-end DevSecOps platform that allows your teams to deliver software faster, without any of the integrations or maintenance? 

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello John,

The reason for my message is because I was reading your LinkedIn profile, and because you mentioned that you were "Part of the Engineering Effectiveness organization whose focus was to find ways to make engineer work better, faster and more efficiently." As such, I thought this might be relevant to you. 

Correct me if I'm wrong, but when it comes to helping engineers work better, faster, and more efficiently, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to deliver software faster, more securely, and more efficiently?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Joe,

The reason for my message is because I was reading your LinkedIn profile, and because you mentioned that you're "Well practiced in software development and QA methodologies (Agile, Scrum, Waterfall, automated testing, continuous integration" I thought this might be relevant to you. 

Correct me if I'm wrong, but when it comes to automated testing and continuous integration, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to have continuous integration and automated testing?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Payman,

The reason for my message is because I was reading your LinkedIn profile, and as a software engineering manager, I thought this might be relevant to you. 

Correct me if I'm wrong, but at the end of the day, Sr Software Engineering Managers care about delivering software faster. What if there was a way have an automated end-to-end DevOps platform that allows your teams to deliver software faster?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



-

Hello George,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Ansible and Puppet. But more importantly, you talk about having a "passion for turning around unstable infrastructures."


Correct me if I'm wrong, but when it comes to turning around unstable infrastructures, you've probably seen how time-consuming it can be to maintain a platform's integrations. What if there was a way have an automated end-to-end DevOps platform that allows your teams to have these capabilities without the integrations or maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Rafael, 

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using tools like Jenkins, Circle CI, and Docker.

Correct me if I'm wrong, but when it comes to using tools like these, you've probably seen how time-consuming it can be to maintain the platform and its integrations. What if there was a way have an automated end-to-end DevOps platform that allows your teams to have these capabilities without the integrations or maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Matthew,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention using infrastructure tools like "Puppet, Ansible, Terraform, Dockers."

Correct me if I'm wrong, but when it comes to using these tools, you've probably seen how time-consuming it can be to maintain the platform and its integrations. What if there was a way have an automated end-to-end infrastructure platform that allows your teams to have these capabilities without the integrations or maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Joel,


The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that your "goals as a leader are to provide autonomy and empowerment to my team; and to develop self-organizing delivery teams."

Correct me if I'm wrong, but when it comes to autonomy and self-organizing delivery teams, having a platform that's easy to use is key. What if there was a way have an automated end-to-end DevOps platform that allows your teams to be autonomous and self-organizing?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and how SRE teams benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Kent,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that "Engineering Effectiveness is responsible for making development fast, efficient and simple for all engineers at Yelp."

Correct me if I'm wrong, but when it comes to making development fast, efficient, and simple, having a DevOps platform that's easy to use is key. What if there was a way have an automated end-to-end DevOps platform that allows your teams develop faster, more efficiently (without the time-consuming maintenance of the platform)?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Shabbir,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Champion design, coding and operational best practices, with in-depth design, security and PR reviews."

Correct me if I'm wrong, but when it comes to operational best practices, having a platform that's easy to use is key. What if there was a way have an end-to-end DevOps platform that allows your teams to easily follow operational best practices?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Bernard,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you're a "Thought leader in the field of information security architecture and process."

Correct me if I'm wrong, but at the end of the day Chief Enterprise Security Architects care about reducing security and compliance risk. What if there was an easy way to architect security capabilities into the SDLC workflow where the developers live? (thereby allowing vulnerabilities to be spotted earlier and reducing your risk)

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does for security and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Adrian,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you empower "developers to efficiently build delightful experiences for our community, crafted upon a foundation of security and reliability."


Correct me if I'm wrong but when it comes to empowering developers, having a platform that's easy to use is key. What if there was an easy to use DevOps platform that was natively built for security, reliability, and the cutting edge work happening there at Twitch?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, huge fan of the team and the work that you all do there at Twitch.


Hello Omid,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you're "Managing and migrating Infrastructure From instances to Micro services using Lambda, EKS, IKS(intuit kubernetes services."

Correct me if I'm wrong but when it comes to migrating infrastructure to microservices and kubernetes, having a platform that's easy to use is key. What if there was an easy to use DevOps platform that was natively built for micro-services and Kubernetes?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February has been off to a great start.



Hello Sabbitha,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Established a process tailored agile SDLC framework across a global team that helped reduce the project delivery times by half."

What if there was a way to reduce project delivery times even more, and allow your teams to ship software faster by using a DevOps platform?

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February has been off to a great start.



Hello Grant,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me is where you mention that you "Moved the company from a review-checkpoint model to a comprehensive security program including threat modeling, code analysis, and automation, with education and tools to allow engineering teams to assess and improve their own security."

Correct me if I'm wrong but when it comes to allowing engineering teams to assess and improve their own security, having a platform that's well integrated is key. What if there was a way to build comprehensive application security capabilities (like SAST/DAST/SCA)into the CI/CD workflow where the developers live? (Thereby allowing engineering teams to identify vulnerabilities and remove them early.)

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, big fan of what you're working on at Twitch. Thanks for keeping the platform secure.


Hello Jim,

The reason for my message is because I was reading your LinkedIn profile and couldn't help but notice your use of SAST/SCA/DAST. But more importantly, one line that stood out to me is where you mention that you "Initiated DevSecOps movement within the organization to bring better collaboration and instill a “shift left” mentality between Development, Security and Infrastructure teams."

Correct me if I'm wrong but when it comes to "shifting security left" having a platform that's well integrated is key. What if there was a way to build application security capabilities (SAST/DAST/SCA)into the CI/CD workflow where the developers live? (Thereby allowing devs to identify vulnerabilities and remove them early.)

If you'll give me an opportunity next Wed at 1 PM, I'll share with you what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Jim,

The reason for my message is because I was reading your LinkedIn profile, where you mention that you "Lead IT security operations."

What if there was a way to build application security capabilities into the CI/CD workflow where the developers live? (Thereby allowing devs to identify vulnerabilities and remove them early.)

If you'll give me an opportunity next Wed at 1 PM, I'll share with you how IT Security Directors have been able to use our platform to cover a large attack surface area effectively. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Aaron,

The reason for my message is because I was reading your LinkedIn profile, and your objective resonated with me. This line specifically struck me as important, your question of "How can we empower the rest of the organization to be security champions, rather than requiring our team to handle everything?"

Here's an idea. What if there was a way to build security capabilities are built into the CI/CD workflow where the developers live. This allows devs to identify vulnerabilities and remove them early. At the same time, this also provides your security team a dashboard to view the items not addressed by the developers.

If you'll give me an opportunity next Wed at 1 PM, I'll share with you how App Sec Managers have been able to use our platform and to cover a large attack surface area effectively. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, you've got a cool mission, I wish you the best in executing that alchemy.


Hello Andrew,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you listed "continuous integration, docker, and test automation" under your skills, a

Correct me if I'm wrong, but when it comes to continuous integration, having platform that's easy to use is key. What if there was a way to have a CI platform that's easy to use and natively built for Docker and test automation?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is going well.


Hello Michal,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Led cloud implementation of highly scalable, available and performant micro services utilizing Azure Event Hubs"

Correct me if I'm wrong, but when it comes to cloud implementation of highly available microservices, having platform that's easy to use is key. What if there was a way to have a DevOps platform that's easy to use and natively built for micro-services and multi-cloud?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is going well.


Hello Robert,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're leveraging "Azure, and CI/CD tooling to develop highly performant systems to process thousands of requests across multiple systems."

Correct me if I'm wrong, but when it comes to CI/CD tooling, having platform that's easy to use is key. What if there was a way to have an easy to use CI/CD platform to help you develop your systems?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is going well.


Hello Mike,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you led the effort to allow workloads to AWS, Azure, and GCP, and that you also "Led efforts to "Dockerize" all products in my group."

With your experience, you've probably seen that containerization and having DevOps platform that's easy to use is key. What if there was a way to have an easy to use DevOps platform that's also natively built for multi-cloud and containerization/Dockerization?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is going well.


----

Hello Ashutosh,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "Led IT Cloud Strategy to enable 34% cost savings, faster time to market, and hyper scale capability on demand"


With your experience, you've probably seen that having a DevOps platform is key to staying competitive. What if there was a way to use a DevOps strategy to further enable cost savings, faster time to market, and scale capability on demand?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, congratulations on tho many CIO and Innovation awards.

Hello Balaji,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "delivering data engineering automation, dev ops and developing self-service deep analytics capabilities."


Correct me if I'm wrong, but when it comes to delivering automation, DevOps, and self-service capabilities, having a platform that's easy to use is key. What if there was a way to have an easy to use platform for your automation, DevOps, and self-service deep learning?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



*


Hello Anasheh,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Partner with engineering teams to innovate, build, and implement enterprise-wide integration strategies."

Correct me if I'm wrong, but when it comes to enterprise-wide integration strategies, having a platform that's easy to use is key. What if there was a way to have an enterprise wide platform for integrations that's easy to use ?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Ramesh,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you're "transforming IT as a service and modernizing technology."

Correct me if I'm wrong, but when it comes to modernizing technology, having a platform that's easy to use is key. What if there was a way to have an end-to-end modern platform for IT service that's easy to use?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Ganesh,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that as part of your technologies, your teams are using tools like JIRA, TFS, and Jenkins.

Correct me if I'm wrong, but you've probably seen first-hand how time-consuming it can be to maintain the integrations between those tools. What if there was a way to have an end-to-end DevOps platform without the maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Suresh,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're "Partnering with cross-functional teams to deliver our cloud strategy of moving more than 100 PB of user images out of Data Center to AWS."

Correct me if I'm wrong, but when it comes to moving to AWS, having a DevOps platform that's easy to use is key. What if there was a way to have an end-to-end DevOps platform that's built for your cloud strategy and ecosystem?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Don,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that in addition to using DevOps, you "Collaborate with other IT Solution Delivery teams to share knowledge, ensure alignment and demonstrate unified IT solutions and services.

Correct me if I'm wrong, but when it comes to leveraging DevOps and aligning solution delivery, having a DevOps platform that's easy to use is key. What if there was a way to have an end-to-end DevOps platform that's built for your different teams to collaborate and ensure alignment?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


*

Hello Tim,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that as "Product owner for Starbucks IOT initiatives," you're using DevOps and agile practices.

Correct me if I'm wrong, but when it comes to leveraging DevOps, having a platform that's easy to use is key. What if there was a way to this less painful by having an end-to-end DevOps platform that's built for your cloud ecosystem?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for IOT and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Larry,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "Drove transition to SaaS and Cloud/container capabilities and business models."

Correct me if I'm wrong, but when it comes a using cloud and containers, having a platform that's easy to use is key. What if there was a way to make adoption easier by having an end-to-end DevOps platform that's built for your container and cloud ecosystem?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Claire,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you created the "QA UI Design Analyst to apply quality checks into the early design process reducing the number of late cycle UI/UX issues." What a great creative solution.

Correct me if I'm wrong, but when it comes to applying quality checks early into the process, having a platform that makes this easy to follow is key. What if there was a way to make the process less painful by having a platform that has QA built in early in the pipeline?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for QA and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Amit,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "Lead the NetBackup and Cloud Solutions product portfolio for private/public cloud, virtualization, disaster recovery, BaaS/SaaS solutions etc."


Correct me if I'm wrong, but when it comes to managing a private or public cloud, having a platform that's easy to use is key. What if there was a way to make the process less painful by having an end-to-end DevOps platform that's built for your cloud ecosystem?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Dave,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you "own Product Mgmt strategy & PM execution for our Cloud Data application stack, architecture & ecosystem.""


Correct me if I'm wrong, but when it comes a cloud Data application ecosystem, having a platform that's easy to use is key. What if there was a way to make the process less painful by having an end-to-end DevOps platform that's built for your cloud ecosystem?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


*

Hello Gaurav,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you're "Transforming the technology to cloud native - Kubernetes, Docker."

Correct me if I'm wrong, but when it comes to being cloud native, having a DevOps platform that's easy to use is key. What if there was a way to make the process less painful by having an end-to-end DevOps platform that's built for cloud native, Docker, and Kubernetes?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Don,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was you mentioning that you're "transitioning into a technical systems engineering role responsible for monitoring cloud (AWS and Azure) operations, financials and governance."

Correct me if I'm wrong, but when it comes to your cloud operations, having a DevOps platform that interacts with your chosen provider is key. What if there was a way to make the process less painful by having a DevOps platform with the necessary libraries, tools, and integrations pre-installed?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Ivy,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using tools like Kubernetes, Docker, and Maven.

Correct me if I'm wrong, but when it comes to using those tools, having a platform that's easy to use is key. What if there was a way to have an end-to-end DevOps platform that's built with Kubernetes and microservices in mind?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Manav,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're "Driving migration to Kubernetes for Intuit Developer Platforms."

Correct me if I'm wrong, but when it comes to driving migration for developers, having a platform that's easy to use is key. What if there was a way to have an end-to-end Developer platform that's built with Kubernetes in mind?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Mani,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you "Developed new training process, utilizing Docker, Kubernetes and VMWare based servers to speed up environment set up issues, resulting in successful new hire issue handling in less than three weeks." What a great achievement.

What if there was a way to speed that up more and get new hires handling issues and deploying on Day One?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



-
Hello Aaron,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using "CI/CD, Kubernetes, Golang, Docker, Jenkins, TeamCity, Microservices, Typescript, React"

Correct me if I'm wrong, but when it comes to using those tools, having a platform that's easy to use is key. What if there was a way to have an end-to-end CI/CD platform that's built with Kubernetes and microservices in mind? 

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Biju,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using "Microservices for the platform with technology stack based on Azure Kubernetes(AKS), Docker, Kafka"

Correct me if I'm wrong, when it comes to using Kubernetes and a microservices, having a platform that's natively built for microservices and Kubernetes is key. What if there was a way to have an end-to-end DevOps platform that's built with Kubernetes and microservices in mind (and also works across Geos for your multiple teams)? 

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



*

Hello Paul,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you "Created strategy for kubernetes and containers for tier1 applications to deliver persistent storage, replication and HA.""

Correct me if I'm wrong, when it comes to HA, and using Kubernetes, having a platform that's easy to use is key. What if there was a way to have an end-to-end DevOps platform that's built with Kubernetes and HA in mind? 

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for cloud and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Vijay,

The reason for my message is because I was reading your LinkedIn profile. What stood out to me was that you're using tools like Kubernetes on Azure.

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how resource intensive it can be to maintain a DevOps platform with its integrations to Kubernetes. What if there was a way to have an end-to-end, automated DevOps platform that works without any of the time consuming maintenance or integrations, and is natively built for microservices architecture and Kubernetes?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Satish,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Led a group of cross-functional distributed architects & technical leads (10) to come up with detailed architecture and high-level design including CI/CD Pipelines & Testing Stacks."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how resource intensive it can be to maintain the integrations between a CI/CD pipeline. What if there was a way to have an end-to-end, automated CI/CD platform that works without any of the time consuming maintenance or integrations, and is natively built for microservices architecture and Kubernetes?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Ramesh,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Transformed the org to shift left by merging Software Configuration Management and Operations teams to DevOps and quality with development engineers, employing continuous performance testing."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how resource intensive it can be to maintain a DevOps pipeline. What if there was a way to have an end-to-end, automated DevSecOps platform that works without any of the time consuming maintenance or integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Lance,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're currently leading the "Tech Infrastructure / Applications Operations (DevOps)."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how resource intensive it can be to maintain a DevOps pipeline. What if there was a way to have an end-to-end, automated DevSecOps platform out of the box, without any of the time consuming maintenance or integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


*

Hello Steve,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you've "Built from the ground-up, a team of DevOps engineers utilizing a global follow-the-sun support model to convert IDPS into a fully managed service."

Correct me if I'm wrong, but having built a DevOps team from the ground up, you've probably seen firsthand how resource intensive it can be to maintain a DevSecOps pipeline. What if there was a way to have an end-to-end, automated DevSecOps platform out of the box, without any of the time consuming maintenance or integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Malini,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you  "Lead the transformation of the engineering organization to devops model."

Correct me if I'm wrong, but when it comes to transformation to a DevOps model, having a platform that is easy to use is crucial for adoption. What if there was a way to have an end-to-end, DevOps platform that helps your teams more easily adopt DevOps, modernize your offerings, and deliver self-service capabilities?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Marianne,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you  "provide leadership and direction on the transformation / modernization of our application development methodologies (Agile, DevOps)."

Correct me if I'm wrong, but when it comes to modernization of methodologies, having a platform that is easy to use is crucial for adoption. What if there was a way to have an end-to-end, DevOps platform that helps your teams more easily adopt modern practices?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.





Hello Mohan,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you are  "Defining & driving KPIs for Incident management like MTTD, MTTE and MTTR."

Correct me if I'm wrong, but when it comes to MTTD and MTTR, having a DevOps platform that gives you visibility into the entire pipeline is key. What if there was a way to have an end-to-end, DevOps platform that helps your teams reduce your MTTR and MTTD?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for incident management and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Gopinath,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that your "team's core focus is to make Shutterfly sites fast and reliable for millions of users."

Correct me if I'm wrong, but when it comes to ensuring that your sites are fast and reliable, having a resilient DevOps pipeline is key. What if there was a way to have a highly available end-to-end, DevOps platform that helps your teams power the infrastructure?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.





*

Hello Taro,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Created hybrid best-in-class build systems using Python, Bash, Maven, Makefile, Docker and Jenkins pipeline scripts to create stable Java build infrastructure."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how resource intensive it can be to maintain a DevOps pipeline with all those tools. What if there was a way to have an automated, end-to-end, DevOps platform without all of the integrations and maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Chris,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're managing teams across DevOps and security.

Correct me if I'm wrong, but at the end of the day, Sr Directors of Engineer care about balancing speed of delivery with reliability. What if there was a way to have an automated, end-to-end, DevOps platform that helps your teams deliver software faster while building in security and reliability?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Shane,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're "Leading teams with vision and strategy for increasing automation, templating, archetypes increasing DevOps maturity and software delivery from months to hours."

Correct me if I'm wrong but when it comes to increasing software delivery from months to hours, having a DevOps platform that's easy to use is key. What if there was a way to have an automated, end-to-end, DevOps platform that works out of the box and is easy to use, so your teams can have templated archetypes and deliver more frequently?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for DevOps maturity and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Rich,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you're "Passionate about establishing an environment that balances developer productivity and effectiveness with the discipline necessary for reliable software production."

Correct me if I'm wrong but when it comes to balancing developer productivity with effectiveness, having a DevOps platform that's easy to use is key. What if there was a way to have an automated, end-to-end, DevOps platform that works out of the box and is easy to use, so your teams can be more productive and your software more reliable?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for DevOps and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Sreejith,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Instill DevOPS and Quality mindset into the team by driving people and
tooling transformation, getting more automation in place and improving
release velocity."

Correct me if I'm wrong but when it comes to increasing the release velocity, having a DevOps platform that's easy to use is key. What if there was a way to have an automated, end-to-end, DevOps platform that works out of the box and is easy to use, so your teams can release more frequently?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for DevOps and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



*****

Hello Dean,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Defined product roadmap that decentralized release management, increasing the rate of releases per year by 25x"

Correct me if I'm wrong but when it comes to increasing the release rate, having a DevOps platform that's easy to use is key?
What if there was a way to have an automated, end-to-end, DevOps platform that works out of the box and is easy to use, so your teams can release more frequently?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for DevOps and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Jeff,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that you "Established services framework to streamline service creation, testing, CI/CD, monitoring and alerting"

With your experience, you've probably seen first-hand how it's key to have a CI/CD platform that's easy to use. What if there was a way to have an automated end-to-end, DevOps platform that works out of the box and is easy to use, so that your engineers don't have to worry about non-functional requirements?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for DevOps and how it can help with your strategic goals. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Pat,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was that, "As a leader of our organization's DevOps and Lean transformation journey, I learn and coach the CALMS model (Culture, Automation, Lean, Measurement, Sharing) using transformational leadership."

Correct me if I'm wrong, but when it comes to a DevOps journey, having a platform that's easy to use is key. What if there was a way to have an automated end-to-end, DevOps platform that works out of the box and fosters culture, automation, lean, measurement, and sharing?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how it fits in to the CALMS model. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Caroline,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you're "Building a CIP strategy for IT that brings together DevOps, Lean and Agile for IT modernization."

Correct me if I'm wrong, but when it comes to modernization, having a platform that's easy to use is key. What if there was a way to have an automated end-to-end, DevOps platform that works out of the box and is easy to use?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how it can help you in CIP. And in case you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Randy,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "Manage a team of devops teams in our Roasting plants, these teams deliver operational infrastructure that helps drive the management and delivery of roasting operations."

Correct me if I'm wrong, but with your experience, you've probably seen first-hand how DevOps tools can require a lot of maintenace and integrations. What if there was a way to have an automated end-to-end, DevOps solution that works out of the box?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how it can help you in delivering operational infrastructure. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your February is off to a great start.

Hello Yvonne,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "Accelerate product delivery in full software lifecycle including CI/CD and DevOps" in order to deliver  business capabilites at scale.

Correct me if I'm wrong, but with your experience, you've probably seen first-hand how DevOps tools can require a lot of maintenace and integrations. What if there was a way to have an automated end-to-end, DevOps solution that works out of the box?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how it can help you accelerate delivering business capabilities. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your February is off to a great start.


Hello Tyler,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out was how you "transformed teams to enable continuous agile delivery and DevOps culture to focus on digital Products and Services for People Technology."

Correct me if I'm wrong, at the end of the day, VPs care about using DevOps to deliver business value faster. But sometimes, the tools get in the way of that. What if there was a way to have an automated end-to-end DevOps solution that works out of the box and gets out of the way?


If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your February is off to a great start.

Hello Adam,

The reason for my message is because I was reading your LinkedIn profile and found your DevOps teams' mantra to be inspiring.

Correct me if I'm wrong, but with your experience, your team has probably seen how time consuming it can be to maintain the integrations between DevOps tools. What if there was a way to have an automated end-to-end DevOps solution that works out of the box with testing (without the time-consuming maintenance)?


If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, here's to empathetic communication and synergistic sustainability.


Hello Eugene,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was that you "Initiated automation training for manual QA engineers using Java, WebDriver, TestNG, Git, Maven, and Spring."

Correct me if I'm wrong, but when it comes to automating QA testing, having an automated DevOps pipeline is key. What if there was a way to have an automated end-to-end DevOps solution that works out of the box with testing (without slowing down your build times?)

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for QA and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


*

Hello Devang,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your "Data driven approach to deliver e2e solutions following vanilla SDLC and DevOps (agile) lifecycles."

Correct me if I'm wrong, but when it comes to following DevOps lifecycles, having a platform that's easy to use is key. What if there was a way to have an automated end-to-end DevOps solution that works out of the box (without the time-consuming maintenance?)

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello John,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your focus on "Enterprise Architecture, Integration, Security, Cloud, Containers/Clustering, DevOps and Big Data." But more importantly, one line that stood out is your "Keen ability to “tame”/”de-fud” technology and lead technical teams to produce consistently architected/scalable/maintainable software systems."

Correct me if I'm wrong, at the end of the day, VPs care about having a streamlined, efficient, and scalable platform. What if there was a way to have an automated end-to-end DevOps solution that works out of the box (without the time-consuming maintenance?)

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Sam,

The reason for my email is because I was reading your LinkedIn profile. One line that stood out to me was that your teams are "responsible for running all of Yelp's technical infrastructure, as well as software development for core shared systems and developer tooling."

Correct me if I'm wrong, but with your experience you've probably seen firsthand how time-consuming it can be to manage the integrations between all the developer tools. What if there was a way to have an automated end-to-end software development solution solution that works out of the box (without the time-consuming maintenance)?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Danny,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out to me was your focus on "covering Enterprise Architecture, Integration, DevOPS."

Correct me if I'm wrong, butHwith your experience you've probably seen firsthand how time-consuming it can be to manage the integrations between DevOps tools. What if there was a way to have an automated end-to-end DevOps solution that works out of the box (without the time-consuming maintenance)?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Damen,

The reason for my message is because I was reading your LinkedIn profile about your use of DevOps." One line that stood out to me was your "Primary focus on fast-paced problem solving."

Correct me if I'm wrong, but when it comes to fast-paced problem solving, having an automated DevOps pipeline is key. What if there was a way to have an automated end-to-end DevOps that works out of the box?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Ranjit,

The reason for my message is because I was reading your LinkedIn profile. One line that you mention DevSecOps as an area of interest.

Correct me if I'm wrong, but at the end of the day, VPs of Cloud Security Architecture care about reducing their security and compliance risk. What if there was a way to shift security left in you DevSecOps platform and thereby allow your teams to find and remediate vulnerabilities sooner?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Susan,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out where you wrote that you "Drive faster digital transformation and technology adoption"

Correct me if I'm wrong, but when it comes to driving digital transformation and adoption, having a platform that's easy to use and maintain is key. What if there was a way to have an automated end-to-end solution, that your teams are eager to adopt (and thereby help you achieve your strategic goals more quickly)?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, congratulations on completing your PhD. I'll bet you're glad that process is over.


Hello Sridhar,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is your "Experience leading teams using Agile/DevOps methodologies in product and feature development."


Correct me if I'm wrong, but when it comes to using DevOps methodologies, having a platform that's easy to use and maintain is key. What if there was a way to have an automated end-to-end DevOps solution, without the time-consuming maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Rohit,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is your expertise in "standardizing DevOps initiatives, practices and schedules on an enterprise level."


Correct me if I'm wrong, but when it comes to standardizing DevOps initiatives, having a platform that's easy to use and maintain is key. What if there was a way to make standardization easier by using an automated end-to-end DevOps solution, without the time-consuming maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does to automate DevOps and serverless, and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

*

Hello Murali,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is that you "love technologies that simplify life for worldwide consumers."

What if there was a way to simplify life 
for your developers and engineers, by using an automated end-to-end DevOps solution, without the time-consuming maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start, and I hope that you get a full list of great applicants for this year's Spark SIP internships.


Hello Anurag,

The reason for my message is because I was reading your LinkedIn profile. One line that stood out is how you "Provided leadership for deployment of a serverless solution in Microsoft Azure fully supported by CI and CD and end to end automation."

Correct me if I'm wrong but with your experience, you've probably seen how resource intensive it can be to maintain a CI/CD pipeline. What if there was a way to get an automated end-to-end solution that enables serverless, without the time-consuming maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Saurabh,

The reason for my message is because I was reading on your LinkedIn profile that you're using tools like Kubernetes. But more importantly, one line that stood aut is that you're "building scalable micro-services architecture based applications."

Correct me if I'm wrong but with your experience, you've probably seen how important it is to have a DevOps pipeline that enables Kubernetes and micro-services architecture. 

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Orlando,

The reason for my message is because I was reading on your LinkedIn profile that you're using tools like Kubernetes and VMWare.

With your experience, you've probably seen first-hand how important it is to have Kubernetes integrated into your DevOps pipeline.

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Timothy,

The reason for my message is because I was reading on your LinkedIn profile that you "introduced Agile, CI and DevOps practices and tools to improve application delivery."

With your experience, you've probably seen first-hand how resource intensive it can be to maintain a CI/DevOps pipeline. What if there was a way to get DevOps working out of the box, without all the work?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.




*

Hello Earl,

The reason for my message is because I was reading on your LinkedIn profile that you're a "Leader in containerization POC, creating the automation patterns for Docker adoption."

Correct me if I'm wrong, but when it comes to automation and adoption, having a tool that's easy to use is key. What if there was a way to get an automated DevOps pipeline that's easy to use and that your teams want to adopt?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, congratulations on REW's first year in business, I hope that 2020 brings you many more successes.


Hello David,

The reason for my message is because I was reading on your LinkedIn profile that you're "Responsible for leading the rearchitecture of the security operation."

Correct me if I'm wrong but when it comes to security operations architecture, shifting security left is key. What if there was a way to reduce your security and compliance risk by integrating security scanning and vulnerability management into the tools where developers live?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does for security and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, huge fan of VSP, I got two new pairs of glasses covered by your company (one in Dec and one in Jan).


Hello Santosh,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that you're using the Atlassian tool suite.

With your experience, you've probably seen how resource-intensive it can be to maintain a DevOps toolchain. What if there was an easier way that didn't require having to integrate a bunch of tools?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that you're having a great start to February.


Hello Vipul,

The reason for my email is because I was reading through your LinkedIn profile, and I thoroughly enjoyed your article on the Design Thinking. One line that stood out to me was "Create a feedback loop with the customer to get early feedback before iterating further. Fail fast and early is always better while building user-centric products."

What if there was a tool that enables this fast, iterative, user-centric process?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for the informative articles, I do hope you'll keep writing them.

Hello Rachit,

The reason for my email is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you're currently establishing "the role of reliability engineering by merging the product development, security and operations (DevSecOps) talent into one team, increasing efficiencies companywide."

What if there was a way to enable your teams by bringing together DevSecOps capabilities in one tool?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of how GitLab's DevSecOps platform can help you increase efficiencies companywide. And if you feel that it's not a fit, I promise not to flood your inbox with annoying follow-up.

In any case, I appreciate your articles, and your recent post on the Belbin leadership styles was very informative and useful to me

With your success in mind,
Kevin

*

Hello Don,


The reason for my email is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you're currently "working on tooling and infrastructure to enable faster and more meaningful feedback loops, increased stability, and automation for development engineers."

What if there was a way to get automated end-to-end DevOps functionality out of the box, with built in support for Kubernetes? 

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll share with you how GitLab can help you get faster feedback loops, increased stability, and automation to improve the quality of life for your engineers and codebase. And if you feel that it's not a good fit, I promise not to be an annoying salesperson.

Either way, it sounds like your teams are about to embark on an exciting DevOps journey, and I wish you all the best.


Hello Ray,

The reason for my email is because I was reading through your LinkedIn profile. One line that stood out to me was that you're "Responsible for writing integration and reusable components."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain the integrations between tools. What if there was a way to get DevOps functionality without the time consuming maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And I promise not to be an annoying salesperson if you feel that we're not a good fit.

Either way, I hope that your February is off to a great start and that BidBeatuy has a record year.


Hello Jyotindra,

The reason for my email is because I saw that you shared the HBR article "Don't Bring Me Problems, Bring Me Solutions" which I enjoyed as well. But more importantly, you mention that your teams have "successfully increased capacity and efficiency of our infrastructure while continuing to drive down our on-call burden."

Correct me if I'm wrong but when it comes to efficiency of your infrastructure, having a streamlined DevOps pipeline is key. What if there was a way to drive down the burden of managing your devops tools?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams might benefit. And I promise not to be an annoying salesperson if you feel that we're not a good fit.

Either way, thanks for the sharing the article, and I hope that your February is off to a great start.

Hello Salman,

The reason for my email is because I couldn't stop reading through your LinkedIn articles. Really enjoyed the 27 life lessons, #21 definitely resonated with me. But more importantly, you mention that you're "Currently leading Twitch’s Data Platform Team. We build Infrastructure, services and tools to transform data into insights for all of Twitch."

Correct me if I'm wrong but when it comes to building a data platform, using DevOps best practices is key. What if there was a way to make sure that your data lake scales well and has security built in (as you described in "Four Reasons Your Data Platform Sucks")?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might benefit. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, congratulations on the recent move to Twitch, and I hope that you're having a great time.


Hello Bryan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you lead the teams responsible for" enterprise applications, internal application development, and endpoint management." But more importantly, I noticed that you're using Atlassian JIRA and Confluence, alongside Okta.

Correct me if I'm wrong but with your experience, you've probably seen first-hand that there's a significant price increase to use Okta SSO with Atlassian. What if there was a way to get the same functionality of the Atlassian tools, and have SSO, without that increased cost?


If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might benefit. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, congratulations on the recent promotion/new title, and I hope that your February is off to a great start.

**************

Hello Avinash,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you've architected the "CI/CD pipeline to automate the application deployments using AWS, Docker, and Kubernetes."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all of those tools. What if there was a way to automate the end-to-end CI/CD and DevOps process, without all of the maintenance, integrations, and manual tasks.

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might benefit. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, I hope that your February is off to a great start.


Hello Ammar,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention under skills that you're using tools like JIRA, Kubernetes, and Maven.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all of those tools. What if there was a way to automate the end-to-end DevOps process, without all of the maintenance, integrations, and manual tasks.

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might benefit. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, I hope that your February is off to a great start.


Hello Balaji,

The reason for my email is because I was reading through your LinkedIn profile. Very cool that you received your Kubernetes certificate from The Linux Foundation.

What if there was a way to get a DevOps platform that natively uses Kubernetes without the time-consuming tool maintenance and integrations?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.

Hello Evelyn,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you're chartered with improving the experience for developers and automating common tasks for infrastructure and tool creation.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all the tools necessary to deploy into production. What if there was a way to automate the end-to-end deployment process so that developers can securely push code, without all of the maintenance, integrations, and manual tasksL

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how others have been able to achieve <1 day deploys. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, thanks for sharing the article about how volunteering service affects your tax and accounting. Great informative read.


Hello Sumit,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention that you've "Led, architected, designed and developed end-to-end Engineering solutions for all phases of SDLC."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all the tools within the SDLC. What if there was a way to get a DevOps platform without the time-consuming tool maintenance and integrations?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial for your SDLC. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Mehregan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you mention High Availability and Disaster Recovery under your industry knowledge section.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate DevOps tools. And when it comes to high availability of your DevOps platform, that maintenance time is key. What if there was a way to automate a DevOps platform without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.

Hello Ramin,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you're a "Strong advocate of CI/CD with right balance of automation and metrics driven quality control"

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a CI/CD platform. What if there was a way to have an automated a CI/CD and platform that enables a faster release cycle, without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.

Hello Mert,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you're using tools like Kubernetes and Docker.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all those tools. What if there was a way to automate a DevOps platform without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.

Hello Ryan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you're using GitHub and JIRA.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all those tools. What if there was a way to automate a DevOps platform without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.




Hello Jaremy,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that as part of ensuring 100% uptime, you're using "tools like AWS, Spinnaker, Jenkins, Terraform."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and integrate all those tools. What if there was a way to automate a DevOps platform without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Rajesh,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you've previously built the "entire CI/CD pipeline and automated database deployments across environments."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a DevOps process within Big Data. What if there was a way to automate a DevOps without the time-consuming platform maintenance, within your data ecosystem?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Burhan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote that you've developed "Jenkins files to automate the entire CICD process for corresponding services."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and automate a CI/CD process. What if there was a way to automate a DevOps and CI/CD process without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Chitrang,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you wrote on your about.me that "Ideas lead to concepts, concepts lead to innovation, innovation leads to change, change leads to transformation which in turn leads to new ideas -- Its a 'vicious' circle !!"

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and a DevOps platform when having to integrate all those tools. What if the most important and transformative innovation in DevOps is having an end-to-end solution so that you could enable an automated DevOps pipeline without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Nick,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you mention you're an "Autonomous contributor of thorough work and end-to-end solutions." But more importantly you mentioned using tools like Jenkins, Kubernetes, and Chef. 

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and a DevOps platform when having to integrate all those tools. What if there was a way to enable an automated DevOps pipeline without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.




Hello Sriramu,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically mentioned your "core competency in Programming, DevOps and automation frameworks." 

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and a DevOps platform. What if there was a way to enable an automated DevOps pipeline without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.


Hello Jason,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically mentioned using tools like Jenkins, Puppet, and Chef.

Correct me if I'm wrong but when it comes to DevOps, you've probably seen how time-consuming it can be to maintain and integrate those tools. What if there was a way to enable an automated DevOps pipeline without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way I hope that your February is off to a great start.

Hello Grant, 

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was that you specifically that you've designed and built out "mechanisms for continuous delivery supporting all Intuit development teams."

Correct me if I'm wrong but when it comes to DevOps and continuous delivery, you've probably seen how time-consuming it can be to manage a DevOps pipeline. What if there was a way to enable an automated DevOps pipeline without the time-consuming platform maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, great TurboTax superbowl commercial this year. (Even though I like RoboChild better)


Hello Rajan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you're "Leading the transformation of monolith Billing and Commerce systems to Microservices based architecture."

Correct me if I'm wrong, but at the end of the day VPs of Engineering care about adoption of their transformation initiatives. What if there was a way to enable your teams to adopt a microservices architecture more easily through DevOPs?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, great TurboTax superbowl commercial this year. (Even though I like RoboChild better)


Hello Bob,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you manage tools like Atlassian JIRA, Confluence, Crucible, and more.

Correct me if I'm wrong, but with your experience you probably have seen how time-consuming it can be to maintain and manage all those tools/integrations. What if there was a way to reduce the amount of time spent managing all those tools?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it beneficial. And I promise not to be an annoying salesperson if we're not a good fit.

Either way, I hope that your February is off to a great start.




Hello Jim,

The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience with creating a fully automated CI/CD environment. One line that stood out to me was where you wrote, that you created a "Jenkins parallel pipeline tests to reduce test time and improve test coverage."

Correct me if I'm wrong, but with your experience you've probably seen first hand how time consuming it can be to maintain Jenkins with all of the integration and plugins it requires. What if there was a way to get the same fully automate CI/CD functionality you've described with one tool?

If you'll give me an opportunity to meet with you next Wed at 1 PM and share how Test Automation Engineers have used GitLab to automate their testing pipeline while reducing work on tool, I promise not to be an annoying salesperson if we're not a good fit.

Either way, I hope your February is off to a great start. I wish you the best of luck with your new consulting/freelance wed development.

With your success in mind,
Kevin


Hello Matt, 

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "Developed and deployed an effective DevOps capability in a startup R&D environment."

With your experience, you've probably seen firsthand how time-consuming it can be to maintain a DevOps pipeline and its integrations. What if there was a way to get an automated DevOps pipeline without the time-consuming tool management?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Shisheer,

The reason for my email is because I was reading through your LinkedIn profile, and it stood out to me that you specifically mention DevOps as part of your technologies in Data.

Correct me if I'm wrong but when it comes to mixing DevOps and Data, having a data pipeline that's automated is key. What if there was a way to automate your data pipeline, without the typical maintenance caused by DevOps tools?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Paul,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you "Developed and deployed an effective DevOps capability in a startup R&D environment."

Correct me if I'm wrong but when it comes to deploying DevOps capabilities in a startup environment, having a platform that doesn't require a ton of resources is key. What if there was a way to enable DevOps capabilities out of the box?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Jonathan,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that you're "Responsible for Operational excellence and enabling foundational capabilities tied to ALM and DevOps in Enterprise Applications ."

Correct me if I'm wrong but when it comes to enabling DevOps capabilities, having a platform that your teams enjoy using is key. What if there was a way to enable automated DevOps capabilities using a platform that your teams want to adopt?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.



Hello Colin,

The reason for my email is because I was reading through your LinkedIn profile. What resonated with me was where you wrote, "We are taking a DevOps enablement approach that allows teams to select and configure tools in harmony with the uniqueness and collaborative synergies of their processes."

Correct me if I'm wrong but when it comes to DevOps enablement, having tools that create harmony instead of friction is key. What if there was a way to get automated DevOps without the frustrating maintenance and integration work sometimes found in the typical DevOps tools?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I appreciate your approach to management and enablement of knowledge workers.


Hello Brian,

The reason for my email is because I was reading through your LinkedIn profile. What stood out to me was where you said that "both Fast and Good IT are what our business demands - and I have the responsibility to help IT transform to be a dynamic and value creating organization."

Correct me if I'm wrong, when it comes to delivering fast and good, having the right DevOps pipeline is key. What if there was a way to encourage adoption of DevOps so that your teams are empowered to be dynamic and deliver value faster?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.


Hello Martina,

The reason for my message is because I was reading through your LinkedIn profile where you talked about reducing the end-to-end timein deploy & test. But more specifically, you mention using tools like JIRA, Jenkins, and Groovy.

Correct me if I'm wrong, but with your experience, you've probably seen first-hand how time-consuming it can be to maintain the integrations between all of your DevOps tools. What if there was a way to get an automated DevOps pipeline without all of the pesky maintenance?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February is off to a great start.

Hello Raghu,

The reason for my letter is because I thoroughly enjoyed reading your article "The 10 Best Books I Read in 2019." But more importantly, as I was reading through your LinkedIn profile, one line stood out, that you "partner with my peers on both Product and Business at a strategic level to translate business imperatives into roadmaps."


With your experience, you've probably seen first-hand how challenging it can be to get buy-in on new business imperatives and deliver real business value. What if there was a way to enable your teams to align strategic initiatives across product and business, while delivering business value faster?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I appreciate your highlighted book suggestions and look forward to reading your list for next year.

P.S. Included please find a copy of "The Unicorn Project." While it's technically considered "business fiction", I believe you might find the situations to be real and relevant to today's IT organizations.

Dear Flavio,


The reason for my letter is because I couldn't stop reading through your LinkedIn profile. The line that resonated most with me was where you wrote, "I love enabling people to be better with technology and I relish the feedback. As I was once told: 'If people are not complaining about your solution is because they are not using it.'

With your experience, you've probably seen first-hand how time-consuming it can be to maintain a complicated platform. What if there was a way to enable your people to be better and get faster feedback?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I appreciate and share your love of books. Thank you for giving me the impetus to finish "The Soul of A New Machine" (I've had the audiobook, so I'll finally get around to it thanks to you). 

P.S. Included please find a copy of The Unicorn Project. I hope you'll enjoy the read as much as I have.





Hello Vijay,

The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was that you're using tools like Jenkins, Docker, Kubernetes, and Chef.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain all the integrations between those tools. What if there was a way to automate your DevOps pipeline without the time-consuming maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your team could benefit. And I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope that your February is off to a great start, and many good wishes to you for the upcoming year.


Hello Venkat,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you've "Evangelized new technology trends, and identify opportunities to improve product productivity and performance."

Correct me if I'm wrong but when it comes to  new technology trends and improving productivity and performance, having a streamlined DevOps tool is key. What if there was a way to streamline your DevOps tools to increase your teams' productivity and performance? 

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why T thought your team could benefit. And I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope that your February is off to a great start, and congratulations to being on the 2020 list of the world's most admired companies for the 6th year in a row.


Hello Steve,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you're using a "DevOps culture to enable businesses."

Correct me if I'm wrong but when it comes to enabling businesses through DevOps and Site Reliability Engineering, having an easy to use tool is key. What if there was a way to make DevOps and SRE easier by giving your teams on easy to use platform, so they can enable the business more?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why T thought your team could benefit. And I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope that your February is off to a great start, and many good wishes to you for the upcoming year.


Hello Varun,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was that you "Created a new Devops Organization focused on Products and Services for FinTech."

Correct me if I'm wrong, but when it comes to creating a DevOps organization, having tools that are easy to use is key. What if there was a way to increase adoption of DevOps by giving your teams an easy to use platform?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your new team in People Technology could benefit.

Either way, congratulations on the promotion, and here's to your continued success in 2020.


Hello Jose,


The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that your "responsibilities included automating all aspects of the SDLC and environment management."

Correct me if I'm wrong, but when it comes to automating your SDLC and CI/CD, managing fewer integrations is key. What if there was a way to get an automated DevOps pipeline out of the box, without the typical maintenance costs?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope that your February has been off to a great start.


****


Hello Franklin,

The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was that you're "Responsible for the design of enterprise cloud architecture."

Correct me if I'm wrong, but when it comes to cloud consumption, adopting a DevOps pipeline is key. What if there was a way to open more avenues by having another DevOps offering?

Would you be open to meeting next Wed at 1 PM, so I can share how other orgs are using GitLab with Azure? If so, I promise not to be an annoying salesperson and flood your inbox with follow up afterwards.

Either way, I hope that your February has been off to a great start. Thanks for connecting with me.


Hello Donna,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. I couldn't agree more when you wrote that "Some of the most successful people think outside the box and utilize creativity." But more importantly, you mention that your role is in "delivering global solutions in the areas of robotic process automation, digital transformation and application integration."

Correct me if I'm wrong but when it comes to digital transformation, having a unified platform is key to a smooth adoption. What if there was a way to use a unified DevOps platform to enable digital transformation?

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me and thank you for being a volunteer with Guardians of Rescue. It's funny, they say that we rescue animals, but really its the animals that rescue us.


Hello Michael,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that your "teams are responsible for instrumentation of POC’s to drive innovation forward." 

Correct me if I'm wrong, but at the end of the day, VPs of Enterprise Technology care about delivering competitive advantages rapidly to enable their business partners. What if there was a way to drive faster innovation through a common global DevOps platform (similar to what you did with ERP?)

If you'll give me an opportunity next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought your teams could benefit. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me, and I hope that your Q1 has been off to a great start.

Hello Dean,

The reason for my message is because I was reading through your LinkedIn profile and couldn't help but notice that you're working on a pretty cool project in IRSA. But more importantly, you mention using tools like Jenkins and Groovy.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time consuming it can be to maintain a CI pipeline. What if there was a way to get DevOps functionality out of the box without the maintenance?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me. I hope that your February is off to a great start.


Hello Adam,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you help businesses "embrace the cloud while automating tasks and increasing business productivity."

Correct me if I'm wrong but when it comes to automation and increasing business productivity, having a DevOps pipeline is key. What if there was a way to deploy DevOps in any cloud, without having to manage multiple tools and integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, it looks like you're coming up on your 1 year anniversary at Crocs, I hope this next year brings you even more great things.




Hello Christopher,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you "accomplish goals through effective communication, collaboration, and partnership."

Correct me if I'm wrong but when it comes to software engineering and collaboration, having a unified software development platform is key. What if there was a way for your software teams to collaborate and develop software all on the same platform?


If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope your February is off to a great start, and thank you for connecting with me.


Hello Mauricio,


The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was where you said that you are "highly driven at guiding business strategy with established and emerging technologies to achieve maximum productivity impacts with minimum resource expenditures."

Correct me if I'm wrong but when it comes to leveraging emerging technologies to achieve maximum productivity, having an automated DevOps pipeline is key. What if there was a way to use DevOps to increase productivity, increase efficiency, while reducing tooling costs?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope your February is off to a great start, and thank you for connecting with me.



Hello Matthew,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was where you said "Organizations looking to improve workforce productivity need to start by delivering a seamless user experience."

What if there was a way to improve productivity by delivering a seamless software development experience?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope your February is off to a great start, and thank you for connecting with me.




Hello David,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. Couldn't agree more when Patton said, "Do everything you ask of those you command." But more importantly, you mentioned leading teams in "24×7 operations for web delivered application development."

Correct me if I'm wrong, but at the end of the day, Engineering Managers care about delivering software faster, and one way to do that is by using DevOps principles. What if there was a way to do this while reducing the maintenance and integration work associated with your typical DevOps tools?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me, and thank you for your service. I hope that your February is off to a great start.




Hello Eric,

The reason for my message is because I was reading through your LinkedIn profile. One line that stood out to me was that you've led "global organizations undergoing rapid growth and change in response to technology shifts."

Correct me if I'm wrong, but at the end of the day VPs of IT Infrastructure care about enabling their teams to respond rapidly to change. What if there was a way to enable speed in your teams, while reducing the amount of maintenance required for your IT infrastructure?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me. I hope that your February is off to a great start.


Hello Michael,

The reason for my email is because I couldn't stop reading through your LinkedIn profile. One line that stood out to me was how you created a team of QA testers and put "testing process in place and reducing number of code release-related production issues by 35%."

Correct me if I'm wrong but when if comes to BI and testing, having an automated pipeline with short build times is key. What if there was a way to manage this all through a single UI, templatize those tests, and automate your data warehouse testing pipeline?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me. I hope that your February is off to a great start.


Hello William,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. What stood out to me was that you've generated the " continuous integration processes to support build and release pipeline via Microsoft Azure DevOps to support agile software development efforts."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a CI/CD pipeline. What if there was a way to get DevOps working without having to maintain all the tools and integrations?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why other Azure DevOps users find it to be beneficial. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, thank you for connecting with me. I hope that your February is off to a great start.


Hello Leah, 

The reason for my message is because I was reading through your LinkedIn profile. What stood out to me was your use of tools like Splunk and SSO.

Correct me if I'm wrong, at the end of the day what Information Security Analysts care about is reducing security and compliance risk. What if there was a way to integrate security into your deployment pipeline, without the added cost of traditional tools?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it useful. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

In any case, I hope that your February has been off to a great start.


Hello Lynn,

The reason for my message is because I couldn't stop reading your LinkedIn profile. What stood out to me was that you "worked at a financial institution building apps and microservices for a merger."

Correct me if I'm wrong but when it comes to using microservices architecture, having an effective DevOps pipeline is key. What if there was a way to develop software faster while increasing operational efficiency?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought we might be a fit.

Either way, I hope your February is off to a great start, and many good wishes to you for the upcoming year.




Hello Ranjit,

The reason for my message is because I couldn't stop reading your LinkedIn profile. What stood out to me was your use of DevSecOps and OWASP.

Correct me if I'm wrong, but at the end of the day, VPs of Cloud Security Architecture care about reducing security and compliance risk. What if there was a way to build security into your cloud architecture without introducing friction?

If you'll give me a shot next Wed at 1 PM to share with you why VPs of Security and banks like Goldman Sachs use GitLab, I'll give you my best dog and pony show of why I thought we might be beneficial to you. And if you feel that it's not a fit, I promise not to be an annoying salesperson.

Either way, I hope your February is off to a great start, and thank you for connecting with me.


Hello Kristen,

The reason for my message is because I enjoyed reading your article "Negative is Normal." One line that stood out to me was " If you can find a diamond in the mud… then you will end up with many diamonds, my friend."

Correct me if I'm wrong but when it comes to finding diamonds in the mud, cross-functional collaboration is key. I've found that using DevOps best practices and methods for collaboration is super important for finding diamonds in the rough and delivering high quality solutions rapidly.

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show on what GitLab does and why I thought you could benefit.

Either way, great article, and I especially appreciated that Jim Rohn quote.


Hello Eric,

The reason for my message is because I was reading through your LinkedIn profile and noticed that you used tools like Github and Maven.

With your experience, you've probably seen first-hand how time-consuming it can be to develop software when you have to use multiple tools. What if there was a way to deliver software faster by using a single platform?

If you'll give me a shot to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you could benefit.

Either way, I hope that your February is off to a great start and thanks for connecting with me.

Hello Beau,

The reason for my message is because I enjoyed reading the Slalom blog post of their Q&A with you. The line that I really resonated with was where you said that "Business is a team sport, and you succeed and win together."

What if there was a way to enable your consultants and clients to win together by using the same tool to collaborate, and therefore achieve business results faster together?


If you'll give me a shot next Wed at 1 PM to share with you what GitLab does and why I thought you might benefit, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope that your February is off to a great start and that this year brings you many opportunities to spend time with Bodhi and Levi at Sanibel Island.


Hello George,

The reason for my message is because I couldn't stop reading your LinkedIn profile. Your line that "Future is Digital" resonates with me. But more importantly, you mention being familiar with DevOps and tools like JIRA, Confluence, and Jenkins.

With your experience, you've probably seen how a time consuming it can be to maintain the integrations between those tools. What if there was a way to get DevOps functionality without having to use all those tools?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought we might be a fit.

In any case, I hope your February is off to a great start.


Hello Ann-Marie,

The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was how you "Increased team efficiencies, predictability and productivity by 45% through development practices and process improvements."

Correct me if I'm wrong but when it comes to increasing efficiency, predictability, and productivity, adopting DevOps practices is key. What if there was a way to deliver software faster and increase operational efficiency, without the costs and maintenance associated with a typical DevOps pipeline?

If you'll give me a shot next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and how I thought you could benefit. And if you feel that it's not a good fit, I promise not to be an annoying salesperson.

In any case, it means a lot to me that you've been a foster parent for dogs awaiting their forever home. Thank you for that, and I hope your February is off to a great start.



Hello Mike, 

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was your "Focus on providing solutions that increase efficiency and performance while maintaining an economic upside to the user."

Correct me if I'm wrong but when it comes to architecting solutions that increase efficiency and performance, not having to integrate tools is key. What if there was a way to use a single platform to increase both efficiency and performance?


If you'll give me a shot next Wed at 1 PM to share what GitLab does and why I thought you could benefit, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope that your February is off to a great start, and thank you for connecting with me.


Hello Robert,

The reason for my message is because I was reading through your LinkedIn profile resonated with your line that you're a "Linux lover." But more importantly, you specifically mentioned using tools like Docker and Kubernetes.

With your experience, you've probably seen how time consuming it can be to maintain a complex pipeline. What if there was a way to manage your Docker containers and Kubernetes clusters out of a single UI?

If you'll give me a chance next Wed to explain why engineers are using GitLab for the container and K8s support, I promise not to be an annoying salesperson if you feel that we're not a fit.

In any case, I hope that you're February is off to a great start. Noticed you went to Wuhan University, so if you still have any friends or family there, I hope they're okay.



Hello Rishi,

The reason for my message is because I was reading trough your LinkedIn profile and noticed that you use GItLab for source control. But more importantly, you mention that you have an understanding of "all the technical capabilities of SaaS, PaaS, on-premise and IaaS from Integration and Extension perspective."

Correct me if I'm wrong, but with your experience you've probably seen how time-consuming it can be to manage the integrations between tools. What if there was a way to reduce the integrations required in your SOA, and thereby increase operational efficiency?

If you'll give me an opportunity to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of how other solution architects are finding value by using more of GitLab. And I promise not to be an annoying salesperson if you feel that it's not a good fit.

In any case, I hope that your February is off to a great start, and thank you for using GitLab.


Hello Becca,

The reason for my message is because I enjoyed reading your articles on LinkedIn. One line from "Courageous Collaboration" that spoke to me was "The most important thing we can do to create this safe space is to consistently encourage not shut-down."

Collaboration is one of our core values at GitLab, (our motto is "Everyone can contribute"). We've found that having a single tool that everyone uses is a major key to enabling that collaboration.

If you'll give me an opportunity to share with you what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I appreciate your well written articles, and thank you for introducing me to Harriett Lerner's work! ("The Dance of Anger" is now on my reading list.)




Hello Rene,

The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me is that you're supporting the "software development and design, deployment of cloud services."

Correct we if I'm wrong but when it comes to deploying to cloud services, being able to deploy to any cloud is key. What if there was a way to develop sofware, use microservices, and deploy to any cloud, all through a single UI?

If you'll give me an opportunity to share with you what GitLab does and why I thought you could benefit, I promise not to be an annoying salesperson if you feel that we're not a good fit. 

Either way, I hope your February is off to a great start, and thank you for connecting with me.


Hello Brandon, 

The reason for my message is because I was reading on your LinkedIn profile about your use of Azure DevOps. What stood out to me is that you were "brought on to build a new distributed, serverless API." 

What if there was a way to allow you to run your own serverless functions-as-a-service an any infrastructure, through a single UI, without the traditional vendor lock-in of cloud function services?

If you'll give me an opportunity to give you my best dog and pony show on what GitLab does for serverless and how I thought you could benefit, I promise not to be an annoying salesperson if you feel that it's not a fit.

In any case, great article on interpretability in machine learning. I hope this year brings you more (tiger bite optional) adventures.


Hello Liang,

The reason for my message is because I couldn't stop reading your LinkedIn profile. One line that stood out to me was how you migrated "multiple legacy backend services from Intuit Data Center to Kubernetes" without impacting the apps in production.

Correct me if I'm wrong, but with your experience in end-to-end design, coding, test automation, deployment, and monitoring, you've probably seen how having the wrong tools can slow a project down. What if there was a way to manage development, testing, deployment, and your Kubernetes clusters out of a single, unified platform?

If you'll give me a shot next Wed at 1 PM to share how Sr Software Engineers are able to deliver software faster by using GitLab, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope that your February is off to a great start, and thank you for connecting with me.

Hello Michael,

The reason for my message is because I couldn't stop reading through your LinkedIn profile. One line that really stood out to me was that you're "directly involved in the development and implementing of multiple technologies and hosting platforms."

With your experience, you've probably seen firsthand how time consuming it can be to implement and integrate a platform with multiple tools. What if there was a way to reduce the WIP and maintenance work in IT operations, and therefore increase operational efficiency?

If you'll give me a shot next Wed at 1 PM to share with you how systems engineers are using GitLab to automate IT operations and increases efficiency, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, thank you for connecting with me. I hope this year brings you many opportunities to go Fly Fishing.


Hello Kevin,

The reason for my message is because I came across your post on growing up professionally at Gateway in '92, and how you've seen amazing cultural changes at UOPX the last two years. I couldn't agree more with you that culture is key.
 

Correct me if I'm wrong, but when it comes to fostering culture, having the right tools that enable collaboration across IT is important. What if there was a way to have multiple teams across architecture, application development, and data collaborate using the same tool?

If you'll give me an opportunity next Wed at 1 PM, to share with you why VPs use GitLab in their software development to foster culture, I promise not to be an annoying salesperson if you feel that its not a fit.

In any case, it's great to read that you haven't had this much fun since Gateway, and I hope that Extended Stay RV has a great year in 2020.



Hello Varun,

The reason for my message is because I was reading through your LinkedIn profile. One line that really stood out to me was that you've "Promoted microservices architecture, dev-ops culture, shared services, software as a service, shared infrastructure and applications to reduce costs and improve information flows."

With your expertise, you've probably seen firsthand how a complex DevOps pipeline can negatively affect adoption of initiatives. What if there was a way to speed up the adoption of DevOps, while reducing costs?

If you'll give me a shot next Wed at PM to share how GitLab helps reduce the costs of a DevOps toolchain, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, thank you for connecting with me. I hope your February is off to a great start.



Hello Vinay,

Thank you for connecting with me. The reason for my message is because I couldn't stop reading on about your use of server automation with continuous integration. But more importantly, you specifically mention using tools like Jenkins, Ansible, and Kubernetes.

Correct me if I'm wrong but with your experience, you've probably seen firsthand how time-consuming it can be to maintain and integrate all those tools. What if there was a simpler way to build out server automation and CI/CD without all the hassle?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share why DevOps Engineers use GitLab, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope that your February is off to a great start, and thanks again for connecting with me.




Hello Selvakumar,

Thank you for connecting with me. The reason for my message is because I couldn't stop reading on your LinkedIn profile about your implementation of DevOps and DevSecOps methodologies. One line that really stood out to me was that you've been responsible for the "quality center of excellence and success of the software test automation initiative."

Correct me if I'm wrong, but when it comes to software testing automation, having your tests built into the pipeline is a major key. What if there was a way to integrate testing into the CI pipeline easily, without adding more complex tools?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope that your February is off to a great start, and thank you again for connecting with me.



Hello Enrico,


Thank you for connecting with me. The reason for my message is because I enjoyed watching your presentation "Stream SQL with Flink." What really stood out to me was when you said that "Everytime we introduced a more powerful tool, the data produced into the same infrastructure is growing." I couldn't agree more.

With your experience, you've seen firsthand how a complex infrastructure leads to more data. What if there was a way to streamline and consolidate the tooling, in order to increase efficiency and speed of innovation within your data pipeline?

If you'll give me an opportunity next Wed at 1 PM to share with you how data infrastructure teams are using GitLab, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, thank you for the great presentation, it's very cool to see how you're Flink, and best of luck as you build the unified API.



Hello Karl,

Thank you for connecting with me. The purpose of my email is because I was really digging your Volcan Acatenango photos -- #18 was my favourite. But more importantly, you mention on your LinkedIn that you're responsible for driving cost optimizations within Micron's IT infrastructure.

Correct me if I'm wrong, but when it comes to infrastructure, the complexity and integrations between tools are like a volcano always threatening to create more WIP. What if there was a way to optimize costs by simplifying your IT infrastructure?

If you'll give me a shot next Thurs at 2 PM to share with you how other Sr Directors of Infrastructure have reduced costs while increasing service quality through GitLab, I promise not to flood your inbox with follow up if you feel that it's not a fit.

In any case, great photography, thank you for sharing, and I hope this year  brings you many opportunities to shoot in cool locations.

****

Hello Gabriel,

The reason for my email is because I couldn't stop reading your Linked profile. One line that stood out to me was you "demonstrated success realizing business value through strategic planning and IT Architecture."

Correct me if I'm wrong, but with your experience, you've probably seen how an overly complex Enterprise Architecture can sometimes be a bottleneck to realizing business value. What if there was a way to address that bottleneck and enable your teams to deliver business value faster.

If you'll give me an opportunity next Wed at 1 PM to share with you how Chief Architects have enabled their teams to deliver business value faster, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, congratulations on receiving Gartner L2's Genius Digital IQ and Top Brands.



Hello Vivek,

The reason for my email is because I was reading on your LinkedIn profile how you're an "Evangelist for building solutions leveraging DevOps and Dev-SecOps principles and practices."

With your experience, you've probably seen first-hand how time-consuming it can be to maintain a DevOps pipeline. What if there was a way to leverage DevSecOps without the maintenance?

If you'll give me an opportunity next Wed at 1 PM to share with you how Engineering Managers have adopted an automated DevSecOps pipeline, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I appreciate your work in improving engagement and presenting contextual information at the right time. I can only hope that my outreach to you was contextual at the right time.


Hello Kent,

The reason for my email is because I couldn't stop reading your LinkedIn profile about how you've modernized Starbucks Loyalty. But more importantly, you specifically mentioned defining the operating models for IT "including DevOps,(Chef, Puppet and Jenkins)."

With your experience, you've probably seen how time-consuming it can be to maintain a DevOps pipeline. What if there was a way to get highly available DevOps functionality without the maintenance?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought you might find it beneficial, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, thank you for your work in modernizing the Loyalty program.



Hello Anoop,

The reason for my email is because I couldn't stop reading your article, "Who will beat Amazon? Perhaps a native AI company?" One line that stood out to me was your comment on data warehousing, "even in today’s native internet company’s data is siloed in verticals and it is difficult to access data easily. So, for an AI company, it is critical to creating a unified data architecture right from the get-go. No more data stewards fighting over access and control over each other’s data."

What if there was a way to architect your data warehouse so that this is true out of the box?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought it might be a fit for you and your team, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, really appreciate your article and perspective, and will be keeping an eye out for new articles from you in the future.


Hello Philippe,

The reason for my email is because I couldn't stop reading through your LinkedIn profile. The line that really stood out to me was, "I love solving puzzles, and solution architecture gives me the opportunity to solve business and technology puzzles which is why I love what I do." I appreciate and relate to that.

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how time-consuming it can be to maintain a DevOps pipeline. What if there was a way to solve that technology puzzle so that your teams can focus on solving the business puzzles?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how Directors of Enterprise Architecture have enabled their DevOps teams, I promise not to be an annoying salesperson if you feel that we're not a fit.

In any case, I hope this year brings you many opportunities to solve the business and technology puzzles you enjoy




Hello James,

The reason I'm reaching out to you is because I was reading on your Linked in profile where you specifically mentioned tools like JIRA, SVN, and Groovy.

With your experience, you've probably seen first-hand how time consuming it can be to maintain the integrations between those tools. What if there was a way to reduce that maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how, I promise not to be an annoying salesperson if you feel that it's not a fit.

In any case, I hope you're doing well and many good wishes to you.



Hello Jacob, 

The reason I'm reaching out to you is because I was reading on your LinkedIn profile how you're "Overseeing multiple projects run in continuous delivery mode." But more importantly, you specifically mentioned containerization and using Kubernetes.

With your experience, you've probably seen how time consuming it can be to maintain a continuous delivery pipeline. What if there was a way to use Kubernetes and get CI/CD out-of-the-box while reducing the time it takes to manage tools?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how Engineering Directors have been able to automate their continuous delivery, I promise not to be an annoying salesperson if you feel that we're not a fit.

In any case, I hope this January has been treating you well, and many good wishes to you for the upcoming year.

Hello John,

The reason for my email is because I was reading on your LinkedIn profile about your focus on continuous integration. But more importantly, you specifically mentioned tools like Hudson/Jenkins and Groovy.

Correct me if I'm wrong, but with your experience chances are you've seen how time-consuming it can be to maintain a CI/CD pipeline that requires many integrations. What if there was a way to reduce the time it takes to manage your CI tool?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share what GitLab does and why I thought you could benefit, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope your week is off to a great start, and I wish you well with your build and release engineering.

*****

Hello Jose,

Thanks for connecting with me. The reason I wanted to connect with you was because I read on your LinkedIn profile about how you're working in a DevOps/SRE capacity. One comment that really stood out was how you said that "I think this RPA is key to DevOps success factors...  I would use this for continuous testing, developing, etc."

With your experience, and correct me if I'm wrong, but you've probably seen firsthand how time-consuming it can be to maintain a DevOps pipeline that relies on scripts, images, and multiple integrations. What if there was a way to enable DevOps best practices without the maintenance?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how other Engineers have used GitLab to achieve an automated DevOps pipeline, I promise not to be an annoying salesperson if you feel that it's not a good fit.

In any case, I hope you're 2020 is off to a great start, and thank you for your service.


Hello Allen,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about how you've lately been playing with Big Data. One line that really stood out to me was how you're focused on "Bringing DevOps and CI/CD concepts to Data Pipelines."

What if there was an easy way to apply DevOps and CI/CD practices like infrastructure as code to ensure speed and reliability of your Data Lake?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how other Software Engineers are able to bring DevOps and CI/CD to their data pipeline by using GitLab, I promise I won't be an annoying salesperson if you don't think we're a fit.

Either way, sounds like you're working on some very exciting Big Data/DevOps/ML projects, so I wish you the best.


Hello Chin,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about how you drive adoption of CI/CD. But more importantly, you listed using tools like JIRA, Jenkins, Ansible, and Kubernetes.
 
Correct me if I'm wrong but with your experience, you've probably seen firsthand how time consuming it can be to maintain a DevOps CI/CD pipeline with multiple tool integrations. What if there was a way to build a DevOps pipeline without the difficulty of managing and integrating multiple tools?

If you'll give me a shot next Wed at 1 PM to share what GitLab does and why I thought you could benefit, I promise not to be an annoying salesperson if you feel it's not a good fit.

Either way, I hope your Q1 is off to a great start, and many good wishes to you for the upcoming year.

Hello David,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile and noticed that you've used GitLab, alongside other tools like Jira, Confluence, and BitBucket.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain and manage a DevOps toolchain with it's multiple integrations. What if there was a single platform that enabled full DevOps functionality without the need to manage multiple tools?

If you'll give me a shot to meet with you next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does, and I promise not to be an annoying salesperson if it turns out we're not a fit.

Either way, thanks for having already used some GitLab, and I hope your 2020 is off to a great start.



Hello Jeremy,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was that you emphasize "the importance of adopting an Agile mindset across the entire organization, not just IT."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how the wrong tools can create siloes and stunt adoption of Agile. What if there was a tool that increases adoption of Agile across the entire organization, while helping the business achieve its strategic goals?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share how a tool like GitLab enables adoption of agile across the entire IT, I promise not to be an annoying salesperson if you feel that we're not a good fit.

In any case, I hope your Q1 is off to a great start, and I wish you the best of luck with the inaugural Agile Anonymous!

Hello Kedar,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your work with Agile methodology and microservices.

Correct me if I'm wrong, but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a software toolchain with it's multiple integrations. What if there was a way to enable microservices and Agile development without the time-consuming maintenance?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought you could benefit, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.  

Hello Travis,

Thank you for connecting with me. The reason I'm reaching out to you is because I enjoying watching your video "Citrix-as-a-Service in 3 minutes." One line that resonated with me was where you said, "What if there was a way to simplify the management of your Citrix to eliminate the need for upgrade and updates, to have a highly available management component, that is deployed in hours instead of weeks."

What if there was a way to accomplish this same simplification and high availability in your DevOps pipeline, without any of the typical time-consuming integrations and maintenance of disparate tools?

If you'll give me a shot next Wed at 1 PM to share with you what GitLab does and why I thought your emerging technologies group might benefit, I promise not to be an annoying salesperson if you feel that we're not a good fit.

Either way, I appreciate your Youtube videos, thank you for taking the time to make great, highly informative content.


Hello Nitin,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was how you're "Leading the build infrastructure charter using GIT, bit-bucket and artifactory."

Correct me if I'm wrong but With your experience, you've probably seen first-hand how important it is to have testing and QA built into your infrastructure. What if there was a way to manage your git, artifacts, CI/CD, and testing all out of one tool, without any of the time-consuming maintenance?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought your team could benefit, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Hello Riyaz,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your history with CloudOps. But more importantly, you specifically mentioned using tools like Azure, Jenkins, and Docker.

Correct me if I'm wrong, but with your experience, you've probably seen first-hand how time-consuming it can be to manage a complex CloudOps pipeline with all of its different tools. What if there was a way to automate your cloud infrastructure and deployments, without the time-consuming tool maintenance? 

If you'll give me a shot next Wed at 1 PM to meet with you and share how Cloud Engineers are automating their infrastructure with GitLab, I promise not to be an annoying salesperson if you feel that we're not a good fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.


Hello Scott,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading on your LinkedIn profile about your work in DevOps. One line that stood out to me was that you're "Responsible for managing all back-end integrations, transactional site operations, and middleware application development"

With your experience, you've probably seen first-hand how time-consuming it can be to maintain a complex DevOps pipeline with multiple integrations. What if there was a way to enable Agile developments and releases, while reducing the time-consuming integrations?

If you'll give me a shot next Wed at 1 PM to share with you what GitLab does and why I thought your team might benefit, I promise not to be an annoying salesperson if you feel we're not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Hello Jason,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile and couldn't help but take note of your integral role in architecting SRE. One line that stood out to me was that you're "I’m defining requirement and integrating monitoring and alerting tooling."

Correct me if I'm wrong, but with your experience you've probably seen first-hand how managing integrations between tools can be time-consuming and a key source of incidents. What if there was a way to enable DevOps practices without the brittle integration?

If you'll give me a shot next Wed at 1 PM to share with you what GitLab does and why I thought it might be a fit for your team, I promise not to be an annoying salesperson if you feel that we're not a fit.

In any case, best wishes to you integrating SRE practices into your 500 person team. 

Hello Bradley,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile. One line that stood out to me was your role in "develop and align the product roadmap across all ITIL processes."

With your experience, you've probably seen first-hand how a disconnected ITIL process can be detrimental to productivity. What if there was a way to have an aligned software development product stream, without any of the time-consuming integrations?

If you'll give me a shot next Wed at 1 PM to share with you how Systems Engineers have used GitLab to align their product roadmap across ITIL processes, I promise not to be an annoying salesperson if you feel that we're not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.




Hello Jack,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your profile and appreciate what you wrote about finding freedom and therapy in coding. 

With your experience, you've probably seen how a disconnected DevOps pipeline can be a barrier to writing code. (For example, stopping your flow to sign into JIRA to log your changes.) What if there was a way to be able to remove those barriers, so that you could simply focus on coding?

If (you'll give me a shot next Wed at 1 PM to share with you how Lead Developers have removed barriers to coding with GitLab,) {
then I promise not to be an annoying salesperson if you feel we're not a good fit.
}
Else {, I can relate to and appreciate your attitude to coding. Many good wishes to you for the upcoming year.}



Hello Jordan,

Thank you for connecting with me. The reason for my reaching out to you is because I was reading through your LinkedIn profile about your experience with DevOps. But more importantly, you specifically mentioned using tools like Azure DevOps, Salt, and F5.

With your experience, you've probably seen firsthand how time-consuming it can be to maintain the integrations between your DevOps tools. What if there was a way to get a completely automated DevOps pipeline without the integrations or maintenance?

If you'll give me a shot next Wed at 1 PM to give you my best dog and pony show of what GitLab does and why Azure DevOps users are moving to our automated pipeline, I promise not to be an annoying salesperson if you feel that it's not a good fit.

Either way, I appreciate your taking the time to connect, and many good wishes to you for the upcoming year.



Hello Jerry, 

Thank you for connecting with me. The reason for my email is because I was reading on your LinkedIn profile about your proficiency with DevOps and CI/CD. But more importantly, you list that you're using tools like Jenkins, Chef, and Maven for release and deployment management.

With your experience, and correct me if I'm wrong, but you've probably seen first-hand how time consuming it can be to maintain a DevOps platform that uses those tools. What if there was a way to automate your CI/CD and DevOps platform, without all of the maintenance and integrations?

If you'll give me a shot next Wed at 1 PM to share with you how other SREs are enabling DevOps without the complexity, I promise not to be an annoying salesperson if you feel that it's not a good fit.

Either way, thanks for making Turbotax highly available. It's a great product for those of us with simpler tax needs.


Hello Murali,

Thank you for connecting with me. The reason I'm reaching out to you is because I enjoyed reading your LinkedIn article "Culture Shift, Transformation, and Change." One line That stood out to me is your exhortation that "Investments in [...] #Automation, #Simplification", [...] #AppModernization, #DigitalTransformation, etc., hold promise. Pursuing these promises require us to act. And we need to act now."

With your experience, you've probably seen how time-consuming it can be to maintain a DevOps platform. What if there was a way to simplify, automate, modernize, and enable digital transformation through an easy to use DevOps platform that your teams want to use?

If you'll give me an opportunity next Wed at 1 PM to share with you how VPs have been able to enable culture shift and transformation by using GitLab, I promise not to be an annoying salesperson if you feel that it's not a suitable solution.

In any case, thank you for the well-written article, and many good wishes to you for the upcoming year.


Hello Kha, 

I saw that you commented on Jon Smart's LinkedIn post about his upcoming book. One line that stood out to me was "I hope you weave in learnings from your Barclays days."

What if there was a platform tailor made for banks looking to adopt DevOps to deliver better value sooner, safer, and happier?

If you'll give me a shot next Wed at 1 PM to share with you how banks like Goldman Sachs and ING use GitLab, I promise not to be an annoying salesperson if you feel we're not a good fit.

In any case, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.



Hello Himanshu,

The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.

But more importantly, I was reading through your LinkedIn profile and noticed that you list Azure, Kubernetes, and Docker under tools that you use.

With your experience, you've may seen how time consuming it can be to manage an Azure DevOps pipeline with it's multiple tool integrations. What if there was a way simplify your DevOps pipeline and make it easier to adopt, by using a single platform, with built in Kubernetes support?

If you give me a shot to meet with you next Wed at 1 PM to share what GitLab does any why I thought you might find it useful, I promise not to be an annoying salesperson if you decide we're not a good fit.

Either way, I hope that your 2020 is off to a great start and many good wishes to you for the upcoming year.

Hello Stuart,

The reason for my email is because I had a nice conversation with Harsha Muktamath. He and I got around to discussing you and your role. He thought it might be beneficial we speak, so I promised him I'd reach out to you.

But more importantly, I was reading through your LinkedIn profile and noted that your specialties includes .NET development. Correct me if I'm wrong, but this likely means you're using TFS or Azure DevOps.

With your experience, you've may seen how time consuming it can be to manage a TFS or Azure DevOps pipeline with it's multiple tool integrations. What if there was a way simplify your DevOps pipeline and make it easier to adopt, by using a single platform?

If you'll give me an opportunity to meet with you next Wed at 1 PM to share what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if you feel it's not a fit.

Either way, I hope that your 2020 is off to great start, and many good wishes to you for the upcoming year.





Hello Harsha, thank you very much for pointing me in the right direction. Any advice on what might be important to them?

In any case, I appreciate your guidance. I'll let you know if I'm able to set up some time with either Himanshu or Stuart.

Wish me luck!
Kevin



Hello Timur,

Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIn profile about your experience with Cloud DevOps. But more importantly, you specifically mentioned that you're "Always looking a way to bring something special and brand new. Always seeking an idea how to improve the company I work for."

With your experience, you've probably seen how time consuming it can be to manage a complex DevOps pipeline with it's multiple tool integrations. What if there was a way to improve your DevOps pipeline, make it easier to adopt, by using a single platform?

If you'll give me a shot to share with you next Wed at 1 PM what GitLab does and how this new platform can help improve your DevOps pipeline, I promise not to be an annoying salesperson if you feel we're not a fit.

In any case, I appreciate your being open-minded to a conversation, and many good wishes to you for the upcoming year.


Hello Shane,

Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIN profile about your role in developing the continuous software release management services. One line that really resonated with me was how you've been able to enable "smaller more continuous delivery of software updates to the customer."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how time consuming it can be to manage a CI/CD pipeline when it comes to maintaining the tool integrations. What if there was a way to enable continuous integration and continuous delivery without the complexity of managing the toolchain?

If you'll give me a shot next Wed at 1 PM to share with what GitLab can do to help you enable smaller continuous delivery, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I wish you a great upcoming tax season, and I appreciate your work in making my online tax filing software better.

Hello Sree,

Thank you for connecting with me. My reason for reaching out to you is because I was reading through your profile about your role in "managing application architecture, development & execution of end to end Quote to Cash business process." 

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how managing a complex application architecture and maintaining the integrations between tools is time consuming. What if there was a way to simplify your architecture so that your teams can deliver solutions faster?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought your application architecture could benefit, I promise not to be an annoying salesperson if you feel that it's not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year. 


Hello Dobroslav,

Thank you for connecting with me. My reason for reaching out to you is because I was reading through your profile about your role in "Growing our DevOps practice" there at Slalom. But more importantly, you've mentioned specifically that you've automated "legacy infrastructure with Chef, Ansible."

Correct me if I'm wrong but with your experience, you've probably seen first hand how managing integrations between multiple tools can be time consuming. What if there was a way to automate your infrastructure without the time consuming tool management, and therefore make it easier to adopt and grow your DevOps practice?

If you'll give me an opportunity next Wed at 1 PM to share with you what GitLab does and why I thought you could use it for growing your DevOps practice, I promise not to be an annoying salesperson if it turns out we're not a great fit.

Either way, I hope your 2020 is off to a great start and best of luck to you with your DevOps growth!



Hello Harsha,

Thank you for connecting with me. The reason for my email today is because I was reading through your LinkedIn profile. More importantly, you specifically mentioned your expertise in technologies like "Docker Containers, Virtualization, Kubernetes."

Correct me if I'm wrong, but when it comes to a data management platform in the cloud, the ability to manage your Docker Containers and Kubernetes clusters in an easy way is key. What if there was a tool that allowed you to manage your data CI/CD pipeline, docker containers, and Kubernetes clusters without adding complexity?

IF you'll give me a shot next Wed at 1 PM to share what GitLab does and why I thought it'd be useful to your as you manage your data platform, I promise not to be an annoying salesperson if you decide it's not a fit.

Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year.


Hello Dariusz,

The reason for my email today is because you had previously mentioned that the beginning of this year might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.

You had mentioned that you were working on AWS and Terraform (switching roles, managing dev accounts, and managing Terraform modules), as well as interested in migrating from Jenkins CI.

If you'll give me another opportunity to share with you next Wed at 1 PM how GitLab could help with both the Terraform management and Jenkins migration, I promise I won't be an annoying salesperson even if you decide it's not a good fit for your needs.

Either way, I hope you're having a great start to 2020, and many good wishes to you for this upcoming year.

Hello Saba,

We have not yet had the fortune of meeting. The reason for my outreach is because I was reading through your LinkedIn profile and appreciated your experience in DevOps. You specifically mentioned that your focus spans "Security / DevOps / Cloud Operations / Secure Deployment and Automation."

What if there was a way to enable all of the functionality of a secure DevOps toolchain that is automated end to end?

If you'll give me an opportunity to meet with you next Wed at 1 to share what GitLab does and why I thought you might find it beneficial, I promise not to be an annoying salesperson if it turns out we're not a fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

With your success in mind,
Kevin

****

Hello Marcello,

Thank you for connecting with me My reason for reaching out to you is because I enjoyed reading through your article on "3 things to consider when operating Kafka Clusters in Production." I couldn't agree more with your sentiment that "you will need a highly technical team to keep it running. It is definitely not a platform you will implement and forget about it."

With your experience, you've probably seen that the same is true for many DevOps pipelines comprised of multiple tools. The integrations can be time consuming to maintain, for example. What if there was a way to implement your DevOps pipeline without all of this hassle (set it and forget it)?

If you would be open to meeting with me next Wed at 1 PM, I'll give you my best dog and pony show of what GitLab does and why I thought you might find it useful, and I promise not to be an annoying salesperson if we're not a good fit.

In any case, really appreciated your article on Kafka - thanks for the informative read!


Hello Jordan,

Thank you for connecting with me. My reason for reaching out to you is because I was reading through your LinkedIn profile about your experience designing highly available and resilient systems with Terraform. One line that stood out to me was how you've designed and implemented "HA and enterprise compliance focused infrastructure."

I have an idea for how you could reduce the time it takes for your release process. 

If you'll give me a shot next Wed at 1 PM to give you my best dog and pony show on what GitLab does, how it integrates with Terraform, and why I thought you might find it useful, I promise not to be an annoying salesperson if you decide it's not a fit.

Either way, I appreciate the work you do at Twitch to keep livestreaming highly available and resilient


Hello Justin,

Thank you for connecting with me. The reason for my email is because I was reading through your profile and really appreciate your mission "to fuse cloud, automation, and security capabilities into increasingly business-facing ones (e.g. data, apps, and organizations) as quickly as it can be accepted".

Correct me if I'm wrong, but in order for those capabilities to be accepted, making the easy/simple to adopt is key. What if there was a way to build automation, cloud, and security capabilities into your DevOps pipeline such that your clients actually enjoy adopting them?

If you'll give me a shot next Wed at 1 PM to share with you what GitLab does and why I thought you might benefit, I promise not to be an annoying salesperson if you decide that it's not a good fit.

In any case, I appreciate the true spirit of DevOps as you see it, and couldn't agree more about being able to be part of every stage of an outcome's lifecycle.


Hello Ben,

Thank you for connecting with me. The reason I'm writing to you is because I was reading through your LinkedIn profile and couldn't help but notice your experience with building a DevOps team. One line that stood out to me was your "strong knowledge of the infrastructure and services needed to support the demands of large enterprise companies."

Correct me if I'm wrong, but with your experiences, you've probably seen firsthand how difficult it can be to manage an enterprise DevOps pipeline with its many integrations. What if there was a way to build the DevOps infrastructure without having to worry about integrations?

If you'll give me an opportunity next Wed at 1 PM to share with you how Sr Directors have created an entirely automated DevOps pipeline that works out of the box, I promise not to be an annoying salesperson if you decide that it's not a good fit.

Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year.

Hello Wynn,

Thank you for connecting with me. The reason for my outreach is because I was reading through your LinkedIn profile relate to your interest in automating everything. One line that really stood out to me was how you led the, "DevOps efforts around automating build and integration pipelines to ensure the repeatability and stability of builds, incorporating security hardening, product testing and metrics gathering."

Correct me if I'm wrong, but with your experience, you've probably seen firsthand how difficult it can be to manage a complicated DevOps pipeline with many integrations. What if there was a way to build and automate the entire pipeline through one tool, without having to worry about integrations?

If you give me an opportunity next Wed at 1 PM to share with you how GitLab enables full stack automation and is built for containerization, I promise not to be an annoying salesperson if you decide it's not the right fit. 

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.


Hello Chris,

Thank you for connecting with me. The reason for my email is because I was reading through your profile and couldn't help but take notice of your DevOps experience. You specifically listed Kubernetes, TFS, and Microsoft Azure under your tools and technologies.

Correct me if I'm wrong, but with your experience, you've probably seen first hand how a complex DevOps pipeline can be time-consuming to maintain. What if there was a way to have SCM + CI/CD and native Kubernetes support available out of the box, while reducing the work to maintain and integrate the tools?

If you give me an opportunity next Wed at 1 PM to share with you what GitLab does and the value other TFS/AzureDevOps users have found, I promise not to be an annoying salesperson if you decide it's not a fit.  

Either way, I hope your 2020 is off to a great start and that you're enjoying your new role at Reply. 


Hello Steven,

Thank you for connecting with me. The reason for my email is because I was reading through your profile and couldn't help but take notice of your DevOps experience. One line that stood out to me was, "continuous integration/deployment, shortening the feedback loop, metrics-driven decision making."

Correct me if I'm wrong, but with your experience, you've probably seen first hand how a complex DevOps pipeline with multiple tools can be time-consuming to maintain and actually lengthen the feedback loop. What if there was a way to get SCM + CD/CD all in one tool out of the box, and thereby shorten the feedback loop?

If you give me the opportunity next Wed at 1 PM to give you my best dog and pony show about what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if you decide it's not a fit.

Either way, I hope your 2020 is off to a great start.

Hello Neha,

Thank you for connecting with me. The reason for my outreach is because I was reading through your profile and was struck by this line you wrote; "Designing and building CICD infrastructure for automatic code deployments to AWS."

Correct me if I'm wrong, but when it comes to building CI/CD for automatic deployments, wouldn't it be great if you could have that working out of the box with one tool?

If you give me the opportunity next Wed at 1 PM to discuss how what GitLab does for CI/CD, Big Data, and ML, I promise not to be an annoying salesperson if you decide it's not a good fit. 

Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year. 


Hello Harpreet,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience as a DevOps consultant. One line that stood out to me was how you've provided "CI/CD for multiple environments through Git, Azure DevOps, and ARM Templates and into the Azure Cloud environment."

Correct me if I'm wrong, but with your experience you've probably seen first-hand how time consuming it can be to maintain the integrations between tools. What if there was a way to have your CI/CD pipeline and DevOps environment all in one tool out of the box, without any of the maintenance required?

If you give me an opportunity next Wed at 1 PM to discuss what GitLab does and how other connected/automotive projects like Jaguar have used our DevOps tool, I promise not to be an annoying salesperson if you decide that it's not the right fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

****

Hello Ken,

Thank you for connecting with me, I hope the new year is finding you well. The reason I'm writing to you is because I was reading through your LinkedIn profile and saw that you've used GitLab. One line that stood out to me was where you wrote about how you drove "continuous delivery of value to the Customer and business, moving towards a DevOps environment."

Would it be at all valuable to you in your new role to have that DevOps environment and continuous delivery all enabled by GitLab?

If you would give me an opportunity next Wed at 1 PM to discuss how GitLab might be useful to you at Umpqua, I promise I won't be an annoying salesperson if you decide it's not a suitable fit.

Either way, I hope that your 2020 is off to a great start and that you're enjoying your new role.

Hello David,

Thank you for connecting with me. The purpose for my outreach is because I was reading through your LinkedIn profile and your implementation of systems automation using tools like Puppet/Ansible/Terraform caught my eye. One line you wrote that really stood out was how you've "created many new build and deployment patterns that save us time and headache."

Correct me if I'm wrong but with your experience, chances are you've seen first-hand how time-consuming and headache inducing it can be to maintain the integrations between tools. What if there was the same way to get the same systems automation functionality without having to use a bunch of disparate tools?

If you would give me an opportunity next Wed at 1 PM to give you my best dog and pony show of what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if you decide it's not a suitable solution.

Either way, I hope your 2020 is off to a great start, and thanks for all you do to keep livestreaming rock solid.

Hello John,

The reason for my outreach is because I was reading on your LinkedIn profile that you're currently leading your department through a DevOps transformation. One line that stood out to me was, "if you don't test it now, you'll have to debug it later." I couldn't agree more about.

Correct me if I'm wrong, but the faster you're able to discover and address bugs, the less costly and time consuming they will be. What if there was a way to build your testing right into the CI/CD pipeline where the developers can catch them early and often?

If you'll give me a shot next Wed at 1 PM to unpack what GitLab does and why I thought your DevOps transformation could benefit, I promise not to be an annoying salesperson if you decide that it's not a good fit.

Either way, I hope your 2020 is off to a great start and that this year brings you plenty of good dives!

Hello Jim,

Thank you for connecting with me. The reason I'm reaching out to you is because I was reading through your LinkedIn profile about your experience with creating a fully automated CI/CD environment. One line that stood out to me was where you wrote, that you created a "Jenkins parallel pipeline tests to reduce test time and improve test coverage." 

Correct me if I'm wrong, but with your experience you've probably seen first hand how time consuming it can be to maintain Jenkins with all of the integration and plugins it requires. What if there was a way to get the same fully automate CI/CD functionality you've described with one tool?

If you'll give me an opportunity to meet with you next Wed at 1 PM and share how Test Automation Engineers have used GitLab to automate their testing pipeline while reducing work on tool, I promise not to be an annoying salesperson if we're not a good fit.

Either way, it sounds like you've already built a robust CI/CD testing environment, but thought you may be open to learning more even if it's only for down the road.

Hello Nick,

Thank you for connecting with me. The reason for my outreach is because I was reading through your LinkedIn profile and noted your expertise on K8s and GCP. One line you wrote that stood out to me was, "Implementation of DevOps best practices and consultancy on organizational and technological transformations."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a complex DevOps pipeline and how this slows down adoption (with all of the tools and integrations required.) What if there was a way to get DevOps functionality from one tool, out of the box (including native support for K8s and GCP), so your teams can spend less time managing the toolchain and do more value-added work?

If you'll give me a shot to meet with you next Wed at 1 PM and give you my best dog and pony show on what GitLab does and why I thought you might find it useful, I promise not to be an annoying salesperson if it turns out we're not a fit.

Either way, I appreciate

With your success in mind,
Kevin

Hello Jim,

Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and was struck by your championing of DevOps at VSP. One line that really stood out to me was how you "Initiated DevSecOps movement within the organization to bring better collaboration and instill a 'shift left' mentality" between the teams.

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a complex DevSecOps pipeline with all of the integrations required. What if there was a way to get DevSecOps functionality from one tool, out of the box (including SAST/DAST/etc), without the complex integrations, so your teams can spend less time managing the toolchain and do more value-added work?

If you'll give me a shot to meet with you next Wed at 1 PM to unpack what GitLab does and how Sr Managers of Software Engineering have leveraged us to get their teams DevSecOps functionality off the shelf, I promise I won't be an annoying salesperson if you decide we're not a suitable solution.

Either way, I appreciate your championing the cause, and many good wishes to your for the upcoming year. (Also, thanks for DevOps-ing my glasses. Just got a new pair through my VSP insurance.)

With your success in mind,
Kevin


Hello Ted,

Thank you for connecting with me. The reason for my email is because I was reading through your LinkedIn profile and couldn't help but notice your experience with leveraging microservices architecture. Two lines that stood out to me were how you "led development of an end to end solution" and streamlined efficiency through a microservice build system.

What if there was a way to increase engineering efficiency through a similar end-to-end solution, that natively leverages microservices to avoid having engineers siloed off with their respective tools?

If you'll give me a shot to meet with you next Wed at 1 PM to unpack how Directors of Engineering have used GitLab to streamline their toolchain and increase efficiency, I promise not to be the annoying salesperson who won't leave you alone if you decide we're not a good fit.

Either way, I hope your Q1 is off to a great start, and many good wishes to your for 2020.

With your success in mind,
Kevin

Hello Gaven,

The reason I'm writing to you is because I was reading through your LinkedIn profile and couldn't help but notice your experience with DevOps. One line that really stood out to me was your, "Leadership of a design, integration, orchestration, automation, and operations organization of DevOps, Systems Engineering, and Service Operations teams."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a complex DevOps pipeline. What if there was a way to get DevOps functionality out of the box, without the complex integrations, so your teams can spend more time doing value-added work?

If you'll give me a shot to meet with you next Wed at 1 PM to unpack how Infrastructure Directors have been able to automate and simplify their DevOps pipeline with GitLab, I promise not to turn into an annoying salesperson if you decide we're not a good fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to your for the upcoming year.

With your success in mind,


Hello Sharadha,

The reason I'm writing to you is because I was reading through your LinkedIn profile about your experience with DevOps, and one line you wrote caught my eye; "optimizing organizations for increased happiness at the workplace, and in turn, increased productivity." I couldn't agree more how important that is, and correct me if I'm wrong, it sounds like you're describing the ideals from Gene Kim's "The Unicorn Project."

What if there was a way to engineer your deployment pipeline for that increased happiness and productivity, by reducing the amount of toil and without adding complexity?

If you'll give me an opportunity to meet with you next Wed at 1 PM to unpack how Directors of Engineering have been able to increase happiness and productivity within their organization through GitLab, I promise not to be an annoying salesperson if it turns out we're not a good fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

With your success in mind,
Kevin

Hello Marc,

The reason I'm writing to you is because I was reading through your LinkedIn profile about how you've built scalable server-less solutions and a "mature CI/CD pipeline with CircleCI for our APIs with full integration testing process."

Correct me if I'm wrong but with your experience, you've probably seen first-hand how time-consuming it can be to maintain a complex CI/CD pipeline. What if there was a way to get DevOps functionality out of the box, without the complex integrations, so your teams can spend more time doing value-added work?

If you'll give me a shot to next Wed at 1 PM to share with you what GitLab does and why I thought you might find it useful, I promise not to turn into an annoying salesperson if it turns out we're not a fit.

In any case, I hope your Q1 is off to a good start and many good wishes to you for the upcoming year.

Hello Josh,

The reason I'm writing to you is because I was reading through your LinkedIn profile, and one line you wrote stood out; "Implementing solutions to verify application security during CI/CD to keep the features flowing." 

What if there was a way to have application security built into the CI/CD pipeline natively and have it work out of the box, using a tool you already have in house? This way you'd have implemented security standards where developers get immediate feedback for remediation.

If you'll give me a chance to meet with you next Wed at 1 PM to unpack that idea, I promise I won't be an annoying salesperson if you decide it's not a fit.

Either way, I hope your 2020 is off to a great start!


Hello Simon,

Thanks for connecting with me.The reason I'm writing to you today is because I was reading through your LinkedIn profile about how you've successfully built out teams covering DevOps.

With your experience, you've probably seen firsthand how time-consuming it can be to maintain a complex DevOps pipeline. What if there was a way to get DevOps functionality out of the box, without the complex integrations, so your teams can spend more time doing value-added work?

If you'll give me a shot to meet with you next Wed at 1 PM and share what GitLab does and why I thought you might find it useful, I promise not to turn into an annoying salesperson if it turns out we're not a fit.

In any case, I appreciate the work you do to keep video streaming fast and reliable!


Hello Dawson,

The reason I'm writing to you today is because was reading through your LinkedIn profile about how you configured a fully automated CI/CD pipeline ("utilizing CircleCI to build Docker images from version control and push them to Docker Hub and Jenkins to deploy containerized applications into prod.")

I was just wondering... if there was a way to do this all using just one platform, and have a fully automated pipeline working out of the box, would that be at all valuable to you?

If you'll give me a shot next Wed at 1 PM to discuss what GitLab can do for you and why I thought you might find it valuable, I promise not to be an annoying salesperson if it's not a good fit.

In any case, sounds like you're already doing a lot of the latest and greatest - so just wishing you a good start to 2020!


Hello Nicole,

The reason I'm writing you today is because I was reading through your LinkedIn profile and noticed your interest in tools like GitLab, Jenkins, and Ansible.

Now, I may not be able to help you with "Donut Eating", but I may have a few ways we can help streamline and automate your DevOps pipeline.

If you'll give me a shot to meet with you next Wed at 1 PM and discuss how GitLab might fit into the picture, I promise not to be an annoying salesperson or eat all the donuts if we're not a good fit.

Either way, hope you're having a great start to 2020 so far! 

Hello Rachit,

Thanks for connecting with me. The reason I'm writing you today is because I enjoyed reading your article, "7 Laws that Guide Tech Leaders." I couldn't agree more with your sentiments around Conway's law and how to mitigate it's effects.

What if there was a way to ensure that your SDLC is consistent and that there's an easy path to onboard, without adding complexity to your environment?

If you'll give me a shot next Wed at 1 PM to unpack how Directors of Engineering have used GitLab to achieve their DevSecOps initiatives, I promise not to be an annoying salesperson if it turns out not to be the right fit.

Whether you meet with me or not, I appreciated you taking the time to write the article and assembling those 7 laws into a coherent guide. Hope your 2020 is off to a great start!


Hello Todd,

The reason for my email is because I enjoyed watching your panel with Vish and Annette. Your comment to "Make friends with your security organization" and to work with them because security is everyone's problem really resonated with me.

I have an idea for how you could build in security to your DevOps pipeline such that this collaboration becomes easier.
 
If you'll allow me an opportunity to meet with you next Wed at 1 PM and unpack this idea, I promise not to flood your inbox with follow up if we're not the right fit.

Either way, I appreciate your comments and insights from the panel!

The reason for my outreach is because I was reading an article about FutureStores where you were quoted as saying, "We want to enable the interactions and not just show technology for the sake of it. The goal is to make the technology invisible and seamless to the customer experience.” 

Correct me if I'm wrong, but when it comes to making technology invisible and seamless, it's just as important to do this for your internal teams. What if there was a way to make your DevOps pipeline seamless and invisible, requiring less maintenance and work from your infrastructure team?

If you'll give me a shot next Wed at 1 PM to unpack how companies like Disney have used GitLab to enable a seamless DevOps experience, I promise not to be an annoying salesperson if you decide that it's not a fit. 

Either way, I hope your 2020 is off to a great start


Hello Suresh 

The reason I'm reaching out to you is because I saw your post on LinkedIn about hiring an infrastructure automation engineer. But more importantly, a line that caught my eye from your profile is that you're "Executing shared DevOps vision."

What if there was a way to execute your DevOps and automate the infrastructure without adding complexity your environment?

If you'll give me a shot next Wed at 1 PM to share how Sr IT Managers have automated their infrastructure and driven DevOps adoption through GitLab, I promise not to be an annoying salesperson if it turns out we're not a good fit.

Either way, I hope that your 2020 is off to a good start, and good luck on hiring your infra automation engineer! 



Hello Karl,

The reason for my outreach is because I was reading through your LinkedIn profile about your responsibilities within infrastructure and operations. One line that caught my eye was, "driving service quality up and cost optimizations through adoption of innovative technology"

Correct me if I'm wrong, but when it comes to infrastructure, the simplicity of your pipeline is key. What if there was a way to achieve cost optimization while driving service quality up, without adding complexity to your environment?

If you'll give me a chance next Wed at 1 PM to share what GitLab does and why I thought you might benefit, I promise not to be an annoying salesperson if it turns out we're not a fit.

Either way, I hope your 2020 is off to a great start and that this year brings you a ton of opportunities for photography!


Hello Ashutosh,

The reason for my email is because I was reading through your LinkedIn profile about your experience, and it was really inspiring to see that you've "Led IT Cloud Strategy to enable 34% cost savings, faster time to market, and hyper scale capability on demand."

It's great to hear that you're doing all of the right things. You're probably not in the market for new DevOps technology right now, but I'd like to share some insights from others we've helped to drive faster-time-to-market, cost savings, and scaling capacity.

If you'll give me a shot next Wed at 1 PM to explain what GitLab does and why I thought it might be a fit for you, I promise I won't be an annoying salesperson if we're not a good fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.


Hello Rich, 

The reason for my email is because I was reading through your profile about your DevOps experience, and you specifically mentioned tools like Jenkins and JIRA.

Correct me if I'm wrong, but often as you scale your DevOps pipeline, maintaining multiple tools and integrations can be time consuming and become a bottleneck. What if there was a simpler way to have all of your DevOps functionality without having to worry about maintaining tools?

If you'll give me a shot next Wed at 1 PM to explain how DevOps Directors have been able to free up their team, I promise not to be an annoying salesperson if you come to the conclusion it's not a suitable solution.

Either way, I hope your year is off to a great start!


Hello Mayank,

The reason for my outreach is because I was reading on your LinkedIn profile that you're currently working on an enterprise-wide Agile transition and adoption.

Correct me if I'm wrong, but when it comes to driving enterprise wide adoption, having a platform that your engineers actually want to use is key. What if there was a way to do this and have adoption spread like wildfire?

If you'll give me an opportunity to meet with you next Wed at 1 PM and share how VPs of Technology have driven adoption of their initiatives through GitLab, I promise I won't be an annoying salesperson if you decide we're not a good fit.

Either way, I hope your year is off to a great start, and many good wishes to you for the upcoming year.



Hello Moninder,

The reason I'm reaching out is because I was reading through your LinkedIn profile and noticed your focus on DevOps and cloud. What stood out to me was that you listed tools like Kubernetes.

What if there was a way to natively use Kubernetes and GCP in your DevOps pipeline without adding complexity?

If you'll give me an opportunity to meet with you next Wed at 1 PM and give you my best dog and pony show on what GitLab does and why I thought it might be useful to your team, I promise not to be an annoying salesperson if you don't think it's the right fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.


Hello Andy,

Thanks for connecting with me.

The reason for my email is because I was reading an article on BankInfoSecurity where you were quoted as saying that shadow IT is a major concern because, "Anyone with a corporate card come in and download a cloud service."

What if there was a way to reduce the amount of tools being used without approval.

If you'll give me an opportunity to meet with you next Wed at 1 PM and unpack how other CISOs have reduced shadow IT in their org through GitLab, I promise not to be an annoying salesperson if you decide that it's not a suitable solution.

Either way, couldn't agree more with your comments that enterprises definitely need to address this issue. I hope your 2020 has been off to a great start.

With your success in mind,
Kevin


Hello Michael,

The purpose for my outreach is because I was reading on your LinkedIn profile about your experience in DevOps.

With your experience, you've probably seen firsthand how time-consuming it can be to maintain a complex DevOps pipeline. What if there was a way to get DevOps functionality out of the box, without the complex integrations, so that it becomes easy to manage?

If you'll give me a shot next Wed at 1 PM to explain how Directors of Software engineering have been able to deploy faster by reducing complexity, I promise not to become an annoying salesperson if you decide it's not a suitable solution for your team.

In any case, I hope your year is off to a great start. 

Hello Jason,

The reason for my email is because I was reading on your LinkedIn profile about your DevOps experience. One line that stood out was that you insulate your team from "administrative minutiae and specification roadblocks to maximize their productivity." I couldn't agree more, and that's what my favourite managers have always done for me.

When if comes to insulating team members from administrative minutiae, what if there was a way to automate your DevOps pipeline such that it works out of the box, is easy to learn, and easy to manage?

If you'll give me an opportunity next Wed at 1 PM to share how Software Development Managers have been able to increase their teams' productivity with GitLab, I promise not to be an annoying salesperson if you decide it's not a suitable solution.

Either way, I hope your year is off to a great start, and thanks for being an exemplary manager.

With your success in mind,



Hello Bindu,

The reason for my email is because I was reading a press release with TechRepublic where you said that "we're constantly evolving and optimizing our business" including your delivery of Zulily's big data platform.

Correct me if I'm wrong, but when it comes to optimizing Big Data, AI and ML are very DevOps oriented and could benefit from native integration with Kubernetes and Jupyter Notebooks. What if there was a way to automate those through GitLab?

If you give me an opportunity to meet with you next Wed at 1 PM and share how Big Data and ML efforts benefit from using GitLab, I promise not to be an annoying salesperson if you decide that we're not the right fit.

In any case, I appreciate your insights from the interview and hope you're having a wonderful start to 2020.

With your success in mind,
Kevin


Hello Blake,

The reason I'm for my email is because I was looking at your LinkedIn profile and noticed that you're working on AI and ML using tools like Kubernetes. I'm currently working on an machine learning project as well and resonated with your headline; "Passionate about utilizing AI to develop next-generation technologies."

Correct me if I'm wrong, but when it comes to developing and productionalizing ML models, ML is very DevOps oriented and could benefit from native integration with K8s and Jupyter Notebooks. What if there was a way to get those taken care of by GitLab?

If you give me a shot next Wed at 1 PM to share how other AI/ML engineers are using GitLab for developing their models, I promise you'll learn a lot and that I won't jam your inbox if you don't think it's a good fit.

Either way, I hope your new year is off to a great start!



Hello Bill,

The reason I'm reaching out is because I was looking through your LinkedIn profile and noticed you use tools like Kubernetes. One line that stood out to me was you "experience leading initiatives in the SRE / DevOps and service development."

As such, you've probably experienced firsthand how complicated it can be to maintain a DevOps pipeline. What if there was a way to more easily build and maintain DevOps pipelines, and also natively enable Kubernetes, all without increasing complexity?

If you'll give me a chance to meet with you next Wed at 1 PM and unpack how, I promise not to be an annoying salesperson tEither way, I hope you're having a great January.

With your success in mind,


Hello Brion,

The reason I'm writing to you today is because I was reading through your GitHub page and noticed that one of your current projects includes using GitLab for build-time container scanning. But more importantly, you also said that you're working to "Optimize DevOps resources by migrating business applications to a Kubernetes cluster".

What if there are ways to optimizing DevOps resources further, and migrate to Kubernetes clusters easier (GitLab has a lot natively built for K8s)?

If you give me an opportunity to meet with you next Wed at 1 PM and discuss your DevOps projects, I promise I won't be an annoying salesperson even if we don't do business together.

In any case, I enjoyed reading through your blog entries, and I hope your year is off to a great start! 



Hello Marc,

The purpose of my email is because I was reading on LinkedIn how you lead DevOps and Infrastructure services. 

Correct me if I'm wrong, but when it comes to adopting DevOps, implementing an infrastructure that isn't complicated is key. What if there was a way to have an automated DevOps pipeline out of the box?

If you give me a shot next Wed at 1 PM to share with you why I thought your DevOps initiatives and infrastructure, I promise I won't be an annoying salesperson if you don't think we're a good fit.

Either way, I hope your Q1 is off to a great start.



Hello Jaipal, 

The reason for my email today is because I was reading on your LinkedIn profile. One line that really stood out was that you're passionate about "designing innovative solutions to high impact problems."

Correct me if I'm wrong, but when it comes to designing innovative solutions, implementing an uncomplicated DevOps toolchain is key to freeing you up to design those innovative solutions.

If you'll give me a shot to unpack that idea next Wed at 1 PM, I promise not to jam your inbox with follow up if you don't think it's a fit.

In any case, many good wishes to you for the upcoming year.

Hello Marty,

The reason for my email today is because I was reading on your LinkedIn Profile how you're implementing serverless micro-service application architecture. One thing that stood out to me was that you're "Passionate about scale and reliability, we are on a journey to achieve the 12 factor app."

What if there was a there was a way to implement a serverless micro-service architecture without adding complexity to your environment?

If you'll give me the opportunity to meet with you next Wednesday at 1 PM and unpack how software engineer managers have been able to quickly implement serverless, I promise not to be an annoying salesperson if you don't think it's a good fit

In any case, I hope you're having a great start to 2020!

Hello Suchi,

The reason for my email is because I was reading your LinkedIn profile. What stood out to me is that your specialties are in DevOps engineering and CI/CD automation.

What there was a way to automate your DevOps while reducing the number of integrations and tools to maintain?

If you'll give me the opportunity to meet with you next Wednesday at 1 PM and unpack how other Infrastructure Engineering Leads have achieved this, I promise not to be an annoying salesperson if you decide it's not the right fit.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Hello Dev,

The reason for my email is because I read your post, "20:20 Vision in 2020" and saw that one of your goal of learning frameworks like Serverless. (I was also the kid always building with Legos)

I have an idea for how you could learn Serverless and apply it to your workflow (made easier since you're already do a bunch of work with Kubernetes).

If you'll give me a chance to meet with you next Wednesday at 1 PM and unpack that idea, I promise we can part ways as friends even if we don't do any business together.

Either way, best of luck on your goals this year!

Hello Jonathan,

The purpose of my email is because I was reading your LinkedIn profile. You mention that your current area of focus is on "security controls commensurate with the risk profiles and risk tolerances of business." 

Hello Kathy,

The reason I'm writing to you today is because I enjoyed your interview on "Beyond the Blue Badge." I couldn't agree more when you said, "Technology and technological advances power science." You also mentioned that the biggest challenges of your role are around digital transformation, modernization, moving to the cloud, and optimizing.

I have an idea on how you could streamline modernization and enable use of the latest cloud technology without adding complexity to your enterprise applications.

If you give me an opportunity next Wednesday at 1 PM to unpack how CIOs have been able to accelerate their digital transformation and modernization, I promise not to be an annoying salesperson if you decide that we're not a good fit.

Either way, I hope you're having a wonderful start to 2020, and thank you for the work you do to enable people to cure cancer.

With your success in mind,
Kevin

Hello Mike,

The reason I'm writing to you is because I was reading on your LinkedIn profile where you mention your experience in DevOps. One line that stood out was how you're helping to "secure a collaborative data science platform and applications."

What if there was a way to build security in to your collaborative platform, without adding complexity to the toolchain?

If you'll give me an opportunity next Wednesday at 1 PM to share how other architects have been able to secure their data science platforms, I promise I won't be an annoying salesperson who floods your inbox with follow up if you decide that it's not a good fit.

Either way, I hope you're having a great start to 2020.

With your success in mind,
Kevin


Hello William,

The purpose of my email is because I noticed on your LinkedIn profile that you mention using GitLab. But more importantly, you specifically mentioned that your work is in appyling "statistical and programming knowledge to various types of laboratory and clinical data" to produce reports and analysis data.

With your permission, I'd like an opportunity to discuss with you some of the ways I've seen other research centers use GitLab and associated practices to make reporting easier.

If you'll give me an opportunity next Thursday at 2 PM how other research centers use GitLab, I promise I won't be a pesky salesperson and flood your inbox with follow up even if we don't do business together.

In any case, I hope you're having a wonderful start to 2020, and thank you for the work you do in furthering HIV/AID research.

With your success in mind,
Kevin



Hello Bruce,

The reason for my email today is because I was reading your LinkedIn profile and noticed that you list GitLab under your skills. But more importantly, I also saw that Fred Hutch recently spun up two instances of GitLab.

With your permission, I'd like an opportunity to discuss with you some best practices we've seen around using CI/CD and DevOps tools in healthcare and research.

If you'll give an opportunity to share with you next Wed at 1 PM how other research centers are using GitLab, I promise not to be an annoying salesperson even if we don't do any business together.

Either way, I hope you're having a great start to 2020, and thanks for using GitLab.

With your success in mind,
Kevin

Hello Chuck,

The reason for my email today is because you had previously mentioned that 2020 might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.

You had mentioned that you use BitBucket and were concerned about costs going up to use Okta for OAuth. Additionally, during my conversation with Ryan and Duane, we found that there were alignments both for the application and cloud security initiatives.

If you'll give me another opportunity to share with you next Wed at 1 PM how GitLab could help with both the implementation and cost reduction that you're looking for, I promise I won't be an annoying salesperson even if you decide it's not a good fit for your needs.

Either way, I hope you're having a great start to 2020, and many good wishes to you for this upcoming year.

With your success in mind,
Kevin 

Hello Phil,

The reason for my email today is because you had previously mentioned that mid-January might be a better time to connect. With your permission, I'd like another opportunity to earn some of your time.

You mentioned during our exchange that your work in Canada is part of the global IT initiative so I respect that this means there's little influence over the tools. I'd simply like to share some insights around what we've seen from other companies, in the event you're looking to drive DevOps adoption and IT efficiency with your team in the future. 

If you'll give me an opportunity to discuss this with you next Wed at 1 PM, I promise I won't be an annoying salesperson and jam your inbox even if we don't do any business together.

Either way, I hope you're having a great start to 2020 and many good wishes to you for this upcoming year.

With your success in mind,
Kevin

Hello Praveen,

The reason for my email today is because I read an article recently about being relevant and always bringing value to your clients, and I was conflicted about that last email I sent.  Maybe I got my research wrong and DevOps is old-hat and not all that compelling to you.

With your permission, I'd like another opportunity to earn some of your time.

You mention on LinkedIn that one of your specialties is technology rationalization and creating "enterprise strategies that cascade throughout an organization."

What if there was a way you could enable your org to adopt DevOps more quickly in order to deliver value faster, while rationalizing technology and reducing tooling cost, footprint, and complexity?

If you give me an opportunity to unpack this idea next Wednesday at 1 PM, and share how other Chief Architects have been able to rationalize technology while increasing DevOps adoption, I promise I won't be an annoying salesperson even if you don't think it's a suitable solution.

Either way, I hope you're having a great start to 2020, and many good wishes to you for the upcoming year!

With your success in mind,
Kevin


Hello Rob,

The reason I'm writing to you today is because I enjoyed your keynote from DevOps Days Boston on "Settlers of DevOps." It really resonated with me when you made the distinction that it needs to be a "pull nature" to be successful.

Correct me if I'm wrong, but in many cases, orgs are using multiple competing tools which then leads to complexity and therefore difficulty in adoption. (I hazard that this is part of the reason why EA is brought in to standardize.) What if there was a way to make adoption easy, by having a single tool that covers the entire DevOps pipeline, so that new teams can pull without added complexity?

If you give me an opportunity to unpack this idea next Wednesday at 1 PM, and share how other orgs have seen teams "pull" and adopt faster than their planned rollout, I promise I won't be an annoying salesperson if you don't think it's a suitable solution.

In any case, thank you for introducing me to Wardley's model - really changes how I view our organization and my role within it. 

With your success in mind,
Kevin

Hello Tom, 

The reason I'm writing to you is because I was reading on your LinkedIn profile that your specialty is in "Scaling and building large physical or cloud deployments."

Correct me if I'm wrong, but when you're scaling large deployments, using DevOps methodologies to automate and secure your pipeline is key. I have an idea for how you could enable a simple, elegant, technical solution to do this, without adding complexity into your pipeline.

If you give me a shot to unpack that idea next Wednesday at 1 PM and share how cloud architects are scaling their applications using GitLab, I promise I won't be an annoying salesperson if you don't think it's a suitable solution.

Either way, I appreciate your work in keeping the caffeine flowing through cloud technologies.

With your success in mind, 
Kevin


Hello Drew, 

The reason for my email is because I was reading on your LinkedIn profile how you're building the Cloud Foundation Services. One line that really stood out was that you're looking to make it "an automated, repeatable, and secure shared platform [...] that allows them to innovate faster and deploy more frequently."

What if you could enable all of this (scalable, automated, secure platform), including built in Kubernetes support, in an out-of-the-box DevOps solution that doesn't add complexity to the environment?

If you give me an opportunity to explain next Wed at 1 PM how Directors of Engineering have been able to launch faster, I promise I won't be an annoying salesperson if you don't think it's a suitable solution for your needs.

In any case, I appreciate that my coffee is fueled by DevOps thanks to you.


Hello Tom,

The reason for my email is because I was reading on LinkedIn how you attended your 4th AWS re:invent and expanded your team's knowledge of serverless.

I'm sure this isn't news to you, but what if there was a way to deploy serverless applications on any infrastructure and manage it all through a single UI?

If you give me a shot to share with you next Wednesday at 1 PM how Software Engineering Managers are able to achieve serverless deployments without having to configure and integrate a bunch of tools, I promise I won't be an annoying salesperson and follow up if you don't think we're a good fit.

Either way, I hope you're having a wonderful start to the New Year. (By the way, great photo of the Grand Tetons & Snake River!)


Hello Tom, 

The purpose of my outreach today is because I thoroughly enjoyed your article "The Toughest Software Bugs." The line that really resonated with me was, "the toughest bugs are the oldest bugs [...] the faster you find it, the easier it is to deal with." I couldn't agree more.

When it comes to finding bugs fast and early in the development cycle, I have an idea for how you could display vulnerabilities right in the CI/CD pipeline where developers live. Thereby allowing them to remediate at each code commit (instead of at UAT).

If you give me an opportunity to meet with you next Thursday at 2 PM and share how to make the responsible way of fixing bugs right away easier, I promise I won't flood your inbox even if you don't think our solution is the right fit.

In any case, thank you for taking the time to write such an insightful article.


In your article "Software Security is Not An Option", you place the onus on the software professional to "make sure that software security is baked in from the beginning."

What if you could make your deployment pipeline such that security scans (like SAST, DAST, container scanning, and license management) are already built in, and happen during every code commit? 

If you give me a chance to explain how next Friday at 3 PM, I promise we can part ways as friends even if it's not suitable to your situation.

Nevertheless, I appreciate your article and the reminder that security is not an option.


Hello Raymond,

The reason for my email is because I was reading on your LinkedIn profile that your specialties include DevOps, CI/CD, and Containerization. 

I'm sure this isn't new to you, but what if there's a way you could have CI/CD, environment provisioning, and even security, all built in and working out-of-the-box, without having to worry about integrating multiple tools?

If you give me an opportunity to share with you next Thursday at 2 PM how Applications Engineering Managers automate their DevOps pipeline while reducing the tools they manage, I promise we can part ways as friends even if you don't think it's a good fit.

In any case, I hope you're having a wonderful start to the New Year.


Hello Dennis, 

The reason for my email is because I was reading on your LinkedIn profile how you're an evangelist for next-gen data engineering. You specifically mentioned using Docker and CI/CD as part of your architecture.

I have an idea for how you could simplify the integrations necessary through applying DevOps, Kubernetes, and native Jupyter Notebooks to your stack.

If you give me a chance to meet with you next Wed at 1 PM to unpack how other Principal Engineers are able to simplify their CI/CD pipelines in their data warehouse, I promise I won't jam your inbox if you don't think we're a fit.

Either way, I hope you're having a great start to 2020!

Hello Jeff,

The purpose for my email is because I saw a video from BlackHat 19 titled "What still scares you about Cybersecurity?" It really resonated when you said, "It really just comes down to security having a hard time keeping up."

What if there was a way to build security into the CI/CD pipeline (where the developers live), and at the same time provide your security team a way to view items not resolved by the developers? This vulnerability-centric approach would then would help each role deal with the relevant work and be able to keep up.

If you give me a chance to meet with you to share how Security Architects have been able to build sec into the DevOps pipeline without added complexity, I promise I won't jam your inbox if you don't think we're a fit.

Either way, that was a fun clip/interview. It's also exciting to hear about Hilary Schneider joining as CEO, and can't wait to see what she does at the helm!

The reason I'm writing to you today is because I was reading on your LinkedIn profile that you list Jenkins under tools & technologies. Other security architects I speak with tell me that self-service Jenkins can be difficult to secure and the plugins often introduce vulnerabilities.

What if there was a way to ensure that the CI/CD pipeline is secure (even including SAST/DAST) without adding yet tool to further complect the pipeline?

If you give me a shot to meet with you and explain next Thursday at 2 PM how other security architects are locking down their CI/CD pipeline, I promise we can part ways as friends even if you don't think it's the right solution for you.

In any case, I hope you're having a wonderful start to the new year.



In a video interview titled "Worst Security Vendor Email Pitches" from BlackHat 2019, your example was when vendors send an email and the follow ups progressively become more and more desperate.

I have an idea that makes compliance audits easier by having development, infrastructure, QA, and security data all available in a single source of truth - without adding tool complexity. Would this be at all valuable to you?

If you give me a shot to unpack that idea next Friday at 3 PM, I promise I won't be an annoying salesperson and follow up desperately if you don't think we're a good fit.

No worries if this isn't of interest; a simple "No thanks" would be appreciated.



Hello James,

The reason I'm writing to you today because I was reading your interview with CIO.com. One line that resonated with me was, “When you grow up in a world where you have to build everything in order to be successful, that gets baked into your DNA."

What if there was a way to enable your teams to have end-to-end DevOps capabilities without have to build and integrate a complex toolchain?

If you give me a shot next Wednesday at 1 PM, to unpack how other CTOs have been able to adopt and implement DevOps quickly, I promise we can part ways as friends if you don't think it's a good fit.

In any case, I hope you're having a great start to the new year.

The purpose of my email is because I was watching your interview with Apigee where you discuss how APIs help Shutterfly bring the right products to market quickly.

I have an idea for how you could enable the same thing within your development pipeline by using DevOps to have faster feedback loops in bringing products/features to market.

If you give me a chance to meet with you next Thursday at 2 PM to share how, I promise I won't jam your inbox if you don't think we're a fit. 

Either way, thanks for fostering innovation and digital transformation at Shutterfly.


You mention on LinkedIn that part of your responsibilities included " significantly increas[ing] interdepartmental efficiencies." 

If you could use DevOps to increase interdepartmental efficiencies, foster collaboration, and reduce silo's within the organization, would this be at all valuable to you?

I'm hoping you'd be open to a brief conversation next Friday at 3 PM so I can share how. At the end of the call, if it's not a fit, I promise I won't be an annoying salesperson and flood your inbox.

No worries if this isn't of interest, a simple "No thanks" would be appreciated.



Hello Aaron,

The reason I'm writing to you today is because I was reading the case study of your use of Prevoty RASP, where you discussed how it "has been effective at covering gaps and providing risk reduction that allows us to focus on engaging engineering teams early in the lifecycle."

I'm sure this isn't new ground to you, but what if there was a way to build security capabilities (like SAST, DAST, Container Scanning) into the CI/CD workflow where the developers live. This would allow them to identify and address vulnerabilities earlier, as well as provide "protection without operational impact" - to borrow your words.

If you would kindly give me the opportunity to meet with you and share how CISOs are able to increase security coverage without added friction, I promise we can part ways as friends if you don't think it's a fit.

Either way, I hope you're having a wonderful start to the New Year.

With your success in mind,
Kevin

The purpose of my outreach today is because I was enjoying your interview from the CISO Security Vendor Relationship Podcast. One line you shared that really resonated was, "We all have a mountain of work to do in security, there's always debt, there's always new things to clean up."

I have an idea for how you could provide your security team a way to view  items not resolved by the developers, and enable a vulnerability-centric approach  that would help each role deal with and prioritize that mountain of work.

If you give me a shot to unpack that idea next Thursday at 2 PM, I promise I won't be an annoying salesperson and follow up incessantly if you don't think we're a good fit.

In any case, thanks for the very informative interview.

You mentioned in your case study for Sophos Cloud Optix that "Our compliance team is now able to run reports for compliance audits in seconds, which was previously manual and exceedingly time consuming."

If there was a way to make compliance audits easier by having development, infrastructure, QA, and security data all available in a single source of truth - without adding tool complexity, would this be at all valuable to you?

I'm hoping you'd be open to a brief conversation next Friday at 3 PM. At the end of that call, if it's not a fit, I promise I won't flood your email with follow ups.

No worries if this is of no interest to you, a simple "No thanks" would be appreciated.



No problem if this isn't of interest to you.

Hello Amit,

The purpose of my outreach today is because I was reading on your LinkedIn profile about you're using Kubernetes to enable increased speed, quality, and hygiene while reducing toil.

I have an idea for how you could further reduce toil while building in security, without adding complexity to your CI/CD pipeline.

If you give me an opportunity to meet with you and unpack that idea next Wednesday at 1 PM, I promise I won't jam your inbox if you don't think we're a fit.

In any case, I hope you're having a great start to 2020, and getting K8s ready for the tax season!

The reason I'm writing to you today is because I enjoyed your series of articles on DevOps. One line that really resonated with me is from part 4, where you said, "Infrastructure is hardest to automate but most critical in my mind."

What if I had an idea that could help you make automating infrastructure easier, more scalable, and faster, without adding complexity into your environment?

If you'll give me a chance to share that idea with you next Thursday at 2 PM, I promise we can part ways as friends even if you think it's not the right fit.

Either way, wanted to say thanks for taking the time to write the article series, I truly appreciate the overview of the DevOps landscape.


You mention on LinkedIn that you're responsible for creating an Operational Data Lake and the migration of the data platform to AWS.

What if there was an easy way to apply DevOps practices like CI/CD and infrastructure as code to ensure speed and reliability of your Data Lake?

Would you be open to a brief conversation next Friday at 3 PM, so we can discuss the intersection of DevOps and Data?

No problem if you're not interested, a simple "No thanks" would be appreciated.


Hello Alfred,

I saw on LinkedIn that you liked a post about the Cloud-Native Kubernetes meetup coming up on Jan 16th.

If you're interested in Kubernetes and Cloud-Native, I have a way for you to make using K8s and going serverless painless and automated.

Would you be willing to give me a few minutes to share how next Friday at 3 PM? I promise not to be an annoying salesperson afterwards if you don't think it's the right solution for you.

No problem if you're not interested, a simple "No, thanks" would suffice.


With your success in mind,
Kevin

The purpose of my email is because I was enjoying watching your presentation from AWS re:invent 2015. The part "Our vision is actually continuous deployment, where deployments into production are all automatic, and deployment approvals are built in."

What if you could have CI/CD, environment provisioning, approvals, and even security (you had mentioned doing SAST/DAST/etc.) all built in and working out-of-the-box, without having to worry about integrating multiple tools?

If you give me an opportunity to share with you next Thursday at 2 PM how DevOps managers automate their deployment pipeline while reducing the tools they manage, I promise I won't flood your email with follow up afterwards.

In any case, thank you for the informative presentation.

With your success in mind,
Kevin


The reason I'm writing to you today is because I was reading on your LinkedIn profile how you're leading the automation of DevOps infrastructure build-out from the ground up.

I have an idea for how you can make CI/CD, automated provisioning, test automation, and security easily available while having to perform less tool integrations and maintenance.

If you give me a chance to meet with you next Wednesday at 1 PM and unpack that I idea, I promise you'll find it informative and we can part ways as friends even if you don't think we're a fit.

Either way, I hope you're having a wonderful start to 2020, and many good wishes to you for the upcoming year.


With your success in mind,
Kevin

Hello Swati

The reason I'm writing to you is because I enjoyed your article on SoftwareTestingHelp about the Best Bug Tracking and Defect/Issue Tracking tools of 2020. One line that really stood out to me was where you said, "I am not a big fan of tools that are single-purpose [...] you want it to serve you in multiple ways."

I couldn't agree more. What if you could have your bug tracking, CI/CD, and testing automation all in one tool, and as a result be able to understand and address defects more easily?

If you give me a chance to meet with you next Wednesday at 1 PM and unpack how this is possible, I promise you I won't jam your inbox if you don't think we're a fit.

Either way, I hope you're having a great start to the New Year, and thank you for the well written articles.

With your success in mind,
Kevin

Hello Cashin,

The reason I'm writing to you is because I was reading your LinkedIn profile and saw that you led the DevOps initiative and transition "towards a true CI/CD environment."

I have an idea on how you could create a fully automated CI/CD environment, and as a result make adopting and transition to DevOps easier for your teams.

If you give me a chance to meet with you next Wednesday at 1 PM PST and unpack that idea, I promise I won't flood your inbox with follow up in case you don't like it.

Either way, I hope you're having a great start to 2020, and many good wishes for a wonderful year to you.

Warm Regards,
Kevin

Hello Cashin,

The purpose of my outreach today is because I was reading your LinkedIn profile how you lead the data management strategies to enable BI analysis and reporting.

Correct me if this isn't the case at FI, but many of the DevOps practitioners I speak with cite Data Architecture as an organizational constraint. What if you could architect data so that it's easily consumable in APIs, and thereby make all of the BI analysis and reporting easier?

If you give me an opportunity next Thursday at 2 PM PST and unpack how teams are using DevOps for data, I promise you'll find it informative and valuable.

In any case, I hope you're having a strong Q1 and enjoying the redwoods!

Warm Regards,
Kevin


The reason for my email today is because I get the feeling that maybe I got my research wrong and DevOps and digital transformation aren't all that relevant to you.

If that is the case, with your permission, I'd like another shot at unpacking why I thought you might find it beneficial to meet.

You mention on your profile that you're using JIRA and working on .NET, which might mean that you're using some form of Azure DevOps or TFS.

I have a few ideas on how you could automate your deployment pipeline so you're spending less time configuring tools and managing integrations.

Would you be willing to give me a few minutes to share how next Friday at 3 PM? 

No problem if not interested. Simply reply back "No, thanks"


Hello Shannon,

The reason I'm writing to you is because I couldn't stop reading your article "Shifting Security to the Left" on DevSecOps.org. One line that really stood out was where you described how security documents are cumbersome and "ultimately burdens a software developer interested in playing their part." (Yikes! friendly fire)

I'm sure this isn't new ground to you, but what if instead, security capabilities were built into the CI/CD workflow where the developers live. This would allow them to identify vulnerabilities and address without introducing friction.

If you give me a chance to meet with you next Tuesday at 11 AM and unpack how GitLab does this, I promise I won't jam your inbox if you don't think it's useful.


Either way, I hope you're having a wonderful start to the new year I appreciate your body of work, from the writing to the presentations. (Especially the comics; they're great!)

Warm Regards,
Kevin

Hello John,

The reason I'm reaching out is because I read on your profile that you were able to lower the TCO by simplifying and modernizing VSP's system landscape.

What if you could do the same within the enterprise architecture, and as a result be able to deliver new features faster?

If you give me a chance to meet with you and unpack that idea, I promise I won't jam your inbox if you don't think there's a fit.

In any case, big fan of VSP and I've been super happy with my vision insurance through you. (Actually, just got my new pair of glasses this week!) 

Warm Regards,
Kevin

****
I am a leader in the movement of building secure processes into modern cloud applications and infrastructure. On the forefront of leading projects to help build secure software delivery strategies and systems, automate validation of environments, create infrastructure that heals itself, and help our clients securely solve their most complex engineering problems. I bring a security lens to cross-functional teams as we co-create the modern software products of tomorrow. 

Hello Caleb,

The reason for my outreach is because I was reading on your profile how you're leading secure software delivery strategies and systems. It's especially refreshing to read that you're building the processes into the applications and infrastructure (rather than having security be separate). 

I have an idea that may help you ensure this happens without introducing friction into the development or ops teams. 

If you give me a shot to unpack how, I promise I won't jam your inbox if you don't think it's a fit.

In any case, thank you very much for connecting with me, and I hope you're having a wonderful start to the new year.

Warm Regards,
Kevin


*****

Hello David,

The reason I'm reaching out is because I had a nice conversation with Terry Parrish, who thought it might be beneficial we speak. I promised him I'd reach out to you. More importantly, 

****

Reid Nabinger @ ALG

Hello Reid,

The reason I'm reaching out to you today is because I read an interview with TravelAgent where you discussed ALG's unified platform. The comment that really stood out to me, and why I thought it would be beneficial we speak, was where you said "This allows us to be on one, unified platform and makes it easier for agents who previously had to visit three different places for all of our offerings."

Correct me if I'm wrong, but when it comes to your software deployment pipeline, the same principle of having a unified platform to enable your development teams to be more efficient is just as important. Imagine having an out-of the-box git repo, CI/CD, security scanning, project management, and more, all on one unified platform. Wouldn't this allow your teams to achieve business goals and deliver software faster? This is the main value GitLab offers to you by enabling your teams to work out of a single platform for software development.

Would you be willing to give me a few minutes next Tuesday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin

****

Hello Sandesh,

The reason I'm writing to you is that I was reading in your LinkedIn profile where you highlighted DevSecOps and CI/CD. One line that really stood out to me was that you're leading the "transformation journey of converting a monolithic application into a cloud-native microservices based architecture", which is why I thought it'd be important we speak.

When it comes to DevSecOps and moving to a microservices architecture, the main value GitLab offers to you is by enabling your teams to work out of a single application. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow them to focus less managing the infrastructure, instead spending more value added time on converting the monolithic application into cloud native?

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****

Hello Jason,

The reason I'm writing to you because I was reading on your LinkedIn where you talk about your passion for building data driven organizations. It really resonated with me where you wrote, "engrain data into the DNA of their culture and are experts at using information for decision making."

Correct me if I'm wrong, but when it comes to ensuring that data is at the heart of decision making, self-service and data availability is key. Making data easily accessible is the value that GitLab provides. One client that was in a similar situation was Verizon. Their teams  were struggling with data center builds that took 30 days, which meant they couldn't use that data effectively for decision making (because it took so long). By using GitLab, they were able to reduce datacenter rebuilds to under 8 hours and engrain data back into decision making.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

******

Hello Leila,

The reason I'm writing to you because of your role in leading performance marketing and marketing analytics for ALG. Correct me if I'm wrong, but my understanding is that ALG is currently moving all data onto the Google Cloud Platform (GCP). This is why I thought it might be beneficial we speak.

When it comes to moving your data to GCP, making this fast and easy is the value that GitLab provides. A client that was in a similar situation was Verizon. Their team wanted to move to GCP because they were struggling with data center builds that took 30 days. By using GitLab, they were able to reduce datacenter rebuilds to under 8 hours.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin


****

Hello David, 

The reason I'm writing to you is because I was reading your LinkedIn profile about your role in Digital Product Development and Software Engineering. You specifically mentioned tools like Ansible, Kubernetes, and GCP, which is why I thought it'd be important we speak. 

When it comes to using GCP and automating your deployment pipeline, the main value GitLab offers to you is by enabling your teams manage your DevOps pipeline out of a single platform. Imagine having an out-of the-box git repo, CI/CD, security scanning, project management, and more, all in one tool. It's especially useful if you're at all looking to utilize canary deployments, or creating a highly available architecture.

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research? 

With your success in mind,
Kevin

****
Steve Dumaine @ ALG

Hello Steve,

The reason I'm reaching out to you today is because I read an interview with TravelAgent where you discussed ALG's unified platform. The comment that really stood out to me, and why I thought it would be beneficial we speak, was where you said "This allows us to be on one, unified platform and makes it easier for agents who previously had to visit three different places for all of our offerings."

Correct me if I'm wrong, but when it comes to your software deployment pipeline, the same principle of having a unified platform to enable your development teams to be more efficient is just as important. Imagine having an out-of the-box git repo, CI/CD, security scanning, project management, and more, all on one unified platform. Wouldn't this allow your teams to achieve business goals and deliver software faster? This is the main value GitLab offers to you by enabling your teams to work out of a single platform for software development.

Would you be willing to give me a few minutes next Tuesday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin

Hello Steve,

Writing to you because I've been reading about ALGV360. Wendy Hoeckwater's quote about "aLGV360 distilling hours of work into seconds" is exactly what we do for software deployment, and why I thought GitLab might be beneficial to you.

Correct me if I'm wrong, but when it comes to your software development, the same principle of distilling hours of work into seconds as key. Imagine having an out-of the-box git repo, CI/CD, security scanning, project management, and more, all on one unified platform. Wouldn't this allow your teams to distill hours of work? This is the main value GitLab offers to you by enabling your teams to have an automated DevOps deployment pipeline.

Looking to schedule some time with you to discuss and share why I thought you might find GitLab useful. Does next Wed at 2 PM work?

With your success in mind,
Kevin


****

Hello Sathish,

The reason I'm reaching out to you today is because I saw your post on LinkedIn where you shared the Forbes article about work-life integration. Couldn't agree more when you said, "achieving "Integration" will surely make professionals more effective in their careers."

Correct me if I'm wrong, but when it comes to Enterprise Architecture, having to achieving integration is key to making your deployment pipeline more effective. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool, already integrated for you. Wouldn't this allow your teams to be more effective and focus less on managing the infrastructure and maintaining integrations?

Correct me if I'm wrong, but when it comes to closing the circle and achieving business goals, creating a fast feedback loop is key. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow your teams to achieve business goals faster? This is the main value GitLab offers to you by enabling your teams to work out of a single application for software development.

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin

****
Hello Baldemar,

The reason I'm reaching out to you today is because I saw your post on LinkedIn about your CHTP and wanted to congratulate you on closing the circle. But more importantly, you specifically mentioned that you work closely with the "business units providing IT leadership to help them achieving their goals." This is why I thought it beneficial that we speak.

Correct me if I'm wrong, but when it comes to closing the circle and achieving business goals, creating a fast feedback loop is key. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow your teams to achieve business goals faster? This is the main value GitLab offers to you by enabling your teams to work out of a single application for software development.

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?


With your success in mind,
{{sender.first_name}}



Hello Baldemar,

The reason I'm reaching out to you today is because I saw your post on LinkedIn about your CHTP and wanted to congratulate you on achieving that goal. But more importantly, you specifically mentioned that you're "responsible for the technical strategy, direction, and management of IT operations." This is why I thought it beneficial that we speak.

When it comes to IT Operations, the main value GitLab offers to you is by enabling your teams to work out of a single application. Imagine having an out-of the-box git repo, CI/CD, project management, and more, all in one tool. Wouldn't this allow your teams to focus less on managing the infrastructure and maintaining integrations?

Looking to schedule some time with you to discuss and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
{{sender.first_name}}

****

Hello Phil,

The reason I'm reaching out to you is that I was reading your LinkedIn post about guest lecturing at University of Toronto about Business Process Architecture. I couldn't agree more with your comment that, "I firmly believe that our legacy is about those that we teach and that sharing our experience, knowledge and ideas with those eager to learn is how progress happens."

When it comes to effecting business change by sharing knowledge, this is one of the key principles of DevOps and why I thought it beneficial we speak. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow your teams to share knowlege and make progress happen faster? This is the main value GitLab offers to you by enabling your teams to work out of a single application for software development. 

Would you be willing to give me a few minutes next Thursday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin


****

Hello Sandesh,

The reason I'm writing to you is that I was reading in your LinkedIn profile where you highlighted DevSecOps and CI/CD. One line that really stood out to me was that you're leading the "transformation journey of converting a monolithic application into a cloud-native microservices based architecture", which is why I thought it'd be important we speak.

When it comes to DevSecOps and moving to a microservices architecture, the main value GitLab offers to you is by enabling your teams to work out of a single application. Imagine having an out-of the-box git repo, CI/CD, security scanning, and more, all in one tool. Wouldn't this allow them to focus less managing the infrastructure, instead spending more value added time on converting the monolithic application into cloud native?

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****
Hello Amitabh,

The reason I'm writing to you is that you mentioned on your LinkedIn profile that you're leading the data engineering efforts for Mars. You specifically mentioned that you're moving "both internal and external data sources into the Mars Data Lake", which is why I thought it'd be important we speak.

When it comes to creating a data lake, the main value GitLab offers to you is by enabling the data infrastructure to be scalable and continously integrated (CI). Your teams can run automated testing and have an effective CI pipeline for their data warehouse. If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****

Hello Ryan,

The reason I'm reaching out is because I had a nice conversation with Chuck Phipps, who thought it might be beneficial we speak. I promised him I'd reach out to you to. But more importantly, I saw on your LinkedIn profile that Vulnerability Scanning and Web Application Security are among your interests.

When it comes to app sec and vulnerability scanning, the main value GitLab offers to you is enabling developer teams to be able to identify vulnerabilities and remediate them immediately. Unlike traditional application security tools which are typically periodic scans, GitLab scans for vulnerabilities during every code commit and displays the results in the CI/CD workflow where the developers live.

Hoping to schedule some time with you to discuss your security initiatives and share why I thought you might find GitLab useful. Would you be open next Thursday at 2 PM?

With your success in mind,
Kevin

P.S. If now's not a good time, I thought you mights still find some value in this slide deck I put together for you: https://ptdrv.linkedin.com/4rv0uvj

*****
Hello Jason,

The reason I'm writing to you is that you mentioned on your LinkedIn profile that you're leading the data science team that owns the marketing analytics strategy. You specifically mentioned that you're "Platforming all data onto the Google Cloud Platform", which is why I thought it'd be important we speak.

When it comes to creating a holistic data architecture approach, the main value GitLab offers to you is by enabling the data warehouse infrastructure to be scalable and continously integrated (CI). If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin

****
Donald Nolan @ ALG

Hello Donald,

The reason I'm writing to you is that you shared an article on your LinkedIn about using data analytics to improve project outcomes. One line that really resonated with me was that, "Data and analytics will become infused in an organization's architecture from end to end, creating a holistic approach." But more importantly, because of your role in Business Intelligence Development, I thought it'd be important we speak.

Correct me if I'm wrong, but my understanding is that ALG is currently moving all data onto the Google Cloud Platform. When it comes to creating a holistic data architecture approach, the main value GitLab offers to you is by enabling the data warehouse infrastructure to be scalable and continously integrated (CI). If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind, 
Kevin


****
Wendy Hoeckwater @ Apple Leisure Group

Hello Wendy,

Thank you for your response. The reason for my outreach is that your quote about "aLGV360 distilling hours of work into seconds" is exactly what we do for marketing analytics.

The main value GitLab offers to you is by enabling the architecture that gives Marketing the ability to create data driven promotions. If data architecture has ever been a bottleneck, or if anyone who needs data as part of their daily work isn't getting what they need, this is where it's most useful.  

Based on my research, the person that would benefit the most from having this conversation is Jason West, your Director of Performance Marketing who leads data science team that owns the marketing analytics strategy and is currently platforming all of your data into Google Cloud Platform.

I'm hoping you could connect me with Jason to schedule some time to discuss your Data initiatives and share why you might find GitLab useful.


With your success in mind,
Kevin


*****

Jim @ Pella


Hello Jim,

The reason I'm reaching out to you is because I saw your interview on theCube where you talk about your approach to strategic partnerships, risk management, and your test dev environment. One comment that really stood out to me was,"Our challenges continuing to go forward is 'How do we consolidate and pull?'"

What if you could choose a strategic partner for your software development pipeline that allows you to consolidate across tools? Wouldn't this enable scalability, flexibility and long-term economics in your pipeline similar to what you've achieved with Dragonhawk? This is what GitLab can do for you as a platform that includes git repo, CI/CD, application security, and more.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought you could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Matt

*****

Matthew Dunlop, Ph.D. , CISO @ UnderAmour

Hello {{first_name}},

Writing to you because I listened to your appearance on the CyberWire Daily, and really resonated with your comments on cloud security, workplace hiring, and cyber hygeine. I couldn't agree more when you said, "At the end of the day, you really have to make sure you're doing the simple stuff right. If you're not doing the simple stuff, I don't care what tools you have in place."

What if you could make it really easy to do the simple stuff right? Especially when it comes to connected devices and application security. That's part of what GitLab does by scanning every line of code, checking it for vulnerabilities, and immediately returning a report to the developers of what needs remediation attention.

Would you be willing to give me a few minutes next Friday at 1 PM, to share with you exactly why I thought the your initatives could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin


Reaching out because I was watching your Cybersecurity Ventures and was pleasantly surprised to hear you talk about SecDevOps. One statement that really stood out to me was you saying that, "It's about bringing security people onto your scrums and agile teams from the beginning."

What if, likewise, you enabled the developer teams to be able to identify vulnerabilities and remediate them at the beginning. One way to do this is by builing application security (like SAST, DAST, etc.) into the CI/CD workflow where the developers live.

Unlike traditional application security tools which typically scan at the end of the lifecycle, GitLab does this at the beginning, scanning for vulnerabilities during every code commit.

Hoping to schedule some time with you to discuss your SecDevOps initiatives and share why I thought you might find GitLab useful. Would you be open this upcoming Thursday at 2 PM?

With your success in mind,
{{sender.first_name}}


****

Ann Funai, SVP Engineering @ UnderArmour

Hello {{first_name}},

Writing to you because I saw the Partnering For Purpose panel, and I appreciate the insights you shared. One quote that really resonated was where you said,  "I went out and looked for interesting assignments. Those things that at the end of the day, I could come home and look myself in the mirror and that there's some purpose to it." 

Correct me if I'm wrong but the way I understand it, a core ideal of the DevOps philosophy is to enable Focus, Flow, and Joy so that work is interesting and purposeful. As part of GitLab, I work with enterprises to accelerate their DevOps adoption so that they can increase developer productivity and velocity, disrupt the competition, and attract and retain top IT talent. 

Hoping to schedule some time with you to discuss your initiatives and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
{{sender.first_name}}



***

Hello Brad,

Reaching out to you because I read your article on "Problems with 'Headcount'". Two of your sentiments that really resonated are "Using headcount as a proxy to control costs often creates political problems" and "Managers make lower-quality decisions with headcount vs. dollars."

In "Project to Product", Dr. Mik Kersten echoes this by warning us that, "The problem is that we are relying on proxy metrics for decision making rather than finding the metrics that directly correspond to business outcomes."

What if there was a way to enable your engineering teams to use the metrics that directly correlate to the business outcomes? GitLab's Productivity Analytics provides graphs and reports to help engineering leaders understand team, project, and group productivity so they can uncover patterns and best practices to improve overall productivity. 

Would you be willing to give me a few minutes next Wednesday at 2 PM, to share with you exactly why I thought the MapMyRun related initiatives could benefit from GitLab based on what I've gleaned in my research?

With your success in mind,
Kevin

Hoping to schedule some time with you to discuss your initiatives and share why I thought you might find GitLab useful. Does this upcoming Thursday at 2 PM work?

With your success in mind,
Kevin
