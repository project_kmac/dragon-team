# 4 - Graceful Dismount

Nevertheless, I appreciate your article and the reminder that security is not an option.
No problem if not interested. Simply reply back "No, thanks"
No problem if this isn't of interest to you.
No problem if you're not interested, a simple "No thanks" would be appreciated.
No problem if you're not interested, a simple "No, thanks" would suffice.
No worries if this is of no interest to you, a simple "No thanks" would be appreciated.
No worries if this isn't of interest, a simple "No thanks" would be appreciated.
No worries if this isn't of interest; a simple "No thanks" would be appreciated.

Either way I hope that your February is off to a great start.

Either way I hope that your February is off to a great start.

Either way I hope that your February is off to a great start.
In any case, best wishes to you integrating SRE practices into your 500 person team.
In any case, great article on interpretability in machine learning. I hope this year brings you more (tiger bite optional) adventures.
In any case, great photography, thank you for sharing, and I hope this year  brings you many opportunities to shoot in cool locations.
In any case, I appreciate that my coffee is fueled by DevOps thanks to you.
In any case, I appreciate the true spirit of DevOps as you see it, and couldn't agree more about being able to be part of every stage of an outcome's lifecycle.
In any case, I appreciate the work you do to keep video streaming fast and reliable!
In any case, I appreciate your being open-minded to a conversation, and many good wishes to you for the upcoming year.
In any case, I appreciate your guidance. I'll let you know if I'm able to set up some time with either Himanshu or Stuart.
In any case, I appreciate your insights from the interview and hope you're having a wonderful start to 2020.
In any case, I enjoyed reading through your blog entries, and I hope your year is off to a great start!
In any case, I hope that you're February is off to a great start. Noticed you went to Wuhan University, so if you still have any friends or family there, I hope they're okay.
In any case, I hope that your February has been off to a great start.
In any case, I hope that your February is off to a great start, and thank you for using GitLab.
In any case, I hope this January has been treating you well, and many good wishes to you for the upcoming year.
In any case, I hope this year brings you many opportunities to solve the business and technology puzzles you enjoy
In any case, I hope you're 2020 is off to a great start, and thank you for your service.
In any case, I hope you're doing well and many good wishes to you.
In any case, I hope you're having a great start to 2020, and getting K8s ready for the tax season!
In any case, I hope you're having a great start to 2020!
In any case, I hope you're having a great start to the new year.
In any case, I hope you're having a strong Q1 and enjoying the redwoods!
In any case, I hope you're having a wonderful start to 2020, and thank you for the work you do in furthering HIV/AID research.
In any case, I hope you're having a wonderful start to the New Year.
In any case, I hope you're having a wonderful start to the new year.
In any case, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
In any case, I hope your February is off to a great start.
In any case, I hope your Q1 is off to a good start and many good wishes to you for the upcoming year.
In any case, I hope your Q1 is off to a great start, and I wish you the best of luck with the inaugural Agile Anonymous!
In any case, I hope your year is off to a great start.
In any case, it means a lot to me that you've been a foster parent for dogs awaiting their forever home. Thank you for that, and I hope your February is off to a great start.
In any case, it's great to read that you haven't had this much fun since Gateway, and I hope that Extended Stay RV has a great year in 2020.
In any case, many good wishes to you for the upcoming year.
In any case, really appreciated your article on Kafka - thanks for the informative read!
In any case, sounds like you're already doing a lot of the latest and greatest - so just wishing you a good start to 2020!
In any case, thank you for introducing me to Wardley's model - really changes how I view our organization and my role within it.
In any case, thank you for taking the time to write such an insightful article.
In any case, thank you for the informative presentation.
In any case, thank you for the well-written article, and many good wishes to you for the upcoming year.
In any case, thanks for the very informative interview.

Whether you meet with me or not, I appreciated you taking the time to write the article and assembling those 7 laws into a coherent guide. Hope your 2020 is off to a great start!

Either way I hope that your February is off to a great start.
Either way I hope that your February is off to a great start.
Either way I hope that your February is off to a great start.
Either way I hope that your February is off to a great start.
Either way I hope that your February is off to a great start.
Either way I hope that your February is off to a great start.
Either way, best of luck on your goals this year!
Either way, congratulations on receiving Gartner L2's Genius Digital IQ and Top Brands.
Either way, congratulations on the promotion, and here's to your continued success in 2020.
Either way, couldn't agree more with your comments that enterprises definitely need to address this issue. I hope your 2020 has been off to a great start.
Either way, great article, and I especially appreciated that Jim Rohn quote.
Either way, great TurboTax superbowl commercial this year. (Even though I like RoboChild better)
Either way, great TurboTax superbowl commercial this year. (Even though I like RoboChild better)
Either way, hope you're having a great start to 2020 so far!
Either way, I appreciate
Either way, I appreciate and share your love of books. Thank you for giving me the impetus to finish "The Soul of A New Machine" (I've had the audiobook, so I'll finally get around to it thanks to you).
Either way, I appreciate the work you do at Twitch to keep livestreaming highly available and resilient
Either way, I appreciate your approach to management and enablement of knowledge workers.
Either way, I appreciate your championing the cause, and many good wishes to your for the upcoming year. (Also, thanks for DevOps-ing my glasses. Just got a new pair through my VSP insurance.)
Either way, I appreciate your comments and insights from the panel!
Either way, I appreciate your highlighted book suggestions and look forward to reading your list for next year.
Either way, I appreciate your taking the time to connect, and many good wishes to you for the upcoming year.
Either way, I appreciate your well written articles, and thank you for introducing me to Harriett Lerner's work! ("The Dance of Anger" is now on my reading list.)
Either way, I appreciate your work in improving engagement and presenting contextual information at the right time. I can only hope that my outreach to you was contextual at the right time.
Either way, I appreciate your work in keeping the caffeine flowing through cloud technologies.
Either way, I appreciate your Youtube videos, thank you for taking the time to make great, highly informative content.
Either way, I hope that your 2020 is off to a good start, and good luck on hiring your infra automation engineer!
Either way, I hope that your 2020 is off to a great start and many good wishes to you for the upcoming year.
Either way, I hope that your 2020 is off to a great start and that you're enjoying your new role.
Either way, I hope that your 2020 is off to great start, and many good wishes to you for the upcoming year.
Either way, I hope that your February has been off to a great start. Thanks for connecting with me.
Either way, I hope that your February is off to a great start and thanks for connecting with me.
Either way, I hope that your February is off to a great start and that this year brings you many opportunities to spend time with Bodhi and Levi at Sanibel Island.
Either way, I hope that your February is off to a great start, and congratulations to being on the 2020 list of the world's most admired companies for the 6th year in a row.
Either way, I hope that your February is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope that your February is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope that your February is off to a great start, and thank you again for connecting with me.
Either way, I hope that your February is off to a great start, and thank you for connecting with me.
Either way, I hope that your February is off to a great start, and thank you for connecting with me.
Either way, I hope that your February is off to a great start, and thanks again for connecting with me.
Either way, I hope you're having a great start to 2020 and many good wishes to you for this upcoming year.
Either way, I hope you're having a great start to 2020, and many good wishes for a wonderful year to you.
Either way, I hope you're having a great start to 2020, and many good wishes to you for the upcoming year!
Either way, I hope you're having a great start to 2020, and many good wishes to you for this upcoming year.
Either way, I hope you're having a great start to 2020, and many good wishes to you for this upcoming year.
Either way, I hope you're having a great start to 2020, and thanks for using GitLab.
Either way, I hope you're having a great start to 2020!
Either way, I hope you're having a great start to 2020.
Either way, I hope you're having a great start to the New Year, and thank you for the well written articles.
Either way, I hope you're having a wonderful start to 2020, and many good wishes to you for the upcoming year.
Either way, I hope you're having a wonderful start to 2020, and thank you for the work you do to enable people to cure cancer.
Either way, I hope you're having a wonderful start to the new year I appreciate your body of work, from the writing to the presentations. (Especially the comics; they're great!)
Either way, I hope you're having a wonderful start to the New Year.
Either way, I hope you're having a wonderful start to the New Year. (By the way, great photo of the Grand Tetons & Snake River!)
Either way, I hope your 2020 is off to a great start
Either way, I hope your 2020 is off to a great start and best of luck to you with your DevOps growth!
Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start and that this year brings you a ton of opportunities for photography!
Either way, I hope your 2020 is off to a great start and that this year brings you plenty of good dives!
Either way, I hope your 2020 is off to a great start and that you're enjoying your new role at Reply.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your 2020 is off to a great start, and many good wishes to your for the upcoming year.
Either way, I hope your 2020 is off to a great start, and thanks for all you do to keep livestreaming rock solid.
Either way, I hope your 2020 is off to a great start!
Either way, I hope your 2020 is off to a great start.
Either way, I hope your February is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your February is off to a great start, and thank you for connecting with me.
Either way, I hope your February is off to a great start, and thank you for connecting with me.
Either way, I hope your February is off to a great start, and thank you for connecting with me.
Either way, I hope your February is off to a great start, and thank you for connecting with me.
Either way, I hope your February is off to a great start, and thank you for connecting with me.
Either way, I hope your February is off to a great start. I wish you the best of luck with your new consulting/freelance wed development.
Either way, I hope your new year is off to a great start!
Either way, I hope your Q1 is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your Q1 is off to a great start, and many good wishes to your for 2020.
Either way, I hope your Q1 is off to a great start.
Either way, I hope your week is off to a great start, and I wish you well with your build and release engineering.
Either way, I hope your year is off to a great start, and many good wishes to you for the upcoming year.
Either way, I hope your year is off to a great start, and thanks for being an exemplary manager.
Either way, I hope your year is off to a great start!
Either way, I wish you a great upcoming tax season, and I appreciate your work in making my online tax filing software better.
Either way, it looks like you're coming up on your 1 year anniversary at Crocs, I hope this next year brings you even more great things.
Either way, it sounds like you've already built a robust CI/CD testing environment, but thought you may be open to learning more even if it's only for down the road.
Either way, really appreciate your article and perspective, and will be keeping an eye out for new articles from you in the future.
Either way, sounds like you're working on some very exciting Big Data/DevOps/ML projects, so I wish you the best.
Either way, thank you for connecting with me and thank you for being a volunteer with Guardians of Rescue. It's funny, they say that we rescue animals, but really its the animals that rescue us.
Either way, thank you for connecting with me, and I hope that your Q1 has been off to a great start.
Either way, thank you for connecting with me, and thank you for your service. I hope that your February is off to a great start.
Either way, thank you for connecting with me. I hope that your February is off to a great start.
Either way, thank you for connecting with me. I hope that your February is off to a great start.
Either way, thank you for connecting with me. I hope that your February is off to a great start.
Either way, thank you for connecting with me. I hope that your February is off to a great start.
Either way, thank you for connecting with me. I hope this year brings you many opportunities to go Fly Fishing.
Either way, thank you for connecting with me. I hope your February is off to a great start.
Either way, thank you for the great presentation, it's very cool to see how you're Flink, and best of luck as you build the unified API.
Either way, thank you for your work in modernizing the Loyalty program.
Either way, thanks for fostering innovation and digital transformation at Shutterfly.
Either way, thanks for having already used some GitLab, and I hope your 2020 is off to a great start.
Either way, thanks for making Turbotax highly available. It's a great product for those of us with simpler tax needs.
Either way, that was a fun clip/interview. It's also exciting to hear about Hilary Schneider joining as CEO, and can't wait to see what she does at the helm!
Either way, wanted to say thanks for taking the time to write the article series, I truly appreciate the overview of the DevOps landscape.
Either way, thanks for sharing the article about how volunteering service affects your tax and accounting. Great informative read.

Either way I hope that your February is off to a great start.

Either way, it sounds like your teams are about to embark on an exciting DevOps journey, and I wish you all the best.

Either way, I hope that your February is off to a great start and that BidBeatuy has a record year.


Either way, congratulations on the recent promotion/new title, and I hope that your February is off to a great start.

Either way, congratulations on the recent move to Twitch, and I hope that you're having a great time.

Either way, thanks for the sharing the article, and I hope that your February is off to a great start.

In any case, here's to empathetic communication and synergistic sustainability.

In any case, I hope that your February is off to a great start.

Either way, I hope that you're enjoying the move to Twitch. Big fan of your platform and the community you've built.

Either way, huge fan of the team and the work that you all do there at Twitch.

Either way, congratulations on the many CIO and Innovation awards.

Either way, big fan of what you're working on at Twitch. Thanks for keeping the platform secure.

Either way, as a venti Cold Brew man myself, I hope that your February is off to a great start. 

Either way, very cool of you to do the Climb For Clean Air last year. Hope that all is well in your world!

I hope that your February is off to a great start.

Either way, I hope that your Lunar New Year is off to a great start.


In any case, I appreciate your articles, and your recent post on the Belbin leadership styles was very informative and useful to me

Either way, I hope that your February is off to a great start, and I hope that you get a full list of great applicants for this year's Spark SIP internships.


Either way, thank you for the informative articles, I do hope you'll keep writing them.

Either way, I hope that you're having a great start to February.

Either way, congratulations on REW's first year in business, I hope that 2020 brings you many more successes.

Either way, huge fan of VSP, I got two new pairs of glasses covered by your company (one in Dec and one in Jan).


Either way, I hope that your Lunar New Year is off to a great start.


Either way, huge fan of the work you do.

In any case, I appreciate that you called out ageism in your Disrupting Bias Training, because it's the bias we address the least and gets the least airplay.


Either way, I really appreciated you sharing that great HBR article on time management.(I learned the word 'polychronicity' from it)

In any case, I appreciate your keynote. You've been part of this great cloud migration and it's super helpful to hear your stories from in the trenches.

In any case, I appreciate your work on FolkMind. The idea of a folksonomy and the ability to work at any level of abstraction are very interesting to me.

In any case, I hope that your week is off to a great start. Thanks for sharing that article on retiring the word "manager." Very interesting angle to look at some of the issues for sure.

In any case, thank you for the enjoyable insights from your appearance on the podcast.

Either way, I hope that your week is off to a great start.

In any case, I hope that your week is off to a great start.

In any case, thank you very much for connecting with me, and I hope you're having a wonderful start to the new year.

In any case, big fan of VSP and I've been super happy with my vision insurance through you. (Actually, just got my new pair of glasses this week!) 

Either way, I hope you're having a wonderful start to the new year I appreciate your body of work, from the writing to the presentations. (Especially the comics; they're great!)

Either way, I hope you're having a great start to 2020, and many good wishes for a wonderful year to you.

In any case, I hope you're having a strong Q1 and enjoying the redwoods!

No problem if not interested. Simply reply back "No, thanks"

Either way, I hope you're having a great start to the New Year, and thank you for the well written articles.

Either way, I hope you're having a wonderful start to 2020, and many good wishes to you for the upcoming year.

In any case, thank you for the informative presentation.

No problem if you're not interested, a simple "No, thanks" would suffice.

In any case, I hope you're having a great start to 2020, and getting K8s ready for the tax season!

No problem if you're not interested, a simple "No thanks" would be appreciated.

Either way, wanted to say thanks for taking the time to write the article series, I truly appreciate the overview of the DevOps landscape.

In any case, thanks for the very informative interview.

Either way, I hope you're having a wonderful start to the New Year.

No worries if this is of no interest to you, a simple "No thanks" would be appreciated.

No worries if this isn't of interest, a simple "No thanks" would be appreciated.

Either way, thanks for fostering innovation and digital transformation at Shutterfly.

In any case, I hope you're having a great start to the new year.

Either way, I hope you're having a great start to 2020!

No worries if this isn't of interest; a simple "No thanks" would be appreciated.

Either way, that was a fun clip/interview. It's also exciting to hear about Hilary Schneider joining as CEO, and can't wait to see what she does at the helm!

In any case, I hope you're having a wonderful start to the new year.

Either way, I hope you're having a wonderful start to the New Year. (By the way, great photo of the Grand Tetons & Snake River!)

In any case, thank you for taking the time to write such an insightful article.

Nevertheless, I appreciate your article and the reminder that security is not an option.

Either way, I appreciate your work in keeping the caffeine flowing through cloud technologies.

In any case, thank you for introducing me to Wardley's model - really changes how I view our organization and my role within it. 

Either way, best of luck on your goals this year!

Either way, I hope you're having a wonderful start to 2020, and thank you for the work you do to enable people to cure cancer.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

In any case, I hope you're having a great start to 2020!

Either way, best of luck on your goals this year!

Either way, I hope your year is off to a great start, and thanks for being an exemplary manager.

In any case, I hope your year is off to a great start. 

Either way, couldn't agree more with your comments that enterprises definitely need to address this issue. I hope your 2020 has been off to a great start.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Either way, I hope your year is off to a great start, and many good wishes to you for the upcoming year.

Either way, I hope your year is off to a great start!

In any case, I appreciate your insights from the interview and hope you're having a wonderful start to 2020.

Either way, I hope your new year is off to a great start!


Either way, I hope your Q1 is off to a great start.

In any case, I enjoyed reading through your blog entries, and I hope your year is off to a great start! 

Either way, I hope you're having a great January.

In any case, many good wishes to you for the upcoming year.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Either way, I hope your 2020 is off to a great start and that this year brings you a ton of opportunities for photography!

Either way, I hope that your 2020 is off to a good start, and good luck on hiring your infra automation engineer!


Either way, I appreciate your comments and insights from the panel!

Either way, I hope your 2020 is off to a great start

Either way, hope you're having a great start to 2020 so far! 

In any case, I appreciate the work you do to keep video streaming fast and reliable!

In any case, sounds like you're already doing a lot of the latest and greatest - so just wishing you a good start to 2020!

Whether you meet with me or not, I appreciated you taking the time to write the article and assembling those 7 laws into a coherent guide. Hope your 2020 is off to a great start!

Either way, I hope your 2020 is off to a great start!

In any case, I hope your Q1 is off to a good start and many good wishes to you for the upcoming year.

Either way, I appreciate your championing the cause, and many good wishes to your for the upcoming year. (Also, thanks for DevOps-ing my glasses. Just got a new pair through my VSP insurance.)

Either way, I hope your Q1 is off to a great start, and many good wishes to your for 2020.

Either way, I hope your 2020 is off to a great start, and many good wishes to your for the upcoming year.

Either way, I hope your 2020 is off to a great start, and many good wishes to you for the upcoming year.

Either way, I hope your 2020 is off to a great start, and thanks for all you do to keep livestreaming rock solid.

Either way, I hope that your 2020 is off to a great start and that you're enjoying your new role.

Either way, I hope your 2020 is off to a great start and that this year brings you plenty of good dives!

Either way, it sounds like you've already built a robust CI/CD testing environment, but thought you may be open to learning more even if it's only for down the road.

